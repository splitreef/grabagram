package com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount;

import java.io.Serializable;

/**
 * Created by  on 05-01-2021.
 */
public class DriverPhones implements Serializable {
    private String phone_manufacture_company;
    private String phone_manufacture_model;
    private String smart_phone;

    public String getPhone_manufacture_company() {
        return phone_manufacture_company;
    }

    public void setPhone_manufacture_company(String phone_manufacture_company) {
        this.phone_manufacture_company = phone_manufacture_company;
    }

    public String getPhone_manufacture_model() {
        return phone_manufacture_model;
    }

    public void setPhone_manufacture_model(String phone_manufacture_model) {
        this.phone_manufacture_model = phone_manufacture_model;
    }

    public String getSmart_phone() {
        return smart_phone;
    }

    public void setSmart_phone(String smart_phone) {
        this.smart_phone = smart_phone;
    }
}
