package com.splitreef.grabagramplaystore.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class MessageManager {
	
	private Context context;
	
	public MessageManager(Context _context)
	{		
		this.context = _context;		
	}
	
	public void DisplayToastMessageAtCenter(String _message)
	{
		Toast toast = Toast.makeText(context, _message, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public void DisplayToastMessage(String _message)
	{
		Toast.makeText(context, _message, Toast.LENGTH_LONG).show();
	}
}
