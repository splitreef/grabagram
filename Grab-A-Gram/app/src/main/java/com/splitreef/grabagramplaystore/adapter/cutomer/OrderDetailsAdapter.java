package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.OrderDetailsHolder> {

    private Context context;
    private ArrayList<ProductCart>productCartArrayList;


    public OrderDetailsAdapter(Context context, ArrayList<ProductCart>productCartArrayList) {
        this.context=context;
        this.productCartArrayList=productCartArrayList;
    }

    @NonNull
    @Override
    public OrderDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout,parent,false);
        return new OrderDetailsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailsHolder holder, int position) {

        ProductCart productCart=productCartArrayList.get(position);
        PicassoManager.setImage(productCart.getImages().get(0),holder.imgProduct);
        holder.txtProductName.setText(""+productCart.getName());
        holder.txtDispensaryName.setText("Sold By: "+productCart.getBusniess_name());
        holder.txtProductPrice.setText("Price:       $"+productCart.getSale_price());
        holder.txtQuantity.setText("Qty:         "+productCart.getQuantity());
        holder.txtTotalPrice.setText("$"+productCart.getTotal_count());


        if (position==productCartArrayList.size()-1) {
          holder.horizontalLine.setVisibility(View.GONE);
        }
        else
        {
            holder.horizontalLine.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return productCartArrayList.size();
    }

    public class OrderDetailsHolder extends RecyclerView.ViewHolder
    {
        AppCompatImageView imgProduct;
        MaterialTextView txtProductName;
        MaterialTextView txtProductPrice;
        MaterialTextView txtDispensaryName;
        MaterialTextView txtQuantity;
        MaterialTextView txtTotalPrice;
        View horizontalLine;


        public OrderDetailsHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct= itemView.findViewById(R.id.img_product);
            txtProductName= itemView.findViewById(R.id.txt_product_name);
            txtProductPrice= itemView.findViewById(R.id.txt_price);
            txtTotalPrice= itemView.findViewById(R.id.txt_total_price);
            txtDispensaryName= itemView.findViewById(R.id.txt_dispensary_name);
            txtQuantity= itemView.findViewById(R.id.txt_qty);
            horizontalLine= itemView.findViewById(R.id.horizontal_line);

        }
    }
}
