package com.splitreef.grabagramplaystore.data.model.customer.myAccount;

import java.io.Serializable;

public class MyAccountResponse implements Serializable {

    private String status;
    private CustomerAccount account;
    private CustomerAddress addresses;
    private CustomerDocImage document_images;
    private CustomerDocumentInfo document_info;
    private CustomerUser user;

    public CustomerAccount getAccount() {
        return account;
    }

    public void setAccount(CustomerAccount account) {
        this.account = account;
    }

    public CustomerAddress getAddresses() {
        return addresses;
    }

    public void setAddresses(CustomerAddress addresses) {
        this.addresses = addresses;
    }

    public CustomerDocImage getDocument_images() {
        return document_images;
    }

    public void setDocument_images(CustomerDocImage document_images) {
        this.document_images = document_images;
    }

    public CustomerDocumentInfo getDocument_info() {
        return document_info;
    }

    public void setDocument_info(CustomerDocumentInfo document_info) {
        this.document_info = document_info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CustomerUser getUser() {
        return user;
    }

    public void setUser(CustomerUser user) {
        this.user = user;
    }
}
