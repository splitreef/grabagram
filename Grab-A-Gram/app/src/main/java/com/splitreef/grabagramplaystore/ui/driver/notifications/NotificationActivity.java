package com.splitreef.grabagramplaystore.ui.driver.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.reflect.TypeToken;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.NewDeliveryAdapter;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityNotificationBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersViewModel;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityNotificationBinding binding;
    private NewDeliveryAdapter newDeliveryAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private GrabAGramApplication application;
    private PendingOrdersViewModel viewModel;
    private double latitude;
    private double longitude;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    ArrayList<OrdersModel> OrdersModelArrayList;
    ArrayList<DeclineOrders> DeclineOrdersList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNotificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        GPSTracker gpsTracker = new GPSTracker(this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);

        initViews();
    }

    private void initViews() {
        application = (GrabAGramApplication) Objects.requireNonNull(this).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory
                (Objects.requireNonNull(this).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);

        binding.imvBack.setOnClickListener(this);
        binding.tvOrderCanceled.setOnClickListener(this);
        binding.tvOrdersInYouArea.setOnClickListener(this);
        binding.tvReadyForPickup.setOnClickListener(this);
        binding.llOrder.setOnClickListener(this);
        binding.btnAccept.setOnClickListener(this);
        binding.btnDecline.setOnClickListener(this);
        binding.imvBack.setOnClickListener(this);
        subscribeObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowLoaderDialog = true;
        isFirst = true;
        viewModel.getDeclineOrders();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finish();
                break;
            case R.id.btn_accept:
                break;
            case R.id.btn_decline:
                break;
            case R.id.tv_order_canceled:
                startActivity(new Intent(NotificationActivity.this, DetailedNotificationOrderActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.CANCEL_ORDER));
                break;
            case R.id.tv_ready_for_pickup:
                startActivity(new Intent(NotificationActivity.this, DetailedNotificationOrderActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.READY_FOR_PICKUP));
                break;
            case R.id.tv_orders_in_you_area:
                startActivity(new Intent(NotificationActivity.this, DetailedNotificationOrderActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.ORDERS_NEAR_YOU));
                break;
            case R.id.ll_order:
                break;
        }
    }

    void subscribeObservers() {

        viewModel.observeGetDeclineList().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            //  binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:
                            DeclineOrdersList = (ArrayList<DeclineOrders>) dataResource.data;
                            getPendingOrdersList();
                            binding.progressBar.setVisibility(View.GONE);

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            binding.llNewDelivery.setVisibility(View.GONE);
                            binding.relAcceptDecline.setVisibility(View.GONE);
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.DECLINE_ORDERS_LIST, new ArrayList<String>());

                            break;

                    }
                }

            }
        });

        viewModel.observeGetPendingOrdersList().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                                // application.dialogManager.displayProgressDialog(getActivity(),"Loading...");
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            // binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            setPendingOrdersList(OrdersModelArrayList);
                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            //    application.dialogManager.dismissProgressDialog();
                            binding.llNewDelivery.setVisibility(View.GONE);
                            binding.relAcceptDecline.setVisibility(View.GONE);

                            // binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, new ArrayList<OrdersModel>());

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });
    }


    public void getPendingOrdersList() {
        if (viewModel != null)
            viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);
    }

    void setPendingOrdersList(ArrayList<OrdersModel> pendingOrdersList) {
        List<OrdersModel> containsList = new ArrayList<>();
        if (pendingOrdersList.size() > 0) {
            if (DeclineOrdersList.size() > 0) {
                List<OrdersModel> list = new ArrayList<>();
                list.addAll(OrdersModelArrayList);
                for (int i = 0; i < pendingOrdersList.size(); i++) {
                    for (int j = 0; j < DeclineOrdersList.size(); j++) {
                        if (DeclineOrdersList.get(j).getOrder_no().equalsIgnoreCase(pendingOrdersList.get(i).getId())) {
                            containsList.add(pendingOrdersList.get(i));
                            for (int k = 0; k < list.size(); k++) {
                                if (pendingOrdersList.get(i).getId() == list.get(k).getId()) {
                                    list.remove(k);
                                    break;
                                }
                            }
                        }

                    }
                }

                OrdersModelArrayList.clear();
                OrdersModelArrayList.addAll(list);
            }
        }


        List<OrdersModel> modelList = new ArrayList<>();
        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            // Time difference between two times
            long mills = AppUtil.getCurrentTimeInMillisecond() - OrdersModelArrayList.get(i).getCreated_at();
            long hours = (mills / (1000 * 60 * 60));
            //int mins = (mills / (1000 * 60)) % 60;
            //String diff = hours + ":" + mins;
            if (hours <= 1) {
                modelList.add(OrdersModelArrayList.get(i));
            }

        }
        OrdersModelArrayList.clear();
        OrdersModelArrayList.addAll(modelList);


        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            double distance = new GetDistance().distance(latitude, longitude, OrdersModelArrayList.get(i).getDispensary_location().getLatitude(),
                    OrdersModelArrayList.get(i).getDispensary_location().getLongitude());
            OrdersModelArrayList.get(i).setDistance(distance);
            OrdersModelArrayList.get(i).setMyOrderNo(Integer.parseInt(OrdersModelArrayList.get(i).getOrder_no()));
        }

        PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);

        ArrayList<OrdersModel> ordersModels =
                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
                }.getType());


        Log.d("size=======", "" + ordersModels.size());

        if (OrdersModelArrayList.size() > 0) {
            newDeliveryAdapter = new NewDeliveryAdapter(NotificationActivity.this, OrdersModelArrayList, latitude, longitude);
            binding.recycleView.setAdapter(newDeliveryAdapter);
            //binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
            binding.llNewDelivery.setVisibility(View.VISIBLE);
            binding.relAcceptDecline.setVisibility(View.VISIBLE);
        } else {
            binding.llNewDelivery.setVisibility(View.GONE);
            binding.relAcceptDecline.setVisibility(View.GONE);
            //  showToast(getContext(),"Dispensary not found");
            //binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);

        }
        binding.progressBar.setVisibility(View.GONE);


    }
}
