package com.splitreef.grabagramplaystore.ui.driver.orders.my_earnings;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.driver.MyEarningsAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.databinding.FragmentMyEarningBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersViewModel;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by  on 25-01-2021.
 */
public class MyEarningsFragment extends BaseFragment {
    FragmentMyEarningBinding binding;
    GrabAGramApplication grabAGramApplication;
    private PendingOrdersViewModel viewModel;
    private int currentItem, totalItemCount, scrollOutItems;
    private boolean isScrolling;
    private LinearLayoutManager linearLayoutManager;
    private MyEarningsAdapter myEarningsAdapter;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    ArrayList<OrdersModel> completOrderArrayList;
    double latitude;
    double longitude;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMyEarningBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowLoaderDialog = true;
        isFirst = true;
        viewModel.getPastDeliveriesList(latitude, longitude, HomeFragment.miles, true);
    }

    private void initView() {
        grabAGramApplication = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), grabAGramApplication.firebaseRepository)).get(PendingOrdersViewModel.class);
        ((HomeActivity)context).setToolbarTitle("MY EARNINGS");
        HomeActivity.tvActiveStatus.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);
        subscribeObservers();

    }

    void subscribeObservers() {
        viewModel.observeGetPastDeliveriesList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                                // application.dialogManager.displayProgressDialog(getActivity(),"Loading...");
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:

                          /*  if (isShowLoaderDialog)
                            {
                                application.dialogManager.dismissProgressDialog();
                                dispensaryModelArrayList= (ArrayList<DispensaryModel>) dataResource.data;
                                setPastDeliveriesList(dispensaryModelArrayList);
                            }
                            else
                            {
                                if (dispensaryAdapter!=null)
                                {
                                    dispensaryAdapter.notifyDataSetChanged();
                                }
                                binding.progressBar.setVisibility(View.GONE);
                            }

                            if (dispensaryAdapter!=null)
                            {
                                PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,dispensaryAdapter.getDispensaryList());
                            }*/

                            //   application.dialogManager.dismissProgressDialog();
                            completOrderArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            setMyEarningsList(completOrderArrayList);
                            binding.progressBar.setVisibility(View.GONE);

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                grabAGramApplication.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            //    application.dialogManager.dismissProgressDialog();
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });


    }

    public void setMyEarningsList(ArrayList<OrdersModel> myEarningsList) {
        if (myEarningsList.size() > 0) {
            myEarningsAdapter = new MyEarningsAdapter(getContext(), myEarningsList, latitude, longitude);
            binding.recycleView.setAdapter(myEarningsAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);

        }

    }

}

