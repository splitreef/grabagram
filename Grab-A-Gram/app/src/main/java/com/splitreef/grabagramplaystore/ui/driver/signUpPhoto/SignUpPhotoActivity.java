package com.splitreef.grabagramplaystore.ui.driver.signUpPhoto;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.databinding.ActivitySignupPhotoBinding;
import com.splitreef.grabagramplaystore.ui.driver.driverRegistration.DriverRegistrationActivity;
import com.splitreef.grabagramplaystore.utils.NetworkManager;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by  on 06-11-2020.
 */
public class SignUpPhotoActivity extends BaseActivity implements View.OnClickListener {
    private ActivitySignupPhotoBinding binding;
    private Bitmap bitmap;
    private GrabAGramApplication application;
    private DriverRegistrationRequest driverRegistrationRequest;
    public static Bitmap bitmapPhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupPhotoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    private void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(this).getApplicationContext();
        driverRegistrationRequest = new DriverRegistrationRequest();
        binding.imvBack.setOnClickListener(this);
        binding.tvTakeAPhoto.setOnClickListener(this);
        binding.tvUploadPhoto.setOnClickListener(this);
        binding.btnSaveAndContinue.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finishAffinity();
                break;
            case R.id.tv_take_a_photo:

                if (!NetworkManager.isNetworkAvailable(this)) {
                    Toast.makeText(this, "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    requestStorageAndCameraPermission(this, BaseActivity.CAMERA_PIC);

                }

                break;
            case R.id.tv_upload_photo:
                if (!NetworkManager.isNetworkAvailable(this)) {
                    Toast.makeText(this, "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    requestStorageAndCameraPermission(this, BaseActivity.GALLERY_PIC);

                }

                break;

            case R.id.btn_save_and_continue:
                if (bitmap != null) {
                    Intent intent = new Intent(this, DriverRegistrationActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    application.messageManager.DisplayToastMessage("Please upload driver photo.");

                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedFileUri);
                    // binding.photo1.setImageBitmap(bitmap);
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    //viewModel.uploadCustomerDocPhoto(bytes);
                    binding.imvDriverImage.setImageBitmap(bitmap);
                    bitmapPhoto = bitmap;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {

                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    //binding.photo1.setImageBitmap(bitmap);
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    //viewModel.uploadCustomerDocPhoto(bytes);
                    binding.imvDriverImage.setImageBitmap(bitmap);
                    bitmapPhoto = bitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            application.messageManager.DisplayToastMessage("Something went wrong");
        }
    }



}
