package com.splitreef.grabagramplaystore.viewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.customer.pastOrders.PastOrderViewModel;


public class PastOrderProviderFactory implements ViewModelProvider.Factory {
    private final FirebaseRepository firebaseRepository;


    public PastOrderProviderFactory(FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(PastOrderViewModel.class))
        {
            return  (T) new PastOrderViewModel(firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}





