package com.splitreef.grabagramplaystore.ui.customer.registration;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.baoyz.actionsheet.ActionSheet;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentConsumer;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.databinding.FragmentCustomerDocumentsBinding;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.viewModelFactory.RegistrationViewModelProviderFactory;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

public class CustomerDocumentsFragment extends BaseFragment implements View.OnClickListener, ActionSheet.ActionSheetListener {

    private FragmentCustomerDocumentsBinding binding;
    private FragmentConsumer fragmentConsumer;
    private CustomerRegisterRequest customerRegisterRequest;
    private GrabAGramApplication application;
    private Bitmap bitmap;
    private RegistrationViewModel viewModel;
    private int photoTypeCount = 0;
    private long exLongDate=0;
    private long recommendationLetterExpDate=0;

    public CustomerDocumentsFragment(FragmentConsumer fragmentConsumer) {
        this.fragmentConsumer = fragmentConsumer;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCustomerDocumentsBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new RegistrationViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(RegistrationViewModel.class);

        if (getArguments() != null) {
            customerRegisterRequest = (CustomerRegisterRequest) getArguments().getSerializable("requestData");
            Log.d("registerRequest", customerRegisterRequest.getState());
        }

        binding.btnContinue.setOnClickListener(this);
        binding.takePhoto1.setOnClickListener(this);
        binding.takePhoto2.setOnClickListener(this);
        binding.takePhoto3.setOnClickListener(this);
        binding.expirationDate.setOnClickListener(this);
        binding.docRecExpirationDate.setOnClickListener(this);

        subscribeObservers();

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                moveNextScreen();
                break;
            case R.id.take_photo1:

                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 1;
                }

                break;
            case R.id.take_photo2:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 2;
                }

                break;
            case R.id.take_photo3:

                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
                } else {
                    openActionSheet();
                    photoTypeCount = 3;
                }

                break;
            case R.id.expiration_date:
                setDate(1);
                break;
            case R.id.doc_rec_expiration_date:
                setDate(2);
                break;
        }
    }


    void moveNextScreen() {
        if (isValidation()) {
            // Set customer doc information
            customerRegisterRequest.setDocType(binding.spinnerDocType.getSelectedItem().toString());
            customerRegisterRequest.setIssuingState(binding.spinnerState.getSelectedItem().toString());
            customerRegisterRequest.setIdentityNumber(binding.driverLicense.getText().toString());
            customerRegisterRequest.setPatientId(binding.patientId.getText().toString());
            customerRegisterRequest.setExpirationDate(exLongDate);
            customerRegisterRequest.setDoctorsRecExpirationDate(recommendationLetterExpDate);
            fragmentConsumer.getData(3, customerRegisterRequest);
        }


    }


    // open action sheet
    void openActionSheet() {
        ActionSheet.createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Camera", "Gallery")
                .setCancelableOnTouchOutside(true)
                .setListener(this).show();
    }


    // For Documents Validation
    boolean isValidation() {
        if (binding.spinnerDocType.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.select_doc_type));
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.issue_date_required));
            return false;
        } else if (Objects.requireNonNull(binding.driverLicense.getText()).toString().equals("")) {
            binding.driverLicense.setError(getString(R.string.driver_license_required));
            binding.driverLicense.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.expirationDate.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.exp_date_government_Id));
            return false;
        } else if (Objects.requireNonNull(binding.patientId.getText()).toString().equals("")) {
            binding.patientId.setError(getString(R.string.patient_id_required));
            binding.patientId.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.docRecExpirationDate.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.exp_doctor_rec));
            return false;
        } else if (customerRegisterRequest.getDoctorRecPhotoBytes() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.doctor_id_image));
            return false;
        } else if (customerRegisterRequest.getGovPhotoIDUrlBytes() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.government_id_image));
            return false;
        } else if (customerRegisterRequest.getAddressProofPhotoBytes() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.address_id_image));
            return false;
        }

        return true;

    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        if (index == 0) {
            requestStorageAndCameraPermission(getActivity(), CAMERA_PIC);
        } else if (index == 1) {
            requestStorageAndCameraPermission(getActivity(), GALLERY_PIC);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedFileUri);
                    bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
                    byte[] bytes = getFileDataFromDrawable(bitmap);

                    if (photoTypeCount==1)
                    {
                        customerRegisterRequest.setDoctorRecPhotoBytes(bytes);
                        binding.photo1.setImageBitmap(bitmap);
                        //viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_DOCTOR_RECOMMENDATION_LETTER);
                    }
                    else if (photoTypeCount==2)
                    {
                        customerRegisterRequest.setGovPhotoIDUrlBytes(bytes);
                        binding.photo2.setImageBitmap(bitmap);
                        //viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_GOVERNMENT_PHOTO_ID);
                    }
                    else if (photoTypeCount==3)
                    {
                       // viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_PERSONAL_ADDRESS);
                        customerRegisterRequest.setAddressProofPhotoBytes(bytes);
                        binding.photo3.setImageBitmap(bitmap);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {
                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
                    //binding.photo1.setImageBitmap(bitmap);
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount==1)
                    {
                        customerRegisterRequest.setDoctorRecPhotoBytes(bytes);
                        binding.photo1.setImageBitmap(bitmap);
                        //viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_DOCTOR_RECOMMENDATION_LETTER);
                    }
                    else if (photoTypeCount==2)
                    {
                       // viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_GOVERNMENT_PHOTO_ID);
                        customerRegisterRequest.setGovPhotoIDUrlBytes(bytes);
                        binding.photo2.setImageBitmap(bitmap);
                    }
                    else if (photoTypeCount==3)
                    {
                       // viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_PERSONAL_ADDRESS);
                        customerRegisterRequest.setAddressProofPhotoBytes(bytes);
                        binding.photo3.setImageBitmap(bitmap);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            application.messageManager.DisplayToastMessage("Image not selected");
        }
    }


    void subscribeObservers() {
        viewModel.observeCustomerDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    PicassoManager.setImage(url, binding.photo1);
                                    customerRegisterRequest.setDoctorRecPhotoUrl(url);
                                } else if (photoTypeCount == 2) {
                                    PicassoManager.setImage(url, binding.photo2);
                                    customerRegisterRequest.setGovPhotoIDUrl(url);
                                } else if (photoTypeCount == 3) {
                                    PicassoManager.setImage(url, binding.photo3);
                                    customerRegisterRequest.setAddressProofPhotoUrl(url);
                                }
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    @SuppressLint("SetTextI18n")
    public void setDate(int flag) {

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();

        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(selection -> {

            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar1.setTimeInMillis(selection);

            int month = calendar1.get(Calendar.MONTH);
            String date = (month + 1) + "-" + calendar1.get(Calendar.DAY_OF_MONTH) + "-" + calendar1.get(Calendar.YEAR);

            if (flag == 1) {
                exLongDate=selection;
                binding.expirationDate.setText("" + date);
            } else if (flag == 2) {
                recommendationLetterExpDate=selection;
                binding.docRecExpirationDate.setText("" + date);
            }

        });
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle oldInstanceState)
    {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }


}
