package com.splitreef.grabagramplaystore.data.model.customer.personalAddress;

import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;

import java.io.Serializable;

public class PersonalAddressResponse implements Serializable {

    private String status;
    private UserAddressModel userAddressModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserAddressModel getUserAddressModel() {
        return userAddressModel;
    }

    public void setUserAddressModel(UserAddressModel userAddressModel) {
        this.userAddressModel = userAddressModel;
    }
}
