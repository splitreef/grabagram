package com.splitreef.grabagramplaystore.data.model.customer;

import java.io.Serializable;
import java.util.ArrayList;

public class AddressList implements Serializable {

    private ArrayList<UserAddressModel> addresses;
    private boolean isPersonAddress;
    private boolean isSecondaryAddress;
    private boolean isBilling;

    public ArrayList<UserAddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<UserAddressModel> addresses) {
        this.addresses = addresses;
    }

    public boolean getIsPersonAddress() {
        return isPersonAddress;
    }

    public void setIsPersonAddress(boolean isPersonAddress) {
        this.isPersonAddress = isPersonAddress;
    }

    public boolean isPersonAddress() {
        return isPersonAddress;
    }

    public void setPersonAddress(boolean personAddress) {
        isPersonAddress = personAddress;
    }

    public boolean isSecondaryAddress() {
        return isSecondaryAddress;
    }

    public void setSecondaryAddress(boolean secondaryAddress) {
        isSecondaryAddress = secondaryAddress;
    }

    public boolean isBilling() {
        return isBilling;
    }

    public void setBilling(boolean billing) {
        isBilling = billing;
    }
}
