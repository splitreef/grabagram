package com.splitreef.grabagramplaystore.ui.forgetPassword;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.databinding.ActivityForgetPasswordBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.ForgetPasswordViewModelProviderFactory;

import java.util.Objects;

/**
 * Created by  on 21-01-2021.
 */
public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {
    ActivityForgetPasswordBinding binding;
    private ForgetPasswordViewModel forgetPasswordViewModel;
    private GrabAGramApplication application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        binding = ActivityForgetPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    // For initialization and set click listener the view of this screen.
    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getApplicationContext());
        forgetPasswordViewModel = new ViewModelProvider(getViewModelStore(), new ForgetPasswordViewModelProviderFactory(getApplication(), application.firebaseRepository)).get(ForgetPasswordViewModel.class);

        binding.btnSubmit.setOnClickListener(this);
        binding.imvBack.setOnClickListener(this);
        subscribeObservers();
    }

    @SuppressLint({"NonConstantResourceId"})
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finish();
                break;
            case R.id.btn_submit:
                if (isValidation()) {
                    forgetPassword();
                }
                break;

        }
    }

    public void forgetPassword() {
        forgetPasswordViewModel.forgetPassword(binding.emailAddress.getText().toString());
    }

    // Validation
    boolean isValidation() {
        if (binding.emailAddress.getText().toString().equals("")) {
            binding.emailAddress.setError("Please enter email");
            binding.emailAddress.requestFocus();
            return false;
        }
        return true;

    }

    private void subscribeObservers() {
        forgetPasswordViewModel.observeForgetPassword().observe(Objects.requireNonNull(this), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(ForgetPasswordActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            // show dialog
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {
                                    finish();
                                }
                            }.showSimpleDialog(ForgetPasswordActivity.this, "Message", getResources().getString(R.string.we_have_just_send), "OK");
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(ForgetPasswordActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
