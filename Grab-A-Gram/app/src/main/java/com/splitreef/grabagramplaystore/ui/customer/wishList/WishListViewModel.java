package com.splitreef.grabagramplaystore.ui.customer.wishList;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WishListViewModel extends ViewModel {

    private static final String TAG = "WishListViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onProductWishList = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onDeleteProductWishList = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public WishListViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getProductWishList()
    {
         firebaseRepository.getProductWishList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<GetProductCartResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onProductWishList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull GetProductCartResponse productCartResponse) {

                         onProductWishList.setValue(DataResource.DataStatus(productCartResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onProductWishList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }




    public  void deleteWishListProduct(String id)
    {
        firebaseRepository.deleteWishListProduct(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onDeleteProductWishList.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onDeleteProductWishList.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onDeleteProductWishList.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }

    LiveData<DataResource> observeGetProductWishList()
    {
        return onProductWishList;
    }


   LiveData<StateResource> observeDeleteProductWishList()
    {
        return onDeleteProductWishList;
    }



}
