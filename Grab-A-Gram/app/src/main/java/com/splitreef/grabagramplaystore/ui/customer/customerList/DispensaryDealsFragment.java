package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.DealListAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.databinding.FragmentDealsListBinding;
import com.splitreef.grabagramplaystore.viewModelFactory.DealListViewModelProviderFactory;
import java.util.ArrayList;
import java.util.Objects;

public class DispensaryDealsFragment extends BaseFragment {
    FragmentDealsListBinding binding;
    private DispensaryDealListViewModel viewModel;
    private GrabAGramApplication application;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding= FragmentDealsListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    public void  initView()
    {

       application=(GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

       viewModel=new ViewModelProvider(getViewModelStore(), new DealListViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(),application.firebaseRepository)).get(DispensaryDealListViewModel.class);

       // ((DispensaryListActivity)context).setToolbarTitle("Deals");
       binding.recycleView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        subscribeObservers();
        viewModel.getDealList();

    }


    void subscribeObservers()
    {
        viewModel.observeGetDealList().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        ArrayList<DealModel> dealModelArrayList= (ArrayList<DealModel>) dataResource.data;
                        updateUI(dealModelArrayList);
                        break;
                    case ERROR:
                        binding.progressBar.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.recycleView.setVisibility(View.GONE);
                        break;

                }
            }

        });

    }

    public void updateUI(ArrayList<DealModel> dispensaryModelArrayList)
    {
        if (dispensaryModelArrayList!=null && dispensaryModelArrayList.size()>0)
        {
            DealListAdapter adapter=new DealListAdapter(getContext(),dispensaryModelArrayList);
            binding.recycleView.setAdapter(adapter);

            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        }
        else
        {
          binding.userMsg.setVisibility(View.VISIBLE);
          binding.recycleView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
     }
}
