package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by  on 30-11-2020.
 */
public class PendingOrdersViewModel extends ViewModel {
    private static final String TAG = "PendingOrdersViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetPendingOrdersList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetUpcoimgDeliveriesList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetPastDeliveriesList = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateOrderToAccept = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateOrderToScheduled = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateDriverLocation = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateOrderToDecline = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onDeclineOrderList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onOrderDetails = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onOrderStatus = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUserDetails = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetNewDeliveriesList = new MediatorLiveData<>();

    private Application mApplication;
    private CompositeDisposable disposable = new CompositeDisposable();


    public PendingOrdersViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication = mApplication;
    }

    public void getPendingOrdersList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getPendingOrdersList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetPendingOrdersList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetPendingOrdersList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetPendingOrdersList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetPendingOrdersList() {
        return onGetPendingOrdersList;
    }

    public void getUpcomingDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getUpcomingDeliveriesList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetUpcoimgDeliveriesList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetUpcoimgDeliveriesList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetUpcoimgDeliveriesList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    public LiveData<DataResource> observeGetUpcomingDeliveriesList() {
        return onGetUpcoimgDeliveriesList;
    }


    public void updateOrderToAccept(String id, String status) {
        firebaseRepository.updateOrderToAccept(id, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateOrderToAccept.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateOrderToAccept.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateOrderToAccept.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public LiveData<StateResource> observeUpdateOrderToAccept() {
        return onUpdateOrderToAccept;
    }


    public void getPastDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getPastDeliveriesList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetPastDeliveriesList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetPastDeliveriesList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetPastDeliveriesList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetPastDeliveriesList() {
        return onGetPastDeliveriesList;
    }

    public void getOrderDetails(String id) {
        firebaseRepository.getOrderDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<OrdersModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onOrderDetails.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull OrdersModel ordersModels) {
                        onOrderDetails.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onOrderDetails.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetOrderDetails() {
        return onOrderDetails;
    }


    public void getOrderStatus(String id) {
        firebaseRepository.getOrderStatus(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onOrderStatus.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                        onOrderStatus.setValue(DataResource.DataStatus(string));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onOrderStatus.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetOrderStatsu() {
        return onOrderStatus;
    }



    public void updateOrderToDecline(String id) {
        firebaseRepository.updateOrderToDecline(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateOrderToDecline.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateOrderToDecline.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateOrderToDecline.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public LiveData<StateResource> observeUpdateOrderToDecline() {
        return onUpdateOrderToDecline;
    }


    public void getDeclineOrders() {
        firebaseRepository.getDeclineOrders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DeclineOrders>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onDeclineOrderList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<DeclineOrders> declineOrders) {
                        onDeclineOrderList.setValue(DataResource.DataStatus(declineOrders));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onDeclineOrderList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetDeclineList() {
        return onDeclineOrderList;
    }

    public void updateOrderToScheduled(String id, String status) {
        firebaseRepository.updateOrderToScheduled(id, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateOrderToScheduled.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateOrderToScheduled.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateOrderToScheduled.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public LiveData<StateResource> observeUpdateOrderToScheduled() {
        return onUpdateOrderToScheduled;
    }


    public void getUserDetails(String userID) {
        firebaseRepository.getUserDetails(userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onUserDetails.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull UserModel userModel) {
                        onUserDetails.setValue(DataResource.DataStatus(userModel));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUserDetails.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetUserDetails() {
        return onUserDetails;
    }


    public void getNewDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getNewDeliveriesList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetNewDeliveriesList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetNewDeliveriesList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetNewDeliveriesList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    public LiveData<DataResource> observeGetNewDeliveriesList() {
        return onGetNewDeliveriesList;
    }


    public void updateDriverLocation(double latitude, double longitude, String orderID) {
        firebaseRepository.updateDriverLocation(latitude, longitude, orderID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateDriverLocation.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateDriverLocation.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateDriverLocation.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public LiveData<StateResource> observeUpdateDriverLocation() {
        return onUpdateDriverLocation;
    }


    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
