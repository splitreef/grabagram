package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.ProductDetailsCallBack;
import com.splitreef.grabagramplaystore.data.model.customer.menu.CategoryMenu;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {
      private Context context;
      private ArrayList<CategoryMenu> categoryMenuArrayList;
      private ProductDetailsCallBack productDetailsCallBack;

    public CategoryAdapter(Context context,ArrayList<CategoryMenu> categoryMenuArrayList,ProductDetailsCallBack productDetailsCallBack) {
        this.context=context;
        this.categoryMenuArrayList=categoryMenuArrayList;
        this.productDetailsCallBack=productDetailsCallBack;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,parent,false);
        return new CategoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {

        CategoryMenu categoryMenu=categoryMenuArrayList.get(position);


        if (categoryMenu.getProductList()!=null && categoryMenu.getProductList().size()>0)
        {
            holder.txtCategoryName.setText(categoryMenu.getCategory_name()+ "("+categoryMenu.getProductList().size()+")");

            holder.rvProduct.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
            ProductAdapter productAdapter=new ProductAdapter(context,categoryMenu.getProductList(),productDetailsCallBack);
            holder.rvProduct.setAdapter(productAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return categoryMenuArrayList.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder
    {
        RecyclerView rvProduct;
        MaterialTextView txtCategoryName;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            rvProduct = itemView.findViewById(R.id.rv_product);
            txtCategoryName = itemView.findViewById(R.id.txt_category_name);
        }
    }
}
