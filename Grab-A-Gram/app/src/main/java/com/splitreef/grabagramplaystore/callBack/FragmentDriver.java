package com.splitreef.grabagramplaystore.callBack;

import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;

/**
 * Created by  on 10-11-2020.
 */
public interface FragmentDriver {
    void getData(int position, DriverRegistrationRequest requestData);

}
