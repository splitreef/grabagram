package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

public class DispensaryReviewAdapter extends RecyclerView.Adapter<DispensaryReviewAdapter.DispensaryReviewHolder> {

    private Context context;
    private ArrayList<DispensaryReviewModel> dispensaryReviewArrayList;


    public DispensaryReviewAdapter(Context context,ArrayList<DispensaryReviewModel> dispensaryReviewArrayList) {
        this.context=context;
        this.dispensaryReviewArrayList=dispensaryReviewArrayList;

    }

    @NonNull
    @Override
    public DispensaryReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dispensary_review,parent,false);
        return new DispensaryReviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DispensaryReviewHolder holder, int position) {

        DispensaryReviewModel dispensaryReviewModel=dispensaryReviewArrayList.get(position);
        holder.txtUserName.setText(dispensaryReviewModel.getUser_name());
        holder.txtCreatedAt.setText(""+ AppUtil.getElapsedTime(dispensaryReviewModel.getCreated_at()));
        holder.txtComment.setText(dispensaryReviewModel.getComment());
        holder.rating.setRating(dispensaryReviewModel.getRating());

    }

    @Override
    public int getItemCount() {
        return dispensaryReviewArrayList.size();
    }

    public class DispensaryReviewHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtUserName;
        MaterialTextView txtCreatedAt;
        MaterialTextView txtComment;
        AppCompatRatingBar rating;

        public DispensaryReviewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txt_user_name);
            txtCreatedAt= itemView.findViewById(R.id.created_at);
            rating= itemView.findViewById(R.id.dispensary_rating);
            txtComment= itemView.findViewById(R.id.txt_comment);
        }
    }
}
