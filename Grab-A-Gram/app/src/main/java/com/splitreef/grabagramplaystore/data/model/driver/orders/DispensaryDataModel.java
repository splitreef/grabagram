package com.splitreef.grabagramplaystore.data.model.driver.orders;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;
import com.splitreef.grabagramplaystore.data.model.customer.OpeningHours;

import java.util.ArrayList;

/**
 * Created by  on 01-12-2020.
 */
public class DispensaryDataModel implements Parcelable {
    private String about_us;
    private AddressModel address;
    private String busniess_name;
    private long created_at;
    private String id;
    private String license_number;
    private GeoPoint location;
    private ArrayList<OpeningHours> opening_hours;
    private String profile_image;
    private boolean subscribe;
    private boolean text_notifications;
    private long updated_at;
    private String user_id;


    public DispensaryDataModel() {
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLicense_number() {
        return license_number;
    }

    public void setLicense_number(String license_number) {
        this.license_number = license_number;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public ArrayList<OpeningHours> getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(ArrayList<OpeningHours> opening_hours) {
        this.opening_hours = opening_hours;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public boolean isText_notifications() {
        return text_notifications;
    }

    public void setText_notifications(boolean text_notifications) {
        this.text_notifications = text_notifications;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static Creator<DispensaryDataModel> getCREATOR() {
        return CREATOR;
    }

    protected DispensaryDataModel(Parcel in) {
        about_us = in.readString();
        address = (AddressModel) in.readValue(AddressModel.class.getClassLoader());
        busniess_name = in.readString();
        created_at = in.readLong();
        id = in.readString();
        license_number = in.readString();
        Double lat = in.readDouble();
        Double lng = in.readDouble();
        location = new GeoPoint(lat, lng);
        // location = (GeoPoint) in.readValue(GeoPoint.class.getClassLoader());
//        if (in.readByte() == 0x01) {
//            opening_hours = new ArrayList<OpeningHours>();
//            in.readList(opening_hours, OpeningHours.class.getClassLoader());
//        } else {
//            opening_hours = null;
//        }
        opening_hours = (ArrayList<OpeningHours>) in.readSerializable();

        profile_image = in.readString();
        subscribe = in.readByte() != 0x00;
        text_notifications = in.readByte() != 0x00;
        updated_at = in.readLong();
        user_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(about_us);
        dest.writeValue(address);
        dest.writeString(busniess_name);
        dest.writeLong(created_at);
        dest.writeString(id);
        dest.writeString(license_number);
        dest.writeValue(location);
        /*if (opening_hours == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(opening_hours);
        }*/
        dest.writeSerializable(opening_hours);
        dest.writeString(profile_image);
        dest.writeByte((byte) (subscribe ? 0x01 : 0x00));
        dest.writeByte((byte) (text_notifications ? 0x01 : 0x00));
        dest.writeLong(updated_at);
        dest.writeString(user_id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DispensaryDataModel> CREATOR = new Parcelable.Creator<DispensaryDataModel>() {
        @Override
        public DispensaryDataModel createFromParcel(Parcel in) {
            return new DispensaryDataModel(in);
        }

        @Override
        public DispensaryDataModel[] newArray(int size) {
            return new DispensaryDataModel[size];
        }
    };
}
