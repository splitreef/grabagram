package com.splitreef.grabagramplaystore.callBack.driver;

import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;

/**
 * Created by  on 02-12-2020.
 */
public interface OrderAcceptDeclineListener {
   public void onAcceptClick(String id, OrdersModel ordersModel);
   public void onDeclineClick(String id);

}
