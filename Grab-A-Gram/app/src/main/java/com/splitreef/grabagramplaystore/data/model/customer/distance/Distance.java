package com.splitreef.grabagramplaystore.data.model.customer.distance;

import java.io.Serializable;

public class Distance implements Serializable {

    private String distance_display;
    private String distance_id;
    private boolean isSelected;

    public String getDistance_display() {
        return distance_display;
    }

    public void setDistance_display(String distance_display) {
        this.distance_display = distance_display;
    }

    public String getDistance_id() {
        return distance_id;
    }

    public void setDistance_id(String distance_id) {
        this.distance_id = distance_id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
