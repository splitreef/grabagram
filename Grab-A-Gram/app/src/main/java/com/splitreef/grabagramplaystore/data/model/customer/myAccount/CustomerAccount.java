package com.splitreef.grabagramplaystore.data.model.customer.myAccount;

import java.io.Serializable;

public class CustomerAccount implements Serializable {

    private long date_of_birth;
    private String first_name;
    private String last_name;
    private String e_signature;
    private boolean message_notification;

    public long getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(long date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public boolean getMessage_notification() {
        return message_notification;
    }

    public void setMessage_notification(boolean message_notification) {
        this.message_notification = message_notification;
    }

    public String getE_signature() {
        return e_signature;
    }

    public void setE_signature(String e_signature) {
        this.e_signature = e_signature;
    }

}
