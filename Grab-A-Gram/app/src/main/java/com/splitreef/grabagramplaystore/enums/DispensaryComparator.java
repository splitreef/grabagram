package com.splitreef.grabagramplaystore.enums;

import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;

import java.util.Comparator;

public enum DispensaryComparator implements Comparator<DispensaryModel> {

    A_TO_Z_SORT {
        public int compare(DispensaryModel o1, DispensaryModel o2) {
            return o1.getBusniess_name().compareTo(o2.getBusniess_name());
        }},
    DIS_MIN_TO_MAX_SORT {
        public int compare(DispensaryModel o1, DispensaryModel o2) {
            return Integer.valueOf(o1.getDistance()).compareTo(o2.getDistance());
        }},

    RATING_LOW_HIGH_SORT {
            public int compare(DispensaryModel o1, DispensaryModel o2) {
                return Float.valueOf(o1.getRating()).compareTo(o2.getRating());
            }};


    public static Comparator<DispensaryModel> descending(final Comparator<DispensaryModel> other) {
        return new Comparator<DispensaryModel>() {
            public int compare(DispensaryModel o1, DispensaryModel o2) {
                return -1 * other.compare(o1, o2);
            }
        };
    }

    public static Comparator<DispensaryModel> getComparator(final DispensaryComparator... multipleOptions) {
        return new Comparator<DispensaryModel>() {
            public int compare(DispensaryModel o1, DispensaryModel o2) {
                for (DispensaryComparator option : multipleOptions) {
                    int result = option.compare(o1, o2);
                    if (result != 0) {
                        return result;
                    }
                }
                return 0;
            }
        };
    }

}
