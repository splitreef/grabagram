package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;

import java.io.Serializable;

public class DriverProfileResponse implements Serializable {

    private String status;
    private DriverProfile driverProfile;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
    }
}
