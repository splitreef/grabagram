package com.splitreef.grabagramplaystore.data.model.customer.cartList;

import java.io.Serializable;
import java.util.ArrayList;

public class GetProductCartResponse implements Serializable {
    private ArrayList<ProductCart>productCarts;
    private double delivery_fee;
    private String dispensary_id;
    private String dispensary_user_id;
    private String used_coupon_id;
    private String coupon_code_name;
    private double total_discount;

    public ArrayList<ProductCart> getProductCarts() {
        return productCarts;
    }

    public void setProductCarts(ArrayList<ProductCart> productCarts) {
        this.productCarts = productCarts;
    }

    public double getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(double delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getDispensary_id() {
        return dispensary_id;
    }

    public void setDispensary_id(String dispensary_id) {
        this.dispensary_id = dispensary_id;
    }

    public String getDispensary_user_id() {
        return dispensary_user_id;
    }

    public void setDispensary_user_id(String dispensary_user_id) {
        this.dispensary_user_id = dispensary_user_id;
    }

    public String getUsed_coupon_id() {
        return used_coupon_id;
    }

    public void setUsed_coupon_id(String used_coupon_id) {
        this.used_coupon_id = used_coupon_id;
    }

    public String getCoupon_code_name() {
        return coupon_code_name;
    }

    public void setCoupon_code_name(String coupon_code_name) {
        this.coupon_code_name = coupon_code_name;
    }

    public double getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(double total_discount) {
        this.total_discount = total_discount;
    }
}
