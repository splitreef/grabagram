package com.splitreef.grabagramplaystore.ui.customer.filter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.common.reflect.TypeToken;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.CustomerCategoryFilterAdapter;
import com.splitreef.grabagramplaystore.adapter.cutomer.CustomerDistanceFilterAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.dispensaryIdList.DispensaryIdListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distance.DispensaryDistanceResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategoryResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityCustomerFilterBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.CustomerFilterProviderFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class   CustomerFilterActivity extends BaseActivity implements View.OnClickListener {
    private ActivityCustomerFilterBinding binding;
    private CustomerFilterViewModel viewModel;
    private GrabAGramApplication application;
    private  CustomerCategoryFilterAdapter customerCategoryFilterAdapter;
    CustomerDistanceFilterAdapter customerDistanceFilterAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityCustomerFilterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }


    public void initView()
    {
        application = (GrabAGramApplication)getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new CustomerFilterProviderFactory(application.firebaseRepository)).get(CustomerFilterViewModel.class);

        subscribeObservers();

        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        binding.llCategory.setOnClickListener(this);
        binding.llDistance.setOnClickListener(this);
        binding.btnApply.setOnClickListener(this);
        selectCategoryDistance(1);

        viewModel.getCategoryList();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void subscribeObservers() {

        viewModel.observeGetCategoryList().observe(CustomerFilterActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CustomerFilterActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        ProductCategoryResponse productCategoryResponse= (ProductCategoryResponse) dataResource.data;
                        if (productCategoryResponse.getStatus().equals(AppConstant.SUCCESS))
                        {
                            setCategoryList(productCategoryResponse);
                        }

                        viewModel.getDistanceList();
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        showToast(dataResource.message);
                }


            }
        });


        viewModel.observeGetDistanceList().observe(CustomerFilterActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                        DispensaryDistanceResponse dispensaryDistanceResponse= (DispensaryDistanceResponse) dataResource.data;
                        if (dispensaryDistanceResponse.getStatus().equals(AppConstant.SUCCESS))
                        {
                            setDistanceList(dispensaryDistanceResponse);
                        }
                        application.dialogManager.dismissProgressDialog();

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        showToast(dataResource.message);

                }
            }
        });

        viewModel.observeGetDispensaryUserIdList().observe(CustomerFilterActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                         application.dialogManager.displayProgressDialog(CustomerFilterActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        DispensaryIdListResponse dispensaryIdListResponse= (DispensaryIdListResponse) dataResource.data;
                        if (dispensaryIdListResponse!=null && dispensaryIdListResponse.getStatus().equals(AppConstant.SUCCESS)) {
                            ArrayList<String> dispensaryUserIdList = dispensaryIdListResponse.getDispensaryId();
                            if (dispensaryUserIdList != null && dispensaryUserIdList.size() > 0)
                            {
                                PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_USER_ID_LIST,dispensaryUserIdList);
                                PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_CATEGORY_FILTER,true);
                            }
                            else
                            {
                                PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_CATEGORY_FILTER,false);
                            }

                            finish();
                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        showToast(dataResource.message);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ll_category:
                selectCategoryDistance(1);
                break;

            case R.id.ll_distance:
                selectCategoryDistance(2);
                break;

            case R.id.btn_apply:
                 applyFilter();
                break;
        }
    }


    void applyFilter()
    {
        if(customerDistanceFilterAdapter!=null)
        {
            customerDistanceFilterAdapter.setSelectedDistance();
        }
        if (customerCategoryFilterAdapter!=null)
        {
            customerCategoryFilterAdapter.setSelectedCategory();
        }


        boolean isDisFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);
        boolean isCatFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_CATEGORY_FILTER);

        if (isDisFilter && isCatFilter)
        {
            ArrayList<String> categoryIdList  = PreferenceManger.getPreferenceManger().getArray(PrefKeys.CATEGORY_ID_LIST, new TypeToken<List<String>>() {
            }.getType());
           viewModel.getDispensaryIdList(categoryIdList);
        }
        else if (isDisFilter)
        {
            finish();
        }
        else if (isCatFilter)
        {
            ArrayList<String> categoryIdList  = PreferenceManger.getPreferenceManger().getArray(PrefKeys.CATEGORY_ID_LIST, new TypeToken<List<String>>() {
            }.getType());
            viewModel.getDispensaryIdList(categoryIdList);

        }
        else
        {
            showToast("Please select distance or category");
        }

    }


    void selectCategoryDistance(int option)
    {
        if (option==1)
        {
            binding.llCategory.setBackgroundColor(ContextCompat.getColor(this,R.color.colorWhite));
            binding.txtCategory.setTextColor(ContextCompat.getColor(this,R.color.colorBlack));

            binding.llDistance.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
            binding.txtDistance.setTextColor(ContextCompat.getColor(this,R.color.darkgray));

            binding.rvCategory.setVisibility(View.VISIBLE);
            binding.rvDistance.setVisibility(View.GONE);
        }
        else if (option==2)
        {
            binding.llDistance.setBackgroundColor(ContextCompat.getColor(this,R.color.colorWhite));
            binding.txtDistance.setTextColor(ContextCompat.getColor(this,R.color.colorBlack));

            binding.llCategory.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
            binding.txtCategory.setTextColor(ContextCompat.getColor(this,R.color.darkgray));

            binding.rvCategory.setVisibility(View.GONE);
            binding.rvDistance.setVisibility(View.VISIBLE);

        }
    }

    public void setCategoryList(ProductCategoryResponse productCategoryResponse)
    {
        if (productCategoryResponse.getProductCategoryList()!=null && productCategoryResponse.getProductCategoryList().size()>0) {
            boolean isCategoryFilter = PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_CATEGORY_FILTER);

            if (isCategoryFilter)
            {
                ArrayList<String> categoryIdList  = PreferenceManger.getPreferenceManger().getArray(PrefKeys.CATEGORY_ID_LIST, new TypeToken<List<String>>() {
                }.getType());

                if (categoryIdList!=null && categoryIdList.size()>0)
                {
                    for (int i=0;i<categoryIdList.size();i++)
                    {
                        for (int j=0;j<productCategoryResponse.getProductCategoryList().size();j++)
                        {
                            if (categoryIdList.get(i).equals(productCategoryResponse.getProductCategoryList().get(j).getId()))
                            {
                                productCategoryResponse.getProductCategoryList().get(j).setSelected(true);
                            }
                        }
                    }
                }
            }

            binding.rvCategory.setLayoutManager(new LinearLayoutManager(CustomerFilterActivity.this, LinearLayoutManager.VERTICAL, false));
            customerCategoryFilterAdapter = new CustomerCategoryFilterAdapter(CustomerFilterActivity.this, productCategoryResponse.getProductCategoryList());
            binding.rvCategory.setAdapter(customerCategoryFilterAdapter);
        }

    }


    public void setDistanceList(DispensaryDistanceResponse dispensaryDistanceResponse)
    {
        if (dispensaryDistanceResponse.getDistanceList()!=null && dispensaryDistanceResponse.getDistanceList().size()>0)
        {
            boolean isDistanceFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);
            int index=-1;

            for (int i=0;i<dispensaryDistanceResponse.getDistanceList().size();i++)
            {
                if (isDistanceFilter)
                {
                    String distanceId=PreferenceManger.getPreferenceManger().getString(PrefKeys.DISTANCE_ID);
                    if (distanceId.equals(dispensaryDistanceResponse.getDistanceList().get(i).getDistance_id()))
                    {
                        Log.d("distanceId",""+distanceId);
                        dispensaryDistanceResponse.getDistanceList().get(i).setSelected(true);
                        index=i;
                        break;
                    }
                }
            }

            binding.rvDistance.setLayoutManager(new LinearLayoutManager(CustomerFilterActivity.this,LinearLayoutManager.VERTICAL,false));
            customerDistanceFilterAdapter=new CustomerDistanceFilterAdapter(CustomerFilterActivity.this,dispensaryDistanceResponse.getDistanceList(),index);
            binding.rvDistance.setAdapter(customerDistanceFilterAdapter);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==R.id.action_reset)
        {
            if (customerCategoryFilterAdapter !=null)
            {
                customerCategoryFilterAdapter.resetCategoryFilter();
            }

            if (customerDistanceFilterAdapter!=null)
            {
                PreferenceManger.getPreferenceManger().setString(PrefKeys.DISTANCE_ID,null);
                customerDistanceFilterAdapter.resetDistanceFilter();
            }

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
