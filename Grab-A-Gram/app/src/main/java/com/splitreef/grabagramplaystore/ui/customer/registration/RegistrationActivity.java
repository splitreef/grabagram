package com.splitreef.grabagramplaystore.ui.customer.registration;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentConsumer;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.databinding.ActivityRegistrationBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;

import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity implements FragmentConsumer, View.OnClickListener {

    private ActivityRegistrationBinding binding;
    private final int[] inActiveTabIcons = {R.drawable.tabone, R.drawable.tabtwo, R.drawable.tabthree, R.drawable.tabfour};
    private final int[] activeTabIcons = {R.drawable.taboneselected, R.drawable.tabtwoselected, R.drawable.tabthreeselected, R.drawable.tabfourselected};
    private final String[] tabTitleArray = {"Account info", "Personal info", "Documents", "Member Agreement"};
    private Fragment fragment;
    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegistrationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    // initialized the view
    void initView() {


        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        // Add custom tab
        addCustomTab();


        if (getIntent()!=null)
        {
            String from=getIntent().getStringExtra(AppConstant.FROM);
            if (from.equals(AppConstant.FROM_SPLASH) || from.equals(AppConstant.FROM_LOGIN))
            {
                CustomerRegisterRequest customerRegisterRequest= (CustomerRegisterRequest) getIntent().getSerializableExtra("requestData");

                // Active 1 index tab
                TabLayout.Tab tab = binding.tabLayout.getTabAt(1);
                View view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 1, true);

                // Firstly call this fragment
                fragment = new CustomerPersonalInfoFragment(this);
                bundle=new Bundle();
                bundle.putSerializable("requestData",customerRegisterRequest);
                fragment.setArguments(bundle);
                replaceFragment(fragment,false);
            }
            else if (from.equals(AppConstant.FROM_ACCOUNT_CHOOSER))
            {
                // Active 0 index tab
                TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
                View view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 0, true);

                // Firstly call this fragment
                fragment = new CustomerAccountInfoFragment(this);
                replaceFragment(fragment,false);
            }
        }

    }


    void addCustomTab() {
        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabLayout.getTabAt(i);
            View view = Objects.requireNonNull(tab).getCustomView();
            setTabView(Objects.requireNonNull(view), i, false);
        }
    }


    void replaceFragment(Fragment fragment,boolean isAllowBack) {

        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (isAllowBack)
        {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();

    }

    // For update tab icon or its title color.
    void setTabView(View view, int position, boolean isActive) {
        AppCompatImageView icon = view.findViewById(R.id.icon);
        MaterialTextView title = view.findViewById(R.id.title);

        if (isActive) {
            icon.setImageDrawable(ContextCompat.getDrawable(RegistrationActivity.this, activeTabIcons[position]));
            title.setText(tabTitleArray[position]);
            title.setTextColor(ContextCompat.getColor(RegistrationActivity.this, R.color.colorAccent));
        } else {
            icon.setImageDrawable(ContextCompat.getDrawable(RegistrationActivity.this, inActiveTabIcons[position]));
            title.setText(tabTitleArray[position]);
            title.setTextColor(ContextCompat.getColor(RegistrationActivity.this, R.color.greyDark));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       manageFragmentBackStack();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void manageFragmentBackStack()
    {
        View view;
        TabLayout.Tab tab;

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (currentFragment != null) {
            if (currentFragment instanceof CustomerAccountInfoFragment) {
                Log.e("Fragment_name", "Account info");
            } else if (currentFragment instanceof CustomerPersonalInfoFragment) {

                Log.e("Fragment_name", "CustomerPersonalInfoFragment");
                tab = binding.tabLayout.getTabAt(1);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 1, true);


                tab = binding.tabLayout.getTabAt(2);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 2, false);



            } else if (currentFragment instanceof CustomerDocumentsFragment) {

                Log.e("Fragment_name", "CustomerDocumentsFragment");

                tab = binding.tabLayout.getTabAt(2);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 2, true);

                tab = binding.tabLayout.getTabAt(3);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 3, false);


            } else if (currentFragment instanceof CustomerMemberAgreementFragment) {
                Log.e("Fragment_name", "CustomerMemberAgreementFragment");
            }
        }
    }

    @Override
    public void getData(int position, CustomerRegisterRequest requestData) {

        View view;
        TabLayout.Tab tab;

        switch (position) {
            case 1:
                fragment = new CustomerPersonalInfoFragment(this);
                bundle=new Bundle();
                bundle.putSerializable("requestData",requestData);
                fragment.setArguments(bundle);
                replaceFragment(fragment,false);
                tab = binding.tabLayout.getTabAt(position);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), position, true);

                tab = binding.tabLayout.getTabAt(0);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 0, false);
                break;

            case 2:
                fragment = new CustomerDocumentsFragment(this);
                bundle=new Bundle();
                bundle.putSerializable("requestData",requestData);
                fragment.setArguments(bundle);
                replaceFragment(fragment,true);
                tab = binding.tabLayout.getTabAt(position);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), position, true);

                tab = binding.tabLayout.getTabAt(1);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 1, false);
                break;

            case 3:
                fragment = new CustomerMemberAgreementFragment(this);
                bundle=new Bundle();
                bundle.putSerializable("requestData",requestData);
                fragment.setArguments(bundle);
                replaceFragment(fragment,true);
                tab = binding.tabLayout.getTabAt(position);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), position, true);

                tab = binding.tabLayout.getTabAt(2);
                view = Objects.requireNonNull(tab).getCustomView();
                setTabView(Objects.requireNonNull(view), 2, false);
                break;
        }
    }


    @Override
    public void onClick(View view) {

    }

}
