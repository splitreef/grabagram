package com.splitreef.grabagramplaystore.utils;
import android.app.ProgressDialog;
import android.content.Context;

import com.splitreef.grabagramplaystore.R;


public class DialogManager {
	public Context context;
	private ProgressDialog progressDialog;
	ProgressDialog dialog;

	//AlertDialog alertDialog;
	public DialogManager(Context _context)
	{		
		this.context = _context;
	}

	public void displayProgressDialog(Context _ctx , String _message)
	{
		dialog = new ProgressDialog(_ctx, R.style.progress_dialog);
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);

		if(_message != null && _message.length() >0)
		{
			dialog.setMessage(_message);
		}
		else
		{
			dialog.setMessage("Loading...");
		}
		dialog.show();
	}

	public void dismissProgressDialog()
	{
		if(dialog != null)
		{
			dialog.dismiss();
		}
	}

}
