package com.splitreef.grabagramplaystore.ui.customer.driverRating;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfileResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRating;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRatingResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DriverRatingViewModel extends ViewModel {

    private static final String TAG = "DispensaryReviewViewModel";
    private  FirebaseRepository firebaseRepository;
    private  MediatorLiveData<DataResource> onGetDriverProfile = new MediatorLiveData<>();
    private  MediatorLiveData<DataResource> onGetDriverRatingData = new MediatorLiveData<>();
    private  MediatorLiveData<DataResource> onSaveDriverRating = new MediatorLiveData<>();
    private  CompositeDisposable disposable = new CompositeDisposable();

    public DriverRatingViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }

    public  void getDriverProfileData(String driverId)
    {
        firebaseRepository.getDriverProfileData(driverId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DriverProfileResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetDriverProfile.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverProfileResponse driverProfileResponse) {

                        onGetDriverProfile.setValue(DataResource.DataStatus(driverProfileResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetDriverProfile.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    public  void getDriverRatingData(String driverId, String orderId)
    {
        firebaseRepository.getDriverRatingData(driverId, orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DriverRatingResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetDriverRatingData.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverRatingResponse driverRatingResponse) {

                        onGetDriverRatingData.setValue(DataResource.DataStatus(driverRatingResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetDriverRatingData.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    void saveDriverRating(DriverRating driverRating, String driverId)
    {
        firebaseRepository.saveDriverRating(driverRating,driverId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onSaveDriverRating.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String message) {
                        onSaveDriverRating.setValue(DataResource.DataStatus(message));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onSaveDriverRating.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }



    LiveData<DataResource> observeGetDriverProfileData()
    {
        return onGetDriverProfile;
    }

    LiveData<DataResource> observeGetDriverRatingData()
    {
        return onGetDriverRatingData;
    }

    LiveData<DataResource>observeSaveDriverRating()
    {
        return onSaveDriverRating;
    }



}
