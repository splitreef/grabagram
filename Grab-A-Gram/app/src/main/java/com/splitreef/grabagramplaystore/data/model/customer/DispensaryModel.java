package com.splitreef.grabagramplaystore.data.model.customer;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;


public class DispensaryModel implements Parcelable{

    private String about_us;
    private String profile_image;
    private long created_at;
    private String id;
    private String license_number;
    private boolean subscribe;
    private boolean text_notifications;
    private String busniess_name;
    private float rating;
    private long updated_at;
    private String user_id;
    private GeoPoint location;
    private DispensaryAddressModel address;
    private ArrayList<OpeningHours>opening_hours;
    private int distance;
    private String name;


    public DispensaryModel() {
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public DispensaryAddressModel getAddress() {
        return address;
    }

    public void setAddress(DispensaryAddressModel address) {
        this.address = address;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public ArrayList<OpeningHours> getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(ArrayList<OpeningHours> opening_hours) {
        this.opening_hours = opening_hours;
    }

    public String getLicense_number() {
        return license_number;
    }

    public void setLicense_number(String license_number) {
        this.license_number = license_number;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public boolean isText_notifications() {
        return text_notifications;
    }

    public void setText_notifications(boolean text_notifications) {
        this.text_notifications = text_notifications;
    }


    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected DispensaryModel(Parcel in) {
        about_us = in.readString();
        profile_image = in.readString();
        created_at = in.readLong();
        id = in.readString();
        license_number=in.readString();
        text_notifications=in.readByte() != 0;
        subscribe=in.readByte() != 0;
        busniess_name = in.readString();
        rating = in.readInt();
        updated_at = in.readLong();
        user_id = in.readString();
        Double lat = in.readDouble();
        Double lng = in.readDouble();
        location = new GeoPoint(lat, lng);
        address = (DispensaryAddressModel) in.readValue(DispensaryAddressModel.class.getClassLoader());
        opening_hours= (ArrayList<OpeningHours>) in.readSerializable();
        distance=in.readInt();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(about_us);
        dest.writeString(profile_image);
        dest.writeLong(created_at);
        dest.writeString(id);
        dest.writeString(license_number);
        dest.writeByte((byte) (text_notifications ? 1 : 0));
        dest.writeByte((byte) (subscribe ? 1 : 0));
        dest.writeString(busniess_name);
        dest.writeFloat(rating);
        dest.writeLong(updated_at);
        dest.writeString(user_id);
        dest.writeDouble(location.getLatitude());
        dest.writeDouble(location.getLongitude());
        dest.writeValue(address);
        dest.writeSerializable(opening_hours);
        dest.writeInt(distance);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DispensaryModel> CREATOR = new Parcelable.Creator<DispensaryModel>() {
        @Override
        public DispensaryModel createFromParcel(Parcel in) {
            return new DispensaryModel(in);
        }

        @Override
        public DispensaryModel[] newArray(int size) {
            return new DispensaryModel[size];
        }
    };



  /*  @Override
    public int compareTo(Object o) {
        int distance =((DispensaryModel)o).getDistance();
        return this.distance-distance;
    }*/


}



