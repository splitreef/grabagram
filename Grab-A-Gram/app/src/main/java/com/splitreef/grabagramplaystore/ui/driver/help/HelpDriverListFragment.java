package com.splitreef.grabagramplaystore.ui.driver.help;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.driver.HelpDriverListAdapter;
import com.splitreef.grabagramplaystore.data.model.customer.help.HelpListResponse;
import com.splitreef.grabagramplaystore.databinding.FragmentHelpListBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.viewModelFactory.HelpDriverListProviderFactory;

import java.util.Objects;

public class HelpDriverListFragment extends Fragment {

    private FragmentHelpListBinding binding;
    private Context context;
    private GrabAGramApplication application;
    private HelpDriverListViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHelpListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    public void initView() {
        application = (GrabAGramApplication) context.getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new HelpDriverListProviderFactory(application.firebaseRepository)).get(HelpDriverListViewModel.class);
        HomeActivity.tvActiveStatus.setVisibility(View.GONE);
        binding.rvHelpList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        subscribeObservers();
        viewModel.getHelpList();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    void subscribeObservers() {
        viewModel.observeHelpList().observe(Objects.requireNonNull(getActivity()), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        HelpListResponse helpListResponse = (HelpListResponse) dataResource.data;
                        updateUi(helpListResponse);
                        break;
                    case ERROR:
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.rvHelpList.setVisibility(View.GONE);
                        binding.progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    void updateUi(HelpListResponse helpListResponse) {
        if (helpListResponse != null && helpListResponse.getHelpList() != null && helpListResponse.getHelpList().size() > 0) {
            binding.userMsg.setVisibility(View.GONE);
            binding.rvHelpList.setVisibility(View.VISIBLE);

            HelpDriverListAdapter helpDriverListAdapter = new HelpDriverListAdapter(context, helpListResponse.getHelpList());
            binding.rvHelpList.setAdapter(helpDriverListAdapter);

        } else {
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.rvHelpList.setVisibility(View.GONE);
        }
    }

}
