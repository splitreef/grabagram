package com.splitreef.grabagramplaystore.callBack;

public interface RemoveWishListProduct {

    void removeWishListProduct(String id);
}
