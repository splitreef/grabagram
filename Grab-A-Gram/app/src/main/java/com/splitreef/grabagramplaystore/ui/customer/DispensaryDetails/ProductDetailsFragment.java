package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.ProductItemGalleryAdapter;
import com.splitreef.grabagramplaystore.adapter.cutomer.ProductReviewAdapter;
import com.splitreef.grabagramplaystore.adapter.cutomer.RelatedProductAdapter;
import com.splitreef.grabagramplaystore.adapter.cutomer.StainAttributeParentAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Effect;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.StainAttributesModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Weights;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Category;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentProductDetailsBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryDetailsViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

public class ProductDetailsFragment extends BaseFragment implements View.OnClickListener {

    private FragmentProductDetailsBinding binding;
    private Context context;
    private ProductListResponse productListResponse;
    private DispensaryModel dispensaryModel;
    private int quantity=1;
    private String weight="";
    private DispensaryDetailsViewModel viewModel;
    private GrabAGramApplication application;
    private String badgeCount2="";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null)
        {
            productListResponse= (ProductListResponse) getArguments().getSerializable(AppConstant.PRODUCT_RESPONSE);
            dispensaryModel= (DispensaryModel) getArguments().getParcelable(AppConstant.DISPENSARY_MODEL);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding=FragmentProductDetailsBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initListeners();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onResume() {
        super.onResume();
        quantity=1;
    }

    @SuppressLint("SetTextI18n")
    public void initView()
    {

        application=(GrabAGramApplication) context.getApplicationContext();
        viewModel=new ViewModelProvider(getViewModelStore(), new DispensaryDetailsViewModelProviderFactory(application.firebaseRepository)).get(DispensaryDetailsViewModel.class);
        subscribeObservers();

        if (productListResponse!=null)
        {
            // Set product gallery image
            if (productListResponse.getProduct_gallery()!=null && productListResponse.getProduct_gallery().size()>0)
            {
                binding.tabDotLayout.setupWithViewPager(binding.viewPagerGallery,true);
                ProductItemGalleryAdapter productItemGalleryAdapter= new ProductItemGalleryAdapter(context,productListResponse.getProduct_gallery());
                binding.viewPagerGallery.setAdapter(productItemGalleryAdapter);
            }

            // Set product's  name,price,SKU, and its description
            binding.txtProductName.setText(""+productListResponse.getName());
            binding.txtProductPrice.setText(""+productListResponse.getSale_price());
            binding.txtSku.setText("SKU : "+productListResponse.getSku());
            binding.txtDescription.setText(""+productListResponse.getDescription());

            // Set product categories name
            if (productListResponse.getCategories()!=null && productListResponse.getCategories().size()>0)
            {
                String categoriesName=getCategoriesName(productListResponse.getCategories());
                binding.txtCategories.setText(categoriesName);
            }


            // Set weights spinner
            if (productListResponse.getWeights()!=null && productListResponse.getWeights().size()>0)
            {

                Weights weights=new Weights();
                weights.setName("Select");
                productListResponse.getWeights().add(0,weights);

                ArrayAdapter<Weights> weightAdapter=new ArrayAdapter<>(context, R.layout.spinner_item, productListResponse.getWeights());
                binding.spinnerWeight.setAdapter(weightAdapter);
            }


            // set related product list
            if (productListResponse.getRelatedProducts()!=null && productListResponse.getRelatedProducts().size()>0)
            {
                binding.rvRelatedProduct.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
                RelatedProductAdapter relatedProductAdapter=new RelatedProductAdapter(context,productListResponse.getRelatedProducts());
                binding.rvRelatedProduct.setAdapter(relatedProductAdapter);
                binding.txtRelatedProductHeading.setVisibility(View.VISIBLE);
            }

            if (productListResponse.getProductReviewList()!=null && productListResponse.getProductReviewList().size()>0)
            {
                binding.rvReview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
                ProductReviewAdapter productReviewAdapter=new ProductReviewAdapter(context,productListResponse.getProductReviewList());
                binding.rvReview.setAdapter(productReviewAdapter);

                binding.userMsgReview.setVisibility(View.GONE);
                binding.rvReview.setVisibility(View.VISIBLE);

                ArrayList<ProductReviewModel>productReviewModelList=productListResponse.getProductReviewList();

                // for calculate product rating
                float productRating=0;
                for (int i=0;i<productReviewModelList.size();i++)
                {
                    ProductReviewModel productReviewModel=productReviewModelList.get(i);
                    productRating=productRating+productReviewModel.getRating();
                }
                productRating=productRating/productReviewModelList.size();
                binding.productRating.setRating(productRating);
            }
            else
            {
                binding.userMsgReview.setVisibility(View.VISIBLE);
                binding.rvReview.setVisibility(View.GONE);
            }


            if (getStainAttributesList().size()>0)
            {
                binding.userMsgStainAttributes.setVisibility(View.GONE);
                binding.rvStainAttributes.setVisibility(View.VISIBLE);

                binding.rvStainAttributes.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
                StainAttributeParentAdapter stainAttributeParentAdapter=new StainAttributeParentAdapter(context,getStainAttributesList());
                binding.rvStainAttributes.setAdapter(stainAttributeParentAdapter);
            }
            else
            {
                binding.userMsgStainAttributes.setVisibility(View.VISIBLE);
                binding.rvStainAttributes.setVisibility(View.GONE);
            }
        }

    }

    public void initListeners()
    {
        binding.txtDescriptionTab.setOnClickListener(this);
        binding.txtStrainAttributesTab.setOnClickListener(this);
        binding.txtReviewTab.setOnClickListener(this);
        binding.addToCart.setOnClickListener(this);
        binding.imgMinus.setOnClickListener(this);
        binding.imgPlus.setOnClickListener(this);
        binding.txtAddToWish.setOnClickListener(this);

        binding.spinnerWeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Weights weights= (Weights) adapterView.getItemAtPosition(i);
                weight=weights.getValue()+" "+weights.getName();
              //  showToast(context,weights.getName());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    String getCategoriesName(ArrayList<Category>categories)
    {
        String categoriesName="";

        for (int i=0;i<categories.size();i++)
        {
            if (i==0)
            {
                categoriesName=categories.get(i).getCategory_name();
            }
            else
            {
                categoriesName=categoriesName+", "+categories.get(i).getCategory_name();
            }
        }

        return categoriesName;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.add_to_cart:
                addProductToCart();
                break;
            case R.id.txt_add_to_wish:
                addProductToWishList();
                break;
            case R.id.img_plus:
                addProductQuantity();
                break;

            case R.id.img_minus:
                removeProductQuantity();
                break;

            case R.id.txt_description_tab:
                manageDescReviewStrainTab(1);
                break;
            case R.id.txt_strain_attributes_tab:
                //showToast(context,"In developing mode.");
                manageDescReviewStrainTab(2);
                break;
            case R.id.txt_review_tab:
              //  showToast(context,"In developing mode.");
                manageDescReviewStrainTab(3);
                break;


        }
    }


    public void addProductToCart()
    {
        if (binding.spinnerWeight.getSelectedItemPosition()==0)
        {
            showToast(context,"Please select weight");
        }
        else
        {
           productListResponse.setBusniess_name(dispensaryModel.getBusniess_name());
           productListResponse.setQuantity(Integer.parseInt(binding.txtQuantity.getText().toString()));
           productListResponse.setTotalPrice(Double.parseDouble(binding.txtProductPrice.getText().toString()));
           productListResponse.setAbout_us(dispensaryModel.getAbout_us());
           productListResponse.setWeightNameValue(weight);
           productListResponse.setRating(dispensaryModel.getRating());
           productListResponse.setDispensaryUserId(dispensaryModel.getUser_id());
           productListResponse.setDispensaryId(dispensaryModel.getId());

           viewModel.addProductToCart(productListResponse);

        }
    }

    public void addProductToWishList()
    {
        if (binding.spinnerWeight.getSelectedItemPosition()==0)
        {
            showToast(context,"Please select weight");
        }
        else
        {
            productListResponse.setBusniess_name(dispensaryModel.getBusniess_name());
            productListResponse.setQuantity(Integer.parseInt(binding.txtQuantity.getText().toString()));
            productListResponse.setTotalPrice(Double.parseDouble(binding.txtProductPrice.getText().toString()));
            productListResponse.setAbout_us(dispensaryModel.getAbout_us());
            productListResponse.setWeightNameValue(weight);
            productListResponse.setRating(dispensaryModel.getRating());
            productListResponse.setDispensaryUserId(dispensaryModel.getUser_id());
            productListResponse.setDeliveryFee(50);
            productListResponse.setDispensaryId(dispensaryModel.getId());


            viewModel.addProductToWishList(productListResponse);
        }
    }

    public void addProductQuantity()
    {
       int quantity = Integer.parseInt(binding.txtQuantity.getText().toString());

       if (quantity<=4)
       {
           ++quantity;
           double totalPrice= quantity*productListResponse.getSale_price();
           binding.txtProductPrice.setText(""+totalPrice);
           binding.txtQuantity.setText(""+quantity);
       }
       else
       {
           showToast(context,"You can add max 5 quantity of product");
       }



    }
    public void removeProductQuantity()
    {
        if (!binding.txtQuantity.getText().toString().equals("1"))
        {
            int quantity = Integer.parseInt( binding.txtQuantity.getText().toString());
            --quantity;
            double totalPrice= quantity*productListResponse.getSale_price();
            binding.txtProductPrice.setText(""+totalPrice);
            binding.txtQuantity.setText(""+quantity);
        }
    }

    void manageDescReviewStrainTab(int tab)
    {
        switch (tab)
        {
            case 1:
                binding.txtDescription.setVisibility(View.VISIBLE);
                binding.llReview.setVisibility(View.GONE);
                binding.llStainAttributes.setVisibility(View.GONE);
                break;
            case 2:
                binding.llStainAttributes.setVisibility(View.VISIBLE);
                binding.txtDescription.setVisibility(View.GONE);
                binding.llReview.setVisibility(View.GONE);
                break;
            case 3:
                binding.txtDescription.setVisibility(View.GONE);
                binding.llStainAttributes.setVisibility(View.GONE);
                binding.llReview.setVisibility(View.VISIBLE);
                break;


        }
    }

    void subscribeObservers()
    {
        viewModel.observeProductAddToCart().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(context,"Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        String msg=(String)dataResource.data;

                        if (msg.equals("Product added successfully."))
                        {
                            showToast(context,"Product added successfully.");
                            //PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,badgeCount2);
                            ((DispensaryDetailsActivity)context).updateBadgeCount();
                        }
                        else
                        {
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {

                                }
                            }.showSimpleDialog(context,"Information",msg,"OK");
                        }
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        showToast(context,"Data not found");
                        break;

                }
            }

        });

        viewModel.observeProductAddToWishList().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(context,"Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        String msg=(String)dataResource.data;

                        if (msg.equals("Product added successfully."))
                        {
                            showToast(context,"Product added successfully.");
                        }
                        else
                        {
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {

                                }
                            }.showSimpleDialog(context,"Information",msg,"OK");
                        }
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        showToast(context,"Data not found");
                        break;

                }
            }

        });

    }


    private  ArrayList<StainAttributesModel> getStainAttributesList() {

        ArrayList<StainAttributesModel> attributesList=new ArrayList<>();
        ArrayList<String> effectsList=new ArrayList<>();

        if (productListResponse.getStain_attributes()!=null)
        {
            if (productListResponse.getStain_attributes().getEffects()!=null)
            {
                Effect effect=productListResponse.getStain_attributes().getEffects();
                effectsList.add("euphoric  "+effect.getEuphoric());
                effectsList.add("happy  "+effect.getHappy());
                effectsList.add("relaxed  "+effect.getRelaxed());
                effectsList.add("uplifted  "+effect.getUplifted());

                StainAttributesModel stainAttributesEffect=new StainAttributesModel();
                stainAttributesEffect.setTitle("Effects");
                stainAttributesEffect.setValues(effectsList);

                attributesList.add(stainAttributesEffect);
            }

            if (productListResponse.getStain_attributes().getMedical_benefits()!=null && productListResponse.getStain_attributes().getMedical_benefits().size()>0)
            {
                ArrayList<String>medicalBenefitStringList = productListResponse.getStain_attributes().getMedical_benefits();
                StainAttributesModel stainAttributesMedicalBenefits=new StainAttributesModel();

                stainAttributesMedicalBenefits.setTitle("Medical Benefits");
                stainAttributesMedicalBenefits.setValues(medicalBenefitStringList);
                attributesList.add(stainAttributesMedicalBenefits);
            }


            if (productListResponse.getStain_attributes().getNegative()!=null && productListResponse.getStain_attributes().getNegative().size()>0)
            {
                ArrayList<String>negativeStringList = productListResponse.getStain_attributes().getNegative();
                StainAttributesModel stainAttributesNegative=new StainAttributesModel();
                stainAttributesNegative.setTitle("Negative");
                stainAttributesNegative.setValues(negativeStringList);
                attributesList.add(stainAttributesNegative);
            }
        }

        return attributesList;
    }

}
