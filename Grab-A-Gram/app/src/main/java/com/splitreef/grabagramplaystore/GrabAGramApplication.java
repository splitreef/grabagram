package com.splitreef.grabagramplaystore;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dataSource.remote.FirebaseDataSource;
import com.splitreef.grabagramplaystore.utils.DialogManager;
import com.splitreef.grabagramplaystore.utils.MessageManager;

public class GrabAGramApplication extends Application {

    public DialogManager dialogManager;
    public FirebaseDataSource firebaseDataSource;
    public  FirebaseRepository firebaseRepository;
    public MessageManager messageManager;

    @Override
    public void onCreate() {
        super.onCreate();

        dialogManager=new DialogManager(this);
        messageManager=new MessageManager(this);
        // For firebase auth or firebase store
        firebaseDataSource=new FirebaseDataSource(FirebaseAuth.getInstance(), FirebaseFirestore.getInstance(), FirebaseStorage.getInstance(),this);
        firebaseRepository=new FirebaseRepository(firebaseDataSource);
        PreferenceManger.initPreference(getApplicationContext());

    }
}
