package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DispensaryDetailsViewModel extends ViewModel {
    private static final String TAG = "DispensaryDetailsViewModel";
    private final FirebaseRepository firebaseRepository;
    private final MediatorLiveData<DataResource> onGetMenuList = new MediatorLiveData<>();
    private final MediatorLiveData<DataResource> onGetDealList = new MediatorLiveData<>();
    private final MediatorLiveData<DataResource> onGetDispensaryReviewList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onAddProductToCart = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onAddProductToWishList = new MediatorLiveData<>();
    private final CompositeDisposable disposable = new CompositeDisposable();


    public DispensaryDetailsViewModel(FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;

    }

    // For get list
    void getMenuList(String dispensaryId)
    {
         firebaseRepository.getMenuList(dispensaryId)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<ArrayList<ProductListResponse>>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {
                         disposable.add(d);
                         onGetMenuList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull ArrayList<ProductListResponse> productListResponses) {
                         onGetMenuList.setValue(DataResource.DataStatus(productListResponses));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {
                         onGetMenuList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });

    }


    // For get deal list by id
    void getDealListById(String userId)
    {
        firebaseRepository.getDealListById(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<DealModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetDealList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull ArrayList<DealModel> dealModels) {
                        onGetDealList.setValue(DataResource.DataStatus(dealModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDealList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    // For get deal list by id
    void getDispensaryReviewList(String dispensaryId)
    {
        firebaseRepository.getDispensaryReviewList(dispensaryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<DispensaryReviewModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetDispensaryReviewList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull ArrayList<DispensaryReviewModel> dispensaryReviewModels) {
                        onGetDispensaryReviewList.setValue(DataResource.DataStatus(dispensaryReviewModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDispensaryReviewList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    // add product to cart

    void addProductToCart(ProductListResponse productListResponse)
    {
        firebaseRepository.addProductToCart(productListResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onAddProductToCart.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String msg) {
                        onAddProductToCart.setValue(DataResource.DataStatus(msg));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onAddProductToCart.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    // add product to cart

    void addProductToWishList(ProductListResponse productListResponse)
    {
        firebaseRepository.addProductToWishList(productListResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onAddProductToWishList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String msg) {
                        onAddProductToWishList.setValue(DataResource.DataStatus(msg));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onAddProductToWishList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    LiveData<DataResource>observeGetDealList()
    {
        return onGetDealList;
    }


    LiveData<DataResource>observeGetMenuList()
    {
        return onGetMenuList;
    }


    LiveData<DataResource>observeGetDispensaryReviewList()
    {
        return onGetDispensaryReviewList;
    }

   LiveData<DataResource>observeProductAddToCart()
    {
        return onAddProductToCart;
    }
   LiveData<DataResource>observeProductAddToWishList()
    {
        return onAddProductToWishList;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }

}
