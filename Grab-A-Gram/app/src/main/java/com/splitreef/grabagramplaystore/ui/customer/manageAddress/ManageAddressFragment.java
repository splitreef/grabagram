package com.splitreef.grabagramplaystore.ui.customer.manageAddress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.ManageAddressAdapter;
import com.splitreef.grabagramplaystore.callBack.DeleteAddressListener;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.databinding.FragmentManageAddressBinding;
import com.splitreef.grabagramplaystore.ui.customer.addNewAddress.AddAddressActivity;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.ManageAddressViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

public class ManageAddressFragment extends BaseFragment implements View.OnClickListener, DeleteAddressListener {

    private FragmentManageAddressBinding binding;
    private Context context;
    private GrabAGramApplication application;
    private ManageAddressViewModel viewModel;
    private  ManageAddressAdapter manageAddressAdapter;
    private int position=-1;
    private AddressList addressList;
    private ArrayList<String> addressTypeList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentManageAddressBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void initView() {

        application=(GrabAGramApplication)context.getApplicationContext();

        viewModel=new ViewModelProvider(getViewModelStore(), new ManageAddressViewModelProviderFactory(application.firebaseRepository)).get(ManageAddressViewModel.class);

        ((DispensaryListActivity)context).setToolbarTitle("Manage addresses");

        binding.rvManageAddress.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));


        binding.btnAddAddress.setOnClickListener(this);

        subscribeObservers();


        binding.cbIsPersonalAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked)
                {
                    if (manageAddressAdapter!=null)
                    {
                        if (manageAddressAdapter.checkedPrimaryAddressContained())
                        {
                            manageAddressAdapter.removeAddressByType();
                        }
                        else
                        {
                           viewModel.updateIsPersonalAddress(true);
                        }

                    }
                }
                else
                {
                    viewModel.updateIsPersonalAddress(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getAddressList();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()== R.id.btn_add_address)
        {
            startActivity(new Intent(context, AddAddressActivity.class)
                    .putExtra(AppConstant.FROM,AppConstant.MANAGE_ADDRESS_SCREEN)
                    .putExtra(AppConstant.FOR,AppConstant.FOR_ADD_ADDRESS)
                    .putStringArrayListExtra("addressTypeList",addressTypeList));
        }
    }


    void subscribeObservers()
    {
        viewModel.observeGetAddressList().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        AddressList addressList=(AddressList)dataResource.data;
                        updateUI(addressList);
                        break;
                    case ERROR:
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                }
            }

        });

        viewModel.observeDeleteAddress().observe(getActivity(), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                         //   application.dialogManager.displayProgressDialog(context, "Loading...");
                            break;
                        case SUCCESS:
                           // application.dialogManager.dismissProgressDialog();
                            if (manageAddressAdapter!=null)
                            {
                                viewModel.getAddressList();

                              //  manageAddressAdapter.removeAddress(position);

                                if (manageAddressAdapter.getAddressList()!=null && manageAddressAdapter.getAddressList().size()>0)
                                {
                                    //getAddressTypeList(manageAddressAdapter.getAddressList());
                                }

                            }

                            break;
                        case ERROR:
                         //   application.dialogManager.dismissProgressDialog();
                            showToast(context,"Something went wrong!");
                            Log.d("error",""+stateResource.message);

                    }
                }
            }
        });


        viewModel.observeUpdatePersonalAddress().observe(getActivity(), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            //   application.dialogManager.displayProgressDialog(context, "Loading...");
                            break;
                        case SUCCESS:
                            // application.dialogManager.dismissProgressDialog();
                            if (manageAddressAdapter!=null)
                            {
                                viewModel.getAddressList();

                                //  manageAddressAdapter.removeAddress(position);

                                if (manageAddressAdapter.getAddressList()!=null && manageAddressAdapter.getAddressList().size()>0)
                                {
                                    //getAddressTypeList(manageAddressAdapter.getAddressList());
                                }

                            }

                            break;
                        case ERROR:
                            //   application.dialogManager.dismissProgressDialog();
                            showToast(context,"Something went wrong!");
                            Log.d("error",""+stateResource.message);

                    }
                }
            }
        });
    }

    private void updateUI(AddressList addressList) {

        if (addressList!=null && addressList.getAddresses()!=null && addressList.getAddresses().size()>0)
        {
            this.addressList= addressList;

            if (addressList.getIsPersonAddress())
            {
                binding.cbIsPersonalAddress.setChecked(true);
            }
            else
            {
                binding.cbIsPersonalAddress.setChecked(false);
                addressList.getAddresses().remove(0);

            }

            manageAddressAdapter = new ManageAddressAdapter(context,addressList.getAddresses(),addressList.getIsPersonAddress(),this);
            binding.rvManageAddress.setAdapter(manageAddressAdapter);


            getAddressTypeList(manageAddressAdapter.getAddressList());

            if(addressTypeList!=null && addressTypeList.size()==1)
            {
                binding.btnAddAddress.setVisibility(View.GONE);
            }
            else
            {
                binding.btnAddAddress.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public void onClickDelete(UserAddressModel userAddressModel, boolean isPersonAddress,int position) {
        this.position=position;
        viewModel.deleteCustomerAddress(userAddressModel,isPersonAddress);
    }

    void getAddressTypeList(ArrayList<UserAddressModel> userAddressModels)
    {
          addressTypeList=AppUtil.getDefaultAddressKey();

        for (int i=0;i<userAddressModels.size();i++)
        {
            UserAddressModel userAddressModel=userAddressModels.get(i);

            if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS) || userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS))
            {
                addressTypeList.remove(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS);
            }
            else
            {
                addressTypeList.remove(userAddressModel.getType());
            }

        }

    }

}
