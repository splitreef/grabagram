package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;
import java.util.ArrayList;

public class StainAttributesModel implements Serializable {

    private String title;
    private ArrayList<String> values;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }
}
