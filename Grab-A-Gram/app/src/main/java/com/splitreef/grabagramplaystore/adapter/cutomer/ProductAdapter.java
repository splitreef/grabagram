package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.ProductDetailsCallBack;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {

    private Context context;
    private ArrayList<ProductListResponse>productList;
    private ProductDetailsCallBack productDetailsCallBack;

    public ProductAdapter(Context context,ArrayList<ProductListResponse>productList,ProductDetailsCallBack productDetailsCallBack) {
        this.context=context;
        this.productList=productList;
        this.productDetailsCallBack=productDetailsCallBack;
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ProductHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {

       ProductListResponse product= productList.get(position);

       if (product.getProduct_gallery()!=null && product.getProduct_gallery().size()>0)
       {
           PicassoManager.setImage(product.getProduct_gallery().get(0),holder.imgProduct);
       }

        holder.txtProductName.setText(product.getName());
        holder.txtProductPrice.setText("Starting At $"+product.getSale_price());

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              productDetailsCallBack.onClickProduct(product);

            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder
    {
        ShapeableImageView imgProduct;
        MaterialTextView txtProductName;
        MaterialTextView txtProductPrice;
        LinearLayout llRoot;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct= itemView.findViewById(R.id.ima_product);
            txtProductName= itemView.findViewById(R.id.txt_product_name);
            txtProductPrice= itemView.findViewById(R.id.txt_product_price);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }
}
