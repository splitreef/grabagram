package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.FilterAdapter;
import com.splitreef.grabagramplaystore.callBack.driver.FilterItemListener;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.databinding.ActivityFilterDataBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 06-01-2021.
 */
public class FilterActivity extends BaseActivity implements View.OnClickListener, FilterItemListener {
    ActivityFilterDataBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private FilterAdapter filterAdapter;
    private List<String> filterItemList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFilterDataBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        intiView();
    }

    private void intiView() {
        filterItemList.add("10 Mile");
        filterItemList.add("25 Mile");
        filterItemList.add("50 Mile");
        filterItemList.add("100 Mile");
        filterItemList.add("250 Mile");
        binding.imvBack.setOnClickListener(this);
        binding.tvCancel.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(FilterActivity.this, LinearLayoutManager.VERTICAL, false);
        binding.recycleFilter.setLayoutManager(linearLayoutManager);
        filterAdapter = new FilterAdapter(FilterActivity.this, filterItemList, this);
        binding.recycleFilter.setAdapter(filterAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finish();
                break;
            case R.id.tv_cancel:
                HomeFragment.miles = 40;
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(int miles) {
        HomeFragment.miles = miles;
        Toast.makeText(this, "" + miles, Toast.LENGTH_SHORT).show();
        finish();
    }
}
