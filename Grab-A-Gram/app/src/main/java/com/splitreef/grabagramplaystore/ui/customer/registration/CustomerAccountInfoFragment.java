package com.splitreef.grabagramplaystore.ui.customer.registration;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentConsumer;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.databinding.FragmentCustomerAccountInfoBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.RegistrationViewModelProviderFactory;

import java.util.Objects;

public class CustomerAccountInfoFragment extends Fragment implements View.OnClickListener {

    private FragmentCustomerAccountInfoBinding binding;
    private FragmentConsumer fragmentConsumer;
    private RegistrationViewModel viewModel;
    private GrabAGramApplication application;
    private CustomerRegisterRequest customerRegisterRequest;


    public CustomerAccountInfoFragment(FragmentConsumer fragmentConsumer) {
        this.fragmentConsumer = fragmentConsumer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCustomerAccountInfoBinding.inflate(inflater, container, false);

        initView();
        subscribeObservers();
        return binding.getRoot();
    }


    void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new RegistrationViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(RegistrationViewModel.class);

        // set click listeners

        binding.btnContinue.setOnClickListener(this);
    }

    // For account info Validation
    boolean isValidation() {

        if (Objects.requireNonNull(binding.emailAddress.getText()).toString().equals("")) {
            binding.emailAddress.setError(getString(R.string.enter_your_email));
            binding.emailAddress.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(binding.emailAddress.getText()).toString()).matches()) {
            binding.emailAddress.setError(getString(R.string.enter_your_valid_email));
            binding.emailAddress.requestFocus();
            return false;
        }
        else if (Objects.requireNonNull(binding.password.getText()).toString().equals("")) {
            binding.password.setError(getString(R.string.enter_your_password));
            binding.password.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_continue) {
            if (!NetworkManager.isNetworkAvailable(getContext())) {
                Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
            } else {
                performRegister();
            }

        }
    }

    private void performRegister() {

        if (isValidation()) {
            customerRegisterRequest = new CustomerRegisterRequest();
            customerRegisterRequest.setEmail(Objects.requireNonNull(binding.emailAddress.getText()).toString());
            customerRegisterRequest.setPassword(Objects.requireNonNull(binding.password.getText()).toString());
           // customerRegisterRequest.setUsername(Objects.requireNonNull(binding.username.getText()).toString());
            customerRegisterRequest.setRole(AppConstant.CUSTOMER_ROLE);
            customerRegisterRequest.setActivateAt(true);
            customerRegisterRequest.setDeviceId(AppConstant.getDeviceId(Objects.requireNonNull(getContext())));
            customerRegisterRequest.setDeviceType(AppConstant.DEVICE_TYPE);
            customerRegisterRequest.setEmailCommunication(binding.cbEmailReceving.isChecked());

            viewModel.checkUserExistsOrNot(binding.emailAddress.getText().toString());

           /* viewModel.registerUser(Objects.requireNonNull(
                    binding.emailAddress.getText()).toString(),
                    Objects.requireNonNull(binding.password.getText()).toString(),binding.cbEmailReceving.isChecked());*/
        } else {
            Log.e("Error==", "Something went wrong");
        }
    }

    void subscribeObservers() {
        viewModel.observeRegister().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();

                            fragmentConsumer.getData(1, customerRegisterRequest);
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "" + stateResource.message, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        viewModel.observeCheckUserExistOrNot().observe(getActivity(), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        Boolean isExist= (Boolean) dataResource.data;
                        if (isExist!=null && isExist)
                        {
                            Toast.makeText(getContext(), getString(R.string.user_already_exist), Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            fragmentConsumer.getData(1, customerRegisterRequest);
                        }
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        Toast.makeText(getContext(), "" + dataResource.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
