package com.splitreef.grabagramplaystore.ui.customer.dispensaryReview;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DispensaryReviewViewModel extends ViewModel {

    private static final String TAG = "DispensaryReviewViewModel";
    private FirebaseRepository firebaseRepository;
    private final MediatorLiveData<DataResource> onGetDispensaryReviewList = new MediatorLiveData<>();
    private final MediatorLiveData<DataResource> onSaveDispensaryReview = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public DispensaryReviewViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    void getDispensaryReviewList(String dispensaryId)
    {
        firebaseRepository.getDispensaryReviewList(dispensaryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<DispensaryReviewModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetDispensaryReviewList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull ArrayList<DispensaryReviewModel> dispensaryReviewModels) {
                        onGetDispensaryReviewList.setValue(DataResource.DataStatus(dispensaryReviewModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDispensaryReviewList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    void saveDispensaryReview(DispensaryReviewModel dispensaryReviewModel,float totalRating)
    {
        firebaseRepository.saveDispensaryReview(dispensaryReviewModel,totalRating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onSaveDispensaryReview.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String message) {
                        onSaveDispensaryReview.setValue(DataResource.DataStatus(message));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onSaveDispensaryReview.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }



    LiveData<DataResource>observeGetDispensaryReviewList()
    {
        return onGetDispensaryReviewList;
    }

    LiveData<DataResource>observeSaveDispensaryReview()
    {
        return onSaveDispensaryReview;
    }



}
