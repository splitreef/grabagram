package com.splitreef.grabagramplaystore.data.model.customer.driverRating;

public class DriverRatingResponse {
    private String status;
    private DriverRating driverRating;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DriverRating getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(DriverRating driverRating) {
        this.driverRating = driverRating;
    }
}
