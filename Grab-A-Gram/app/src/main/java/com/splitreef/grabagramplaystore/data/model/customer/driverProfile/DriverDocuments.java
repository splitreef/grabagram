package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;

import java.io.Serializable;

public class DriverDocuments implements Serializable {
    private String document_type;
    private long expiration_date;
    private String identity_number;
    private String issuing_state;
    private String licence_issuing_state;
    private String licence_plate_number;
    private String manufacture_company;
    private String manufacture_model;
    private String manufacture_year;
    private String regular_maintenance;

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public long getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(long expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getIssuing_state() {
        return issuing_state;
    }

    public void setIssuing_state(String issuing_state) {
        this.issuing_state = issuing_state;
    }

    public String getLicence_issuing_state() {
        return licence_issuing_state;
    }

    public void setLicence_issuing_state(String licence_issuing_state) {
        this.licence_issuing_state = licence_issuing_state;
    }

    public String getLicence_plate_number() {
        return licence_plate_number;
    }

    public void setLicence_plate_number(String licence_plate_number) {
        this.licence_plate_number = licence_plate_number;
    }

    public String getManufacture_company() {
        return manufacture_company;
    }

    public void setManufacture_company(String manufacture_company) {
        this.manufacture_company = manufacture_company;
    }

    public String getManufacture_model() {
        return manufacture_model;
    }

    public void setManufacture_model(String manufacture_model) {
        this.manufacture_model = manufacture_model;
    }

    public String getManufacture_year() {
        return manufacture_year;
    }

    public void setManufacture_year(String manufacture_year) {
        this.manufacture_year = manufacture_year;
    }

    public String getRegular_maintenance() {
        return regular_maintenance;
    }

    public void setRegular_maintenance(String regular_maintenance) {
        this.regular_maintenance = regular_maintenance;
    }
}
