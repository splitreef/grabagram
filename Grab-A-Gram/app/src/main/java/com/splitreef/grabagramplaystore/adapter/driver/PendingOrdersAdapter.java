package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.driver.OrderAcceptDeclineListener;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.GetDistance;

import java.util.ArrayList;

/**
 * Created by  on 01-12-2020.
 */
public class PendingOrdersAdapter extends RecyclerView.Adapter<PendingOrdersAdapter.PendingOrdersHolder> {
    private ArrayList<OrdersModel> ordersList;
    private Context context;
    private double latitude;
    private double longitude;
    private OrderAcceptDeclineListener orderAcceptDeclineListener;


    public PendingOrdersAdapter(@NonNull Context context, ArrayList<OrdersModel> ordersList, double latitude, double longitude, OrderAcceptDeclineListener orderAcceptDeclineListener) {
        this.context = context;
        this.ordersList = ordersList;
        this.latitude = latitude;
        this.longitude = longitude;
        this.orderAcceptDeclineListener = orderAcceptDeclineListener;
    }


    public ArrayList<OrdersModel> getDispensaryList() {
        return ordersList;
    }

    @NonNull
    @Override
    public PendingOrdersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending_order_view, parent, false);
        return new PendingOrdersHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull PendingOrdersHolder holder, int position) {

        OrdersModel ordersModel = ordersList.get(holder.getAdapterPosition());
        holder.tv_order_number.setText("#" + ordersModel.getOrder_no());
        holder.tv_schedule_for.setText("Schedule for " + AppUtil.getDateFromMillisecondTime(ordersModel.getCreated_at()) +
                " at " + AppUtil.getTimeFromMillisecondTime(ordersModel.getCreated_at()));
        double distance = new GetDistance().distance(latitude, longitude, ordersModel.getDispensary_location().getLatitude(), ordersModel.getDispensary_location().getLongitude());
        holder.tv_miles_away.setText(String.format("%.2f %s", distance, "MILES AWAY"));
        holder.ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OrdersDetailsActivity.class)
                        .putExtra(AppConstant.ID, ordersModel.getId()));

            }
        });
        holder.tv_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderAcceptDeclineListener.onAcceptClick(ordersModel.getId(), ordersModel);
            }
        });
        holder.tv_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderAcceptDeclineListener.onDeclineClick(ordersModel.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    class PendingOrdersHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_order_number, tv_schedule_for;
        MaterialTextView tv_miles_away, tv_time_in_minute, tv_accept, tv_decline;
        private RelativeLayout rel_time_in_minute;
        private LinearLayout ll_top;

        public PendingOrdersHolder(@NonNull View itemView) {
            super(itemView);
            tv_order_number = itemView.findViewById(R.id.tv_order_number);
            tv_schedule_for = itemView.findViewById(R.id.tv_schedule_for);
            tv_miles_away = itemView.findViewById(R.id.tv_miles_away);
            tv_time_in_minute = itemView.findViewById(R.id.tv_time_in_minute);
            rel_time_in_minute = itemView.findViewById(R.id.rel_time_in_minute);
            tv_accept = itemView.findViewById(R.id.tv_accept);
            tv_decline = itemView.findViewById(R.id.tv_decline);
            ll_top = itemView.findViewById(R.id.ll_top);

        }
    }
}
