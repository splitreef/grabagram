package com.splitreef.grabagramplaystore.enums;

import androidx.annotation.NonNull;

public enum WeekDayEnum {

    Monday("Monday"),
    Tuesday("Tuesday"),
    Wednesday("Wednesday"),
    Thursday("Thursday"),
    Friday("Friday"),
    Saturday("Saturday"),
    Sunday("Sunday");

    String day;
    WeekDayEnum(String day) {
        this.day=day;
    }

    @NonNull
    @Override
    public String toString() {
        return day;
    }

    public boolean isEqual(String value) {
        return this.day.equals(value);
    }

}
