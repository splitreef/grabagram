package com.splitreef.grabagramplaystore.data.model.customer;

import java.io.Serializable;
import java.util.ArrayList;

public class GetPlacedOrdersResponse implements Serializable {

    private ArrayList<Order>orders;
    private String message;

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
