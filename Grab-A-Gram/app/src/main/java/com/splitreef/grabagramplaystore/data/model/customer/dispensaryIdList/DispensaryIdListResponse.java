package com.splitreef.grabagramplaystore.data.model.customer.dispensaryIdList;

import java.util.ArrayList;

public class DispensaryIdListResponse {

    private String status;
    private ArrayList<String>dispensaryId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getDispensaryId() {
        return dispensaryId;
    }

    public void setDispensaryId(ArrayList<String> dispensaryId) {
        this.dispensaryId = dispensaryId;
    }
}
