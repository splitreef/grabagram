package com.splitreef.grabagramplaystore.ui.driver.notifications;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.reflect.TypeToken;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.DeclineOrdersAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.orders.ItemsModel;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActiivytDeclineOrderBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersViewModel;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Created by  on 14-01-2021.
 */
public class DetailedNotificationOrderActivity extends BaseActivity implements View.OnClickListener {
    ActiivytDeclineOrderBinding binding;
    private RecyclerView recyclerView;
    private DeclineOrdersAdapter declineOrdersAdapter;
    private LinearLayoutManager linearLayoutManager;
    private PendingOrdersViewModel viewModel;
    private ArrayList<ItemsModel> itemsList = new ArrayList<>();
    private GrabAGramApplication application;
    private double latitude;
    private double longitude;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    ArrayList<OrdersModel> OrdersModelArrayList;
    ArrayList<DeclineOrders> DeclineOrdersList;
    String from = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActiivytDeclineOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        GPSTracker gpsTracker = new GPSTracker(this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);
        initViews();
    }

    private void initViews() {
        if (getIntent() != null) {
            from = getIntent().getStringExtra(AppConstant.FROM);
        }
        application = (GrabAGramApplication) Objects.requireNonNull(this).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory
                (Objects.requireNonNull(this).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);
        binding.imvBack.setOnClickListener(this);
        subscribeObservers();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finish();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowLoaderDialog = true;
        isFirst = true;
        if (from != null && from.equalsIgnoreCase(AppConstant.CANCEL_ORDER)) {

            viewModel.getDeclineOrders();

        } else if (from != null && from.equalsIgnoreCase(AppConstant.READY_FOR_PICKUP)) {

            viewModel.getUpcomingDeliveriesList(latitude, longitude, HomeFragment.miles, true);

        } else if (from != null && from.equalsIgnoreCase(AppConstant.ORDERS_NEAR_YOU)) {

            viewModel.getDeclineOrders();

        }

    }

    void subscribeObservers() {
        viewModel.observeGetDeclineList().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:
                            DeclineOrdersList = (ArrayList<DeclineOrders>) dataResource.data;
                            getPendingOrdersList();
                            binding.progressBar.setVisibility(View.GONE);

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.DECLINE_ORDERS_LIST, new ArrayList<String>());

                            break;

                    }
                }

            }
        });

        viewModel.observeGetPendingOrdersList().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                                // application.dialogManager.displayProgressDialog(getActivity(),"Loading...");
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:

                          /*  if (isShowLoaderDialog)
                            {
                                application.dialogManager.dismissProgressDialog();
                                dispensaryModelArrayList= (ArrayList<DispensaryModel>) dataResource.data;
                                setOrdersReadyNearBy(dispensaryModelArrayList);
                            }
                            else
                            {
                                if (dispensaryAdapter!=null)
                                {
                                    dispensaryAdapter.notifyDataSetChanged();
                                }
                                binding.progressBar.setVisibility(View.GONE);
                            }

                            if (dispensaryAdapter!=null)
                            {
                                PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,dispensaryAdapter.getPendingOrdersList());
                            }*/

                            //   application.dialogManager.dismissProgressDialog();
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            if (from != null && from.equalsIgnoreCase(AppConstant.CANCEL_ORDER)) {
                                setDeclineOrdersList(OrdersModelArrayList);
                            } else if (from != null && from.equalsIgnoreCase(AppConstant.ORDERS_NEAR_YOU)) {
                                setOrdersReadyNearBy(OrdersModelArrayList);

                            }

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            //    application.dialogManager.dismissProgressDialog();
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, new ArrayList<OrdersModel>());

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });

        viewModel.observeGetUpcomingDeliveriesList().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                                // application.dialogManager.displayProgressDialog(getActivity(),"Loading...");
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:

                          /*  if (isShowLoaderDialog)
                            {
                                application.dialogManager.dismissProgressDialog();
                                dispensaryModelArrayList= (ArrayList<DispensaryModel>) dataResource.data;
                                setUpcomingDeliveriesList(dispensaryModelArrayList);
                            }
                            else
                            {
                                if (dispensaryAdapter!=null)
                                {
                                    dispensaryAdapter.notifyDataSetChanged();
                                }
                                binding.progressBar.setVisibility(View.GONE);
                            }

                            if (dispensaryAdapter!=null)
                            {
                                PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,dispensaryAdapter.getDispensaryList());
                            }*/

                            //   application.dialogManager.dismissProgressDialog();
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);
                            setUpcomingDeliveriesList(OrdersModelArrayList);

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            //    application.dialogManager.dismissProgressDialog();
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });

    }

    public void getPendingOrdersList() {
        if (viewModel != null)
            viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);
    }

    /**
     * set decline order list
     *
     * @param pendingOrdersList
     */
    void setDeclineOrdersList(ArrayList<OrdersModel> pendingOrdersList) {
        List<OrdersModel> containsList = new ArrayList<>();
        if (pendingOrdersList.size() > 0) {
            if (DeclineOrdersList.size() > 0) {
                List<OrdersModel> list = new ArrayList<>();
                //list.addAll(OrdersModelArrayList);
                for (int i = 0; i < pendingOrdersList.size(); i++) {
                    for (int j = 0; j < DeclineOrdersList.size(); j++) {
                        if (DeclineOrdersList.get(j).getOrder_no().equalsIgnoreCase(pendingOrdersList.get(i).getId())) {
                            containsList.add(pendingOrdersList.get(i));
                            break;

                            /*for (int k = 0; k < list.size(); k++) {
                                if (pendingOrdersList.get(i).getId() == list.get(k).getId()) {
                                    list.add(pendingOrdersList.get(i));
                                    break;
                                }
                            }*/
                        }

                    }
                }

                OrdersModelArrayList.clear();
                OrdersModelArrayList.addAll(containsList);
            } else {
                OrdersModelArrayList.clear();
            }
        }


        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            double distance = new GetDistance().distance(latitude, longitude, OrdersModelArrayList.get(i).getDispensary_location().getLatitude(),
                    OrdersModelArrayList.get(i).getDispensary_location().getLongitude());
            OrdersModelArrayList.get(i).setDistance(distance);
            OrdersModelArrayList.get(i).setMyOrderNo(Integer.parseInt(OrdersModelArrayList.get(i).getOrder_no()));
        }

        PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);

        ArrayList<OrdersModel> ordersModels =
                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
                }.getType());


        Log.d("size=======", "" + ordersModels.size());

        if (OrdersModelArrayList.size() > 0) {
            declineOrdersAdapter = new DeclineOrdersAdapter(DetailedNotificationOrderActivity.this, OrdersModelArrayList);
            binding.recycleView.setAdapter(declineOrdersAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            //  showToast(getContext(),"Dispensary not found");
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);
        }

        binding.progressBar.setVisibility(View.GONE);

    }

    /**
     * set near by order's list
     *
     * @param pendingOrdersList
     */
    void setOrdersReadyNearBy(ArrayList<OrdersModel> pendingOrdersList) {
        List<OrdersModel> containsList = new ArrayList<>();
        if (pendingOrdersList.size() > 0) {
            if (DeclineOrdersList.size() > 0) {
                List<OrdersModel> list = new ArrayList<>();
                list.addAll(OrdersModelArrayList);
                for (int i = 0; i < pendingOrdersList.size(); i++) {
                    for (int j = 0; j < DeclineOrdersList.size(); j++) {
                        if (DeclineOrdersList.get(j).getOrder_no().equalsIgnoreCase(pendingOrdersList.get(i).getId())) {
                            containsList.add(pendingOrdersList.get(i));
                            for (int k = 0; k < list.size(); k++) {
                                if (pendingOrdersList.get(i).getId() == list.get(k).getId()) {
                                    list.remove(k);
                                    break;
                                }
                            }
                        }

                    }
                }

                OrdersModelArrayList.clear();
                OrdersModelArrayList.addAll(list);
            }
        }


        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            double distance = new GetDistance().distance(latitude, longitude, OrdersModelArrayList.get(i).getDispensary_location().getLatitude(),
                    OrdersModelArrayList.get(i).getDispensary_location().getLongitude());
            OrdersModelArrayList.get(i).setDistance(distance);
            OrdersModelArrayList.get(i).setMyOrderNo(Integer.parseInt(OrdersModelArrayList.get(i).getOrder_no()));
        }


        Collections.sort(OrdersModelArrayList, new Comparator<OrdersModel>() {
            @Override
            public int compare(OrdersModel o1, OrdersModel o2) {
                int value = 0;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    value = Double.compare(o2.getDistance(), o1.getDistance());
                }
                return value;
            }
        });

        if (OrdersModelArrayList.size() > 5) {
            List<OrdersModel> modelList = new ArrayList<>();
            modelList.add(OrdersModelArrayList.get(0));
            modelList.add(OrdersModelArrayList.get(1));
            modelList.add(OrdersModelArrayList.get(2));
            modelList.add(OrdersModelArrayList.get(3));
            modelList.add(OrdersModelArrayList.get(4));
            OrdersModelArrayList.clear();
            OrdersModelArrayList.addAll(modelList);
        }

        PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);

        ArrayList<OrdersModel> ordersModels =
                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
                }.getType());


        Log.d("size=======", "" + ordersModels.size());

        if (OrdersModelArrayList.size() > 0) {
            declineOrdersAdapter = new DeclineOrdersAdapter(DetailedNotificationOrderActivity.this, OrdersModelArrayList);
            binding.recycleView.setAdapter(declineOrdersAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            //  showToast(getContext(),"Dispensary not found");
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);
        }
        binding.progressBar.setVisibility(View.GONE);


    }

    /**
     * set upcoming order's list
     *
     * @param upcomingDeliveriesList
     */
    void setUpcomingDeliveriesList(ArrayList<OrdersModel> upcomingDeliveriesList) {
//        ArrayList<OrdersModel> ordersModels =
//                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
//                }.getType());
//
//
//        Log.d("size=======", "" + ordersModels.size());

        if (upcomingDeliveriesList.size() > 0) {
            declineOrdersAdapter = new DeclineOrdersAdapter(DetailedNotificationOrderActivity.this, upcomingDeliveriesList);
            binding.recycleView.setAdapter(declineOrdersAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            //  showToast(getContext(),"Dispensary not found");
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);

        }
        binding.progressBar.setVisibility(View.GONE);

    }


}
