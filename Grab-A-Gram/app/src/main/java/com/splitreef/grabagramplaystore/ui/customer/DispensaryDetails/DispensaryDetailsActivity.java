package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.tabs.TabLayout;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.DispensaryDetailsPagerAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityDispensaryDetailsBinding;
import com.splitreef.grabagramplaystore.ui.customer.cart.CartActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.PicassoManager;


public class DispensaryDetailsActivity extends BaseActivity {

    private ActivityDispensaryDetailsBinding binding;
    private DispensaryModel dispensaryModel;
    private Menu menu;
    TextView txtCartCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDispensaryDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();

    }

    @Override
    public boolean onSupportNavigateUp() {

        FragmentManager fm = getSupportFragmentManager();
        for (Fragment frag : fm.getFragments()) {
            if (frag.isVisible()) {
                FragmentManager childFm = frag.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                   // showToast("Again Press back button");
                    return true;
                }
            }


        }
       // super.onBackPressed();
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBadgeCount();


    }

    public void initView() {

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);


        if (getIntent() != null) {

            dispensaryModel = (DispensaryModel) getIntent().getParcelableExtra(AppConstant.DISPENSARY_MODEL);

            if (dispensaryModel != null) {

                PicassoManager.setImage(dispensaryModel.getProfile_image(), binding.dispensaryImage);
                PicassoManager.setImage(dispensaryModel.getProfile_image(), binding.imgBanner);

                binding.dispensaryName.setText(dispensaryModel.getBusniess_name());
                binding.dispensaryRating.setRating(dispensaryModel.getRating());

                DispensaryDetailsPagerAdapter adapter = new DispensaryDetailsPagerAdapter(DispensaryDetailsActivity.this, getSupportFragmentManager(), binding.tabLayout.getTabCount(), dispensaryModel);
                binding.viewPager.setAdapter(adapter);
                binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
                binding.viewPager.setOffscreenPageLimit(0);

                // For change fragment on click of tab
                binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        binding.viewPager.setCurrentItem(tab.getPosition());
                    }
                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }
                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }

        }

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        FragmentManager fm = getSupportFragmentManager();
        for (Fragment frag : fm.getFragments()) {
            if (frag.isVisible()) {
                FragmentManager childFm = frag.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                    return;
                }

            }

        }
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.cart_menu,menu);
         View cartView = menu.findItem(R.id.action_cart).getActionView();
         txtCartCount = cartView.findViewById(R.id.btn_cart_count);

        cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DispensaryDetailsActivity.this, CartActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);

            }
        });

        String badgeCount= PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
        Log.d("badgeCount==","==="+badgeCount);

        if (badgeCount!=null && (!badgeCount.equals("0")))
        {
            txtCartCount.setVisibility(View.VISIBLE);
            txtCartCount.setText(badgeCount+"");

        }
        else
        {
            txtCartCount.setVisibility(View.GONE);
        }


        return super.onCreateOptionsMenu(menu);
    }

    public void updateBadgeCount()
    {
        String badgeCount= PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
        Log.d("badgeCount==","==="+badgeCount);

        if (txtCartCount!=null)
        {
            if (badgeCount!=null && (!badgeCount.equals("0")))
            {
                txtCartCount.setVisibility(View.VISIBLE);
                txtCartCount.setText(badgeCount+"");

            }
            else
            {
                txtCartCount.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==R.id.action_cart)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
