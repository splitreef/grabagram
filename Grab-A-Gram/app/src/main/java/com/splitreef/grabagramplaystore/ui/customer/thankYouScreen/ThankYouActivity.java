package com.splitreef.grabagramplaystore.ui.customer.thankYouScreen;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import androidx.annotation.Nullable;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.OrderResponse;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityThankYouBinding;
import com.splitreef.grabagramplaystore.ui.customer.dashboard.DashboardActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;


public class ThankYouActivity extends BaseActivity {

    private ActivityThankYouBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityThankYouBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();

    }

    public void initView()
    {
        UserModel userModel=PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER,UserModel.class);

        SpannableString content = new SpannableString(userModel.getEmail());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        binding.txtEmail.setText(content);


        if (getIntent()!=null)
        {
           OrderResponse orderResponse = (OrderResponse) getIntent().getSerializableExtra(AppConstant.ORDER_RESPONSE);

           binding.txtOrderNo.setText(orderResponse.getOrderNo());
           binding.txtConfirmationCode.setText(orderResponse.getConfirmationCode());
        }


        binding.btnBackToHome.setOnClickListener(view -> {

            Intent intent = new Intent(ThankYouActivity.this, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
            finish();
        });


    }

    @Override
    public void onBackPressed() {
        showToast("Please click on Back To home button");
    }

}
