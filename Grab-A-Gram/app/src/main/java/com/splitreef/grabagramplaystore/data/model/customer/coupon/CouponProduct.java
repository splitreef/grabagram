package com.splitreef.grabagramplaystore.data.model.customer.coupon;

import java.io.Serializable;

public class CouponProduct implements Serializable {

    private String product_id;
    private String product_name;
    private String product_sku;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_sku() {
        return product_sku;
    }

    public void setProduct_sku(String product_sku) {
        this.product_sku = product_sku;
    }
}
