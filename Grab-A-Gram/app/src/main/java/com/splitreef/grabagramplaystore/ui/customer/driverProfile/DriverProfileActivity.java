package com.splitreef.grabagramplaystore.ui.customer.driverProfile;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverAccount;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfile;
import com.splitreef.grabagramplaystore.databinding.ActivityDriverProfileBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DriverProfileActivity extends BaseActivity {
    private ActivityDriverProfileBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityDriverProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }


    public void initView()
    {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        if (getIntent()!=null)
        {
            DriverProfile driverProfile= (DriverProfile) getIntent().getSerializableExtra(AppConstant.DRIVER_PROFILE_RESPONSE);

            if (driverProfile!=null)
            {
                updateUi(driverProfile);
            }
        }

    }


    @SuppressLint("SetTextI18n")
    public void updateUi(DriverProfile driverProfile)
    {
        if (driverProfile.getProfile_image()!=null)
        {
            PicassoManager.setImage(driverProfile.getProfile_image(),binding.imgDriverProfile);
        }

        if (driverProfile!=null && driverProfile.getAccount()!=null)
        {
            DriverAccount driverAccount=driverProfile.getAccount();
            binding.txtDriverName.setText(""+driverAccount.getFirst_name()+" "+driverAccount.getLast_name());
        }

        Log.d("lat===","=="+driverProfile.getLat());
        Log.d("lng===","=="+driverProfile.getLng());
        binding.txtDriverAddress.setText(getAddress(driverProfile.getLat(),driverProfile.getLng()));

        binding.txtManufactureCompany.setText(driverProfile.getDocuments().getManufacture_model()+" "+driverProfile.getDocuments().getManufacture_company());
    }


    private String getAddress(double latitude, double longitude) {
        StringBuilder result_loc = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(DriverProfileActivity.this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                result_loc.append(addresses.get(0).getFeatureName()).append(", ").append(addresses.get(0).getLocality()).append(", ").append(addresses.get(0).getAdminArea()).append(", ").append(addresses.get(0).getCountryName());

                Log.e("result_loc", ""+result_loc);
            }
        } catch (IOException e) {
           Log.e("Exception==",""+e.getMessage());
        }
        return result_loc.toString();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }
}
