package com.splitreef.grabagramplaystore.data.repository;

import com.google.firebase.auth.FirebaseUser;
import com.splitreef.grabagramplaystore.data.model.PaymentMethod;
import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.AddressRequestParams;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.OrderRequest;
import com.splitreef.grabagramplaystore.data.model.customer.OrderResponse;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponModel;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.data.model.customer.dispensaryIdList.DispensaryIdListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distance.DispensaryDistanceResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverLocation;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfileResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRating;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRatingResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverTokens.DriverTokens;
import com.splitreef.grabagramplaystore.data.model.customer.help.HelpListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategoryResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productReviewResponse.ProductReviewResponse;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.DriverLocationStatusModel;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.data.model.driver.driverRating.MyDriverRating;
import com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount.MyDriverAccountResponse;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.dataSource.remote.FirebaseDataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class FirebaseRepository {

    private FirebaseDataSource firebaseDataSource;

    public FirebaseRepository(FirebaseDataSource firebaseDataSource) {
        this.firebaseDataSource = firebaseDataSource;
    }

    // For getting current user
    public FirebaseUser getCurrentUser() {
        return firebaseDataSource.getCurrentUser();
    }

    // For getting user id
    public String getCurrentUid() {
        return firebaseDataSource.getCurrentUid();
    }

    // For create new user
    public Completable registerUser(String email, String password) {
        return firebaseDataSource.registerUser(email, password);
    }

    public Single<Boolean> checkUserExistsOrNot(String email) {
        return firebaseDataSource.checkUserExistsOrNot(email);
    }

    // For customer doc photos
    public Single<String> uploadCustomerDocPhoto(byte[] bytes, String imageName) {
        return firebaseDataSource.uploadCustomerDocPhoto(bytes, imageName);
    }

    // For add customer data
    public Completable addCustomerData(CustomerRegisterRequest customerRegisterRequest) {
        return firebaseDataSource.addCustomerData(customerRegisterRequest);
    }

    // For get customer data
    public Single<MyAccountResponse> getCustomerMyAccountData() {
        return firebaseDataSource.getCustomerMyAccountData();
    }

    // For update customer data
    public Completable updateCustomerMyAccountData(CustomerRegisterRequest customerRegisterRequest, MyAccountResponse myAccountResponse) {
        return firebaseDataSource.updateCustomerMyAccountData(customerRegisterRequest, myAccountResponse);
    }

    // For login user

    public Single<UserModel> loginUser(String userName, String password) {
        return firebaseDataSource.loginUser(userName, password);
    }

    public void userLogout() {
        firebaseDataSource.userLogout();
    }

    public Single<ArrayList<DispensaryModel>> getDispensaryList(double latitude, double longitude, double distance) {
        return firebaseDataSource.getDispensaryList(latitude, longitude, distance);
    }

    // For getting deal list
    public Single<ArrayList<DealModel>> getDealList() {
        return firebaseDataSource.getDealList();
    }

    // For getting deal list by userId
    public Single<ArrayList<DealModel>> getDealListById(String userId) {
        return firebaseDataSource.getDealListById(userId);
    }

    // For getting menu list
    public Single<ArrayList<ProductListResponse>> getMenuList(String dispensaryId) {
        return firebaseDataSource.getMenuList(dispensaryId);
    }

    // For getting dispensary review  list
    public Single<ArrayList<DispensaryReviewModel>> getDispensaryReviewList(String dispensaryId) {
        return firebaseDataSource.getDispensaryReviewList(dispensaryId);
    }

    public Single<ProductReviewResponse> getProductReview(String orderId, String productId) {
        return firebaseDataSource.getProductReview(orderId, productId);
    }

    public Single<String> saveProductReview(ProductReviewModel productReviewModel) {
        return firebaseDataSource.saveProductReview(productReviewModel);
    }

    public Single<String> saveDispensaryReview(DispensaryReviewModel dispensaryReviewModel, float totalRating) {
        return firebaseDataSource.saveDispensaryReview(dispensaryReviewModel, totalRating);
    }

    // For getting badge count
    public Single<String> getBadgeCount() {
        return firebaseDataSource.getBadgeCount();
    }

    // For getting cart list
    public Single<GetProductCartResponse> getProductCartList() {
        return firebaseDataSource.getProductCartList();
    }

    // For getting coupon prodducts
    public Single<CouponResponse> getCouponProducts(String couponCode) {
        return firebaseDataSource.getCouponProducts(couponCode);
    }

    // For update coupon code
    public Completable updateCouponCode(CouponModel couponModel) {
        return firebaseDataSource.updateCouponCode(couponModel);
    }

    public Completable updateCouponCodeLimit(long limitUsed,String couponId) {
        return firebaseDataSource.updateCouponCodeLimit(limitUsed, couponId);
    }

    // For update coupon code
    public Completable cancelCouponCode(String couponId) {
        return firebaseDataSource.cancelCouponCode(couponId);
    }


    // For getting wish list
    public Single<DriverTokens> getDriversTokens(double latitude, double longitude, double distance) {
        return firebaseDataSource.getDriversTokens(latitude, longitude, distance);
    }


    // For getting wish list
    public Single<GetProductCartResponse> getProductWishList() {
        return firebaseDataSource.getProductWishList();
    }

    // For getting help list
    public Single<HelpListResponse> getHelpList() {
        return firebaseDataSource.getHelpList();
    }

    // get dispensary data
    public Single<DispensaryModel> getDispensary(String dispensaryId) {
        return firebaseDataSource.getDispensary(dispensaryId);
    }

    // for get product category list
    public Single<ProductCategoryResponse> getCategoryList() {
        return firebaseDataSource.getCategoryList();
    }

    // for get dispensary user id list
    public Single<DispensaryIdListResponse> getDispensaryIdList(ArrayList<String> categoryIdList) {
        return firebaseDataSource.getDispensaryIdList(categoryIdList);
    }

    // for distance list
    public Single<DispensaryDistanceResponse> getDistanceList() {
        return firebaseDataSource.getDistanceList();
    }


    // For update Cart product
    public Single<OrderResponse> placeOrder(OrderRequest orderRequest) {
        return firebaseDataSource.placeOrder(orderRequest);
    }


    // For update Cart product
    public Completable updateOrderIsSendNotification(boolean isSend, String orderId) {
        return firebaseDataSource.updateOrderIsSendNotification(isSend, orderId);
    }

    public Completable updatePaymentStatus(boolean paymentSuccess, PaymentMethod paymentMethod, String orderId) {
        return firebaseDataSource.updatePaymentStatus(paymentSuccess, paymentMethod, orderId);
    }


    // For get placed orders
    public Single<GetPlacedOrdersResponse> getPlacedOrders(String status) {
        return firebaseDataSource.getPlacedOrders(status);
    }

    // For get order details by order id
    public Single<Order> getOrderDetailsByOrderId(String orderId) {
        return firebaseDataSource.getOrderDetailsByOrderId(orderId);
    }

    // For get track orders
    public Single<GetPlacedOrdersResponse> getTrackOrders() {
        return firebaseDataSource.getTrackOrders();
    }

    // For get Driver profile data
    public Single<DriverProfileResponse> getDriverProfileData(String driverId) {
        return firebaseDataSource.getDriverProfileData(driverId);
    }

    // For get Driver rating data
    public Single<DriverRatingResponse> getDriverRatingData(String driverId, String orderId) {
        return firebaseDataSource.getDriverRatingData(driverId, orderId);
    }

    // For get Driver rating data
    public Single<String> saveDriverRating(DriverRating driverRating, String driverId) {
        return firebaseDataSource.saveDriverRating(driverRating, driverId);
    }

    // For get Driver location
    public Flowable<DriverLocation> getDriverLocation(String driverId, String orderId) {
        return firebaseDataSource.getDriverLocation(driverId, orderId);
    }


    // For get past orders
    public Single<GetPlacedOrdersResponse> getPastOrders() {
        return firebaseDataSource.getPastOrders();
    }

    // For delete order
    public Single<String> updateOrderStatus(String id, String status) {
        return firebaseDataSource.updateOrderStatus(id, status);
    }


    // For update Cart product
    public Completable updateCartProduct(String id, int quantity, double totalPrice) {
        return firebaseDataSource.updateCartProduct(id, quantity, totalPrice);
    }

    // For delete Cart product
    public Completable deleteCartProduct(String id,boolean isLastItem) {
        return firebaseDataSource.deleteCartProduct(id,isLastItem);
    }

    // For delete wish list product
    public Completable deleteWishListProduct(String id) {
        return firebaseDataSource.deleteWishListProduct(id);
    }

    // For getting address list
    public Single<AddressList> getAddressList() {
        return firebaseDataSource.getAddressList();
    }

    // Add new address
    public Completable addCustomerNewAddress(AddressRequestParams addressRequestParams) {
        return firebaseDataSource.addCustomerNewAddress(addressRequestParams);
    }

    // Add new address
    public Completable updateCustomerAddress(AddressRequestParams addressRequestParams, UserAddressModel userAddressModel) {
        return firebaseDataSource.updateCustomerAddress(addressRequestParams, userAddressModel);
    }

    // delete  address
    public Completable deleteCustomerAddress(UserAddressModel userAddressModel, boolean isPersonAddress) {
        return firebaseDataSource.deleteCustomerAddress(userAddressModel, isPersonAddress);
    }


    public Completable updateIsPersonalAddress(boolean isPersonAddress) {
        return firebaseDataSource.updateIsPersonalAddress(isPersonAddress);
    }


    public Single<String> addProductToCart(ProductListResponse productListResponse) {
        return firebaseDataSource.addProductToCart(productListResponse);
    }

    public Single<String> addProductToWishList(ProductListResponse productListResponse) {
        return firebaseDataSource.addProductToWishList(productListResponse);
    }

    // For create new user for driver
    public Completable registerDriverUser(String email, String password) {
        return firebaseDataSource.registerDriverUser(email, password);
    }

    // For driver doc photos
    public Single<String> uploadDriverDocPhoto(byte[] bytes, String imageName) {
        return firebaseDataSource.uploadDriverDocPhoto(bytes, imageName);
    }

    // For add driver data
    public Completable addDriverData(DriverRegistrationRequest driverRegistrationRequest) {
        return firebaseDataSource.addDriverData(driverRegistrationRequest);
    }

    //get pending orders list
    public Single<List<OrdersModel>> getPendingOrdersList(double latitude, double longitude, double distance, boolean isFirst) {
        return firebaseDataSource.getPendingOrdersList(latitude, longitude, distance, isFirst);
    }

    //get upcoming deliveries list
    public Single<List<OrdersModel>> getUpcomingDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        return firebaseDataSource.getUpcomingDeliveriesList(latitude, longitude, distance, isFirst);
    }

    //get upcoming deliveries list
    public Completable updateOrderToAccept(String id, String status) {
        return firebaseDataSource.updateOrderToAccept(id, status);
    }//get upcoming deliveries list

    public Completable updateOrderToScheduled(String id, String status) {
        return firebaseDataSource.updateOrderToScheduled(id, status);
    }

    //get upcoming deliveries list
    public Completable updateOrderToDecline(String id) {
        return firebaseDataSource.updateOrderToDecline(id);
    }

    //update driver location status online
    public Completable updateActiveStatus(DriverLocationStatusModel driverLocationStatusModel) {
        return firebaseDataSource.updateActiveStatus(driverLocationStatusModel);
    }

    public Completable updateDriverOnlineOfflineStatus(String driverStatus) {
        return firebaseDataSource.updateDriverOnlineOfflineStatus(driverStatus);
    }

    //update driver location status offline
    public Completable updateInActiveStatus(String status) {
        return firebaseDataSource.updateInActiveStatus(status);
    }

    //get upcoming deliveries list
    public Single<List<DeclineOrders>> getDeclineOrders() {
        return firebaseDataSource.getDeclineOrders();
    }

    //get upcoming deliveries list
    public Single<List<OrdersModel>> getPastDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        return firebaseDataSource.getPastDeliveriesList(latitude, longitude, distance, isFirst);
    }

    //get Order Details
    public Single<OrdersModel> getOrderDetails(String id) {
        return firebaseDataSource.getOrderDetails(id);
    }

    //get User Details
    public Single<UserModel> getUserDetails(String userID) {
        return firebaseDataSource.getUserDetails(userID);
    }

    //get driver data
    public Single<MyDriverAccountResponse> getDriverMyAccountInfo() {
        return firebaseDataSource.getDriverMyAccountInfo();
    }

    // For update customer data
    public Completable updateDrverMyAccountInfo(MyDriverAccountResponse myDriverAccountResponse) {
        return firebaseDataSource.updateDrverMyAccountInfo(myDriverAccountResponse);
    }

    //get upcoming deliveries list
    public Single<List<OrdersModel>> getNewDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        return firebaseDataSource.getNewDeliveriesList(latitude, longitude, distance, isFirst);
    }

    //update driver location
    public Completable updateDriverLocation(double latitude, double longitude, String orderID) {
        return firebaseDataSource.updateDriverLocation(latitude, longitude, orderID);
    }


    //get driver status
    public Single<DriverLocationStatusModel> getDriverStatus() {
        return firebaseDataSource.getDriverStatus();
    }

    //get Order status
    public Single<String> getOrderStatus(String id) {
        return firebaseDataSource.getOrderStatus(id);
    }

    public Completable forgetPassword(String emailAddress) {
        return firebaseDataSource.forgetPassword(emailAddress);
    }

    //get vehicle model  list
    public Single<List<VehicleModel>> getVehicleModelList() {
        return firebaseDataSource.getVehicleModelList();
    }

    //get vehicle company  list
    public Single<List<VehicleCompany>> getVehicleCompanyList() {
        return firebaseDataSource.getVehicleCompanyList();
    }



    //get driver rating list
    public Single<List<MyDriverRating>> getDriverRatingList() {
        return firebaseDataSource.getDriverRatingList();
    }

}
