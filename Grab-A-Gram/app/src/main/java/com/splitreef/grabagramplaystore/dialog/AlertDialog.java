package com.splitreef.grabagramplaystore.dialog;

import android.content.Context;
import android.content.DialogInterface;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;


public abstract class AlertDialog {


    public abstract void onPositiveButtonClick();

    public void showAlertDialog(Context context, String title, String message, String btnPosName, String btnNegName) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnPosName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onPositiveButtonClick();
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton(btnNegName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        builder.show();

    }


}
