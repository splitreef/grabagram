package com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount;

import java.io.Serializable;

/**
 * Created by  on 05-01-2021.
 */
public class DriverDocumentImages implements Serializable {
    //document_images
    String car_insurance_policy;
    String car_registration;
    String caregiver_certificate;
    String driving_license;
    String photo;

    public String getCar_insurance_policy() {
        return car_insurance_policy;
    }

    public void setCar_insurance_policy(String car_insurance_policy) {
        this.car_insurance_policy = car_insurance_policy;
    }

    public String getCaregiver_certificate() {
        return caregiver_certificate;
    }

    public void setCaregiver_certificate(String caregiver_certificate) {
        this.caregiver_certificate = caregiver_certificate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDriving_license() {
        return driving_license;
    }

    public void setDriving_license(String driving_license) {
        this.driving_license = driving_license;
    }

    public String getCar_registration() {
        return car_registration;
    }

    public void setCar_registration(String car_registration) {
        this.car_registration = car_registration;
    }
}
