package com.splitreef.grabagramplaystore.data.model.driver.vehicle;

import java.io.Serializable;

/**
 * Created by  on 22-01-2021.
 */
public class VehicleCompany implements Serializable {
    private String company_name;
    //private String created_at;
    private String id;
   // private String updated_at;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

  /*  public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
*/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public String getUpdated_at() {
//        return updated_at;
//    }
//
//    public void setUpdated_at(String updated_at) {
//        this.updated_at = updated_at;
//    }
}
