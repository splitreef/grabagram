package com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount;

import java.io.Serializable;

/**
 * Created by  on 05-01-2021.
 */
public class MyDriverAccountResponse implements Serializable {
    private String status;
    private String about_us;
    private DriverAccount account;
    private DriverAddress address;
    private long created_at;
    private DriverDocumentImages document_images;
    private DriverDocuments documents;
    private DriverPhones phones;
    private String profile_image;
    private long updated_at;
    private String user_id;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public DriverAccount getAccount() {
        return account;
    }

    public void setAccount(DriverAccount account) {
        this.account = account;
    }

    public DriverAddress getAddress() {
        return address;
    }

    public void setAddress(DriverAddress address) {
        this.address = address;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public DriverDocumentImages getDocument_images() {
        return document_images;
    }

    public void setDocument_images(DriverDocumentImages document_images) {
        this.document_images = document_images;
    }

    public DriverDocuments getDocuments() {
        return documents;
    }

    public void setDocuments(DriverDocuments documents) {
        this.documents = documents;
    }

    public DriverPhones getPhones() {
        return phones;
    }

    public void setPhones(DriverPhones phones) {
        this.phones = phones;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
