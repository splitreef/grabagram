package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textview.MaterialTextView;
import com.google.common.reflect.TypeToken;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dialog.CustomDilalog;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class PendingOrdersMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    ArrayList<OrdersModel> OrdersModelArrayList;
    private PendingOrdersViewModel viewModel;
    private GrabAGramApplication application;
    GoogleMap googleMap;
    double latitude;
    double longitude;
    SupportMapFragment mapFragment;
    ArrayList<DeclineOrders> DeclineOrdersList;
    OrdersModel tempOrdersModel = new OrdersModel();
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);

        subscribeObservers();

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        /// viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        initView();
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        View view = inflater.inflate(R.layout.fragment_pending_orders_map, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

     /*   if (mapFragment != null) {
            mapFragment.getMapAsync(DispensaryMapsFragment.this);
        }*/

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        // OrdersModelArrayList = PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
        // }.getType());
        viewModel.getDeclineOrders();
        // viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);

        /*if (OrdersModelArrayList != null && OrdersModelArrayList.size() > 0) {
            if (mapFragment != null) {
                mapFragment.getMapAsync(PendingOrdersMapFragment.this);
            }
        }*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(this);

        if (OrdersModelArrayList != null && OrdersModelArrayList.size() > 0) {

            Log.d("sizeinsidemap=", "" + OrdersModelArrayList.size());
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < OrdersModelArrayList.size(); i++) {

                OrdersModel model = OrdersModelArrayList.get(i);
                Marker marker = createMarker(model.getDispensary_location().getLatitude(), model.getDispensary_location().getLongitude(), model.getBusniess_name(), model.getDispensary_data().getAbout_us(), R.mipmap.location_map);
                marker.setTag(i);
                builder.include(marker.getPosition());

                LatLng latLng = new LatLng(model.getDispensary_location().getLatitude(), model.getDispensary_location().getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                // googleMap.addMarker(marker);
                //  createMarker(markersArray.get(i).getLatitude(), markersArray.get(i).getLongitude(), markersArray.get(i).getTitle(), markersArray.get(i).getSnippet(), markersArray.get(i).getIconResID());
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            LatLngBounds bounds = builder.build();
            int padding = 25; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            //  googleMap.moveCamera(cu);
            googleMap.animateCamera(cu);

        } else {
            Log.d("sizeinsidemap=", "0");
        }
    }


    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                //  .anchor(0.5f, 0.5f)
                //  .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }


    void subscribeObservers() {
        viewModel.observeGetPendingOrdersList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            setPendingOrdersList(OrdersModelArrayList);
                            application.dialogManager.dismissProgressDialog();

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            break;

                    }
                }

            }
        });

        viewModel.observeUpdateOrderToAccept().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            //getPendingOrdersList();
                            viewModel.getDeclineOrders(); // application.dialogManager.displayProgressDialog(getActivity(),"Registration completed successfully");
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeGetDeclineList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                         //   application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            DeclineOrdersList = (ArrayList<DeclineOrders>) dataResource.data;
                         //   application.dialogManager.dismissProgressDialog();
                            getPendingOrdersList();
                            break;
                        case ERROR:
                           // application.dialogManager.dismissProgressDialog();
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.DECLINE_ORDERS_LIST, new ArrayList<String>());

                            break;

                    }
                }

            }
        });

        viewModel.observeUpdateOrderToDecline().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            //getPendingOrdersList();
                            viewModel.getDeclineOrders();
                            // application.dialogManager.displayProgressDialog(getActivity(),"Registration completed successfully");
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeGetOrderStatsu().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getContext(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String orderStatus = (String) dataResource.data;
                            Log.d("orderStatus", "" + orderStatus);
                            if (orderStatus != null && !orderStatus.equalsIgnoreCase("")) {
                                if (orderStatus.equalsIgnoreCase(AppConstant.SCHEDULED)) {
                                    viewModel.updateOrderToAccept(tempOrdersModel.getId(), "ACCEPT");
                                } else {
                                    new SimpleDialog() {
                                        @Override
                                        public void onPositiveButtonClick() {
                                            // onResume();
                                        }
                                    }.showSimpleDialog(getContext(), "Message", getResources().getString(R.string.order_already_accepted_by_another_driver), "OK");

                                }
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });


    }

    void setPendingOrdersList(ArrayList<OrdersModel> pendingOrdersList) {
        List<OrdersModel> containsList = new ArrayList<>();
        if (pendingOrdersList.size() > 0) {
            if (DeclineOrdersList.size() > 0) {
                List<OrdersModel> list = new ArrayList<>();
                list.addAll(OrdersModelArrayList);
                for (int i = 0; i < pendingOrdersList.size(); i++) {
                    for (int j = 0; j < DeclineOrdersList.size(); j++) {
                        if (DeclineOrdersList.get(j).getOrder_no().equalsIgnoreCase(pendingOrdersList.get(i).getId())) {
                            containsList.add(pendingOrdersList.get(i));
                            for (int k = 0; k < list.size(); k++) {
                                if (pendingOrdersList.get(i).getId() == list.get(k).getId()) {
                                    list.remove(k);
                                    break;
                                }
                            }
                        }

                    }
                }

                OrdersModelArrayList.clear();
                OrdersModelArrayList.addAll(list);
            }
        }


        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            double distance = new GetDistance().distance(latitude, longitude, OrdersModelArrayList.get(i).getDispensary_location().getLatitude(),
                    OrdersModelArrayList.get(i).getDispensary_location().getLongitude());
            OrdersModelArrayList.get(i).setDistance(distance);
            OrdersModelArrayList.get(i).setMyOrderNo(Integer.parseInt(OrdersModelArrayList.get(i).getOrder_no()));
        }

        PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);

        ArrayList<OrdersModel> ordersModels =
                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
                }.getType());


        Log.d("size=======", "" + ordersModels.size());

        if (OrdersModelArrayList != null && OrdersModelArrayList.size() > 0) {
            if (mapFragment != null) {
                mapFragment.getMapAsync(PendingOrdersMapFragment.this);
            }
        }


    }


    public void getPendingOrdersList() {
        if (viewModel != null)
            viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getSnippet() == null) {
            googleMap.moveCamera(CameraUpdateFactory.zoomIn());
            return true;
        }
        //    marker.showInfoWindow();
        final OrdersModel ordersModel = OrdersModelArrayList.get((Integer) marker.getTag());
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //d.setTitle("Select");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.layout_pending_order_pin_popup);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        AppCompatImageView close = dialog.findViewById(R.id.close);
        AppCompatImageView dispensaryImage = dialog.findViewById(R.id.img_dispensary);
        MaterialTextView dispensaryName = dialog.findViewById(R.id.txt_dispensary_name);
        MaterialTextView txt_address = dialog.findViewById(R.id.txt_address);
        AppCompatRatingBar dispensaryRating = dialog.findViewById(R.id.dispensary_rating);
        MaterialTextView milesAway = dialog.findViewById(R.id.txt_map);
        MaterialTextView tvAccept = dialog.findViewById(R.id.tv_accept);
        RelativeLayout relAccept = dialog.findViewById(R.id.rel_accept);
        RelativeLayout relDecline = dialog.findViewById(R.id.rel_decline);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        relAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempOrdersModel = ordersModel;
                dialog.dismiss();
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        viewModel.getOrderStatus(ordersModel.getId());
                        // viewModel.updateOrderToAccept(tempOrderDetails.getId(), "ACCEPT");

                    }
                }.showCustomDialog(getContext(), getResources().getString(R.string.confirm_accept), getResources().getString(R.string.are_you_sure_you_want_to_accept_this_order), "Yes", "No");


            }
        });
        relDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        viewModel.updateOrderToDecline(ordersModel.getId());

                    }
                }.showCustomDialog(getContext(), getResources().getString(R.string.order_decline), getResources().getString(R.string.are_you_sure_want_to_decline_of_this_order), "Yes", "No");


            }
        });

        PicassoManager.setImage(ordersModel.getDispensary_data().getProfile_image(), dispensaryImage);
        dispensaryName.setText(ordersModel.getBusniess_name());
        txt_address.setText(ordersModel.getDispensary_data().getAddress().getStreet1());
        double distance = new GetDistance().distance(latitude, longitude, ordersModel.getDispensary_location().getLatitude(), ordersModel.getDispensary_location().getLongitude());
        milesAway.setText(String.format("%.2f %s", distance, "miles away"));

        dialog.show();
        return true;
    }

}