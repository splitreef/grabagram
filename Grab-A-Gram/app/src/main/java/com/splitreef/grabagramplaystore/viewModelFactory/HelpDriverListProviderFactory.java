package com.splitreef.grabagramplaystore.viewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.driver.help.HelpDriverListViewModel;

/**
 * Created by  on 08-02-2021.
 */
public class HelpDriverListProviderFactory implements ViewModelProvider.Factory {
    private final FirebaseRepository firebaseRepository;


    public HelpDriverListProviderFactory(FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(HelpDriverListViewModel.class))
        {
            return  (T) new HelpDriverListViewModel(firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}






