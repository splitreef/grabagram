package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.DispensaryAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentDispensaryListBinding;
import com.splitreef.grabagramplaystore.enums.DispensaryComparator;
import com.splitreef.grabagramplaystore.ui.customer.cart.CartActivity;
import com.splitreef.grabagramplaystore.ui.customer.filter.CustomerFilterActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryListViewModelProviderFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class DispensaryListFragment extends BaseFragment implements SearchView.OnQueryTextListener{
    FragmentDispensaryListBinding binding;
    private DispensaryListViewModel viewModel;
    private GrabAGramApplication application;
    private int currentItem ,totalItemCount,scrollOutItems;
    private boolean isScrolling;
    private LinearLayoutManager linearLayoutManager;
    private DispensaryAdapter dispensaryAdapter;
    private boolean isShowLoaderDialog=true;
    private boolean isFirst=true;
    ArrayList<DispensaryModel> dispensaryModelArrayList;
    double latitude;
    double longitude;
    private Context context;
    private boolean isSorting=false;
    private String sortingType="";
    TextView txtCartCount;
    private ArrayList<DispensaryModel>dispensaryList;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding= FragmentDispensaryListBinding.inflate(inflater,container,false);
        initView();
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        GPSTracker gpsTracker=new GPSTracker(getActivity());
        latitude=gpsTracker.getLatitude();
        longitude=gpsTracker.getLongitude();

        Log.d("latitude=",""+latitude);
        Log.d("longitude=",""+longitude);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        subscribeObservers();
        getDispensaryList();
        isShowLoaderDialog=true;
        isFirst=true;
        ((DispensaryListActivity)context).setToolbarTitle("Dispensary List");
        updateBadgeCount();
    }


    private void getDispensaryList()
    {
        boolean isDistanceFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);
        if (isDistanceFilter)
        {
            String distanceIDStr=PreferenceManger.getPreferenceManger().getString(PrefKeys.DISTANCE_ID);
            int distance= Integer.parseInt(distanceIDStr);
            viewModel.getDispensaryList(latitude,longitude,distance);
        }
        else
        {
            viewModel.getDispensaryList(latitude,longitude,AppConstant.DEFAULT_DISTANCE);
        }
    }

    public void  initView()
    {

        application=(GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel=new ViewModelProvider(getViewModelStore(), new DispensaryListViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(),application.firebaseRepository)).get(DispensaryListViewModel.class);


        linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        binding.recycleView.setLayoutManager(linearLayoutManager);

      /*  binding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState== AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem= linearLayoutManager.getChildCount();
                totalItemCount= linearLayoutManager.getItemCount();
                scrollOutItems=linearLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItem+ scrollOutItems== totalItemCount))
                {
                    isScrolling=false;
                    isShowLoaderDialog=false;
                    isFirst=false;
                    viewModel.getDispensaryList(latitude,longitude,40,false,isSorting,sortingType);

                }
            }
        });*/



        DispensaryHomeFragment.btnSort.setOnClickListener(view -> showSortDialog());
        DispensaryHomeFragment.btnFilter.setOnClickListener(view -> startActivity(new Intent(context, CustomerFilterActivity.class)));

    }



    public  void showSortDialog() {
        try {
            BottomSheetDialog sortOption = new BottomSheetDialog(context,R.style.CustomDialog);
            View sheetView = getLayoutInflater().inflate(R.layout.layout_sort, null);

           MaterialTextView txtCancel = sheetView.findViewById(R.id.txt_cancel);
           LinearLayout llAToZ = sheetView.findViewById(R.id.ll_a_to_z);
           LinearLayout llZToA = sheetView.findViewById(R.id.ll_z_to_a);
           LinearLayout llDisMaxMin = sheetView.findViewById(R.id.ll_dis_max_min);
           LinearLayout llDisMinMax = sheetView.findViewById(R.id.ll_dis_min_max);
           LinearLayout llRatingHighLow = sheetView.findViewById(R.id.ll_rating_high_low);
           LinearLayout llRatingLowHigh = sheetView.findViewById(R.id.ll_rating_low_high);

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();


            int height = displayMetrics.heightPixels;

            int maxHeight = (int) (height*0.88);

            sortOption.setDismissWithAnimation(true);
            sortOption.getBehavior().setPeekHeight(maxHeight);
            sortOption.setContentView(sheetView);

            txtCancel.setOnClickListener(view -> sortOption.cancel());
            llAToZ.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.ALPHABETICALLY_A_Z_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });

            llZToA.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.ALPHABETICALLY_Z_A_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });

            llDisMaxMin.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.DISTANCE_MAX_MIN_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });


            llDisMinMax.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.DISTANCE_MIN_MAX_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });

            llRatingHighLow.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.RATING_HIGH_LOW_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });

            llRatingLowHigh.setOnClickListener(view -> {
                isSorting=true;
                isFirst=true;
                sortingType= AppConstant.RATING_LOW_HIGH_SORTING_TYPE;
                getDispensaryList();
                sortOption.cancel();

            });

            txtCancel.setOnClickListener(view -> sortOption.cancel());


            sortOption.show();

        } catch (Exception e) {
            Log.e("showSortDialog_error",e.getMessage());
        }

    }

    void subscribeObservers()
    {
        viewModel.observeGetDispensaryList().observe((LifecycleOwner) context, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource!=null)
                {
                    switch (dataResource.status)
                    {
                        case LOADING:
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:
                            dispensaryModelArrayList= (ArrayList<DispensaryModel>) dataResource.data;
                            setDispensaryList(dispensaryModelArrayList);
                            binding.progressBar.setVisibility(View.GONE);
                            break;
                        case ERROR:
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);
                            break;
                    }
                }

            }
        });

    }


   void setDispensaryList(ArrayList<DispensaryModel>dispensaryList)
    {
        if (dispensaryList.size() > 0)
        {
            if (isSorting)
            {
                if (sortingType.equals(AppConstant.ALPHABETICALLY_A_Z_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.A_TO_Z_SORT));
                }
                else if (sortingType.equals(AppConstant.ALPHABETICALLY_Z_A_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.A_TO_Z_SORT)));
                }
               else if (sortingType.equals(AppConstant.DISTANCE_MIN_MAX_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.DIS_MIN_TO_MAX_SORT));
                }
                else if (sortingType.equals(AppConstant.DISTANCE_MAX_MIN_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.DIS_MIN_TO_MAX_SORT)));
                }
                else if (sortingType.equals(AppConstant.RATING_LOW_HIGH_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.RATING_LOW_HIGH_SORT));
                }
                else if (sortingType.equals(AppConstant.RATING_HIGH_LOW_SORTING_TYPE))
                {
                    Collections.sort(dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.RATING_LOW_HIGH_SORT)));
                }
            }

            PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,dispensaryList);
            this.dispensaryList=dispensaryList;

            dispensaryAdapter=new DispensaryAdapter(context,dispensaryList,latitude,longitude);
            binding.recycleView.setAdapter(dispensaryAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        }
        else
        {
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);
        }

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        menu.clear();

        Objects.requireNonNull(getActivity()).getMenuInflater().inflate(R.menu.dispensary_list_top_menu,menu);

        View cartView = menu.findItem(R.id.action_cart).getActionView();
        txtCartCount = cartView.findViewById(R.id.btn_cart_count);


        MenuItem actionSearch=menu.findItem(R.id.action_search);
        actionSearch.setVisible(true);


        cartView.setOnClickListener(view -> {
            startActivity(new Intent(context, CartActivity.class));
            ((DispensaryListActivity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
        });

        String badgeCount= PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);

        if (badgeCount!=null && (!badgeCount.equals("0")))
        {
            txtCartCount.setVisibility(View.VISIBLE);
            txtCartCount.setText(badgeCount+"");
        }
        else
        {
            txtCartCount.setVisibility(View.GONE);
        }


        final SearchView searchView = (SearchView) actionSearch.getActionView();

        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }


    @SuppressLint("SetTextI18n")
    public void updateBadgeCount()
    {
        String badgeCount = PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
        if (txtCartCount!=null)
        {
            if (badgeCount!=null && (!badgeCount.equals("0")))
            {
                txtCartCount.setVisibility(View.VISIBLE);
                txtCartCount.setText(badgeCount+"");
            }
            else
            {
                txtCartCount.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {

        if(dispensaryAdapter!=null && dispensaryList!=null && dispensaryList.size()>0)
        {
            dispensaryAdapter.getFilter().filter(query);
        }
        return false;


    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if(dispensaryList!=null && dispensaryList.size()>0)
        {
            dispensaryAdapter.getFilter().filter(newText);

        }

        return false;
    }

}
