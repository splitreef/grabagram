package com.splitreef.grabagramplaystore.ui.customer.myAccount;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CustomerMyAccountViewModel extends ViewModel {

    private static final String TAG = "CustomerMyAccountViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetMyAccountData = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUploadCustomerDocPhoto = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateMyAccountData = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public CustomerMyAccountViewModel(FirebaseRepository firebaseRepository) {
        this.firebaseRepository=firebaseRepository;
    }


    public  void getCustomerMyAccountData()
    {
         firebaseRepository.getCustomerMyAccountData()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<MyAccountResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onGetMyAccountData.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull MyAccountResponse myAccountResponse) {

                         onGetMyAccountData.setValue(DataResource.DataStatus(myAccountResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onGetMyAccountData.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }


    void uploadCustomerDocPhoto(byte[] bytes,String imageName)
    {
        firebaseRepository.uploadCustomerDocPhoto(bytes,imageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUploadCustomerDocPhoto.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatus(s));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    void updateCustomerMyAccountData(CustomerRegisterRequest customerRegisterRequest, MyAccountResponse myAccountResponse)
    {
        firebaseRepository.updateCustomerMyAccountData(customerRegisterRequest,myAccountResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateMyAccountData.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateMyAccountData.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateMyAccountData.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeGetMyAccountData()
    {
        return onGetMyAccountData;
    }
    LiveData<DataResource>observeCustomerDocPhoto()
    {
        return onUploadCustomerDocPhoto;
    }

    LiveData<StateResource>observeUpdateMyAccountData()
    {
        return onUpdateMyAccountData;
    }

}
