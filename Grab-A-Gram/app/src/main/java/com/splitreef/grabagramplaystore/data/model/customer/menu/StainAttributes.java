package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;
import java.util.ArrayList;

public class StainAttributes implements Serializable {

    private Effect effects;
    private ArrayList<String> medical_benefits;
    private ArrayList<String> negative;

    public Effect getEffects() {
        return effects;
    }

    public void setEffects(Effect effects) {
        this.effects = effects;
    }

    public ArrayList<String> getMedical_benefits() {
        return medical_benefits;
    }

    public void setMedical_benefits(ArrayList<String> medical_benefits) {
        this.medical_benefits = medical_benefits;
    }

    public ArrayList<String> getNegative() {
        return negative;
    }

    public void setNegative(ArrayList<String> negative) {
        this.negative = negative;
    }
}
