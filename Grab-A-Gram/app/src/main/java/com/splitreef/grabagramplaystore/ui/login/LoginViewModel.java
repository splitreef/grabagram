package com.splitreef.grabagramplaystore.ui.login;


import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {

    private static final String TAG = "LoginViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onLogin = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public LoginViewModel(Application application, FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }


    public void login(String userName, String password) {
        firebaseRepository.loginUser(userName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onLogin.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull UserModel userModel) {

                        onLogin.setValue(DataResource.DataStatus(userModel));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onLogin.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }

    LiveData<DataResource> observeLogin() {
        return onLogin;
    }
}
