package com.splitreef.grabagramplaystore.ui.customer.wishList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.WishListAdapter;
import com.splitreef.grabagramplaystore.callBack.RemoveWishListProduct;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.databinding.FragmentWishListBinding;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.WishListProviderFactory;

import java.util.Objects;

public class WishListFragment extends Fragment implements RemoveWishListProduct {

    private FragmentWishListBinding binding;
    private Context context;
    private GrabAGramApplication application;
    private WishListViewModel viewModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding=FragmentWishListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    public void initView()
    {
        application=(GrabAGramApplication)context.getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new WishListProviderFactory(application.firebaseRepository)).get(WishListViewModel.class);
        ((DispensaryListActivity)context).setToolbarTitle("WishList");
        binding.rvWishList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        subscribeObservers();
        viewModel.getProductWishList();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    void subscribeObservers() {
        viewModel.observeGetProductWishList().observe(Objects.requireNonNull(getActivity()), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        GetProductCartResponse productCartResponse = (GetProductCartResponse) dataResource.data;
                        updateUi(productCartResponse);
                        break;
                    case ERROR:
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.rvWishList.setVisibility(View.GONE);
                        binding.progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.observeDeleteProductWishList().observe(getActivity(), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            viewModel.getProductWishList();
                            break;
                        case ERROR:
                            binding.progressBar.setVisibility(View.GONE);

                    }
                }
            }
        });
    }

    void updateUi(GetProductCartResponse productCartResponse)
    {
        if (productCartResponse!=null && productCartResponse.getProductCarts()!=null && productCartResponse.getProductCarts().size()>0)
        {

            binding.userMsg.setVisibility(View.GONE);
            binding.rvWishList.setVisibility(View.VISIBLE);

            WishListAdapter wishListAdapter=new WishListAdapter(context,productCartResponse.getProductCarts(),this);
            binding.rvWishList.setAdapter(wishListAdapter);

        }
        else
        {

            binding.userMsg.setVisibility(View.VISIBLE);
            binding.rvWishList.setVisibility(View.GONE);
        }
    }

    @Override
    public void removeWishListProduct(String id) {
        viewModel.deleteWishListProduct(id);
    }
}
