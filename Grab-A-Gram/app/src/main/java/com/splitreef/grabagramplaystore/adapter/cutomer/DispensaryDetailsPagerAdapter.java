package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.DispensaryDetailsDealsFragment;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.DispensaryDetailsReviewFragment;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.DispensaryOverviewFragment;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.MenuParentFragment;
import com.splitreef.grabagramplaystore.utils.AppConstant;


public class DispensaryDetailsPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    int totalTabs;
    private final DispensaryModel dispensaryModel;

    public DispensaryDetailsPagerAdapter(Context context, FragmentManager fm, int totalTabs,DispensaryModel dispensaryModel) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
        this.totalTabs = totalTabs;
        this.dispensaryModel=dispensaryModel;
    }

    // this is for fragment tabs
    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;
        Bundle bundle=null;

        switch (position) {

            case 0:
                fragment=new DispensaryOverviewFragment();
                bundle = new Bundle();
                bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment=new MenuParentFragment();
                bundle = new Bundle();
                bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
                fragment.setArguments(bundle);
                break;
            case 2:
                fragment=new DispensaryDetailsDealsFragment();
                bundle = new Bundle();
                bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
                fragment.setArguments(bundle);
                break;
            case 3:
                fragment=new DispensaryDetailsReviewFragment();
                bundle = new Bundle();
                bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
                fragment.setArguments(bundle);
                break;

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
