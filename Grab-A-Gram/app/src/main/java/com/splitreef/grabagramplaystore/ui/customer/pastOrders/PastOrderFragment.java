package com.splitreef.grabagramplaystore.ui.customer.pastOrders;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.ScheduledOrderAdapter;
import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.databinding.FragmentScheduledOrdersBinding;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.viewModelFactory.PastOrderProviderFactory;

import java.util.Objects;

public class PastOrderFragment extends Fragment {

    private FragmentScheduledOrdersBinding binding;
    private Context context;
    private PastOrderViewModel viewModel;
    private GrabAGramApplication application;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding=FragmentScheduledOrdersBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPastOrders();
    }

    public void initView()
    {
        ((DispensaryListActivity)context).setToolbarTitle("Past Orders");

        application=(GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel=new ViewModelProvider(getViewModelStore(), new PastOrderProviderFactory(application.firebaseRepository)).get(PastOrderViewModel.class);
        subscribeObservers();

        binding.rvScheduledOrder.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

    }


    void subscribeObservers()
    {
       viewModel.observeGetPastOrders().observe((LifecycleOwner) context, new Observer<DataResource>() {
           @Override
           public void onChanged(DataResource dataResource) {
               switch (dataResource.status)
               {
                   case LOADING:
                       binding.progressBar.setVisibility(View.VISIBLE);
                       break;
                   case SUCCESS:
                       binding.progressBar.setVisibility(View.GONE);
                       GetPlacedOrdersResponse getPlacedOrdersResponse= (GetPlacedOrdersResponse) dataResource.data;
                       if (getPlacedOrdersResponse.getMessage().equals(AppConstant.SUCCESS))
                       {
                           setScheduledOrderAdapter(getPlacedOrdersResponse);
                       }
                       else
                       {
                         binding.userMsg.setVisibility(View.VISIBLE);
                       }
                       break;

                   case ERROR:
                       binding.progressBar.setVisibility(View.GONE);
                       binding.userMsg.setVisibility(View.VISIBLE);

               }
           }
       });

    }

    void setScheduledOrderAdapter(GetPlacedOrdersResponse getPlacedOrdersResponse)
    {
       if (getPlacedOrdersResponse.getOrders()!=null && getPlacedOrdersResponse.getOrders().size()>0)
       {
           ScheduledOrderAdapter adapter=new ScheduledOrderAdapter(context,getPlacedOrdersResponse.getOrders());
           binding.rvScheduledOrder.setAdapter(adapter);
       }
       else
       {
           binding.progressBar.setVisibility(View.GONE);
           binding.userMsg.setVisibility(View.VISIBLE);
       }
    }
}
