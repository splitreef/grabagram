package com.splitreef.grabagramplaystore.ui.customer.myAccount;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.actionsheet.ActionSheet;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAddress;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerUser;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentCustomerMyAccountBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.CustomerMyAccountProviderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_KEY;
import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_URL;

public class CustomerMyAccountFragment extends BaseFragment implements View.OnClickListener, ActionSheet.ActionSheetListener {

    private FragmentCustomerMyAccountBinding binding;
    private CustomerMyAccountViewModel viewModel;
    private Bitmap bitmap;
    private int photoTypeCount = 0;
    private GrabAGramApplication application;
    private long date = 0;
    private Context context;
    private long exLongDate = 0;
    private long recommendationLetterExpDate = 0;
    private CustomerRegisterRequest registerRequest;
    private CustomerAddress customerAddress;
    private UserAddressModel userAddressModel;
    private MyAccountResponse myAccountResponse;
    private int textLength=0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCustomerMyAccountBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new CustomerMyAccountProviderFactory(application.firebaseRepository)).get(CustomerMyAccountViewModel.class);
        ((DispensaryListActivity) context).setToolbarTitle("Account Info");

        binding.imgEditPhoto1.setOnClickListener(this);
        binding.imgEditPhoto2.setOnClickListener(this);
        binding.imgEditPhoto3.setOnClickListener(this);
        binding.expirationDate.setOnClickListener(this);
        binding.docRecExpirationDate.setOnClickListener(this);
        binding.dateOfBirth.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);

        subscribeObservers();
        viewModel.getCustomerMyAccountData();


        binding.mobilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.mobilePhone.getText().toString();
                textLength = binding.mobilePhone.getText().length();
                if (text.endsWith("-") || text.endsWith(" "))
                    return;
                if (textLength == 1) {
                    if (!text.contains("(")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 5) {
                    if (!text.contains(")")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 6) {
                    binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                } else if (textLength == 10) {
                    if (!text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 15) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 18) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.date_of_birth:
                setDateOfBirth();
                break;
            case R.id.img_edit_photo1:
                openActionSheet();
                photoTypeCount = 1;
                break;
            case R.id.img_edit_photo2:
                openActionSheet();
                photoTypeCount = 2;
                break;
            case R.id.img_edit_photo3:
                openActionSheet();
                photoTypeCount = 3;
                break;
            case R.id.expiration_date:
                setDate(1);
                break;
            case R.id.doc_rec_expiration_date:
                setDate(2);
                break;
            case R.id.btn_save:
                updateMyAccountData();
                break;
        }
    }


    // open action sheet
    void openActionSheet() {
        ActionSheet.createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Camera", "Gallery")
                .setCancelableOnTouchOutside(true)
                .setListener(this).show();
    }


    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        if (index == 0) {
            requestStorageAndCameraPermission(getActivity(), CAMERA_PIC);
        } else if (index == 1) {
            requestStorageAndCameraPermission(getActivity(), GALLERY_PIC);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedFileUri);
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount == 1) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_DOCTOR_RECOMMENDATION_LETTER);
                    } else if (photoTypeCount == 2) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_GOVERNMENT_PHOTO_ID);
                    } else if (photoTypeCount == 3) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_PERSONAL_ADDRESS);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {
                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount == 1) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_DOCTOR_RECOMMENDATION_LETTER);
                    } else if (photoTypeCount == 2) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_GOVERNMENT_PHOTO_ID);
                    } else if (photoTypeCount == 3) {
                        viewModel.uploadCustomerDocPhoto(bytes, AppConstant.IMAGE_PERSONAL_ADDRESS);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            application.messageManager.DisplayToastMessage("Something went wrong");
        }
    }

    public void setDate(int flag) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();

        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(selection -> {

            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar1.setTimeInMillis(selection);

            int month = calendar1.get(Calendar.MONTH);
            String date = (month + 1) + "-" + calendar1.get(Calendar.DAY_OF_MONTH) + "-" + calendar1.get(Calendar.YEAR);

            if (flag == 1) {
                exLongDate = selection;
                binding.expirationDate.setText("" + date);
            } else if (flag == 2) {
                recommendationLetterExpDate = selection;
                binding.docRecExpirationDate.setText("" + date);
            }

        });
    }

    @SuppressLint("SetTextI18n")
    public void setDateOfBirth() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointBackward.now());

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();

        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(selection -> {

            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar1.setTimeInMillis(selection);
            int month = calendar1.get(Calendar.MONTH);
            String dateOfBirth = (month + 1) + "-" + calendar1.get(Calendar.DAY_OF_MONTH) + "-" + calendar1.get(Calendar.YEAR);


            if (AppUtil.subYears(AppUtil.getCurrentDate(), dateOfBirth) < 18) {
                date = 0;
                binding.dateOfBirth.setText("");
                Toast.makeText(getContext(), "Please enter the valid date of birth", Toast.LENGTH_SHORT).show();
            } else {
                date = selection;
                binding.dateOfBirth.setText("" + dateOfBirth);
            }

        });

    }

    void subscribeObservers() {
        viewModel.observeGetMyAccountData().observe(Objects.requireNonNull(getActivity()), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        MyAccountResponse myAccountResponse = (MyAccountResponse) dataResource.data;
                        if (myAccountResponse.getStatus().equals(AppConstant.SUCCESS)) {
                            updateUi(myAccountResponse);
                        }

                        break;
                    case ERROR:
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.progressBar.setVisibility(View.GONE);
                }
            }
        });


        viewModel.observeUpdateMyAccountData().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            //Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                            UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
                            userModel.setEmail_communication(registerRequest.isEmailCommunication());
                            userModel.setPatient_id(registerRequest.getPatientId());
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.LOGIN_USER, userModel);
                            application.dialogManager.dismissProgressDialog();

                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {

                                }
                            }.showSimpleDialog(getContext(), "Success", "Account information has been updated successfully.", "OK");

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        viewModel.observeCustomerDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    PicassoManager.setImage(url, binding.photo1);
                                    registerRequest.setDoctorRecPhotoUrl(url);
                                } else if (photoTypeCount == 2) {
                                    PicassoManager.setImage(url, binding.photo2);
                                    registerRequest.setGovPhotoIDUrl(url);
                                } else if (photoTypeCount == 3) {
                                    PicassoManager.setImage(url, binding.photo3);
                                    registerRequest.setAddressProofPhotoUrl(url);
                                    userAddressModel.setStreet_1(binding.streetAddress.getText().toString());
                                    userAddressModel.setStreet_2(binding.streetAddress2.getText().toString());
                                    userAddressModel.setCity(binding.city.getText().toString());
                                    userAddressModel.setState(binding.spinnerState.getSelectedItem().toString());
                                    userAddressModel.setZip_code(binding.zipCode.getText().toString());
                                }
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void updateUi(MyAccountResponse myAccountResponse) {

        this.myAccountResponse = myAccountResponse;
        userAddressModel = new UserAddressModel();
        registerRequest = new CustomerRegisterRequest();
     //   UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
        List<String> states = Arrays.asList(getResources().getStringArray(R.array.states));
        List<String> docIds = Arrays.asList(getResources().getStringArray(R.array.id_type));

        if (myAccountResponse.getUser()!=null) {
            CustomerUser customerUser =myAccountResponse.getUser();
            binding.emailAddress.setText(customerUser.getEmail());
            binding.cbEmailReceiving.setChecked(customerUser.getEmail_communication());
        }

        if (myAccountResponse.getAccount() != null) {

            binding.firstName.setText(myAccountResponse.getAccount().getFirst_name());
            binding.lastName.setText(myAccountResponse.getAccount().getLast_name());
            binding.dateOfBirth.setText(AppUtil.getDateMDYFromMillisecond(myAccountResponse.getAccount().getDate_of_birth()));
            binding.cbSendNotification.setChecked(myAccountResponse.getAccount().getMessage_notification());
            date = myAccountResponse.getAccount().getDate_of_birth();

            registerRequest.seteSignature(myAccountResponse.getAccount().getE_signature());
        }

        if (myAccountResponse.getAddresses() != null) {

            binding.streetAddress.setText(myAccountResponse.getAddresses().getStreet_1());
            binding.streetAddress2.setText(myAccountResponse.getAddresses().getStreet_2());
            binding.city.setText(myAccountResponse.getAddresses().getCity());
            binding.spinnerState.setSelection(states.indexOf(myAccountResponse.getAddresses().getState()));
            binding.zipCode.setText(myAccountResponse.getAddresses().getZip_code());

             if (myAccountResponse.getAddresses().getPhone_number()!=null && myAccountResponse.getAddresses().getPhone_number().length()==10)
             {
                 StringBuilder phoneNumber=new StringBuilder(myAccountResponse.getAddresses().getPhone_number()).insert(0, "(");
                 phoneNumber=new StringBuilder(phoneNumber).insert(4, ")");
                 phoneNumber=new StringBuilder(phoneNumber).insert(5, " ");
                 phoneNumber=new StringBuilder(phoneNumber).insert(9, "-");
                 binding.mobilePhone.setText(phoneNumber);
             }

            //binding.mobilePhone.setText(myAccountResponse.getAddresses().getPhone_number());


            userAddressModel.setStreet_1(myAccountResponse.getAddresses().getStreet_1());
            userAddressModel.setStreet_2(myAccountResponse.getAddresses().getStreet_2());
            userAddressModel.setCity(myAccountResponse.getAddresses().getCity());
            userAddressModel.setState(myAccountResponse.getAddresses().getState());
            userAddressModel.setZip_code(myAccountResponse.getAddresses().getZip_code());
        }

        if (myAccountResponse.getDocument_info() != null) {
            binding.spinnerDocType.setSelection(docIds.indexOf(myAccountResponse.getDocument_info().getDocument_type()));
            binding.spinnerState2.setSelection(states.indexOf(myAccountResponse.getAddresses().getState()));
            binding.driverLicense.setText(myAccountResponse.getDocument_info().getIdentity_number());
            binding.expirationDate.setText(AppUtil.getDateMDYFromMillisecond(myAccountResponse.getDocument_info().getExpiration_date()));
            binding.patientId.setText(myAccountResponse.getDocument_info().getPatient_id());
            binding.docRecExpirationDate.setText(AppUtil.getDateMDYFromMillisecond(myAccountResponse.getDocument_info().getRecommendation_letter_expiration_date()));
            exLongDate = myAccountResponse.getDocument_info().getExpiration_date();
            recommendationLetterExpDate = myAccountResponse.getDocument_info().getRecommendation_letter_expiration_date();
        }

        if (myAccountResponse.getDocument_images() != null) {
            PicassoManager.setImage(myAccountResponse.getDocument_images().getDoctor_recommendation_letter(), binding.photo1);
            PicassoManager.setImage(myAccountResponse.getDocument_images().getGovernment_photo_id(), binding.photo2);
            PicassoManager.setImage(myAccountResponse.getDocument_images().getAddress_proof(), binding.photo3);

            registerRequest.setDoctorRecPhotoUrl(myAccountResponse.getDocument_images().getDoctor_recommendation_letter());
            registerRequest.setGovPhotoIDUrl(myAccountResponse.getDocument_images().getGovernment_photo_id());
            registerRequest.setAddressProofPhotoUrl(myAccountResponse.getDocument_images().getAddress_proof());
        }

    }

    boolean isValidation() {
        if (Objects.requireNonNull(binding.firstName.getText()).toString().equals("")) {
            binding.firstName.setError(getString(R.string.enter_your_first_name));
            binding.firstName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.lastName.getText()).toString().equals("")) {
            binding.lastName.setError(getString(R.string.enter_your_last_name));
            binding.lastName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.streetAddress.getText()).toString().equals("")) {
            binding.streetAddress.setError(getString(R.string.enter_your_address));
            binding.streetAddress.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.city.getText()).toString().equals("")) {
            binding.city.setError(getString(R.string.enter_your_city));
            binding.city.requestFocus();
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.enter_your_state));
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().equals("")) {
            binding.zipCode.setError(getString(R.string.enter_your_zip_code));
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().trim().length() < 5) {
            binding.zipCode.setError(getString(R.string.enter_your_valid_zip_code));
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().equals("")) {
            binding.mobilePhone.setError(getString(R.string.enter_your_mobile_number));
            binding.mobilePhone.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().length() < 14) {
            binding.mobilePhone.setError(getString(R.string.enter_your_valid_mobile_number));
            binding.mobilePhone.requestFocus();
            return false;
        } else if (binding.dateOfBirth.getText().toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.enter_date_of_birth));
            return false;
        } else if (binding.spinnerDocType.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.select_doc_type));
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.issue_date_required));
            return false;
        } else if (Objects.requireNonNull(binding.driverLicense.getText()).toString().equals("")) {
            binding.driverLicense.setError(getString(R.string.driver_license_required));
            binding.driverLicense.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.expirationDate.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.exp_date_government_Id));
            return false;
        } else if (Objects.requireNonNull(binding.patientId.getText()).toString().equals("")) {
            binding.patientId.setError(getString(R.string.patient_id_required));
            binding.patientId.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.docRecExpirationDate.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.exp_doctor_rec));
            return false;
        } else if (registerRequest.getDoctorRecPhotoUrl() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.doctor_id_image));
            return false;
        } else if (registerRequest.getGovPhotoIDUrl() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.government_id_image));
            return false;
        } else if (registerRequest.getAddressProofPhotoUrl() == null) {
            application.messageManager.DisplayToastMessage(getString(R.string.address_id_image));
            return false;
        }

        return true;

    }

    void updateMyAccountData() {

        if ((!userAddressModel.getStreet_1().equals(binding.streetAddress.getText().toString())) || (!userAddressModel.getStreet_2().equals(binding.streetAddress2.getText().toString()))
                || (!userAddressModel.getCity().equals(binding.city.getText().toString())) || (!userAddressModel.getState().equals(binding.spinnerState.getSelectedItem().toString()))
                || (!userAddressModel.getZip_code().equals(binding.zipCode.getText().toString()))) {
            registerRequest.setAddressProofPhotoUrl(null);
            PicassoManager.setImage("No image", binding.photo3);
        }

        if (isValidation()) {


            String tmp = Objects.requireNonNull(binding.mobilePhone.getText()).toString();
            String phoneNumber = tmp.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");

            registerRequest.setEmail(binding.emailAddress.getText().toString());
            registerRequest.setEmailCommunication(binding.cbEmailReceiving.isChecked());

            registerRequest.setFirstName(binding.firstName.getText().toString());
            registerRequest.setLastName(binding.lastName.getText().toString());
            registerRequest.setStreetAddress(binding.streetAddress.getText().toString());
            registerRequest.setStreetAddress2(binding.streetAddress2.getText().toString());
            registerRequest.setCity(binding.city.getText().toString());
            registerRequest.setState(binding.spinnerState.getSelectedItem().toString());
            registerRequest.setZipCode(binding.zipCode.getText().toString());
            registerRequest.setMobileNumber(phoneNumber);
            registerRequest.setDateOfBirth(date);
            registerRequest.setSendNotification(binding.cbSendNotification.isChecked());

            registerRequest.setDocType(binding.spinnerDocType.getSelectedItem().toString());
            registerRequest.setIssuingState(binding.spinnerState.getSelectedItem().toString());
            registerRequest.setIdentityNumber(binding.driverLicense.getText().toString());
            registerRequest.setPatientId(binding.patientId.getText().toString());
            registerRequest.setExpirationDate(exLongDate);
            registerRequest.setDoctorsRecExpirationDate(recommendationLetterExpDate);

            String address = registerRequest.getStreetAddress() + "," + registerRequest.getCity() + "," + registerRequest.getState() + "," + registerRequest.getZipCode();

            getLatLng(address);
            application.dialogManager.displayProgressDialog(getActivity(), "Loading");

            String json = new Gson().toJson(myAccountResponse.getAddresses(), CustomerAddress.class);
            Log.d("CustomerAddress_json==", json);

        }
    }

    void getLatLng(String address) {

        /// String address = "Vijay nagar,indore,Madhya Pradesh,452010";
        String googleApiUrl = GOOGLE_API_URL + "?address=" + address + "&key=" + GOOGLE_API_KEY;


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                googleApiUrl, new Response.Listener<String>() {

            public void onResponse(String response) {

                application.dialogManager.dismissProgressDialog();

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");

                        if (status.equals("OK")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("geometry");

                            JSONObject jsonObject3 = jsonObject2.getJSONObject("location");

                            double latitude = jsonObject3.getDouble("lat");
                            double longitude = jsonObject3.getDouble("lng");


                            registerRequest.setLatitude(latitude);
                            registerRequest.setLongitude(longitude);

                            viewModel.updateCustomerMyAccountData(registerRequest, myAccountResponse);


                            Log.d("status", status + "lat/longitude" + latitude + "---" + longitude);

                        } else {
                            Toast.makeText(getContext(), "Invalid address", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                Log.d("Response", response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
}
