package com.splitreef.grabagramplaystore.ui.customer.checkout;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.PaymentMethod;
import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.OrderRequest;
import com.splitreef.grabagramplaystore.data.model.customer.OrderResponse;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverTokens.DriverTokens;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CheckoutViewModel extends ViewModel {

    private static final String TAG = "CheckoutViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<StateResource> onUpdateOrderNotificationStatus = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdatePaymentStatus = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetAddressList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetDispensary = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onPlaceOrder = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onCouponCode = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onDriverTokens = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateCouponLimit = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public CheckoutViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    void getAddressList()
    {
        firebaseRepository.getAddressList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AddressList>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetAddressList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull AddressList addressList) {
                        onGetAddressList.setValue(DataResource.DataStatus(addressList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetAddressList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    void getDispensary(String dispensaryId)
    {
        firebaseRepository.getDispensary(dispensaryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DispensaryModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetDispensary.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DispensaryModel dispensaryModel) {
                        onGetDispensary.setValue(DataResource.DataStatus(dispensaryModel));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDispensary.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    void placeOrder(OrderRequest orderRequest)
    {
        firebaseRepository.placeOrder(orderRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<OrderResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onPlaceOrder.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull OrderResponse orderResponse) {
                        onPlaceOrder.setValue(DataResource.DataStatus(orderResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onPlaceOrder.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public  void getCouponProducts(String couponCode)
    {
        firebaseRepository.getCouponProducts(couponCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CouponResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onCouponCode.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull CouponResponse couponResponse) {

                        onCouponCode.setValue(DataResource.DataStatus(couponResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onCouponCode.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    public  void getDriversTokens(double latitude, double longitude, double distance)
    {
        firebaseRepository.getDriversTokens(latitude,longitude,distance)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DriverTokens>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onDriverTokens.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverTokens driverTokens) {

                        onDriverTokens.setValue(DataResource.DataStatus(driverTokens));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onDriverTokens.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    public  void updateCartProduct(boolean isSend, String orderId)
    {
        firebaseRepository.updateOrderIsSendNotification(isSend,orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateOrderNotificationStatus.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdateOrderNotificationStatus.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUpdateOrderNotificationStatus.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }

    public  void updateCouponCodeLimit(long limitUsed,String couponId)
    {
        firebaseRepository.updateCouponCodeLimit(limitUsed, couponId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateCouponLimit.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdateCouponLimit.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUpdateCouponLimit.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }


    public  void updatePaymentStatus(boolean paymentSuccess, PaymentMethod paymentMethod, String orderId)
    {
        firebaseRepository.updatePaymentStatus(paymentSuccess, paymentMethod, orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUpdatePaymentStatus.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdatePaymentStatus.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUpdatePaymentStatus.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }



    LiveData<DataResource>observeGetAddressList()
    {
        return onGetAddressList;
    }
    LiveData<DataResource>observeGetDispensary()
    {
        return onGetDispensary;
    }
    LiveData<DataResource>observePlaceOrder()
    {
        return onPlaceOrder;
    }
    LiveData<DataResource> observeOngGetCouponProduct()
    {
        return onCouponCode;
    }

    LiveData<DataResource> observeOnGetDriversTokens()
    {
        return onDriverTokens;
    }

    LiveData<StateResource> observeUpdateOrderNotificationStatus()
    {
        return onUpdateOrderNotificationStatus;
    }

    LiveData<StateResource> observeUpdatePaymentStatus()
    {
        return onUpdatePaymentStatus;
    }
    LiveData<StateResource> observeUpdateCouponLimit()
    {
        return onUpdateCouponLimit;
    }

}
