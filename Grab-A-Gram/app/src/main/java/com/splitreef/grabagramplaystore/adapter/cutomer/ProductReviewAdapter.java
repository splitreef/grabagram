package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

public class ProductReviewAdapter extends RecyclerView.Adapter<ProductReviewAdapter.ProductReviewHolder> {

    private Context context;
    private ArrayList<ProductReviewModel> productReviewArrayList;


    public ProductReviewAdapter(Context context, ArrayList<ProductReviewModel> productReviewArrayList) {
        this.context=context;
        this.productReviewArrayList=productReviewArrayList;

    }

    @NonNull
    @Override
    public ProductReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dispensary_review,parent,false);
        return new ProductReviewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductReviewHolder holder, int position) {

        ProductReviewModel productReviewModel=productReviewArrayList.get(position);
        holder.txtUserName.setText(productReviewModel.getUser_name());
        holder.txtCreatedAt.setText(""+ AppUtil.getElapsedTime(productReviewModel.getCreated_at()));
        holder.txtComment.setText(productReviewModel.getComment());
        holder.rating.setRating(productReviewModel.getRating());

    }

    @Override
    public int getItemCount() {
        return productReviewArrayList.size();
    }

    public class ProductReviewHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtUserName;
        MaterialTextView txtCreatedAt;
        MaterialTextView txtComment;
        AppCompatRatingBar rating;

        public ProductReviewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txt_user_name);
            txtCreatedAt= itemView.findViewById(R.id.created_at);
            rating= itemView.findViewById(R.id.dispensary_rating);
            txtComment= itemView.findViewById(R.id.txt_comment);
        }
    }
}
