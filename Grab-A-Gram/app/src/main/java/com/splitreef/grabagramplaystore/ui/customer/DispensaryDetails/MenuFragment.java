package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.CategoryAdapter;
import com.splitreef.grabagramplaystore.callBack.ProductDetailsCallBack;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Category;
import com.splitreef.grabagramplaystore.data.model.customer.menu.CategoryMenu;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.databinding.FragmentMenuBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryDetailsViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

public class MenuFragment extends BaseFragment implements ProductDetailsCallBack {

    private FragmentMenuBinding binding;
    private Context context;
    private DispensaryDetailsViewModel viewModel;
    private GrabAGramApplication application;
    private DispensaryModel dispensaryModel;
    ArrayList<CategoryMenu>categoryMenuArrayList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments()!=null)
        {
            dispensaryModel= (DispensaryModel) getArguments().getParcelable(AppConstant.DISPENSARY_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (binding==null)
        {
            binding=FragmentMenuBinding.inflate(inflater,container,false);
        }

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    void initView()
    {
        application=(GrabAGramApplication) context.getApplicationContext();
        viewModel=new ViewModelProvider(getViewModelStore(), new DispensaryDetailsViewModelProviderFactory(application.firebaseRepository)).get(DispensaryDetailsViewModel.class);
        subscribeObservers();

        if (dispensaryModel!=null)
        {
            viewModel.getMenuList(dispensaryModel.getUser_id());
        }
        else
        {
            showToast(context,"Data Not found!");
        }



        binding.rvCategory.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
      //  binding.rvCategory.setItemAnimator(new DefaultItemAnimator());
    }


    void subscribeObservers()
    {
        viewModel.observeGetMenuList().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);

                        ArrayList<ProductListResponse> productArrayList= (ArrayList<ProductListResponse>) dataResource.data;

                        if (productArrayList!=null && productArrayList.size()>0)
                        {
                            makingProductList(productArrayList);
                            binding.userMsg.setVisibility(View.GONE);
                        }
                        else
                        {
                            binding.userMsg.setVisibility(View.VISIBLE);
                        }

                     //   updateUI(dealModelArrayList);
                        // showToast(getContext(),"Success deal fragment");
                        Log.d("size==================",""+productArrayList.size());
                        break;
                    case ERROR:
                        binding.progressBar.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.VISIBLE);
                       // showToast(context,"Data not found");
                        break;

                }
            }

        });

    }

    void makingProductList(ArrayList<ProductListResponse> productArrayList)
    {
        categoryMenuArrayList=new ArrayList<>();

        if (productArrayList!=null && productArrayList.size()>0)
        {
            for (int i=0;i<productArrayList.size();i++)
            {
                ArrayList<Category>categoryArrayList=productArrayList.get(i).getCategories();

                if (categoryArrayList!=null && categoryArrayList.size()>0)
                {
                    for (int j=0;j<categoryArrayList.size();j++)
                    {
                        CategoryMenu categoryMenu=new CategoryMenu();

                        if (i==0 && j==0)
                        {
                            Category category=categoryArrayList.get(0);
                            categoryMenu.setCategory_id(category.getCategory_id());
                            categoryMenu.setCategory_name(category.getCategory_name());
                            categoryMenuArrayList.add(categoryMenu);
                        }
                        else
                        {
                            if (categoryMenuArrayList!=null && categoryMenuArrayList.size()>0)
                            {
                                boolean isContained=isContainedCategory(categoryMenuArrayList,categoryArrayList.get(j).getCategory_id());

                                if (!isContained)
                                {
                                    Category category=categoryArrayList.get(j);
                                    categoryMenu.setCategory_id(category.getCategory_id());
                                    categoryMenu.setCategory_name(category.getCategory_name());
                                    categoryMenuArrayList.add(categoryMenu);
                                }
                            }
                        }


                    }
                }

            }


            if (categoryMenuArrayList!=null && categoryMenuArrayList.size()>0)
            {
                for (int i=0;i<categoryMenuArrayList.size();i++)
                {
                    ArrayList<ProductListResponse>productList=new ArrayList<>();

                    for (int j=0;j<productArrayList.size();j++)
                    {
                        ArrayList<Category>categoryArrayList=productArrayList.get(j).getCategories();

                        for (int k=0;k<categoryArrayList.size();k++)
                        {
                            if (categoryArrayList.get(k).getCategory_id().equals(categoryMenuArrayList.get(i).getCategory_id()))
                            {
                                productList.add(productArrayList.get(j));
                            }
                        }
                    }

                    categoryMenuArrayList.get(i).setProductList(productList);
                }


                if (categoryMenuArrayList!=null && categoryMenuArrayList.size()>0)
                {
                    CategoryAdapter categoryAdapter=new CategoryAdapter(context,categoryMenuArrayList,this);
                    binding.rvCategory.setAdapter(categoryAdapter);
                }

            }

        }

    }


    boolean isContainedCategory(ArrayList<CategoryMenu>categoryMenuArrayList,String categoryId)
    {
        boolean isContained=false;

        for (int i=0;i<categoryMenuArrayList.size();i++)
        {
            if (categoryMenuArrayList.get(i).getCategory_id().equals(categoryId))
            {
                isContained=true;
                break;
            }
        }

        return  isContained;
    }

    @Override
    public void onClickProduct(ProductListResponse productListResponse) {

        MenuParentFragment parentFrag = ((MenuParentFragment) (MenuFragment.this.getParentFragment()));
        Fragment fragment=new ProductDetailsFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstant.PRODUCT_RESPONSE,productListResponse);
        bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
        fragment.setArguments(bundle);
        assert parentFrag != null;
        parentFrag.replaceFragment(fragment,true);
    }


}
