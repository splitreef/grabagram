package com.splitreef.grabagramplaystore.ui.customer.orderDetails;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.OrderDetailsAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.databinding.ActivityCustomerOrderDetailsBinding;
import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.ui.customer.dispensaryReview.DispensaryReviewActivity;
import com.splitreef.grabagramplaystore.ui.customer.driverRating.DriverRatingActivity;
import com.splitreef.grabagramplaystore.ui.customer.productReview.ProductReviewActivity;
import com.splitreef.grabagramplaystore.ui.customer.trackOrder.TrackOrderActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.viewModelFactory.CustomerOrderDetailsProviderFactory;

import java.util.ArrayList;
import java.util.Objects;


public class CustomerOrderDetailsActivity extends BaseActivity implements View.OnClickListener {
    private ActivityCustomerOrderDetailsBinding binding;
    private GrabAGramApplication application;
    private CustomerOrderDetailsViewModel viewModel;
    private Order order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityCustomerOrderDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    public void initView()
    {
        application = (GrabAGramApplication) Objects.requireNonNull(getApplicationContext());

        viewModel = new ViewModelProvider(getViewModelStore(), new CustomerOrderDetailsProviderFactory(application.firebaseRepository)).get(CustomerOrderDetailsViewModel.class);
        subscribeObservers();

        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        binding.btnReview.setOnClickListener(this);
        binding.btnTrackOrder.setOnClickListener(this);

         if (getIntent()!=null)
         {
             String orderId = getIntent().getStringExtra(AppConstant.ORDER_ID);
             if (orderId!=null)
             {
                 viewModel.getOrderDetailsByOrderId(orderId);
             }

         }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void updateUi(Order order)
    {
       if (order!=null) {

           binding.txtOrderDate.setText(""+ AppUtil.getDateFromMillisecond(order.getCreated_at()));
           binding.txtOrderNo.setText(""+order.getOrder_no());
           binding.txtConfirmCode.setText(""+order.getConfirmation_code());
           binding.txtOrderTotal.setText(""+String.format("$%.2f", order.getGrand_total()));

           binding.txtOrderDate2.setText(""+AppUtil.getDateWithDayNameFromMillisecondTime(order.getUpdated_at())+" at "+AppUtil.getTimeFromMillisecondTime(order.getUpdated_at()));

           binding.txtOrderStatus.setText(""+order.getStatus());

           if (order.getStatus().equals(AppConstant.ORDER_TYPE_SCHEDULED) || order.getStatus().equals(AppConstant.ORDER_TYPE_ACCEPT))
           {
               binding.btnReview.setText("Cancel");
               binding.btnReview.setVisibility(View.VISIBLE);
               binding.btnTrackOrder.setVisibility(View.GONE);
           }
           else if (order.getStatus().equals(AppConstant.ORDER_TYPE_COMPLETED))
           {
               binding.btnReview.setText("Review");
               binding.btnReview.setVisibility(View.VISIBLE);
               binding.btnTrackOrder.setVisibility(View.GONE);
           }
           else if (order.getStatus().equals(AppConstant.ORDER_TYPE_PICKED))
           {
               binding.btnTrackOrder.setVisibility(View.VISIBLE);
               binding.btnReview.setVisibility(View.GONE);
           }
           else
           {
               binding.btnReview.setVisibility(View.GONE);
               binding.btnTrackOrder.setVisibility(View.GONE);
           }

           // set addresses
           setAddress(order);

           if (order.getItems() != null && order.getItems().size() > 0)
           {
               binding.rvOrder.setLayoutManager(new LinearLayoutManager(CustomerOrderDetailsActivity.this,LinearLayoutManager.VERTICAL,false));
               OrderDetailsAdapter orderDetailsAdapter =new OrderDetailsAdapter(CustomerOrderDetailsActivity.this,order.getItems());
               binding.rvOrder.setAdapter(orderDetailsAdapter);
           }


           // set order summary

           binding.txtSubtotal.setText(""+String.format("$%.2f", order.getSub_total()));
           binding.txtDeliveryFee.setText(""+String.format("$%.2f", order.getDelivery_fee()));
           binding.txtSalesTax.setText(""+String.format("$%.2f", order.getSales_tax()));
           binding.txtDiscount.setText(""+String.format("$%.2f", order.getDiscount_amount()));
           binding.txtPaymentMethod.setText(""+order.getPayment_method().getAccountType());
           binding.txtTotal.setText(""+String.format("$%.2f", order.getGrand_total()));
       }
    }

    private void setAddress(Order order)
    {
        if (order.getAddresses()!=null && order.getAddresses().size()>0)
        {
            UserAddressModel billingAddress = filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS,order.getAddresses());

            if (billingAddress!=null)
            {
                String address = billingAddress.getStreet_1()+"\n"+
                        billingAddress.getCity()+","+billingAddress.getState()+","+billingAddress.getZip_code()+"\n"+billingAddress.getPhone_number();

                binding.txtBillingAddress.setText(address);
            }

            UserAddressModel PersonalAddress = filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS,order.getAddresses());

            if (PersonalAddress!=null)
            {
                String address = PersonalAddress.getStreet_1()+"\n"+
                        PersonalAddress.getCity()+","+PersonalAddress.getState()+","+PersonalAddress.getZip_code()+
                        "\n"+PersonalAddress.getPhone_number();

                binding.txtShippingAddress.setText(address);
            }

            UserAddressModel primaryAddress = filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS,order.getAddresses());

            if (primaryAddress!=null)
            {
                String address = primaryAddress.getStreet_1()+"\n"+
                        primaryAddress.getCity()+","+primaryAddress.getState()+","+primaryAddress.getZip_code()+
                        "\n"+primaryAddress.getPhone_number();

                binding.txtShippingAddress.setText(address);
            }

            UserAddressModel secondaryAddress = filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS,order.getAddresses());

            if (secondaryAddress!=null)
            {
                String address = secondaryAddress.getStreet_1()+"\n"+
                        secondaryAddress.getCity()+","+secondaryAddress.getState()+","+secondaryAddress.getZip_code()+
                        "\n"+secondaryAddress.getPhone_number();

                binding.txtShippingAddress.setText(address);
            }

        }
    }



    private UserAddressModel filterAddressType(String addressType, ArrayList<UserAddressModel> list)
    {
        UserAddressModel userAddressModel=null;

        for (int i=0;i<list.size();i++)
        {
            if (addressType.equals(list.get(i).getType()))
            {
                userAddressModel = list.get(i);
                break;
            }
        }

        return userAddressModel;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void subscribeObservers() {
        viewModel.observeUpdateOrderStatus().observe(CustomerOrderDetailsActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CustomerOrderDetailsActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        String message  = (String) dataResource.data;
                        if (message.equals(AppConstant.SUCCESS))
                        {
                            showToast("Order is cancel successfully.");
                            finish();
                        }
                        else
                        {
                            showToast("Error!");
                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });

        viewModel.observeGetOrderDetails().observe(CustomerOrderDetailsActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CustomerOrderDetailsActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        order  = (Order) dataResource.data;
                        if (order!=null)
                        {
                            updateUi(order);
                        }
                        else
                        {
                            showToast("Data not found!");
                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_review:
                 if (binding.btnReview.getText().toString().equalsIgnoreCase("Cancel"))
                 {
                     new AlertDialog() {
                         @Override
                         public void onPositiveButtonClick() {
                             viewModel.updateOrderStatus(order.getId(),AppConstant.ORDER_TYPE_CANCELED);
                         }
                     }.showAlertDialog(CustomerOrderDetailsActivity.this,getString(R.string.cancel),getString(R.string.cancel_order),getString(R.string.dialog_yes),getString(R.string.dialog_no));

                 }
                 else
                 {
                     if (order!=null)
                     {
                         showReviewOptionDialog();
                     }
                     else
                     {
                         showToast("Data not found");
                     }
                 }
                 break;

            case R.id.btn_track_order:
                startActivity(new Intent(CustomerOrderDetailsActivity.this, TrackOrderActivity.class)
                        .putExtra(AppConstant.ORDER_MODEL,order));
                break;


        }
    }


    public  void showReviewOptionDialog() {
        try {
            BottomSheetDialog reviewOption = new BottomSheetDialog(CustomerOrderDetailsActivity.this,R.style.CustomDialog);
            View sheetView = getLayoutInflater().inflate(R.layout.layout_review_dialog, null);

            MaterialTextView txtDismiss = sheetView.findViewById(R.id.txt_dismiss);
            LinearLayout llDispensaryReview = sheetView.findViewById(R.id.ll_dispensary_review);
            LinearLayout llProductReview = sheetView.findViewById(R.id.ll_product_review);
            LinearLayout llDriverRating = sheetView.findViewById(R.id.ll_driver_rating);

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

            int height = displayMetrics.heightPixels;

            int maxHeight = (int) (height*0.88);

            reviewOption.setDismissWithAnimation(true);
            reviewOption.getBehavior().setPeekHeight(maxHeight);
            reviewOption.setContentView(sheetView);

            llDispensaryReview.setOnClickListener(view -> {
                startActivity(new Intent(CustomerOrderDetailsActivity.this, DispensaryReviewActivity.class)
                        .putExtra(AppConstant.ORDER_MODEL,order));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                reviewOption.cancel();

            });

            llProductReview.setOnClickListener(view -> {

                startActivity(new Intent(CustomerOrderDetailsActivity.this, ProductReviewActivity.class)
                        .putExtra(AppConstant.ORDER_MODEL,order));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                reviewOption.cancel();

            });

            llDriverRating.setOnClickListener(view -> {

                startActivity(new Intent(CustomerOrderDetailsActivity.this, DriverRatingActivity.class)
                        .putExtra(AppConstant.ORDER_MODEL,order));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                reviewOption.cancel();

            });


            txtDismiss.setOnClickListener(view -> reviewOption.cancel());

            reviewOption.show();

        } catch (Exception e) {
            Log.e("showSortDialog_error",e.getMessage());
        }

    }
}
