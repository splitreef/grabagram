package com.splitreef.grabagramplaystore.ui.driver.driverRegistration;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentDriver;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentDriverPersonalInfoBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.viewModelFactory.DriverRegistrationViewModalProviderFactory;

import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;


public class DriverPersonalInfoFragment extends BaseFragment implements View.OnClickListener {
    private FragmentDriverPersonalInfoBinding binding;
    private FragmentDriver fragmentConsumer;
    private GrabAGramApplication application;
    private DriverRegistrationRequest driverRegistrationRequest;
    private DriverRegistrationViewModel driverRegistrationViewModel;
    private boolean isShow = false;
    private long date = 0;
    private int requestType = 0;
    private Context context;
    int textLength = 0;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public DriverPersonalInfoFragment(FragmentDriver fragmentConsumer) {
        this.fragmentConsumer = fragmentConsumer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDriverPersonalInfoBinding.inflate(inflater, container, false);
        initView();
        subscribeObservers();
        return binding.getRoot();
    }

    void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        driverRegistrationViewModel = new ViewModelProvider(getViewModelStore(), new DriverRegistrationViewModalProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(DriverRegistrationViewModel.class);

        binding.llPersonalInfo.setVisibility(View.GONE);
        binding.btnShow.setOnClickListener(this);
        binding.btnContinue.setOnClickListener(this);
        binding.btnVerify.setOnClickListener(this);
        binding.dateOfBirth.setOnClickListener(this);

        binding.mobilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.mobilePhone.getText().toString();
                textLength = binding.mobilePhone.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength == 1) {
                    if (!text.contains("(")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 5) {
                    if (!text.contains(")")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 6) {
                    binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                } else if (textLength == 10) {
                    if (!text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 15) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 18) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_show:
                if (!isShow) {
                    isShow = true;
                    // show password
                    binding.btnShow.setText(R.string.hide);
                    binding.edtPassoword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                } else {
                    isShow = false;
                    // hide password
                    binding.btnShow.setText(R.string.show);
                    binding.edtPassoword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
                break;
            case R.id.btn_continue:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
                } else {
                    moveNextScreen();
                }
                break;
            case R.id.btn_verify:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
                } else {
                    performRegistration();
                    // fragmentConsumer.getData(3, driverRegistrationRequest);

                }
                break;
            case R.id.date_of_birth:
                setDateOfBirth();
                break;
            case R.id.tv_privacy_policy:
                break;
        }
    }

    private void performRegistration() {
        String deviceID = PreferenceManger.getFcmToken(getContext());
        if (isAccountInfoValidation()) {
            driverRegistrationRequest = new DriverRegistrationRequest();
            driverRegistrationRequest.setEmail(binding.edtEmail.getText().toString());
            driverRegistrationRequest.setPassword(binding.edtPassoword.getText().toString());
            driverRegistrationRequest.setEmailCommunication(binding.accountInfoEmailNotification.isChecked());

            driverRegistrationRequest.setRole(AppConstant.DRIVER_ROLE);
            driverRegistrationRequest.setSetActivateAt(false);
            driverRegistrationRequest.setDeviceId(deviceID);
            driverRegistrationRequest.setDeviceType(AppConstant.DEVICE_TYPE);
            requestType = 1;
            driverRegistrationViewModel.checkUserExistsOrNot(Objects.requireNonNull(binding.edtEmail.getText()).toString());
            // driverRegistrationViewModel.registerUser(Objects.requireNonNull(binding.edtEmail.getText()).toString(), Objects.requireNonNull(binding.edtPassoword.getText()).toString());

        }

    }

    // For account info Validation
    boolean isAccountInfoValidation() {
        if (Objects.requireNonNull(binding.edtEmail.getText()).toString().equals("")) {
            binding.edtEmail.setError("Please enter email");
            binding.edtEmail.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(binding.edtEmail.getText()).toString()).matches()) {
            binding.edtEmail.setError("Please enter valid Email");
            binding.edtEmail.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPassoword.getText()).toString().equals("")) {
            binding.edtPassoword.setError("Please enter Password");
            binding.edtPassoword.requestFocus();
            return false;
        }
        return true;

    }


    void moveNextScreen() {
        if (isPersonalInfoValidation()) {
            driverRegistrationRequest.setCity(binding.city.getText().toString());
            driverRegistrationRequest.setDateOfBirth(date);
            driverRegistrationRequest.setFirstName(binding.firstName.getText().toString());
            driverRegistrationRequest.setLastName(binding.lastName.getText().toString());
            driverRegistrationRequest.setIsCareGiver(binding.rbYes.isChecked());

            driverRegistrationRequest.setMessageNotification(binding.perInfoMessageNotification.isChecked());
            //  driverRegistrationRequest.setPhoneNumber(binding.mobilePhone.getText().toString());
            String tmp = binding.mobilePhone.getText().toString();
            String phone = tmp.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            driverRegistrationRequest.setPhoneNumber(phone);


            driverRegistrationRequest.setState(binding.spinnerState.getSelectedItem().toString());
            driverRegistrationRequest.setStreetAddress1(binding.streetAddress.getText().toString());
            driverRegistrationRequest.setStreetAddress2(binding.streetAddress2.getText().toString());
            driverRegistrationRequest.setTermAndCondition(binding.termsCondition.isChecked());

            driverRegistrationRequest.setZipCode(binding.zipCode.getText().toString());

            fragmentConsumer.getData(1, driverRegistrationRequest);

        }
    }

    // For personal info Validation
    boolean isPersonalInfoValidation() {
        if (Objects.requireNonNull(binding.firstName.getText()).toString().equals("")) {
            binding.firstName.setError("Please enter First Name");
            binding.firstName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.lastName.getText()).toString().equals("")) {
            binding.lastName.setError("Please enter Last Name");
            binding.lastName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.streetAddress.getText()).toString().equals("")) {
            binding.streetAddress.setError("Please enter Address");
            binding.streetAddress.requestFocus();
            return false;
        } /*else if (Objects.requireNonNull(binding.streetAddress2.getText()).toString().equals("")) {
            binding.streetAddress2.setError("Please enter Address 2");
            binding.streetAddress2.requestFocus();
            return false;
        } */ else if (Objects.requireNonNull(binding.city.getText()).toString().equals("")) {
            binding.city.setError("Please enter City");
            binding.city.requestFocus();
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please select state");
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().equals("")) {
            binding.zipCode.setError("Please enter Zip Code");
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().trim().length() < 5) {
            binding.zipCode.setError("Please Enter Valid Zip Code");
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().equals("")) {
            binding.mobilePhone.setError("Please enter mobile number");
            binding.mobilePhone.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().length() < 14) {
            binding.mobilePhone.setError("Please Enter Valid Phone Number");
            binding.mobilePhone.requestFocus();
            return false;
        } else if (binding.dateOfBirth.getText().toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please select Date of Birth");
            return false;
        } else if (!binding.rbYes.isChecked() && !binding.rbNo.isChecked()) {
            application.messageManager.DisplayToastMessage("Please select cardriver option");
            return false;
        }
        return true;

    }

    public void setDateOfBirth() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        // calendar.set(Calendar.YEAR,-18);
        //    calendar.add(Calendar.YEAR,32);
        //   long time= calendar.getTimeInMillis();

        // long today=MaterialDatePicker.todayInUtcMilliseconds();


        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointBackward.now());
        // constraintBuilder.setEnd(time);

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        // builder.setSelection(today);
        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {


                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar.setTimeInMillis(selection);
                int month = calendar.get(Calendar.MONTH);
                String dateOfBirth = (month + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.YEAR);


                if (AppUtil.subYears(AppUtil.getCurrentDate(), dateOfBirth) < 18) {
                    date = 0;
                    binding.dateOfBirth.setText("");
                    Toast.makeText(getContext(), "Please enter the valid date of birth", Toast.LENGTH_SHORT).show();
                } else {
                    date = selection;
                    binding.dateOfBirth.setText("" + dateOfBirth);
                }

            }
        });
    }

    public void subscribeObservers() {
        driverRegistrationViewModel.observeUserExistOrNot().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            Boolean flag = (Boolean) dataResource.data;
                            Log.d("url====", "" + flag);

                            if (!flag) {
                                binding.llPersonalInfo.setVisibility(View.VISIBLE);
                                binding.llayoutAccountInfo.setEnabled(false);

                                binding.edtPassoword.setAlpha(0.5f);
                                binding.edtEmail.setAlpha(0.5f);
                                binding.btnShow.setAlpha(0.5f);
                                binding.accountInfoEmailNotification.setAlpha(0.5f);
                                binding.btnVerify.setAlpha(0.5f);

                                binding.edtPassoword.setClickable(false);
                                binding.edtEmail.setClickable(false);
                                binding.edtPassoword.setFocusable(false);
                                binding.edtEmail.setFocusable(false);

                                binding.accountInfoEmailNotification.setClickable(false);
                                binding.btnShow.setClickable(false);
                                binding.btnVerify.setClickable(false);
                            } else {
                                Toast.makeText(getContext(), "Email already used.", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
       /* driverRegistrationViewModel.observeRegister().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS: {
                            application.dialogManager.dismissProgressDialog();
                            driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable(SignUpPhotoActivity.bitmapPhoto),
                                    UUID.randomUUID() + AppConstant.PHOTO + ".jpg");

                        }
                        break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "" + stateResource.message, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });*/

       /* driverRegistrationViewModel.observeDriverDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                driverRegistrationRequest.setPhotoURl(url);
                                binding.llPersonalInfo.setVisibility(View.VISIBLE);
                                binding.llayoutAccountInfo.setEnabled(false);

                                binding.edtPassoword.setAlpha(0.5f);
                                binding.edtEmail.setAlpha(0.5f);
                                binding.btnShow.setAlpha(0.5f);
                                binding.accountInfoEmailNotification.setAlpha(0.5f);
                                binding.btnVerify.setAlpha(0.5f);

                                binding.edtPassoword.setClickable(false);
                                binding.edtEmail.setClickable(false);
                                binding.edtPassoword.setFocusable(false);
                                binding.edtEmail.setFocusable(false);

                                binding.accountInfoEmailNotification.setClickable(false);
                                binding.btnShow.setClickable(false);
                                binding.btnVerify.setClickable(false);
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/
    }

}



