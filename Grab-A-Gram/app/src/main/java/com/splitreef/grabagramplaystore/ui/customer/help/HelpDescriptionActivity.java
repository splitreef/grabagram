package com.splitreef.grabagramplaystore.ui.customer.help;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.help.Help;
import com.splitreef.grabagramplaystore.databinding.ActivityHelpDescriptionBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;

public class HelpDescriptionActivity extends BaseActivity {

    private ActivityHelpDescriptionBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityHelpDescriptionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    @SuppressLint("SetTextI18n")
    public void initView()
    {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        if (getIntent()!=null)
        {
            String from=getIntent().getStringExtra(AppConstant.FROM);

            if (from.equals(AppConstant.FROM_HELP_SCREEN))
            {
                Help help= (Help) getIntent().getSerializableExtra(AppConstant.HELP_MODEL);

                binding.txtTitle.setText(""+help.getTitle());
                binding.txtDescription.setText(""+help.getDescription());
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }
}
