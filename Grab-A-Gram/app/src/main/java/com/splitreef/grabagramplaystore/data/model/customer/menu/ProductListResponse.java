package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductListResponse implements Serializable {

    private ArrayList<Category> categories;
    private long created_at;
    private String description;
    private String dispensary_id;
    private String feature_image;
    private String id;
    private boolean isDeleted;
    private boolean isPublic;
    private String name;
    private ArrayList<String> product_gallery;
    private double regular_price;
    private double sale_price;
    private String sku;
    private int stock_quantity;
    private int tax_rate;
    private long updated_at;
    private ArrayList<Weights>weights;
    private ArrayList<RelatedProduct>relatedProducts;
    private StainAttributes stain_attributes;

    private String busniess_name;
    private double totalPrice;
    private int quantity;
    private String about_us;
    private String weightNameValue;
    private float rating;
    private String dispensaryUserId;
    private double deliveryFee;
    private String badgeCount;
    private String dispensaryId;
    private  ArrayList<ProductReviewModel>productReviewList;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDispensary_id() {
        return dispensary_id;
    }

    public void setDispensary_id(String dispensary_id) {
        this.dispensary_id = dispensary_id;
    }

    public String getFeature_image() {return feature_image;
    }


    public void setFeature_image(String feature_image) {
        this.feature_image = feature_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getProduct_gallery() {
        return product_gallery;
    }

    public void setProduct_gallery(ArrayList<String> product_gallery) {
        this.product_gallery = product_gallery;
    }

    public double getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(double regular_price) {
        this.regular_price = regular_price;
    }

    public double getSale_price() {
        return sale_price;
    }

    public void setSale_price(double sale_price) {
        this.sale_price = sale_price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(int stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public int getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(int tax_rate) {
        this.tax_rate = tax_rate;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<Weights> getWeights() {
        return weights;
    }

    public void setWeights(ArrayList<Weights> weights) {
        this.weights = weights;
    }

    public ArrayList<RelatedProduct> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(ArrayList<RelatedProduct> relatedProducts) {
        this.relatedProducts = relatedProducts;

    }
    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public String getWeightNameValue() {
        return weightNameValue;
    }

    public void setWeightNameValue(String weightNameValue) {
        this.weightNameValue = weightNameValue;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDispensaryUserId() {
        return dispensaryUserId;
    }

    public void setDispensaryUserId(String dispensaryUserId) {
        this.dispensaryUserId = dispensaryUserId;
    }

    public double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(String badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getDispensaryId() {
        return dispensaryId;
    }

    public void setDispensaryId(String dispensaryId) {
        this.dispensaryId = dispensaryId;
    }

    public StainAttributes getStain_attributes() {
        return stain_attributes;
    }

    public void setStain_attributes(StainAttributes stain_attributes) {
        this.stain_attributes = stain_attributes;
    }

    public ArrayList<ProductReviewModel> getProductReviewList() {
        return productReviewList;
    }

    public void setProductReviewList(ArrayList<ProductReviewModel> productReviewList) {
        this.productReviewList = productReviewList;
    }
}
