package com.splitreef.grabagramplaystore.viewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.driver.orders.myAccount.MyAccountViewModel;


/**
 * Created by  on 05-01-2021.
 */
public class DriverMyAccountProviderFactory implements ViewModelProvider.Factory {
    private final FirebaseRepository firebaseRepository;


    public DriverMyAccountProviderFactory(FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(MyAccountViewModel.class)) {
            return (T) new MyAccountViewModel(firebaseRepository);
        }

        throw new IllegalArgumentException("View model not found");
    }
}





