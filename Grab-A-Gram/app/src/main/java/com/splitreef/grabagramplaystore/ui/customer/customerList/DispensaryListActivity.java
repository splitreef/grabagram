package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.ui.customer.cart.CartActivity;
import com.splitreef.grabagramplaystore.ui.customer.dashboard.DashboardActivity;
import com.splitreef.grabagramplaystore.ui.customer.help.HelpListFragment;
import com.splitreef.grabagramplaystore.ui.customer.manageAddress.ManageAddressFragment;
import com.splitreef.grabagramplaystore.ui.customer.myAccount.CustomerMyAccountFragment;
import com.splitreef.grabagramplaystore.ui.customer.pastOrders.PastOrderFragment;
import com.splitreef.grabagramplaystore.ui.customer.scheduledOrder.ScheduledOrderFragment;
import com.splitreef.grabagramplaystore.ui.customer.trackOrder.TrackOrderFragment;
import com.splitreef.grabagramplaystore.ui.customer.wishList.WishListFragment;
import com.splitreef.grabagramplaystore.ui.login.LoginActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryListViewModelProviderFactory;

import java.util.Objects;


public class DispensaryListActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DispensaryListViewModel viewModel;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    public MaterialTextView toolbarTitle;
    TextView txtCartCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispensary_list);
        initView();
        initListeners();

    }

    // Initialized view of this screen.
    public void initView() {

        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbarTitle=findViewById(R.id.toolbar_title);
        drawerLayout=findViewById(R.id.drawer_layout);
        NavigationView navigationView =findViewById(R.id.nav_view);
       // View headerView=navigationView.getHeaderView(0);
       // MaterialTextView search=headerView.findViewById(R.id.search);


        GrabAGramApplication application = (GrabAGramApplication) getApplicationContext();
        viewModel=new ViewModelProvider(getViewModelStore(), new DispensaryListViewModelProviderFactory(getApplication(), application.firebaseRepository)).get(DispensaryListViewModel.class);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolbarTitle("Dispensary List");

        navigationView.setNavigationItemSelectedListener(this);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.mipmap.menu);

        if (getIntent() != null) {
            String from = getIntent().getStringExtra(AppConstant.FROM);
            if (from.equals(AppConstant.FROM_CHECKOUT_SCREEN)) {
                String _for = getIntent().getStringExtra(AppConstant.FOR);
                if (_for.equals(AppConstant.FOR_MANAGE_ADDRESS)) {
                    replaceFragment(new ManageAddressFragment(), false);
                } else if (_for.equals(AppConstant.FOR_DISPENSARY_LIST)) {
                    replaceFragment(new DispensaryHomeFragment(), false);
                }

            } else if (from.equals(AppConstant.FROM_DASHBOARD_SCREEN)) {
                String _for = getIntent().getStringExtra(AppConstant.FOR);
                if (_for.equals(AppConstant.FOR_DISPENSARY_LIST)) {
                    replaceFragment(new DispensaryHomeFragment(), false);
                } else if (_for.equals(AppConstant.FOR_MY_ACCOUNT)) {
                    replaceFragment(new CustomerMyAccountFragment(), false);
                } else if (_for.equals(AppConstant.FOR_PAST_ORDERS)) {
                    replaceFragment(new PastOrderFragment(), false);
                } else if (_for.equals(AppConstant.FOR_TRACK_ORDERS)) {
                    replaceFragment(new TrackOrderFragment(), false);
                }

            }
        }

    }

    void initListeners() {

        actionBarDrawerToggle.setToolbarNavigationClickListener(view -> {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBadgeCount();
    }

    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }

    public void replaceFragment(Fragment fragment, boolean isAllowBack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (isAllowBack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_home:
                moveToDashBoardScreen();
                break;
            case R.id.nav_scheduled_orders:
                replaceFragment(new ScheduledOrderFragment(), true);
                break;
            case R.id.nav_track_your_order:
                replaceFragment(new TrackOrderFragment(), true);
                break;
            case R.id.nav_past_orders:
                replaceFragment(new PastOrderFragment(), true);
                break;
            case R.id.nav_my_account:
                replaceFragment(new CustomerMyAccountFragment(), true);
                break;
            case R.id.nav_manage_addresses:
                replaceFragment(new ManageAddressFragment(), true);
                break;
            case R.id.nav_wish_list:
                replaceFragment(new WishListFragment(), true);
                break;
            case R.id.nav_help:
                replaceFragment(new HelpListFragment(), true);
                break;
            case R.id.nav_logout:
                logout();
                break;
        }

        drawerLayout.closeDrawers();
        return false;
    }


    public void moveToDashBoardScreen() {
        Intent intent = new Intent(DispensaryListActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void logout() {
        new AlertDialog() {
            @Override
            public void onPositiveButtonClick() {
                viewModel.userLogout();
                PreferenceManger.getPreferenceManger().clearSession();
                Intent intent = new Intent(DispensaryListActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        }.showAlertDialog(DispensaryListActivity.this,"Logout",getString(R.string.would_you_like_to_logout),"Yes","No");
    }


    @SuppressLint("SetTextI18n")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dispensary_list_top_menu, menu);
        View cartView = menu.findItem(R.id.action_cart).getActionView();
        txtCartCount = cartView.findViewById(R.id.btn_cart_count);

        MenuItem actionSearch = menu.findItem(R.id.action_search);
        actionSearch.setVisible(false);

        cartView.setOnClickListener(view -> {
            startActivity(new Intent(DispensaryListActivity.this, CartActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.left_out);

        });

        String badgeCount = PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
        Log.d("badgeCount==", "===" + badgeCount);

        if (badgeCount != null && (!badgeCount.equals("0"))) {
            txtCartCount.setVisibility(View.VISIBLE);
            txtCartCount.setText(badgeCount + "");
        } else {
            txtCartCount.setVisibility(View.GONE);
        }

        return super.onCreateOptionsMenu(menu);
    }



    @SuppressLint("SetTextI18n")
    public void updateBadgeCount(){

        String badgeCount = PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
        Log.d("badgeCount==", "===" + badgeCount);

        if (txtCartCount != null) {
            if (badgeCount != null && (!badgeCount.equals("0"))) {
                txtCartCount.setVisibility(View.VISIBLE);
                txtCartCount.setText(badgeCount + "");
            } else {
                txtCartCount.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_cart) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.close);
    }
}
