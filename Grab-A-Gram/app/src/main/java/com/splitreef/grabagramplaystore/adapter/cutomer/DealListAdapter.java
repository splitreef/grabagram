package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class DealListAdapter extends RecyclerView.Adapter<DealListAdapter.DealListHolder> {

    private Context context;
    private ArrayList<DealModel> dispensaryModelArrayList;

    public DealListAdapter(@NonNull Context context, ArrayList<DealModel> dispensaryModelArrayList) {
        this.context = context;
        this.dispensaryModelArrayList = dispensaryModelArrayList;
    }

    @NonNull
    @Override
    public DealListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deal_list_view, parent, false);
        return new DealListHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull DealListHolder holder, int position) {
        DealModel dealModel = dispensaryModelArrayList.get(position);

        PicassoManager.setImage(dealModel.getImage(), holder.dealImage);
        //  holder.dealTitle.setText("Get "+dealModel.getPercentage()+"% with coupon code "+dealModel.getCoupon_code_name());
        holder.dealTitle.setText(dealModel.getCoupon_code_name());
        holder.expiredDate.setText("Expires " + AppUtil.convertMilliSecondsToDate(dealModel.getExpiration_date(), AppUtil.DATE_FORMAT_DD_MM_YYYY));
        holder.aboutDeal.setText(dealModel.getCoupon_description());
        holder.percentageOff.setText(dealModel.getPercentage() + "%");

        holder.dealTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // For copy text
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("text", holder.dealTitle.getText().toString());
                clipboard.setPrimaryClip(clip);

                Toast.makeText(context, "Copied " + holder.dealTitle.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return dispensaryModelArrayList.size();
    }

    class DealListHolder extends RecyclerView.ViewHolder {
        AppCompatImageView dealImage;
        MaterialTextView dealTitle;
        MaterialTextView aboutDeal;
        MaterialTextView expiredDate;
        MaterialTextView percentageOff;
        LinearLayout llTop;

        public DealListHolder(@NonNull View itemView) {
            super(itemView);

            dealImage = itemView.findViewById(R.id.img_deal);
            dealTitle = itemView.findViewById(R.id.deal_title);
            expiredDate = itemView.findViewById(R.id.expires_date);
            aboutDeal = itemView.findViewById(R.id.txt_about);
            llTop = itemView.findViewById(R.id.ll_top);
            percentageOff = itemView.findViewById(R.id.percentage_off);

        }
    }
}
