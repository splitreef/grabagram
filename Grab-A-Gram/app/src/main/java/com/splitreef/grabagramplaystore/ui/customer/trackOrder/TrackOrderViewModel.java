package com.splitreef.grabagramplaystore.ui.customer.trackOrder;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverLocation;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfileResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrackOrderViewModel extends ViewModel {

    private static final String TAG = "ScheduledOrderViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetTrackOrders = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetDriverProfile = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetDriverLocation = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public TrackOrderViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getTrackOrders()
    {
         firebaseRepository.getTrackOrders()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<GetPlacedOrdersResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {
                         disposable.add(d);
                         onGetTrackOrders.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull GetPlacedOrdersResponse placedOrdersResponse) {

                         onGetTrackOrders.setValue(DataResource.DataStatus(placedOrdersResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onGetTrackOrders.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }


    /*public  void getDriverLocation2(String driverId,String orderId)
    {
        firebaseRepository.getDriverLocation(driverId,orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()
                .subscribe(new SingleObserver<DriverLocation>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetDriverLocation.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverLocation driverLocation) {

                        onGetDriverLocation.setValue(DataResource.DataStatus(driverLocation));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetDriverLocation.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }*/


    public  void getDriverLocation(String driverId,String orderId)
    {
        firebaseRepository.getDriverLocation(driverId,orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .subscribe(new Observer<DriverLocation>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                      }

                    @Override
                    public void onNext(@NonNull DriverLocation driverLocation) {

                        onGetDriverLocation.setValue(DataResource.DataStatus(driverLocation));

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDriverLocation.setValue(DataResource.DataStatusError(e.getMessage()));
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    public  void getDriverProfileData(String driverId)
    {
        firebaseRepository.getDriverProfileData(driverId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DriverProfileResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetDriverProfile.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverProfileResponse driverProfileResponse) {

                        onGetDriverProfile.setValue(DataResource.DataStatus(driverProfileResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetDriverProfile.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeGetTrackOrders()
    {
        return onGetTrackOrders;
    }

    LiveData<DataResource> observeGetDriverProfileData()
    {
        return onGetDriverProfile;
    }

    LiveData<DataResource> observeGetDriverLocation()
    {
        return onGetDriverLocation;
    }




}
