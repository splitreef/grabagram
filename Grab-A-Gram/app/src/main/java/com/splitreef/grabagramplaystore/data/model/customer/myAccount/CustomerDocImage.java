package com.splitreef.grabagramplaystore.data.model.customer.myAccount;

public class CustomerDocImage {
    private String address_proof;
    private String doctor_recommendation_letter;
    private String government_photo_id;

    public String getAddress_proof() {
        return address_proof;
    }

    public void setAddress_proof(String address_proof) {
        this.address_proof = address_proof;
    }

    public String getDoctor_recommendation_letter() {
        return doctor_recommendation_letter;
    }

    public void setDoctor_recommendation_letter(String doctor_recommendation_letter) {
        this.doctor_recommendation_letter = doctor_recommendation_letter;
    }

    public String getGovernment_photo_id() {
        return government_photo_id;
    }

    public void setGovernment_photo_id(String government_photo_id) {
        this.government_photo_id = government_photo_id;
    }
}
