package com.splitreef.grabagramplaystore.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

public class AppConstant {

    public static final String SERVER_KEY = "AAAA9llwam4:APA91bGwRrJQKVoCeRj38u3R6sTaOO2m7TmYDgvVmNo-_gqYqDdfttNglKk0bTnqtSv7-Q0MEQzYkq7CEHALbcQQIfa4YAMgfaTAXkdD5DBnjBUF0PhpphqtJUeUNdaM5xG25RL1OMkB";
    public static final String SERVER_KEY_ = "key=" + SERVER_KEY;
    public static final String FCM_API = "https://fcm.googleapis.com/fcm/send";
    public static final String CONTENT_TYPE = "application/json";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String DRIVER_OPERATION = "driver_operation";
    public static final String ORDER_ID = "order_id";
    public static final String USER_TYPE = "user_type";
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String BADGE_COUNT = "badgeCount";
    public static final String SOUND = "sound";
    public static final int DEFAULT_DISTANCE = 40;
    //public static final String NET_AUTHORISE_CLIENT_KEY = "53vuzh6PBEg2Ye9ftUSUK8XmP7tgJLHTD3HZpMU9JeX6z4ksqt5p4TT22emNUpT6";
    public static final String NET_AUTHORISE_CLIENT_KEY = "99ELP7X75kDjN3N5LvQY6FranLPvQta8nWXNa86eu7xXs22G96JvXGLq7hyH5fab";
    //public static final String NET_AUTHORISE_API_LOGIN_ID = "5yAuU5Y9f7ez";
    public static final String NET_AUTHORISE_API_LOGIN_ID = "5Rk269qX2";
    public static final String TAX_JAR_LIVE_TOKEN = "c3c952ff1032617ea21e3396238d1bed";


    public static final String DEVICE_TYPE = "Android";
    public static final String CUSTOMER_ROLE = "Customer";
    public static final String DRIVER_ROLE = "Driver";

    public static final String KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS = "Personal Address";
    public static final String KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS = "Primary Address";
    public static final String KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS = "Secondary Address";
    public static final String KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS = "Billing Address";
    public static final String KEY_ADDRESS_TYPE_DEFAULT_OTHER_ADDRESS = "OTHER";


    public static final String GOOGLE_API_KEY = "AIzaSyDO6PDftrAIISl0H5qPPXNluhP7yu1vmwY";
    public static final String GOOGLE_API_URL = "https://maps.googleapis.com/maps/api/geocode/json";
    public static final String GOOGLE_DIRECTIONS_API_URL = "https://maps.googleapis.com/maps/api/directions/json";
    public static final String GOOGLE_DISTANCE_MATRIX_API_URL = "https://maps.googleapis.com/maps/api/distancematrix/json";
    public static final String PAYMENT_API = "http://work.splitreef.com/client/development/grab-gram/api/payment-api";
    public static final String TAX_JAR_API = "https://api.taxjar.com/v2/taxes";


    // Firebase collection name
    public static final String USER_COLLECTION = "User";
    public static final String DISPENSARY_WEB_COLLECTION = "dispensary";
    public static final String CUSTOMER_COLLECTION = "Customer";
    public static final String COUPON_COLLECTION = "Coupon";
    public static final String DRIVER_LOCATION_STATUS = "driver_location_status";
    public static final String PRODUCT_COLLECTION = "product";
    public static final String PRODUCT_CATEGORIES = "product_categories";
    public static final String DISTANCE_COLLECTION = "Distance";
    public static final String HELP_COLLECTION = "Help";
    public static final String WEIGHTS_SUB_COLLECTION = "weights";
    public static final String RELATED_PRODUCTS_SUB_COLLECTION = "related_products";
    public static final String REVIEWS_SUB_COLLECTION = "reviews";


    public static final String DRIVER_COLLECTION = "Driver";
    public static final String DRIVER_DECLINE_ORDER = "driver_decline_order";
    public static final String ORDER_COLLECTION = "Order";
    public static final String USER_LOCATION = "user_location";
    public static final String VEHICLE_COMPANY = "vehicle_company";
    public static final String VEHICLE_MODEL = "vehicle_model";


    // Sorting type status.

    public static final String ALPHABETICALLY_A_Z_SORTING_TYPE = "alphabetically_A_Z";
    public static final String ALPHABETICALLY_Z_A_SORTING_TYPE = "alphabetically_Z_A";
    public static final String DISTANCE_MAX_MIN_SORTING_TYPE = "distance_max_min";
    public static final String DISTANCE_MIN_MAX_SORTING_TYPE = "distance_min_max";
    public static final String RATING_HIGH_LOW_SORTING_TYPE = "rating_high_low";
    public static final String RATING_LOW_HIGH_SORTING_TYPE = "rating_low_high";

    // Filter type status
    public static final String DISTANCE_FILTER_TYPE = "distance_filter_type";
    public static final String CATEGORY_FILTER_TYPE = "category_filter_type";


    //public static final String COUPON_NOT_VALID = "Coupon code is not valid.";
    public static final String COUPON_NOT_VALID = "Please enter the valid coupon code.";
    public static final String COUPON_DISPENSARY_NOT_MATCH = "Coupon code is not exist in the dispensary.";
    public static final String COUPON__PRODUCTS_NOT_MATCH = "Coupon code is not apply in this products.";
    public static final String USER_COUPON_LIMIT = "User coupon limit exceeded.";


    // order status
    public static final String ORDER_TYPE_SCHEDULED = "SCHEDULED";
    public static final String ORDER_TYPE_ACCEPT = "ACCEPT";
    public static final String ORDER_TYPE_CANCELED = "CANCELED";
    public static final String ORDER_TYPE_PICKED_UP_READY = "PICKEDUP_READY";
    public static final String ORDER_TYPE_PICKED = "PICKED";
    public static final String ORDER_TYPE_COMPLETED = "COMPLETED";
    public static final String ORDER_TYPE_DELIVERY_FAILED = "DELIVERY_FAILED";
    public static final String ORDER_TYPE_RETURN_PENDING = "RETURN_PENDING";
    public static final String ORDER_TYPE_RETURN_COMPLETE = "RETURN";


    public static final String ONLINE = "Online";
    public static final String TAP_TO_GO_ONLINE = "Tap to go Online";
    public static final String TAP_TO_GO_OFFLINE = "Tap to go Offline";


    // response status
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";


    // For screen navigation constant flag
    public static final String FROM = "from";
    public static final String FOR = "for";
    public static final String FOR_MY_ACCOUNT = "forMyAccount";
    public static final String FOR_PAST_ORDERS = "forPastOrders";
    public static final String FOR_TRACK_ORDERS = "forTrackOrders";
    public static final String FOR_DISPENSARY_LIST = "forDispensaryList";
    public static final String FOR_MANAGE_ADDRESS = "forManageAddress";
    public static final String FROM_LOGIN = "fromLogin";
    public static final String FROM_SPLASH = "fromSplash";
    public static final String FROM_ACCOUNT_CHOOSER = "fromAccountChooser";
    public static final String DISPENSARY_MODEL = "dispensaryModel";
    public static final String PRODUCT_RESPONSE = "productResponse";
    public static final String MANAGE_ADDRESS_SCREEN = "manageAddressScreen";
    public static final String FOR_ADD_ADDRESS = "forAddAddress";
    public static final String FOR_UPDATE_ADDRESS = "forUpdateAddress";
    public static final String FROM_CART_SCREEN = "fromCartActivity";
    public static final String FROM_CHECKOUT_SCREEN = "fromCheckoutActivity";
    public static final String FROM_DASHBOARD_SCREEN = "fromDashboardActivity";
    public static final String FROM_HELP_SCREEN = "fromHelpScreen";
    public static final String FROM_SCHEDULED_ORDER_ADAPTER = "fromScheduledOrderAdapter";
    public static final String ID = "ID";
    public static final String CANCEL_ORDER = "cancel_order";
    public static final String READY_FOR_PICKUP = "ready_for_pickp";
    public static final String ORDERS_NEAR_YOU = "orders+near_you";


    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";


    // For model class name
    public static final String GET_PRODUCT_CART_RESPONSE = "getProductCartResponse";
    public static final String ORDER_MODEL = "order";
    public static final String HELP_MODEL = "help";
    public static final String DRIVER_PROFILE_RESPONSE = "driverProfileResponse";
    public static final String ORDER_RESPONSE = "orderResponse";

    // Image name
    public static final String IMAGE_PERSONAL_ADDRESS = "personal_address_proof_photo";
    public static final String IMAGE_PRIMARY_ADDRESS = "primary_address_photo";
    public static final String IMAGE_SECONDARY_SHIPPING_ADDRESS = "secondary_shipping_address_photo";
    public static final String IMAGE_DOCTOR_RECOMMENDATION_LETTER = "doctor_recommendation_letter_photo";
    public static final String IMAGE_GOVERNMENT_PHOTO_ID = "government_photo_id";

    //Order status
    public static final String ORDER_ACCEPTED = "Accepted";//"Order Accepted";
    public static final String ORDER_PICKED = "Picked";//"Order Picked";
    public static final String ORDER_COMPLETED = "Completed";//"Order Completed";
    public static final String ORDER_DELIVERY_FAILED = "Delivery failed";//"Order delivery failed";
    public static final String COMPLETED = "COMPLETED";
    public static final String DELIVERY_FAILED = "DELIVERY_FAILED";
    public static final String PICKED = "PICKED";
    public static final String RETURN_PENDING = "RETURN_PENDING";
    public static final String ACCEPT = "ACCEPT";
    public static final String SCHEDULED = "SCHEDULED";


    public static final String OFFLINE = "Offline";
    public static final String PHOTO = "photo";
    public static final String CAR_INSURANCE_POLICY = "car_insurance_policy";
    public static final String DRIVING_LICENCE = "driving_license";
    public static final String CARE_GIVER_CERTIFICATE = "caregiver_certificate";
    public static final String CAR_REGISTRATION = "car_registration";
    public static final String EMAIL = "grabagram.info@gmail.com";


    public static String getDeviceId(Context context) {
        @SuppressLint("HardwareIds") String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

}
