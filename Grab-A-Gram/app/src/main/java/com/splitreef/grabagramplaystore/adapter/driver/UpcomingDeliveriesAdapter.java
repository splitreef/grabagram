package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.GetDistance;

import java.util.ArrayList;

/**
 * Created by  on 03-12-2020.
 */
public class UpcomingDeliveriesAdapter extends RecyclerView.Adapter<UpcomingDeliveriesAdapter.UpcomingDeliveriesHolder> {
    private ArrayList<OrdersModel> ordersList;
    private Context context;
    private double latitude;
    private double longitude;


    public UpcomingDeliveriesAdapter(@NonNull Context context, ArrayList<OrdersModel> ordersList, double latitude, double longitude) {
        this.context = context;
        this.ordersList = ordersList;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public ArrayList<OrdersModel> getDispensaryList() {
        return ordersList;
    }

    @NonNull
    @Override
    public UpcomingDeliveriesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_upcoming_delivery_view, parent, false);
        return new UpcomingDeliveriesHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull UpcomingDeliveriesHolder holder, int position) {

        OrdersModel ordersModel = ordersList.get(position);
        holder.tv_order_number.setText("#" + ordersModel.getOrder_no());
        holder.tv_schedule_for.setText("Schedule for " + AppUtil.getDateFromMillisecondTime(ordersModel.getCreated_at()) +
                " at " + AppUtil.getTimeFromMillisecondTime(ordersModel.getCreated_at()));
        double distance = new GetDistance().distance(latitude, longitude, ordersModel.getDispensary_location().getLatitude(), ordersModel.getDispensary_location().getLongitude());
        holder.tv_miles_away.setText(String.format("%.2f %s", distance, "MILES AWAY"));

        holder.ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OrdersDetailsActivity.class)
                        .putExtra(AppConstant.ID, ordersModel.getId()));
            }
        });


    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    class UpcomingDeliveriesHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_order_number, tv_schedule_for;
        MaterialTextView tv_miles_away, tv_time_in_minute;
        private RelativeLayout rel_time_in_minute;
        private LinearLayout ll_top;

        public UpcomingDeliveriesHolder(@NonNull View itemView) {
            super(itemView);
            tv_order_number = itemView.findViewById(R.id.tv_order_number);
            tv_schedule_for = itemView.findViewById(R.id.tv_schedule_for);
            tv_miles_away = itemView.findViewById(R.id.tv_miles_away);
            tv_time_in_minute = itemView.findViewById(R.id.tv_time_in_minute);
            rel_time_in_minute = itemView.findViewById(R.id.rel_time_in_minute);
            ll_top = itemView.findViewById(R.id.ll_top);

        }
    }
}
