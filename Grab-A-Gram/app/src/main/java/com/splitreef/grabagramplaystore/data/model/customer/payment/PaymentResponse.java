package com.splitreef.grabagramplaystore.data.model.customer.payment;

import java.io.Serializable;

public class PaymentResponse implements Serializable {

    private int status;
    private String getTransId;
    private String getResponseCode;
    private String getAuthCode;
    private String accountType;
    private String getDescription;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGetTransId() {
        return getTransId;
    }

    public void setGetTransId(String getTransId) {
        this.getTransId = getTransId;
    }

    public String getGetResponseCode() {
        return getResponseCode;
    }

    public void setGetResponseCode(String getResponseCode) {
        this.getResponseCode = getResponseCode;
    }

    public String getGetAuthCode() {
        return getAuthCode;
    }

    public void setGetAuthCode(String getAuthCode) {
        this.getAuthCode = getAuthCode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getGetDescription() {
        return getDescription;
    }

    public void setGetDescription(String getDescription) {
        this.getDescription = getDescription;
    }
}
