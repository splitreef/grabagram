package com.splitreef.grabagramplaystore.data.model.customer;

import java.io.Serializable;

public class CustomerRegisterRequest implements Serializable {


    // For account info
    boolean isActivateAt;
    String role;
    String username;
    String email;
    String password;
    String deviceId;
    String deviceType;
    String  userId;
    boolean isEmailCommunication;

    //For Personal info
    String firstName;
    String lastName;
    String streetAddress;
    String streetAddress2;
    String city;
    String state;
    String zipCode;
    String mobileNumber;
    long dateOfBirth;
    boolean isSendNotification;
    double latitude;
    double longitude;

    // For documentation

    String docType;
    String issuingState;
    String identityNumber;
    long expirationDate;
    String patientId;
    long doctorsRecExpirationDate;
    String addressProofPhotoUrl;
    String doctorRecPhotoUrl;
    String govPhotoIDUrl;

    byte[] addressProofPhotoBytes;
    byte[] doctorRecPhotoBytes;
    byte[] govPhotoIDUrlBytes;



    // For Member Agreement
    String eSignature;

    public boolean isActivateAt() {
        return isActivateAt;
    }

    public void setActivateAt(boolean activateAt) {
        isActivateAt = activateAt;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }




    //---------------------------------For personal info-------------------------------

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isSendNotification() {
        return isSendNotification;
    }

    public void setSendNotification(boolean sendNotification) {
        isSendNotification = sendNotification;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    //---------------------------------For Documents information-------------------------------


    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getIssuingState() {
        return issuingState;
    }

    public void setIssuingState(String issuingState) {
        this.issuingState = issuingState;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public long getDoctorsRecExpirationDate() {
        return doctorsRecExpirationDate;
    }

    public void setDoctorsRecExpirationDate(long doctorsRecExpirationDate) {
        this.doctorsRecExpirationDate = doctorsRecExpirationDate;
    }

    public String getAddressProofPhotoUrl() {
        return addressProofPhotoUrl;
    }

    public void setAddressProofPhotoUrl(String addressProofPhotoUrl) {
        this.addressProofPhotoUrl = addressProofPhotoUrl;
    }

    public String getDoctorRecPhotoUrl() {
        return doctorRecPhotoUrl;
    }

    public void setDoctorRecPhotoUrl(String doctorRecPhotoUrl) {
        this.doctorRecPhotoUrl = doctorRecPhotoUrl;
    }

    public String getGovPhotoIDUrl() {
        return govPhotoIDUrl;
    }

    public void setGovPhotoIDUrl(String govPhotoIDUrl) {
        this.govPhotoIDUrl = govPhotoIDUrl;
    }


    public String geteSignature() {
        return eSignature;
    }

    public void seteSignature(String eSignature) {
        this.eSignature = eSignature;
    }

    public boolean isEmailCommunication() {
        return isEmailCommunication;
    }

    public void setEmailCommunication(boolean emailCommunication) {
        isEmailCommunication = emailCommunication;
    }

    public byte[] getAddressProofPhotoBytes() {
        return addressProofPhotoBytes;
    }

    public void setAddressProofPhotoBytes(byte[] addressProofPhotoBytes) {
        this.addressProofPhotoBytes = addressProofPhotoBytes;
    }

    public byte[] getDoctorRecPhotoBytes() {
        return doctorRecPhotoBytes;
    }

    public void setDoctorRecPhotoBytes(byte[] doctorRecPhotoBytes) {
        this.doctorRecPhotoBytes = doctorRecPhotoBytes;
    }

    public byte[] getGovPhotoIDUrlBytes() {
        return govPhotoIDUrlBytes;
    }

    public void setGovPhotoIDUrlBytes(byte[] govPhotoIDUrlBytes) {
        this.govPhotoIDUrlBytes = govPhotoIDUrlBytes;
    }
}
