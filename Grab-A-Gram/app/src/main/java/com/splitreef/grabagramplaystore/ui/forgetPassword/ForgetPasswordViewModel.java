package com.splitreef.grabagramplaystore.ui.forgetPassword;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by  on 21-01-2021.
 */
public class ForgetPasswordViewModel extends ViewModel {

    private static final String TAG = "ForgetPasswordViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<StateResource> onForgetPassword = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public ForgetPasswordViewModel(Application application, FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }

    public void forgetPassword(String emailAddress) {
        firebaseRepository.forgetPassword(emailAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onForgetPassword.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onForgetPassword.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onForgetPassword.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }


    LiveData<StateResource> observeForgetPassword() {
        return onForgetPassword;
    }


}
