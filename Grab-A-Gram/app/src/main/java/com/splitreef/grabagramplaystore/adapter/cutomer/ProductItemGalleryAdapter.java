package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.google.android.material.imageview.ShapeableImageView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import java.util.ArrayList;


public class ProductItemGalleryAdapter extends PagerAdapter {

    private final ArrayList<String> galleryList;
    private final Context context;

    public ProductItemGalleryAdapter(Context context,ArrayList<String> galleryList) {

        this.galleryList=galleryList;
        this.context=context;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate (R.layout.layout_product_gallery, container, false );
        container.addView (view);
        ShapeableImageView imgGallery=view.findViewById(R.id.product_image);
        PicassoManager.setImage(galleryList.get(position),imgGallery);
        return view;
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView ( view );
    }
}
