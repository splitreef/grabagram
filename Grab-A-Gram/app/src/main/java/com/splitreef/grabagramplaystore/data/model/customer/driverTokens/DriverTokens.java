package com.splitreef.grabagramplaystore.data.model.customer.driverTokens;

import java.util.ArrayList;

public class DriverTokens {

    private ArrayList<String> fcmToken;
    private ArrayList<String>drivers_id;
    private String status;

    public ArrayList<String> getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(ArrayList<String> fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getDrivers_id() {
        return drivers_id;
    }

    public void setDrivers_id(ArrayList<String> drivers_id) {
        this.drivers_id = drivers_id;
    }
}
