package com.splitreef.grabagramplaystore.adapter.driver;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersListFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersMapFragment;

/**
 * Created by  on 30-11-2020.
 */
public class DriverPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    int totalTabs;

    public DriverPagerAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position) {

            case 0:
                fragment = new PendingOrdersListFragment();
                break;
            case 1:
                fragment = new PendingOrdersMapFragment();
                break;

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
