package com.splitreef.grabagramplaystore.ui.customer.addNewAddress;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.actionsheet.ActionSheet;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.AddressRequestParams;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.databinding.ActivityNewAddressBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.AddNewAddressViewModelProviderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_KEY;
import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_URL;
import static com.splitreef.grabagramplaystore.utils.AppConstant.IMAGE_PRIMARY_ADDRESS;
import static com.splitreef.grabagramplaystore.utils.AppConstant.IMAGE_SECONDARY_SHIPPING_ADDRESS;

public class AddAddressActivity extends BaseActivity implements View.OnClickListener,ActionSheet.ActionSheetListener {

    private ActivityNewAddressBinding binding;
    private Bitmap bitmap;
    private AddNewAddressViewModel viewModel;
    private GrabAGramApplication application;
    private String photoUrl = null;
    AddressRequestParams addressRequestParams = null;
    String flag = "";
    UserAddressModel userAddressModel = null;
    ArrayAdapter<String> addressTypeAdapter;
    ArrayList<String> addressTypeList;
    private int textLength = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewAddressBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
        onClickListeners();

    }

    public void initView() {

        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.getStringExtra(AppConstant.FROM).equals(AppConstant.MANAGE_ADDRESS_SCREEN)) {
                if (intent.getStringExtra(AppConstant.FOR).equals(AppConstant.FOR_ADD_ADDRESS)) {
                    addressTypeList = intent.getStringArrayListExtra("addressTypeList");
                    setAddressTypeSpinner(addressTypeList);
                    flag = AppConstant.FOR_ADD_ADDRESS;

                } else if (intent.getStringExtra(AppConstant.FOR).equals(AppConstant.FOR_UPDATE_ADDRESS)) {
                    flag = AppConstant.FOR_UPDATE_ADDRESS;
                    userAddressModel = (UserAddressModel) intent.getSerializableExtra("userAddressModel");
                    if (userAddressModel != null) {
                        setAddressScreenFields(userAddressModel);
                    }
                    else
                    {
                        showToast("error!");
                    }

                }
            }
        }


        application = (GrabAGramApplication) getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new AddNewAddressViewModelProviderFactory(application.firebaseRepository)).get(AddNewAddressViewModel.class);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        if (flag.equals(AppConstant.FOR_ADD_ADDRESS)) {
            binding.toolbarTitle.setText("Add New Address");
            binding.btnSave.setText("Save");
        } else if (flag.equals(AppConstant.FOR_UPDATE_ADDRESS)) {
            binding.toolbarTitle.setText("Update Address");
            binding.btnSave.setText("Update");
        }

        binding.takePhoto.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);

        subscribeObservers();


    }

    void setAddressTypeSpinner(ArrayList<String> addressTypeList) {

        if (addressTypeList != null && addressTypeList.size() > 0) {
            addressTypeAdapter = new ArrayAdapter<String>(AddAddressActivity.this, R.layout.spinner_item, addressTypeList);
            binding.spinnerAddressType.setAdapter(addressTypeAdapter);
        }
    }


    void onClickListeners() {

        binding.spinnerAddressType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (adapterView.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS) || adapterView.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS)) {
                    binding.llPhotoProof.setVisibility(View.VISIBLE);
                    binding.cbIsPersonalAddress.setVisibility(View.GONE);

                    if (flag.equals(AppConstant.FOR_ADD_ADDRESS))
                    {
                        enableFields();
                    }

                } else if ((adapterView.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS))) {
                    binding.llPhotoProof.setVisibility(View.GONE);

                    if (flag.equals(AppConstant.FOR_ADD_ADDRESS))
                    {
                        binding.cbIsPersonalAddress.setChecked(false);
                        binding.cbIsPersonalAddress.setVisibility(View.VISIBLE);
                    }

                }
                else
                {
                    binding.cbIsPersonalAddress.setVisibility(View.GONE);
                    enableFields();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        binding.editPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.editPhoneNumber.getText().toString();
                textLength = binding.editPhoneNumber.getText().length();
                if (text.endsWith("-") || text.endsWith(" "))
                    return;
                if (textLength == 1) {
                    if (!text.contains("(")) {
                        binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                    }
                } else if (textLength == 5) {
                    if (!text.contains(")")) {
                        binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                    }
                } else if (textLength == 6) {
                    binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                } else if (textLength == 10) {
                    if (!text.contains("-")) {
                        binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                    }
                } else if (textLength == 15) {
                    if (text.contains("-")) {
                        binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                    }
                } else if (textLength == 18) {
                    if (text.contains("-")) {
                        binding.editPhoneNumber.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.editPhoneNumber.setSelection(binding.editPhoneNumber.getText().length());
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        binding.cbIsPersonalAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    viewModel.getCustomerMyAccountData();

                } else {
                    enableFields();
                }
            }
        });
    }

    // Field Validation
    boolean isValidation() {

        if (flag.equals(AppConstant.FOR_ADD_ADDRESS))
        {
            if (binding.spinnerAddressType.getSelectedItemPosition() == 0) {
                showToast(getString(R.string.select_address_type));
                return false;
            } else if (Objects.requireNonNull(binding.editFirstName.getText()).toString().equals("")) {
                binding.editFirstName.setError(getString(R.string.enter_your_first_name));
                binding.editFirstName.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editLastName.getText()).toString().equals("")) {
                binding.editLastName.setError(getString(R.string.enter_your_last_name));
                binding.editLastName.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editStreetAddress.getText()).toString().equals("")) {
                binding.editStreetAddress.setError(getString(R.string.enter_your_address));
                binding.editStreetAddress.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editCity.getText()).toString().equals("")) {
                binding.editCity.setError(getString(R.string.enter_your_city));
                binding.editCity.requestFocus();
                return false;
            } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
                showToast(getString(R.string.enter_your_state));
                return false;
            } else if (Objects.requireNonNull(binding.editZip.getText()).toString().equals("")) {
                binding.editZip.setError(getString(R.string.enter_your_zip_code));
                binding.editZip.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editZip.getText()).toString().trim().length() < 5) {
                binding.editZip.setError(getString(R.string.enter_your_valid_zip_code));
                binding.editZip.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editPhoneNumber.getText()).toString().equals("")) {
                binding.editPhoneNumber.setError(getString(R.string.enter_your_mobile_number));
                binding.editPhoneNumber.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editPhoneNumber.getText()).toString().length() < 14) {
                binding.editPhoneNumber.setError(getString(R.string.enter_your_valid_mobile_number));
                binding.editPhoneNumber.requestFocus();
                return false;
            } else if (photoUrl == null) {

                if (!binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS)) {
                    application.messageManager.DisplayToastMessage(getString(R.string.address_id_image));
                    return false;
                }

            }

        }
        else if (flag.equals(AppConstant.FOR_UPDATE_ADDRESS))
        {
            if (Objects.requireNonNull(binding.editFirstName.getText()).toString().equals("")) {
                binding.editFirstName.setError(getString(R.string.enter_your_first_name));
                binding.editFirstName.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editLastName.getText()).toString().equals("")) {
                binding.editLastName.setError(getString(R.string.enter_your_last_name));
                binding.editLastName.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editStreetAddress.getText()).toString().equals("")) {
                binding.editStreetAddress.setError(getString(R.string.enter_your_address));
                binding.editStreetAddress.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editCity.getText()).toString().equals("")) {
                binding.editCity.setError(getString(R.string.enter_your_city));
                binding.editCity.requestFocus();
                return false;
            } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
                showToast(getString(R.string.enter_your_state));
                return false;
            } else if (Objects.requireNonNull(binding.editZip.getText()).toString().equals("")) {
                binding.editZip.setError(getString(R.string.enter_your_zip_code));
                binding.editZip.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editZip.getText()).toString().trim().length() < 5) {
                binding.editZip.setError(getString(R.string.enter_your_valid_zip_code));
                binding.editZip.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editPhoneNumber.getText()).toString().equals("")) {
                binding.editPhoneNumber.setError(getString(R.string.enter_your_mobile_number));
                binding.editPhoneNumber.requestFocus();
                return false;
            } else if (Objects.requireNonNull(binding.editPhoneNumber.getText()).toString().length() < 14) {
                binding.editPhoneNumber.setError(getString(R.string.enter_your_valid_mobile_number));
                binding.editPhoneNumber.requestFocus();
                return false;
            } else if (photoUrl == null) {

                if (!binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS)) {
                    application.messageManager.DisplayToastMessage(getString(R.string.address_id_image));
                    return false;
                }

            }
        }


        return true;

    }


    void setAddressScreenFields(UserAddressModel userAddressModel) {
        ArrayList<String> addTypeList = new ArrayList<>();
        addTypeList.add(userAddressModel.getType());
        addressTypeAdapter = new ArrayAdapter<>(AddAddressActivity.this, R.layout.spinner_item, addTypeList);
        binding.spinnerAddressType.setAdapter(addressTypeAdapter);


        List<String> states = Arrays.asList(getResources().getStringArray(R.array.states));

        binding.editFirstName.setText(userAddressModel.getFirst_name());
        binding.editLastName.setText(userAddressModel.getLast_name());
        binding.editStreetAddress.setText(userAddressModel.getStreet_1());
        binding.editStreetAddress2.setText(userAddressModel.getStreet_2());
        binding.editCity.setText(userAddressModel.getCity());
        binding.spinnerState.setSelection(states.indexOf(userAddressModel.getState()));
        binding.editZip.setText(userAddressModel.getZip_code());



        if (userAddressModel.getPhone_number() != null && userAddressModel.getPhone_number().length() == 10) {
            StringBuilder phoneNumber = new StringBuilder(userAddressModel.getPhone_number()).insert(0, "(");
            phoneNumber = new StringBuilder(phoneNumber).insert(4, ")");
            phoneNumber = new StringBuilder(phoneNumber).insert(5, " ");
            phoneNumber = new StringBuilder(phoneNumber).insert(9, "-");
            binding.editPhoneNumber.setText(phoneNumber);
        }


        photoUrl = userAddressModel.getAddress_proof();


        if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS)) {
            binding.llPhotoProof.setVisibility(View.GONE);
        } else {
            binding.llPhotoProof.setVisibility(View.VISIBLE);
            PicassoManager.setImage(userAddressModel.getAddress_proof(), binding.photo);
        }
    }


    // open action sheet
    void openActionSheet() {
        ActionSheet.createBuilder(AddAddressActivity.this, getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Camera", "Gallery")
                .setCancelableOnTouchOutside(true)
                .setListener(this).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        if (index == 0) {
            requestStorageAndCameraPermission(AddAddressActivity.this, CAMERA_PIC);
        } else if (index == 1) {
            requestStorageAndCameraPermission(AddAddressActivity.this, GALLERY_PIC);
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.take_photo) {
            openActionSheet();
        } else if (view.getId() == R.id.btn_save) {
            if (isValidation()) {

                String tmp = Objects.requireNonNull(binding.editPhoneNumber.getText()).toString();
                String phoneNumber = tmp.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");

                addressRequestParams = new AddressRequestParams();
                addressRequestParams.setType(binding.spinnerAddressType.getSelectedItem().toString());
                addressRequestParams.setFirst_name(Objects.requireNonNull(binding.editFirstName.getText()).toString());
                addressRequestParams.setLast_name(Objects.requireNonNull(binding.editLastName.getText()).toString());
                addressRequestParams.setStreet_1(Objects.requireNonNull(binding.editStreetAddress.getText()).toString());
                addressRequestParams.setStreet_2(Objects.requireNonNull(binding.editStreetAddress2.getText()).toString());
                addressRequestParams.setCity(Objects.requireNonNull(binding.editCity.getText()).toString());
                addressRequestParams.setState(binding.spinnerState.getSelectedItem().toString());
                addressRequestParams.setZip_code(Objects.requireNonNull(binding.editZip.getText()).toString());
                addressRequestParams.setPhone_number(phoneNumber);

                 if (addressRequestParams.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS)) {
                     addressRequestParams.setAddress_proof("");
                 }
                else
                {
                    addressRequestParams.setAddress_proof(photoUrl);
                }

                addressRequestParams.setPersonAddress(false);

                String address = addressRequestParams.getStreet_1() + "," + addressRequestParams.getCity() + "," + addressRequestParams.getState() + "," + addressRequestParams.getZip_code();
                getLatLng(address);

            }
        } else if (view.getId() == R.id.btn_cancel) {
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedFileUri);
                    //  binding.photo.setImageBitmap(bitmap);
                    byte[] bytes = getFileDataFromDrawable(bitmap);

                    if (binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS)) {
                        viewModel.uploadCustomerDocPhoto(bytes, IMAGE_PRIMARY_ADDRESS);
                    } else if (binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS)) {
                        viewModel.uploadCustomerDocPhoto(bytes, IMAGE_SECONDARY_SHIPPING_ADDRESS);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {

                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    //   binding.photo.setImageBitmap(bitmap);
                    byte[] bytes = getFileDataFromDrawable(bitmap);

                    if (binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS)) {
                        viewModel.uploadCustomerDocPhoto(bytes, IMAGE_PRIMARY_ADDRESS);
                    } else if (binding.spinnerAddressType.getSelectedItem().toString().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS)) {
                        viewModel.uploadCustomerDocPhoto(bytes, IMAGE_SECONDARY_SHIPPING_ADDRESS);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            showToast("Something went wrong");
        }
    }

    void subscribeObservers() {
        viewModel.observeCustomerDocPhoto().observe(AddAddressActivity.this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(AddAddressActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                PicassoManager.setImage(url, binding.photo);
                                photoUrl = url;
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });


        viewModel.observeAddNewAddress().observe(AddAddressActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(AddAddressActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            finish();
                            showToast(getString(R.string.address_added));
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            showToast("Something went wrong!!" + stateResource.message);
                            Log.d("error", "" + stateResource.message);

                    }
                }
            }
        });

        viewModel.observeUpdateAddress().observe(AddAddressActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(AddAddressActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            finish();
                            showToast(getString(R.string.address_updated));
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            showToast("Something went wrong!!" + stateResource.message);
                            Log.d("error", "" + stateResource.message);

                    }
                }
            }
        });


        viewModel.observeGetMyAccountData().observe(Objects.requireNonNull(AddAddressActivity.this), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(AddAddressActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        MyAccountResponse myAccountResponse = (MyAccountResponse) dataResource.data;
                        if (myAccountResponse.getStatus().equals(AppConstant.SUCCESS)) {
                            updateUi(myAccountResponse);
                        }
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();

                }
            }
        });
    }


    private void updateUi(MyAccountResponse myAccountResponse) {

        List<String> states = Arrays.asList(getResources().getStringArray(R.array.states));

        if (myAccountResponse != null) {
            if (myAccountResponse.getAccount() != null) {

                binding.editFirstName.setText(myAccountResponse.getAccount().getFirst_name());
                binding.editLastName.setText(myAccountResponse.getAccount().getLast_name());
            }

            if (myAccountResponse.getAddresses() != null) {

                binding.editStreetAddress.setText(myAccountResponse.getAddresses().getStreet_1());
                binding.editStreetAddress2.setText(myAccountResponse.getAddresses().getStreet_2());
                binding.editCity.setText(myAccountResponse.getAddresses().getCity());
                binding.spinnerState.setSelection(states.indexOf(myAccountResponse.getAddresses().getState()));
                binding.editZip.setText(myAccountResponse.getAddresses().getZip_code());

                if (myAccountResponse.getAddresses().getPhone_number() != null && myAccountResponse.getAddresses().getPhone_number().length() == 10) {
                    StringBuilder phoneNumber = new StringBuilder(myAccountResponse.getAddresses().getPhone_number()).insert(0, "(");
                    phoneNumber = new StringBuilder(phoneNumber).insert(4, ")");
                    phoneNumber = new StringBuilder(phoneNumber).insert(5, " ");
                    phoneNumber = new StringBuilder(phoneNumber).insert(9, "-");
                    binding.editPhoneNumber.setText(phoneNumber);
                }

            }

            disableFields();
        }

    }

    public void disableFields()
    {
        binding.editFirstName.setEnabled(false);
        binding.editLastName.setEnabled(false);
        binding.editStreetAddress.setEnabled(false);
        binding.editStreetAddress2.setEnabled(false);
        binding.editCity.setEnabled(false);
        binding.spinnerState.setEnabled(false);
        binding.editZip.setEnabled(false);
        binding.editPhoneNumber.setEnabled(false);
    }

    public void enableFields()
    {
        binding.editFirstName.setEnabled(true);
        binding.editLastName.setEnabled(true);
        binding.editStreetAddress.setEnabled(true);
        binding.editStreetAddress2.setEnabled(true);
        binding.editCity.setEnabled(true);
        binding.spinnerState.setEnabled(true);
        binding.editZip.setEnabled(true);
        binding.editPhoneNumber.setEnabled(true);

        binding.editFirstName.setText("");
        binding.editLastName.setText("");
        binding.editStreetAddress.setText("");
        binding.editStreetAddress2.setText("");
        binding.editCity.setText("");
        binding.editZip.setText("");
        binding.editPhoneNumber.setText("");
    }


    void getLatLng(String address) {

        /// String address = "Vijay nagar,indore,Madhya Pradesh,452010";
        String googleApiUrl = GOOGLE_API_URL + "?address=" + address + "&key=" + GOOGLE_API_KEY;


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                googleApiUrl, new Response.Listener<String>() {

            public void onResponse(String response) {

                application.dialogManager.dismissProgressDialog();

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");

                        if (status.equals("OK")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("geometry");

                            JSONObject jsonObject3 = jsonObject2.getJSONObject("location");

                            double latitude = jsonObject3.getDouble("lat");
                            double longitude = jsonObject3.getDouble("lng");


                            if (addressRequestParams!=null)
                            {
                                addressRequestParams.setLatitude(latitude);
                                addressRequestParams.setLongitude(longitude);

                                if (flag.equals(AppConstant.FOR_ADD_ADDRESS))
                                {
                                    viewModel.addCustomerNewAddress(addressRequestParams);
                                }
                                else if (flag.equals(AppConstant.FOR_UPDATE_ADDRESS))
                                {
                                    viewModel.updateCustomerAddress(addressRequestParams,userAddressModel);
                                }

                            }


                            Log.d("status", status + "lat/longitude" + latitude + "---" + longitude);

                        }
                        else
                        {
                            showToast("Invalid address");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                Log.d("Response", response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                showToast("Something went wrong");
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(AddAddressActivity.this);
        requestQueue.add(stringRequest);
    }

}
