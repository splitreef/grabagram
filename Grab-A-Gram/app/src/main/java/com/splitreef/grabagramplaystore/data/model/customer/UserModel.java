package com.splitreef.grabagramplaystore.data.model.customer;

import java.io.Serializable;

public class UserModel implements Serializable {

    private boolean activated_at;
    private String email;
    private String name;
    private String role;
    private String user_id;
    private String street_1;
    private String driver_photo;
    private String badgeCount;
    private String patient_id;
    private boolean email_communication;

    private String device_id;
    private String device_type;



    public boolean isActivated_at() {
        return activated_at;
    }

    public void setActivated_at(boolean activated_at) {
        this.activated_at = activated_at;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStreet_1() {
        return street_1;
    }

    public void setStreet_1(String street_1) {
        this.street_1 = street_1;
    }

    public String getDriver_photo() {
        return driver_photo;
    }

    public void setDriver_photo(String driver_photo) {
        this.driver_photo = driver_photo;
    }

    public String getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(String badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public boolean getEmail_communication() {
        return email_communication;
    }

    public void setEmail_communication(boolean email_communication) {
        this.email_communication = email_communication;
    }



    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
}
