package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;

public class Weights implements Serializable {

   private long created_at;
   private String name;
   private String product_id;
   private long updated_at;
   private int value;

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

   @Override
    public String toString() {

        if (getName().equals("Select"))
        {
            return getName();
        }
        else
        {
            return getValue()+" "+getName();
        }
    }
}
