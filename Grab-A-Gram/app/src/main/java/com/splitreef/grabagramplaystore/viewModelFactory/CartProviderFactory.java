package com.splitreef.grabagramplaystore.viewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.customer.cart.CartViewModel;


public class CartProviderFactory implements ViewModelProvider.Factory {
    private final FirebaseRepository firebaseRepository;


    public CartProviderFactory(FirebaseRepository firebaseRepository) {

        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(CartViewModel.class))
        {
            return  (T) new CartViewModel(firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}





