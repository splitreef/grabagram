package com.splitreef.grabagramplaystore.ui.customer.addNewAddress;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.AddressRequestParams;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddNewAddressViewModel extends ViewModel {
    private static final String TAG = "AddNewAddressViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetAddressList = new MediatorLiveData<>();
    private  CompositeDisposable disposable = new CompositeDisposable();
    private MediatorLiveData<DataResource> onUploadCustomerDocPhoto = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onAddNewAddress = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateAddress = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetMyAccountData = new MediatorLiveData<>();


    public AddNewAddressViewModel(FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;

    }


    void uploadCustomerDocPhoto(byte[] bytes,String imageName)
    {
        firebaseRepository.uploadCustomerDocPhoto(bytes,imageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUploadCustomerDocPhoto.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatus(s));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    void addCustomerNewAddress(AddressRequestParams addressRequestParams)
    {
        firebaseRepository.addCustomerNewAddress(addressRequestParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onAddNewAddress.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onAddNewAddress.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onAddNewAddress.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    void updateCustomerAddress(AddressRequestParams addressRequestParams, UserAddressModel userAddressModel)
    {
        firebaseRepository.updateCustomerAddress(addressRequestParams,userAddressModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateAddress.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateAddress.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateAddress.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public  void getCustomerMyAccountData()
    {
        firebaseRepository.getCustomerMyAccountData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MyAccountResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetMyAccountData.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull MyAccountResponse myAccountResponse) {

                        onGetMyAccountData.setValue(DataResource.DataStatus(myAccountResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetMyAccountData.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource>observeCustomerDocPhoto()
    {
        return onUploadCustomerDocPhoto;
    }

    LiveData<StateResource>observeAddNewAddress()
    {
        return onAddNewAddress;
    }
    LiveData<StateResource>observeUpdateAddress()
    {
        return onUpdateAddress;
    }

    LiveData<DataResource> observeGetMyAccountData()
    {
        return onGetMyAccountData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }

}
