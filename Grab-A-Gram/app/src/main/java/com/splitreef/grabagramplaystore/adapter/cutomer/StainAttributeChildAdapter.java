package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;

import java.util.ArrayList;

public class StainAttributeChildAdapter extends RecyclerView.Adapter<StainAttributeChildAdapter.StainAttributeChildHolder> {

    private Context context;
    private ArrayList<String> StainAttributeChildList;

    public StainAttributeChildAdapter(Context context, ArrayList<String> StainAttributeChildList) {
        this.context=context;
        this.StainAttributeChildList=StainAttributeChildList;
    }

    @NonNull
    @Override
    public StainAttributeChildHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stain_attributes_child,parent,false);
        return new StainAttributeChildHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StainAttributeChildHolder holder, int position) {
        String value=StainAttributeChildList.get(position);
        holder.txtValue.setText(value);

    }

    @Override
    public int getItemCount() {
        return StainAttributeChildList.size();
    }

    public class StainAttributeChildHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtValue;

        public StainAttributeChildHolder(@NonNull View itemView) {
            super(itemView);
            txtValue= itemView.findViewById(R.id.value);
        }
    }
}
