package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.distance.Distance;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;

import java.util.ArrayList;

public class CustomerDistanceFilterAdapter extends RecyclerView.Adapter<CustomerDistanceFilterAdapter.CustomerDistanceFilterHolder> {

    private Context context;
    private ArrayList<Distance>distanceList;
    private int index=-1;


    public CustomerDistanceFilterAdapter(Context context, ArrayList<Distance>distanceList, int index) {
        this.context=context;
        this.distanceList=distanceList;
        this.index=index;
    }

    @NonNull
    @Override
    public CustomerDistanceFilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_category,parent,false);
        return new CustomerDistanceFilterHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CustomerDistanceFilterHolder holder, int position) {

        Distance distance=distanceList.get(position);
        holder.txtCategoryName.setText(""+distance.getDistance_display());

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index=position;
                notifyDataSetChanged();
            }
        });

        Log.d("DistaneId_Adapter==",""+distance.getDistance_id()+" "+distance.isSelected());


       /* if (distance.isSelected())
        {
            holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_cheked);
        }
        else
        {
            holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_unchked);
        }*/

        if (index==position)
        {
              distance.setSelected(true);
              holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_cheked);
        }
        else
        {
            distance.setSelected(false);
            holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_unchked);
        }

    }

    @Override
    public int getItemCount() {
        return distanceList.size();
    }

    public class CustomerDistanceFilterHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtCategoryName;
        AppCompatImageView imgRadioBtn;
        LinearLayout llRoot;

        public CustomerDistanceFilterHolder(@NonNull View itemView) {
            super(itemView);
            txtCategoryName= itemView.findViewById(R.id.txt_category_name);
            imgRadioBtn= itemView.findViewById(R.id.img_radio_btn);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }

    public void resetDistanceFilter()
    {
        if (distanceList!=null && distanceList.size()>0)
        for (int i=0;i<distanceList.size();i++)
        {
            distanceList.get(i).setSelected(false);
        }

        PreferenceManger.getPreferenceManger().setString(PrefKeys.DISTANCE_ID,null);
        PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_DISTANCE_FILTER,false);

        notifyDataSetChanged();
        index=-1;
    }

    public void setSelectedDistance()
    {

        for (int i=0;i<distanceList.size();i++)
        {
            boolean isSelected=distanceList.get(i).isSelected();
            if (isSelected)
            {
                PreferenceManger.getPreferenceManger().setString(PrefKeys.DISTANCE_ID,distanceList.get(i).getDistance_id());
                PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_DISTANCE_FILTER,true);
                break;
            }
        }


    }
}
