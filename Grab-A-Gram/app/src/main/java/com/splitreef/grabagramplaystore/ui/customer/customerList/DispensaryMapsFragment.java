package com.splitreef.grabagramplaystore.ui.customer.customerList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textview.MaterialTextView;
import com.google.common.reflect.TypeToken;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryListViewModelProviderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DispensaryMapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    ArrayList<DispensaryModel> dispensaryModels;
    private DispensaryListViewModel viewModel;
    private GrabAGramApplication application;
    GoogleMap googleMap;
    double latitude;
    double longitude;
    SupportMapFragment mapFragment;
    private Context context;

    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new DispensaryListViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(DispensaryListViewModel.class);

      //  ((DispensaryListActivity)context).setToolbarTitle("Customers List");

        subscribeObservers();

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        //viewModel.getDispensaryList(latitude, longitude, 40, true);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        View view = inflater.inflate(R.layout.fragment_dispensary_map, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

     /*   if (mapFragment != null) {
            mapFragment.getMapAsync(DispensaryMapsFragment.this);
        }*/

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        dispensaryModels = PreferenceManger.getPreferenceManger().getArray(PrefKeys.DISPENSARY_LIST, new TypeToken<List<DispensaryModel>>() {
        }.getType());

        if (dispensaryModels != null && dispensaryModels.size() > 0) {
            if (mapFragment != null) {
                mapFragment.getMapAsync(DispensaryMapsFragment.this);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(this);

        if (dispensaryModels != null && dispensaryModels.size() > 0) {

            Log.d("sizeinsidemap=", "" + dispensaryModels.size());
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < dispensaryModels.size(); i++) {

                DispensaryModel model = dispensaryModels.get(i);
                Marker marker = createMarker(model.getLocation().getLatitude(), model.getLocation().getLongitude(), model.getBusniess_name(), model.getAbout_us(), R.mipmap.location_map);
                marker.setTag(i);
                builder.include(marker.getPosition());


                LatLng latLng = new LatLng(model.getLocation().getLatitude(), model.getLocation().getLongitude());

               // googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
              //  googleMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));


            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            LatLngBounds bounds = builder.build();
            int padding = 20; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

            //googleMap.animateCamera(cu);

        } else {
            Log.d("sizeinsidemap=", "0");
        }
    }


    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                //  .anchor(0.5f, 0.5f)
                //  .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }


    void subscribeObservers() {
        viewModel.observeGetDispensaryList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:

                            dispensaryModels = (ArrayList<DispensaryModel>) dataResource.data;
                            if (dispensaryModels != null && dispensaryModels.size() > 0) {
                                if (mapFragment != null) {
                                    mapFragment.getMapAsync(DispensaryMapsFragment.this);
                                }
                            }

                            application.dialogManager.dismissProgressDialog();

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            break;

                    }
                }

            }
        });

    }


    @SuppressLint("DefaultLocale")
    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getSnippet() == null) {
            googleMap.moveCamera(CameraUpdateFactory.zoomIn());
            return true;
        }

        final DispensaryModel dispensaryModel = dispensaryModels.get((Integer) marker.getTag());
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.layout_pin_popup);


        AppCompatImageView close = dialog.findViewById(R.id.close);
        AppCompatImageView dispensaryImage = dialog.findViewById(R.id.img_dispensary);
        MaterialTextView dispensaryName = dialog.findViewById(R.id.txt_dispensary_name);
        AppCompatRatingBar dispensaryRating = dialog.findViewById(R.id.dispensary_rating);
        MaterialTextView milesAway = dialog.findViewById(R.id.txt_map);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        PicassoManager.setImage(dispensaryModel.getProfile_image(), dispensaryImage);
        dispensaryName.setText(dispensaryModel.getBusniess_name());
        dispensaryRating.setRating(dispensaryModel.getRating());
        double distance = new GetDistance().distance(latitude, longitude, dispensaryModel.getLocation().getLatitude(), dispensaryModel.getLocation().getLongitude());
        milesAway.setText(String.format("%.2f %s", distance, "miles away"));

        dialog.show();
        return true;
    }


}