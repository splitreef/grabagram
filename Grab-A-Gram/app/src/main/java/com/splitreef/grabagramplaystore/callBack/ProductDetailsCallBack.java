package com.splitreef.grabagramplaystore.callBack;

import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;

public interface ProductDetailsCallBack {

    void onClickProduct(ProductListResponse productListResponse);
}
