package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DispensaryListViewModel extends ViewModel {
    private static final String TAG = "RegistrationViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetDispensaryList = new MediatorLiveData<>();
    private  Application mApplication;
    private  CompositeDisposable disposable = new CompositeDisposable();


    public DispensaryListViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication= mApplication;
    }

    void getDispensaryList(double latitude, double longitude,double distance)
    {
         firebaseRepository.getDispensaryList(latitude,longitude,distance)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<ArrayList<DispensaryModel>>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onGetDispensaryList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull ArrayList<DispensaryModel> dispensaryModels) {
                        onGetDispensaryList.setValue(DataResource.DataStatus(dispensaryModels));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {
                         onGetDispensaryList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });

    }



    LiveData<DataResource>observeGetDispensaryList()
    {
        return onGetDispensaryList;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }

    public  void userLogout()
    {
        firebaseRepository.userLogout();
    }


}
