package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.driver.orders.ItemsModel;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

/**
 * Created by  on 07-12-2020.
 */
public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.OrderItemsHolder> {
    private Context context;
    private double latitude;
    private double longitude;
    private ArrayList<ItemsModel> itemsList;


    public OrderItemsAdapter(@NonNull Context context, ArrayList<ItemsModel> itemsList) {//, ArrayList<OrdersModel> ordersList, double latitude, double longitude) {
        this.context = context;
        this.itemsList = itemsList;
//        this.latitude = latitude;
//        this.longitude = longitude;
    }


   /* public ArrayList<OrdersModel> getDispensaryList() {
        return ordersList;
    }*/

    @NonNull
    @Override
    public OrderItemsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_orders_view, parent, false);
        return new OrderItemsHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull OrderItemsHolder holder, int position) {

        ItemsModel itemsModel = itemsList.get(position);
        holder.tv_item_name.setText(itemsModel.getName());
        holder.tv_quantity.setText(""+itemsModel.getQuantity());
        holder.tv_item_price.setText("$" + itemsModel.getRegular_price());
        holder.tv_sold_by_name.setText(itemsModel.getBusniess_name());
        PicassoManager.setImage(itemsModel.getImages().get(0), holder.img_item);
        if (holder.getAdapterPosition() == itemsList.size() - 1) {
            holder.view.setVisibility(View.GONE);
        } else {
            holder.view.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class OrderItemsHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_item_name, tv_quantity, tv_item_price, tv_sold_by_name;
        AppCompatImageView img_item;
        View view;

        public OrderItemsHolder(@NonNull View itemView) {
            super(itemView);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_item_price = itemView.findViewById(R.id.tv_item_price);
            tv_sold_by_name = itemView.findViewById(R.id.tv_sold_by_name);
            img_item = itemView.findViewById(R.id.img_item);
            view = itemView.findViewById(R.id.view);
        }
    }
}
