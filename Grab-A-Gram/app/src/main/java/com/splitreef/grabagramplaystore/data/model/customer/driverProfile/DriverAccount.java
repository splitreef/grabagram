package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;

import java.io.Serializable;

public class DriverAccount implements Serializable {

    private long date_of_birth;
    private String first_name;
    private String last_name;


    public long getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(long date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

}
