
package com.splitreef.grabagramplaystore.data.model.customer.distanceMatrix;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Row implements Serializable
{

    @SerializedName("elements")
    @Expose
    private List<Element> elements = null;
    private final static long serialVersionUID = -6813683006033583558L;

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

}
