package com.splitreef.grabagramplaystore.ui.customer.productReview;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerUser;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productReviewResponse.ProductReviewResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityProductReviewBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.viewModelFactory.ProductReviewProviderFactory;


public class ProductReviewActivity extends BaseActivity implements View.OnClickListener {

    private ActivityProductReviewBinding binding;
    private ProductReviewViewModel viewModel;
    private GrabAGramApplication application;
    private Order order;
    private DispensaryReviewModel dispensaryOwnReview=null;
    private String productId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityProductReviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
        initListeners();
    }

    public void initView()
    {

        application = (GrabAGramApplication) getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new ProductReviewProviderFactory(application.firebaseRepository)).get(ProductReviewViewModel.class);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);
        subscribeObservers();

        binding.btnSave.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);


        if (getIntent()!=null)
        {
            order= (Order) getIntent().getParcelableExtra(AppConstant.ORDER_MODEL);
            if (order!=null)
            {
                updateUi(order);
            }
        }

    }

    public void initListeners()
    {
        binding.spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ProductCart productCart= (ProductCart) adapterView.getItemAtPosition(i);

                if (i!=0)
                {
                    viewModel.getProductReview(order.getId(),productCart.getProduct_id());
                    productId=productCart.getProduct_id();
                }
                else
                {
                    binding.rating.setRating(0);
                    binding.editReview.setText("");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    void updateUi(Order order)
    {
        if (order.getItems()!=null && order.getItems().size()>0)
        {
            ProductCart productCart=new ProductCart();
            productCart.setName("Select");
            order.getItems().add(0,productCart);
            ArrayAdapter<ProductCart> productAdapter=new ArrayAdapter<>(ProductReviewActivity.this, R.layout.spinner_item,order.getItems());
            binding.spinnerProduct.setAdapter(productAdapter);
        }



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }


   void subscribeObservers() {

       viewModel.observeGetProductReview().observe(ProductReviewActivity.this, dataResource -> {
           if (dataResource!=null)
           {
               switch (dataResource.status)
               {
                   case LOADING:
                       application.dialogManager.displayProgressDialog(ProductReviewActivity.this,"Loading...");
                       break;
                   case SUCCESS:
                       application.dialogManager.dismissProgressDialog();
                       ProductReviewResponse productReviewResponse = (ProductReviewResponse) dataResource.data;
                       if (productReviewResponse.getStatus().equals(AppConstant.SUCCESS))
                       {
                           setReviewAndRating(productReviewResponse);
                       }
                       else
                       {
                           binding.rating.setRating(0);
                           binding.editReview.setText("");
                           binding.btnSave.setVisibility(View.VISIBLE);
                           binding.btnCancel.setVisibility(View.VISIBLE);
                       }
                       break;
                   case ERROR:
                       application.dialogManager.dismissProgressDialog();
                       binding.rating.setRating(0);
                       binding.editReview.setText("");
                       binding.btnSave.setVisibility(View.VISIBLE);
                       binding.btnCancel.setVisibility(View.VISIBLE);
                       break;
               }
           }

       });

       viewModel.observeSaveProductReview().observe(ProductReviewActivity.this, dataResource -> {
           if (dataResource!=null)
           {
               switch (dataResource.status)
               {
                   case LOADING:
                       application.dialogManager.displayProgressDialog(ProductReviewActivity.this,"Loading...");
                       break;
                   case SUCCESS:
                       application.dialogManager.dismissProgressDialog();
                       String message = (String) dataResource.data;
                       if (message.equals(AppConstant.SUCCESS))
                       {
                           showToast("Review save successfully.");
                       }
                       else
                       {
                           showToast("Data not found");
                       }
                       break;
                   case ERROR:
                       application.dialogManager.dismissProgressDialog();
                       break;
               }
           }

       });

    }

    public void setReviewAndRating(ProductReviewResponse productReviewResponse)
    {
          ProductReviewModel productReviewModel=productReviewResponse.getProductReviewModel();
          binding.rating.setRating(productReviewModel.getRating());
          binding.editReview.setText(productReviewModel.getComment());

          binding.btnSave.setVisibility(View.GONE);
          binding.btnCancel.setVisibility(View.GONE);

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btn_save:
                if (binding.spinnerProduct.getSelectedItemPosition()==0)
                {
                    showToast("Please select product name");
                }
                else if (binding.editReview.getText().toString().isEmpty())
                {
                    showToast("Please enter review message");
                }
                else
                {
                    saveReview();
                }

                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private void saveReview()
    {
        MyAccountResponse myAccountResponse = PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);

        CustomerUser customerUser=myAccountResponse.getUser();
        CustomerAccount customerAccount=myAccountResponse.getAccount();

        ProductReviewModel productReviewModel=new ProductReviewModel();
        productReviewModel.setComment(binding.editReview.getText().toString());
        productReviewModel.setCreated_at(AppUtil.getCurrentTimeInMillisecond());
        productReviewModel.setOrder_id(order.getId());
        productReviewModel.setUpdated_at(AppUtil.getCurrentTimeInMillisecond());
        productReviewModel.setUser_id(customerUser.getUser_id());
        productReviewModel.setUser_name(customerAccount.getFirst_name()+" "+customerAccount.getLast_name());
        productReviewModel.setRating(binding.rating.getRating());
        productReviewModel.setProduct_id(productId);

       viewModel.saveProductReview(productReviewModel);
    }
}
