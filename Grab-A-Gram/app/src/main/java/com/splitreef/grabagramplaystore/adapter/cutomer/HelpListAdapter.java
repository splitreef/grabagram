package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.help.Help;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.ui.customer.help.HelpDescriptionActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;

import java.util.ArrayList;

public class HelpListAdapter extends RecyclerView.Adapter<HelpListAdapter.HelpListHolder> {

    private Context context;
    private ArrayList<Help>helpsList;

    public HelpListAdapter(Context context, ArrayList<Help>helpsList) {
        this.context=context;
        this.helpsList=helpsList;

    }

    @NonNull
    @Override
    public HelpListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_help_list,parent,false);
        return new HelpListHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HelpListHolder holder, int position) {

        Help help=helpsList.get(position);
        holder.txtTitle.setText(""+help.getTitle());

        holder.llRoot.setOnClickListener(view -> {

            context.startActivity(new Intent(context, HelpDescriptionActivity.class)
                    .putExtra(AppConstant.FROM,AppConstant.FROM_HELP_SCREEN)
                    .putExtra(AppConstant.HELP_MODEL,help));

            ((DispensaryListActivity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
        });

    }

    @Override
    public int getItemCount() {
        return helpsList.size();
    }

    public class HelpListHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtTitle;
        LinearLayout llRoot;

        public HelpListHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle= itemView.findViewById(R.id.txt_title);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }
}
