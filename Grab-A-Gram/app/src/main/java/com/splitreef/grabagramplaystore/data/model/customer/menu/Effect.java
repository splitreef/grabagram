package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;

public class Effect implements Serializable {
    private String euphoric;
    private String happy;
    private String relaxed;
    private String uplifted;

    public String getEuphoric() {
        return euphoric;
    }

    public void setEuphoric(String euphoric) {
        this.euphoric = euphoric;
    }

    public String getHappy() {
        return happy;
    }

    public void setHappy(String happy) {
        this.happy = happy;
    }

    public String getRelaxed() {
        return relaxed;
    }

    public void setRelaxed(String relaxed) {
        this.relaxed = relaxed;
    }

    public String getUplifted() {
        return uplifted;
    }

    public void setUplifted(String uplifted) {
        this.uplifted = uplifted;
    }
}
