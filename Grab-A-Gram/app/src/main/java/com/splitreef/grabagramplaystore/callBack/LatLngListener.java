package com.splitreef.grabagramplaystore.callBack;

public interface LatLngListener {

    void getLatLng(double latitude,double longitude);
}
