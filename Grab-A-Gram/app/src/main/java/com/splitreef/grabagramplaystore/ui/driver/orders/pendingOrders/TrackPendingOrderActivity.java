package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.firestore.GeoPoint;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.databinding.ActivityTrackPendingOrderBinding;
import com.splitreef.grabagramplaystore.ui.customer.trackOrder.DirectionParser;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.GPSTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_KEY;


/**
 * Created by  on 11-01-2021.
 */
public class TrackPendingOrderActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private ActivityTrackPendingOrderBinding binding;
    private ArrayList<LatLng> latLngs;
    private GoogleMap googleMap;
    PolylineOptions poly;
    Polyline polyline;
    Marker marker;
    ArrayList<Marker> markerList;
    private LatLng mOrigin;
    private LatLng mDestination;
    double currentLat = 0;
    double currentLng = 0;
    double destinLat = 0;
    double destinLng = 0;
    private GeoPoint geoPoint;
    private boolean isFirstTime = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTrackPendingOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    public void initView() {

        if (getIntent() != null) {
            // order = (Order) getIntent().getParcelableExtra((AppConstant.ORDER_MODEL));
            destinLat = getIntent().getDoubleExtra(AppConstant.LATITUDE, 0.0);
            destinLng = getIntent().getDoubleExtra(AppConstant.LONGITUDE, 0.0);
            Log.d("location=", "destinLat" + destinLat);
            Log.d("location=", "destinLng" + destinLng);
            mOrigin = new LatLng(destinLat, destinLng);
        }


        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        GPSTracker gpsTracker = new GPSTracker(TrackPendingOrderActivity.this);
        currentLat = gpsTracker.getLatitude();
        currentLng = gpsTracker.getLongitude();
        Log.d("location=", "currentLat" + currentLat);
        Log.d("location=", "currentLng" + currentLng);

       /* mDestination = new LatLng(22.7814, 75.9035);
        mOrigin= new LatLng(22.7244, 75.8839);
*/
        mDestination = new LatLng(currentLat, currentLng);

        latLngs = new ArrayList<>();
        latLngs.add(mOrigin);
        latLngs.add(mDestination);
        initMap();
        callGoogleDirectionsApi();


       /* googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                // Move camera.
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 10));
                // Remove listener to prevent position reset on camera move.
                googleMap.setOnCameraChangeListener(null);
            }
        });*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
       // googleMap.setOnMarkerClickListener(this);

        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            markerList = new ArrayList<>();

            for (int i = 0; i < latLngs.size(); i++) {
                LatLng latLng = latLngs.get(i);
                if (i == 0) {
                    Log.d("map_error","latLng.latitude0="+latLng.latitude+"latLng.longitude0"+latLng.longitude);
                    marker = createMarker(latLng.latitude, latLng.longitude, R.mipmap.caricoprofile);
                } else if (i == 1) {
                    Log.d("map_error","latLng.latitude1="+latLng.latitude+"latLng.longitude1"+latLng.longitude);
                    marker = createMarker(latLng.latitude, latLng.longitude, R.mipmap.location_map);
                }

                marker.setTag(i);
                builder.include(marker.getPosition());
                markerList.add(marker);
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            LatLngBounds bounds = builder.build();

            // Setup camera movement
            final int width = getResources().getDisplayMetrics().widthPixels;
            final int height = getResources().getDisplayMetrics().heightPixels;
            final int minMetric = Math.min(width, height);
            final int padding = (int) (minMetric * 0.40); //0.40// offset from edges of the map in pixels


         //   int padding =5; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            googleMap.moveCamera(cu);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
        } catch (Exception e) {
            Log.d("map_error",e.toString());
            e.printStackTrace();
        }

    }

    protected Marker createMarker(double latitude, double longitude, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                //  .anchor(0.5f, 0.5f)
                //  .title(title)
                .snippet("snippet")
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


    void callGoogleDirectionsApi() {

        String googleApiUrl = AppConstant.GOOGLE_DIRECTIONS_API_URL + "?origin=" + mOrigin.latitude + "," + mOrigin.longitude + "&destination=" + mDestination.latitude + "," + mDestination.longitude + "&sensor=true&mode=driving" + "&key=" + GOOGLE_API_KEY;

        Log.d("Url", googleApiUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                googleApiUrl, new Response.Listener<String>() {

            public void onResponse(String response) {
                Log.d("Response", response);
                TaskParseDirection parseResult = new TaskParseDirection();
                parseResult.execute(response);

                Toast.makeText(TrackPendingOrderActivity.this, "success", Toast.LENGTH_SHORT).show();


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                Toast.makeText(TrackPendingOrderActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(TrackPendingOrderActivity.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.close);
    }


    public class TaskParseDirection extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonString) {
            List<List<HashMap<String, String>>> routes = null;
            JSONObject jsonObject = null;

            try {
                jsonObject = new JSONObject(jsonString[0]);
                DirectionParser parser = new DirectionParser();
                routes = parser.parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            super.onPostExecute(lists);
            ArrayList points = null;
            PolylineOptions polylineOptions = null;

            if (polyline != null) {
                polyline.remove();
            }
            if (markerList != null) {

                for (int i = 0; i < markerList.size(); i++) {
                    markerList.get(i).remove();
                }
            }


            addMarker();

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lng"));

                    points.add(new LatLng(lat, lon));
                }
                polylineOptions.addAll(points);
                polylineOptions.width(10f);
                polylineOptions.color(Color.BLUE);
                polylineOptions.geodesic(true);
            }
            if (polylineOptions != null) {
                polyline = googleMap.addPolyline(polylineOptions);
            } else {
                Toast.makeText(getApplicationContext(), "Direction not found", Toast.LENGTH_LONG).show();
            }
        }

    }

    void addMarker() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        markerList = new ArrayList<>();

        for (int i = 0; i < latLngs.size(); i++) {
            LatLng latLng = latLngs.get(i);
            if (i == 0) {
                marker = createMarker(latLng.latitude, latLng.longitude, R.mipmap.caricoprofile);
            } else if (i == 1) {
                marker = createMarker(latLng.latitude, latLng.longitude, R.mipmap.location_map);
            }
            marker.setTag(i);
            builder.include(marker.getPosition());
            markerList.add(marker);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getSnippet() == null) {
            googleMap.moveCamera(CameraUpdateFactory.zoomIn());
            return true;
        }

        final Dialog dialog = new Dialog(TrackPendingOrderActivity.this, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.layout_driver_pin_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        AppCompatImageView close = dialog.findViewById(R.id.close);
        AppCompatImageView imgDriver = dialog.findViewById(R.id.img_driver);
        MaterialTextView txtDriverName = dialog.findViewById(R.id.txt_driver_name);
        MaterialTextView txtLicensePlate = dialog.findViewById(R.id.txt_license_plate);
        AppCompatImageView imgInformation = dialog.findViewById(R.id.img_information);

        /*if (driverProfileResponse != null && driverProfileResponse.getDocument_images() != null &&
                driverProfileResponse.getDocument_images().size() > 0 && driverProfileResponse.getDocument_images().get(0) != null
                && driverProfileResponse.getDocument_images().get(0).getPhoto() != null) {
            PicassoManager.setImage(driverProfileResponse.getDocument_images().get(0).getPhoto(), imgDriver);
        }

        if (driverProfileResponse != null && driverProfileResponse.getAccount() != null) {
            DriverAccount driverAccount = driverProfileResponse.getAccount();
            txtDriverName.setText("" + driverAccount.getFirst_name() + " " + driverAccount.getLast_name());
        }

        if (driverProfileResponse != null && driverProfileResponse.getDocuments() != null) {
            DriverDocuments driverDocuments = driverProfileResponse.getDocuments();
            txtLicensePlate.setText("License Plate:- " + driverDocuments.getManufacture_model());
        }*/

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        /*imgInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (geoPoint != null) {
                    driverProfileResponse.setLat(geoPoint.getLatitude());
                    driverProfileResponse.setLng(geoPoint.getLongitude());
                }

                startActivity(new Intent(TrackPendingOrderActivity.this, DriverProfileActivity.class)
                        .putExtra(AppConstant.DRIVER_PROFILE_RESPONSE, driverProfileResponse));
                dialog.dismiss();
            }
        });
*/

        if ((Integer) marker.getTag() == 0) {
            dialog.show();
        }

        return true;
    }


    void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

}


