package com.splitreef.grabagramplaystore.callBack;

import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;

public interface DeleteAddressListener {

    void onClickDelete(UserAddressModel userAddressModel,boolean isPersonAddress,int position);
}
