package com.splitreef.grabagramplaystore.ui.customer.dashboard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityDashboardBinding;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.viewModelFactory.DashBoardProviderFactory;
import java.util.Objects;

public class DashboardActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = DashboardActivity.class.getName();
    private ActivityDashboardBinding binding;
    private DashBoardViewModel viewModel;
    private GrabAGramApplication application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getApplicationContext());

        viewModel = new ViewModelProvider(getViewModelStore(), new DashBoardProviderFactory(application.firebaseRepository)).get(DashBoardViewModel.class);

        binding.cardDispensaries.setOnClickListener(this);
        binding.cardTrackOrder.setOnClickListener(this);
        binding.cardPastOrders.setOnClickListener(this);
        binding.cardMyAccount.setOnClickListener(this);
        subscribeObservers();

        viewModel.getBadgeCount();



        GPSTracker gpsTracker = new GPSTracker(DashboardActivity.this);
        Log.d(TAG, "Lat_lng"+ gpsTracker.getLatitude() + "" + gpsTracker.getLongitude());

    }

    @Override
    protected void onResume() {
        super.onResume();
        setDispensaryMiles();
    }

    @SuppressLint("SetTextI18n")
    private void setDispensaryMiles()
    {
        boolean isDistanceFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);
        if (isDistanceFilter)
        {
            String distanceIDStr=PreferenceManger.getPreferenceManger().getString(PrefKeys.DISTANCE_ID);
            int distance= Integer.parseInt(distanceIDStr);
            binding.txtMiles.setText("within \n"+distance+" miles");
        }
        else
        {
            binding.txtMiles.setText("within \n"+AppConstant.DEFAULT_DISTANCE+" miles");
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_dispensaries:
                startActivity(new Intent(DashboardActivity.this, DispensaryListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.FROM_DASHBOARD_SCREEN)
                        .putExtra(AppConstant.FOR,AppConstant.FOR_DISPENSARY_LIST));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.card_track_order:
                startActivity(new Intent(DashboardActivity.this, DispensaryListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.FROM_DASHBOARD_SCREEN)
                        .putExtra(AppConstant.FOR,AppConstant.FOR_TRACK_ORDERS));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.card_past_orders:
                startActivity(new Intent(DashboardActivity.this, DispensaryListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.FROM_DASHBOARD_SCREEN)
                        .putExtra(AppConstant.FOR,AppConstant.FOR_PAST_ORDERS));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.card_my_account:
                startActivity(new Intent(DashboardActivity.this, DispensaryListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.FROM_DASHBOARD_SCREEN)
                        .putExtra(AppConstant.FOR,AppConstant.FOR_MY_ACCOUNT));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    void subscribeObservers() {
        viewModel.observeGetBadgeCount().observe(DashboardActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(DashboardActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                       // application.dialogManager.dismissProgressDialog();
                        String badgeCount = (String) dataResource.data;
                        Log.d("badgeCount_onSuccess",""+badgeCount);
                        PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,badgeCount);
                        viewModel.getCustomerMyAccountData();
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });

        viewModel.observeGetMyAccountData().observe(DashboardActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        MyAccountResponse myAccountResponse = (MyAccountResponse) dataResource.data;
                        if (myAccountResponse.getStatus().equals(AppConstant.SUCCESS)) {
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.MY_ACCOUNT_DATA,myAccountResponse);

                            if (myAccountResponse!=null)
                            {
                                if (myAccountResponse.getAccount()!=null)
                                {
                                    CustomerAccount customerAccount=myAccountResponse.getAccount();
                                    binding.userName.setText(customerAccount.getFirst_name()+" "+customerAccount.getLast_name());
                                }

                                if (myAccountResponse.getAddresses()!=null)
                                {
                                    binding.address.setText((myAccountResponse.getAddresses().getStreet_1()));
                                }

                            }

                        }
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();

                }
            }
        });

    }
}
