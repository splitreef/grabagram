package com.splitreef.grabagramplaystore.dataSource.local.sharePref;

public class PrefKeys {

    public static String PREFERENCE_NAME="Grab_A_Gram";
    public static String PREFERENCE_FCM_TOKEN="preference_fcm_token";
    public static String LOGIN_USER="loginUser";
    public static String DISPENSARY_LIST = "dispensary_list";
    public static String BADGE_COUNT = "badgeCount";
    public static String PENDING_ORDERS_LIST = "pending_orders_list";
    public  static String DISPENSARY_MODEL="dispensary_model";

    public  static String DISTANCE_ID="distance_id";
    public  static String IS_CATEGORY_FILTER="is_category_filter";
    public  static String IS_DISTANCE_FILTER="is_distance_filter";
    public  static String DISPENSARY_USER_ID_LIST="dispensary_user_id_list";
    public  static String CATEGORY_ID_LIST="category_id_list";
    public static String DECLINE_ORDERS_LIST="dewcline_order_list";
    public static String FCM_TOKEN="fcm_token";
    public static String ACTIVE_STATUS="active_status";
    public static String MY_ACCOUNT_DATA="my_account_data";
    public static String REGISTRATION_REQUEST="registration_request";

}
