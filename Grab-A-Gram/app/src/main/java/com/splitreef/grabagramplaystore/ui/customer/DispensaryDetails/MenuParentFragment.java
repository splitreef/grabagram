package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.databinding.FragmentParentMenuBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;

public class MenuParentFragment extends Fragment {

    private FragmentParentMenuBinding binding;
    private Context context;
    private DispensaryModel dispensaryModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null)
        {
            dispensaryModel= (DispensaryModel) getArguments().getParcelable(AppConstant.DISPENSARY_MODEL);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentParentMenuBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        //initView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    public void initView()
    {
        Fragment fragment= new MenuFragment();
        Bundle bundle=new Bundle();
        bundle.putParcelable(AppConstant.DISPENSARY_MODEL,dispensaryModel);
        fragment.setArguments(bundle);
        replaceFragment(fragment,false);
    }

    public void replaceFragment(Fragment fragment,boolean isAllowBack)
    {
        FragmentTransaction fragmentTransaction=getChildFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.container, fragment);
        if (isAllowBack)
        {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }


}
