package com.splitreef.grabagramplaystore.data.model.driver;

/**
 * Created by  on 25-11-2020.
 */
public class UserDriverAccountModel {
    // For account info
    String city;
    String date_of_birth;
    String device_id;
    String device_mac_id;
    String device_type;
    String email;
    String email_communication;
    String first_name;
    String is_caregiver;
    String last_name;
    String message_notification;
    String password;
    String phone_number;
    String role;
    String state;
    String street_1;
    String street_2;
    String zip_code;
    String term_and_condition;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_mac_id() {
        return device_mac_id;
    }

    public void setDevice_mac_id(String device_mac_id) {
        this.device_mac_id = device_mac_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail_communication() {
        return email_communication;
    }

    public void setEmail_communication(String email_communication) {
        this.email_communication = email_communication;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getIs_caregiver() {
        return is_caregiver;
    }

    public void setIs_caregiver(String is_caregiver) {
        this.is_caregiver = is_caregiver;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMessage_notification() {
        return message_notification;
    }

    public void setMessage_notification(String message_notification) {
        this.message_notification = message_notification;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet_1() {
        return street_1;
    }

    public void setStreet_1(String street_1) {
        this.street_1 = street_1;
    }

    public String getStreet_2() {
        return street_2;
    }

    public void setStreet_2(String street_2) {
        this.street_2 = street_2;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getTerm_and_condition() {
        return term_and_condition;
    }

    public void setTerm_and_condition(String term_and_condition) {
        this.term_and_condition = term_and_condition;
    }
}
