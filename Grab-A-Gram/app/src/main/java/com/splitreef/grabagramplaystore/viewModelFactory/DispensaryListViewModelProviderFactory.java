
package com.splitreef.grabagramplaystore.viewModelFactory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListViewModel;


public class DispensaryListViewModelProviderFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final FirebaseRepository firebaseRepository;


    public DispensaryListViewModelProviderFactory(Application application, FirebaseRepository firebaseRepository) {
        mApplication = application;
        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(DispensaryListViewModel.class))
        {
            return  (T) new DispensaryListViewModel(mApplication, firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}





