package com.splitreef.grabagramplaystore.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class AppUtil {

    public static String DATE_FORMAT_DD_MM_YYYY = "dd/MM/yyyy";
    public static long ONE_DAY_TIME = 1000 * 60 * 60 * 24;

    public static double subYears(String currentDate, String pastDate) {
        long diff = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
            d1 = format.parse(currentDate);
            d2 = format.parse(pastDate);
            diff = d1.getTime() - d2.getTime();
            Log.e("TEST", d1.getTime() + " - " + d2.getTime() + " - " + diff);
        } catch (ParseException e) {
            Log.e("TEST", "Exception", e);
        }


        return diff / 1000 / 60 / 60 / 24 / 365;
    }


    public static String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        String currentDate = df.format(c);

        return currentDate;
    }


    public static String convertMilliSecondsToDate(long timeInMilliSecond, String format) {
        @SuppressLint("SimpleDateFormat") String dateString = new SimpleDateFormat(format).format(new Date(timeInMilliSecond));
        return dateString;
    }

    public static String convertMilliSecondsToTime(long timeInMilliSecond) {
        @SuppressLint("SimpleDateFormat") String dateString = new SimpleDateFormat("HH:mm:ss").format(new Date(timeInMilliSecond));
        return dateString;
    }

    public static String getDayOfWeek() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        return sdf.format(d);
    }

    public static boolean isDispensaryOpen(String openingTime, String closingTime) {
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String getCurrentTime = sdf.format(c.getTime());

        if (getCurrentTime.compareTo(openingTime) >= 0 && getCurrentTime.compareTo(closingTime) <= 0) {

            return true;
        } else {
            return false;
        }

    }

    public static String getElapsedTime(long from) {
        StringBuilder result = new StringBuilder("");
        Date date = new Date(from);
        long elapsedMillis = System.currentTimeMillis() - date.getTime();
        long millisPerMinute = 1000 * 60;
        long millisPerHour = millisPerMinute * 60;
        long millisPerDay = millisPerMinute * 60 * 24;
        long millisPerMonth = millisPerMinute * 60 * 24 * 30;
        long millisPerYear = millisPerMinute * 60 * 24 * 365;
        long elapsedHours = elapsedMillis / millisPerHour;
        long elapsedMinutes = elapsedMillis / millisPerMinute;
        long elapsedDay = elapsedMillis / millisPerDay;
        long elapsedMonth = elapsedMillis / millisPerMonth;
        long elapsedYear = elapsedMillis / millisPerYear;

        if (elapsedYear > 0) {
            result.append(elapsedYear).append(" Year").append(elapsedYear > 1 ? "s" : "").append(" Ago");
        } else if (elapsedMonth > 0) {
            result.append(elapsedMonth).append(" Month").append(elapsedMonth > 1 ? "s" : "").append(" Ago");
        } else if (elapsedDay > 0) {
            result.append(elapsedDay).append(" Day").append(elapsedDay > 1 ? "s" : "").append(" Ago");
        } else if (elapsedHours > 0) {
            result.append(elapsedHours).append(" Hour").append(elapsedHours > 1 ? "s" : "").append(" Ago");
        } else if (elapsedMinutes > 0) {
            result.append(elapsedMinutes).append(" Minute").append(elapsedMinutes > 1 ? "s" : "").append(" Ago");
        } else {
            result.append("Just now");
        }

        return result.toString();
    }


    public static ArrayList<String> getDefaultAddressKey() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Select");
        //arrayList.add("PERSON_SHIPPING");
        arrayList.add(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS);
        arrayList.add(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS);
        arrayList.add(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS);

        return arrayList;
    }


    public static String getDateFromMillisecondTime(Long timeL) {
        Date date = new Date(timeL);
        String dateWithoutTime = null;
        try {
            @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("MMMM, dd, yyyy");
            dateWithoutTime = format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateWithoutTime;

    }

    public static String getDateMDYFromMillisecond(Long timeL) {
        Date date = new Date(timeL);
        String dateWithoutTime = null;
        try {
            @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("MM-dd-yyyy");
            dateWithoutTime = format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateWithoutTime;

    }

    public static String getDateFromMillisecond(Long timeL) {
        Date date = new Date(timeL);
        String dateWithoutTime = null;
        try {
            @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("MMMM dd, yyyy");
            dateWithoutTime = format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateWithoutTime;

    }

    public static String getTimeFromMillisecondTime(Long timeL) {
        Date date = new Date(timeL);
        String dateWithoutTime = null;
        try {
            @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("hh:mm aa");
            dateWithoutTime = format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateWithoutTime;

    }

    public static long getCurrentTimeInMillisecond() {
        //creating Calendar instance
        long timeMilli2 = 0;
        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            timeMilli2 = calendar.getTimeInMillis();
            Log.d("TimeInMilliseconds", "" + timeMilli2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeMilli2;
    }


    public static String getDateWithDayNameFromMillisecondTime(Long timeL) {
        Date date = new Date(timeL);
        String dateWithoutTime = null;
        try {
            @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("EEEE,MMM, dd, yyyy");
            dateWithoutTime = format.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateWithoutTime;

    }


    public static int getCurrentYear() {
        int year = 0;
        try {
            year = Calendar.getInstance().get(Calendar.YEAR);
            Log.d("date===", "year=" + year);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return year;

    }

    public static int getCurrentMonth() {
        int month = 0;
        try {
            month = Calendar.getInstance().get(Calendar.MONTH);//this is april so you will receive  3 instead of 4.
            month=month+1;
            Log.d("date===", "month=" + month);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return month;

    }

    public static int getYearByDate(String date) {
        int year = 0;
        try {
            DateFormat dateFormat = new SimpleDateFormat("YYYY");
            Date datee = new Date(date);
            year = Integer.parseInt(dateFormat.format(datee));
            Log.d("date===", "" + date + "==year=" + dateFormat.format(datee));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return year;

    }

    public static int getMonthByDate(String datee) {
        int month = 0;
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM");
            Date date = new Date(datee);
            month = Integer.parseInt(dateFormat.format(date));
            Log.d("date===", "" + date + "==month=" + dateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return month;

    }


    public static int getDayByDate(String datee) {
        int day = 0;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd");
            Date date = new Date(datee);
            day = Integer.parseInt(dateFormat.format(date));
            Log.d("date===", "" + date + "==Day=" + dateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return day;

    }

    public static String getLastDateOfGivenMonth(int year, int month) {
        LocalDate endOfMonth =LocalDate.now();
        try {
            YearMonth yearMonth = YearMonth.of(year, month);
            endOfMonth = yearMonth.atEndOfMonth();
            Log.d("date===", "month_days==" + endOfMonth);

        } catch (Exception e) {
            e.printStackTrace();

        }
        String s=endOfMonth.toString();
        return s;
    }

    public static String getStateCode(String state)
    {
        Map<String, String> states = new HashMap<String, String>();
        states.put("Alabama","AL");
        states.put("Alaska","AK");
        states.put("Alberta","AB");
        states.put("American Samoa","AS");
        states.put("Arizona","AZ");
        states.put("Arkansas","AR");
        states.put("Armed Forces (AE)","AE");
        states.put("Armed Forces Americas","AA");
        states.put("Armed Forces Pacific","AP");
        states.put("British Columbia","BC");
        states.put("California","CA");
        states.put("Colorado","CO");
        states.put("Connecticut","CT");
        states.put("Delaware","DE");
        states.put("District Of Columbia","DC");
        states.put("Florida","FL");
        states.put("Georgia","GA");
        states.put("Guam","GU");
        states.put("Hawaii","HI");
        states.put("Idaho","ID");
        states.put("Illinois","IL");
        states.put("Indiana","IN");
        states.put("Iowa","IA");
        states.put("Kansas","KS");
        states.put("Kentucky","KY");
        states.put("Louisiana","LA");
        states.put("Maine","ME");
        states.put("Manitoba","MB");
        states.put("Maryland","MD");
        states.put("Massachusetts","MA");
        states.put("Michigan","MI");
        states.put("Minnesota","MN");
        states.put("Mississippi","MS");
        states.put("Missouri","MO");
        states.put("Montana","MT");
        states.put("Nebraska","NE");
        states.put("Nevada","NV");
        states.put("New Brunswick","NB");
        states.put("New Hampshire","NH");
        states.put("New Jersey","NJ");
        states.put("New Mexico","NM");
        states.put("New York","NY");
        states.put("Newfoundland","NF");
        states.put("North Carolina","NC");
        states.put("North Dakota","ND");
        states.put("Northwest Territories","NT");
        states.put("Nova Scotia","NS");
        states.put("Nunavut","NU");
        states.put("Ohio","OH");
        states.put("Oklahoma","OK");
        states.put("Ontario","ON");
        states.put("Oregon","OR");
        states.put("Pennsylvania","PA");
        states.put("Prince Edward Island","PE");
        states.put("Puerto Rico","PR");
        states.put("Quebec","PQ");
        states.put("Rhode Island","RI");
        states.put("Saskatchewan","SK");
        states.put("South Carolina","SC");
        states.put("South Dakota","SD");
        states.put("Tennessee","TN");
        states.put("Texas","TX");
        states.put("Utah","UT");
        states.put("Vermont","VT");
        states.put("Virgin Islands","VI");
        states.put("Virginia","VA");
        states.put("Washington","WA");
        states.put("West Virginia","WV");
        states.put("Wisconsin","WI");
        states.put("Wyoming","WY");
        states.put("Yukon Territory","YT");

        return  states.get(state);
    }


  /*  function getState(zipString) {

        *//* Ensure param is a string to prevent unpredictable parsing results *//*
        if (typeof zipString !== 'string') {
            console.log('Must pass the zipcode as a string.');
            return;
        }

        *//* Ensure we have exactly 5 characters to parse *//*
        if (zipString.length !== 5) {
            console.log('Must pass a 5-digit zipcode.');
            return;
        }

        *//* Ensure we don't parse strings starting with 0 as octal values *//*
  const zipcode = parseInt(zipString, 10);

        let st;
        let state;

        *//* Code cases alphabetized by state *//*
        if (zipcode >= 35000 && zipcode <= 36999) {
            st = 'AL';
            state = 'Alabama';
        } else if (zipcode >= 99500 && zipcode <= 99999) {
            st = 'AK';
            state = 'Alaska';
        } else if (zipcode >= 85000 && zipcode <= 86999) {
            st = 'AZ';
            state = 'Arizona';
        } else if (zipcode >= 71600 && zipcode <= 72999) {
            st = 'AR';
            state = 'Arkansas';
        } else if (zipcode >= 90000 && zipcode <= 96699) {
            st = 'CA';
            state = 'California';
        } else if (zipcode >= 80000 && zipcode <= 81999) {
            st = 'CO';
            state = 'Colorado';
        } else if ((zipcode >= 6000 && zipcode <= 6389) || (zipcode >= 6391 && zipcode <= 6999)) {
            st = 'CT';
            state = 'Connecticut';
        } else if (zipcode >= 19700 && zipcode <= 19999) {
            st = 'DE';
            state = 'Delaware';
        } else if (zipcode >= 32000 && zipcode <= 34999) {
            st = 'FL';
            state = 'Florida';
        } else if ( (zipcode >= 30000 && zipcode <= 31999) || (zipcode >= 39800 && zipcode <= 39999) ) {
            st = 'GA';
            state = 'Georgia';
        } else if (zipcode >= 96700 && zipcode <= 96999) {
            st = 'HI';
            state = 'Hawaii';
        } else if (zipcode >= 83200 && zipcode <= 83999) {
            st = 'ID';
            state = 'Idaho';
        } else if (zipcode >= 60000 && zipcode <= 62999) {
            st = 'IL';
            state = 'Illinois';
        } else if (zipcode >= 46000 && zipcode <= 47999) {
            st = 'IN';
            state = 'Indiana';
        } else if (zipcode >= 50000 && zipcode <= 52999) {
            st = 'IA';
            state = 'Iowa';
        } else if (zipcode >= 66000 && zipcode <= 67999) {
            st = 'KS';
            state = 'Kansas';
        } else if (zipcode >= 40000 && zipcode <= 42999) {
            st = 'KY';
            state = 'Kentucky';
        } else if (zipcode >= 70000 && zipcode <= 71599) {
            st = 'LA';
            state = 'Louisiana';
        } else if (zipcode >= 3900 && zipcode <= 4999) {
            st = 'ME';
            state = 'Maine';
        } else if (zipcode >= 20600 && zipcode <= 21999) {
            st = 'MD';
            state = 'Maryland';
        } else if ( (zipcode >= 1000 && zipcode <= 2799) || (zipcode == 5501) ) {
            st = 'MA';
            state = 'Massachusetts';
        } else if (zipcode >= 48000 && zipcode <= 49999) {
            st = 'MI';
            state = 'Michigan';
        } else if (zipcode >= 55000 && zipcode <= 56899) {
            st = 'MN';
            state = 'Minnesota';
        } else if (zipcode >= 38600 && zipcode <= 39999) {
            st = 'MS';
            state = 'Mississippi';
        } else if (zipcode >= 63000 && zipcode <= 65999) {
            st = 'MO';
            state = 'Missouri';
        } else if (zipcode >= 59000 && zipcode <= 59999) {
            st = 'MT';
            state = 'Montana';
        } else if (zipcode >= 27000 && zipcode <= 28999) {
            st = 'NC';
            state = 'North Carolina';
        } else if (zipcode >= 58000 && zipcode <= 58999) {
            st = 'ND';
            state = 'North Dakota';
        } else if (zipcode >= 68000 && zipcode <= 69999) {
            st = 'NE';
            state = 'Nebraska';
        } else if (zipcode >= 88900 && zipcode <= 89999) {
            st = 'NV';
            state = 'Nevada';
        } else if (zipcode >= 3000 && zipcode <= 3899) {
            st = 'NH';
            state = 'New Hampshire';
        } else if (zipcode >= 7000 && zipcode <= 8999) {
            st = 'NJ';
            state = 'New Jersey';
        } else if (zipcode >= 87000 && zipcode <= 88499) {
            st = 'NM';
            state = 'New Mexico';
        } else if ( (zipcode >= 10000 && zipcode <= 14999) || (zipcode == 6390) ) {
            st = 'NY';
            state = 'New York';
        } else if (zipcode >= 43000 && zipcode <= 45999) {
            st = 'OH';
            state = 'Ohio';
        } else if ((zipcode >= 73000 && zipcode <= 73199) || (zipcode >= 73400 && zipcode <= 74999) ) {
            st = 'OK';
            state = 'Oklahoma';
        } else if (zipcode >= 97000 && zipcode <= 97999) {
            st = 'OR';
            state = 'Oregon';
        } else if (zipcode >= 15000 && zipcode <= 19699) {
            st = 'PA';
            state = 'Pennsylvania';
        } else if (zipcode >= 300 && zipcode <= 999) {
            st = 'PR';
            state = 'Puerto Rico';
        } else if (zipcode >= 2800 && zipcode <= 2999) {
            st = 'RI';
            state = 'Rhode Island';
        } else if (zipcode >= 29000 && zipcode <= 29999) {
            st = 'SC';
            state = 'South Carolina';
        } else if (zipcode >= 57000 && zipcode <= 57999) {
            st = 'SD';
            state = 'South Dakota';
        } else if (zipcode >= 37000 && zipcode <= 38599) {
            st = 'TN';
            state = 'Tennessee';
        } else if ( (zipcode >= 75000 && zipcode <= 79999) || (zipcode >= 73301 && zipcode <= 73399) ||  (zipcode >= 88500 && zipcode <= 88599) ) {
            st = 'TX';
            state = 'Texas';
        } else if (zipcode >= 84000 && zipcode <= 84999) {
            st = 'UT';
            state = 'Utah';
        } else if (zipcode >= 5000 && zipcode <= 5999) {
            st = 'VT';
            state = 'Vermont';
        } else if ( (zipcode >= 20100 && zipcode <= 20199) || (zipcode >= 22000 && zipcode <= 24699) || (zipcode == 20598) ) {
            st = 'VA';
            state = 'Virgina';
        } else if ( (zipcode >= 20000 && zipcode <= 20099) || (zipcode >= 20200 && zipcode <= 20599) || (zipcode >= 56900 && zipcode <= 56999) ) {
            st = 'DC';
            state = 'Washington DC';
        } else if (zipcode >= 98000 && zipcode <= 99499) {
            st = 'WA';
            state = 'Washington';
        } else if (zipcode >= 24700 && zipcode <= 26999) {
            st = 'WV';
            state = 'West Virginia';
        } else if (zipcode >= 53000 && zipcode <= 54999) {
            st = 'WI';
            state = 'Wisconsin';
        } else if (zipcode >= 82000 && zipcode <= 83199) {
            st = 'WY';
            state = 'Wyoming';
        } else {
            st = 'none';
            state = 'none';
            console.log('No state found matching', zipcode);
        }

        return st;
    }*/
}
