package com.splitreef.grabagramplaystore.data.model.driver.orders;

import java.io.Serializable;

/**
 * Created by  on 01-12-2020.
 */
public class PaymentMethodModel implements Serializable {
    private long assigned_at;
    private long created_at;
    private String type;
    private long updated_at;

    public long getAssigned_at() {
        return assigned_at;
    }

    public void setAssigned_at(long assigned_at) {
        this.assigned_at = assigned_at;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
