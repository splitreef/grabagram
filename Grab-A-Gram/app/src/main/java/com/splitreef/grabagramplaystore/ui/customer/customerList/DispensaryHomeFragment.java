package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.splitreef.grabagramplaystore.adapter.cutomer.CustomerPagerAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentDispensaryHomeBinding;


public class DispensaryHomeFragment extends BaseFragment{

    public FragmentDispensaryHomeBinding binding;
    CustomerPagerAdapter adapter;
    public static MaterialButton btnSort;
    public static MaterialButton btnFilter;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentDispensaryHomeBinding.inflate(inflater, container, false);
        btnSort = binding.btnSort;
        btnFilter = binding.btnFilter;
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    public void initView() {

        adapter = new CustomerPagerAdapter(getContext(), getChildFragmentManager(), binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        binding.viewPager.setOffscreenPageLimit(0);

        // For change fragment on click of tab
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 1 || position == 2) {
                    binding.btnSort.setVisibility(View.GONE);
                    binding.btnFilter.setVisibility(View.GONE);
                } else {
                    binding.btnSort.setVisibility(View.VISIBLE);
                    binding.btnFilter.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();

        boolean isCategoryFilter = PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_CATEGORY_FILTER);
        boolean isDistanceFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);

        if (isCategoryFilter || isDistanceFilter)
        {
           binding.txtFilterShape.setVisibility(View.VISIBLE);
        }
        else
        {
            binding.txtFilterShape.setVisibility(View.GONE);
        }
    }
}
