package com.splitreef.grabagramplaystore.callBack;

public interface UpdateCartProduct {

    void updateCartProduct(String id,int quantity,double totalPrice);
    void removeCartProduct(String id,boolean isLastItem);
}
