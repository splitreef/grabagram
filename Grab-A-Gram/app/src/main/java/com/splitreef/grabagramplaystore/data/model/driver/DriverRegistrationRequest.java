package com.splitreef.grabagramplaystore.data.model.driver;

import java.io.Serializable;
import java.util.List;

/**
 * Created by  on 10-11-2020.
 */
public class DriverRegistrationRequest implements Serializable {
    // For account info
    String city;
    long dateOfBirth;
    String deviceId;
    String deviceMacId;
    String deviceType;
    String email;
    boolean emailCommunication;
    String firstName;
    boolean isCareGiver;
    String lastName;
    boolean messageNotification;
    String password;
    String phoneNumber;
    String role;
    String state;
    String streetAddress1;
    String streetAddress2;
    String zipCode;
    boolean termAndCondition;
    boolean setActivateAt;


    //document_images
    String carInsurancePolicyUrl;
    String careGiverCertificateURl;
    String photoURl;
    String drivingLicenseUrl;
    String carRegistrationUrl;

    //documents
    String documentType;
    long expirationDate;
    String identityNumber;
    String issuingState;
    String licenceIssuingState;
    String licencePlateNumber;
    String manufactureCompany;
    String manufactureModel;
    String manufactureYear;
    String regularMaintenance;


    // phones
    String phoneManufactureCompany;
    String phoneManufactureModel;
    String havingSmartPhone;

    //
    private List<DriverRefrences> driverRefrencesList;

    public List<DriverRefrences> getDriverRefrencesList() {
        return driverRefrencesList;
    }

    public void setDriverRefrencesList(List<DriverRefrences> driverRefrencesList) {
        this.driverRefrencesList = driverRefrencesList;
    }

    String userId;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceMacId() {
        return deviceMacId;
    }

    public void setDeviceMacId(String deviceMacId) {
        this.deviceMacId = deviceMacId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getEmailCommunication() {
        return emailCommunication;
    }

    public void setEmailCommunication(boolean emailCommunication) {
        this.emailCommunication = emailCommunication;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean getIsCareGiver() {
        return isCareGiver;
    }

    public void setIsCareGiver(boolean isCareGiver) {
        this.isCareGiver = isCareGiver;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getMessageNotification() {
        return messageNotification;
    }

    public void setMessageNotification(boolean messageNotification) {
        this.messageNotification = messageNotification;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean getTermAndCondition() {
        return termAndCondition;
    }

    public void setTermAndCondition(boolean termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public boolean isSetActivateAt() {
        return setActivateAt;
    }

    public void setSetActivateAt(boolean setActivateAt) {
        this.setActivateAt = setActivateAt;
    }

    public String getCarInsurancePolicyUrl() {
        return carInsurancePolicyUrl;
    }

    public void setCarInsurancePolicyUrl(String carInsurancePolicyUrl) {
        this.carInsurancePolicyUrl = carInsurancePolicyUrl;
    }

    public String getCareGiverCertificateURl() {
        return careGiverCertificateURl;
    }

    public void setCareGiverCertificateURl(String careGiverCertificateURl) {
        this.careGiverCertificateURl = careGiverCertificateURl;
    }

    public String getPhotoURl() {
        return photoURl;
    }

    public void setPhotoURl(String photoURl) {
        this.photoURl = photoURl;
    }

    public String getDrivingLicenseUrl() {
        return drivingLicenseUrl;
    }

    public void setDrivingLicenseUrl(String drivingLicenseUrl) {
        this.drivingLicenseUrl = drivingLicenseUrl;
    }

    public String getCarRegistrationUrl() {
        return carRegistrationUrl;
    }

    public void setCarRegistrationUrl(String carRegistrationUrl) {
        this.carRegistrationUrl = carRegistrationUrl;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getIssuingState() {
        return issuingState;
    }

    public void setIssuingState(String issuingState) {
        this.issuingState = issuingState;
    }

    public String getLicenceIssuingState() {
        return licenceIssuingState;
    }

    public void setLicenceIssuingState(String licenceIssuingState) {
        this.licenceIssuingState = licenceIssuingState;
    }

    public String getLicencePlateNumber() {
        return licencePlateNumber;
    }

    public void setLicencePlateNumber(String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }

    public String getManufactureCompany() {
        return manufactureCompany;
    }

    public void setManufactureCompany(String manufactureCompany) {
        this.manufactureCompany = manufactureCompany;
    }

    public String getManufactureModel() {
        return manufactureModel;
    }

    public void setManufactureModel(String manufactureModel) {
        this.manufactureModel = manufactureModel;
    }

    public String getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(String manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public String getRegularMaintenance() {
        return regularMaintenance;
    }

    public void setRegularMaintenance(String regularMaintenance) {
        this.regularMaintenance = regularMaintenance;
    }

    public String getPhoneManufactureCompany() {
        return phoneManufactureCompany;
    }

    public void setPhoneManufactureCompany(String phoneManufactureCompany) {
        this.phoneManufactureCompany = phoneManufactureCompany;
    }

    public String getPhoneManufactureModel() {
        return phoneManufactureModel;
    }

    public void setPhoneManufactureModel(String phoneManufactureModel) {
        this.phoneManufactureModel = phoneManufactureModel;
    }

    public String getHavingSmartPhone() {
        return havingSmartPhone;
    }

    public void setHavingSmartPhone(String havingSmartPhone) {
        this.havingSmartPhone = havingSmartPhone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}