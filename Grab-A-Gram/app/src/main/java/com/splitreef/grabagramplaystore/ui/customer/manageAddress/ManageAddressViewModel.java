package com.splitreef.grabagramplaystore.ui.customer.manageAddress;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ManageAddressViewModel extends ViewModel {
    private static final String TAG = "ManageAddressViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetAddressList = new MediatorLiveData<>();
    private  CompositeDisposable disposable = new CompositeDisposable();
    private MediatorLiveData<StateResource> onDeleteAddress = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdatePersonalAddress = new MediatorLiveData<>();


    public ManageAddressViewModel(FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;

    }

    void getAddressList()
    {
         firebaseRepository.getAddressList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<AddressList>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {
                         disposable.add(d);
                         onGetAddressList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull AddressList addressList) {
                         onGetAddressList.setValue(DataResource.DataStatus(addressList));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {
                         onGetAddressList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });

    }


    void deleteCustomerAddress(UserAddressModel userAddressModel,boolean isPersonAddress)
    {
        firebaseRepository.deleteCustomerAddress(userAddressModel,isPersonAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onDeleteAddress.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onDeleteAddress.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onDeleteAddress.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    void updateIsPersonalAddress(boolean isPersonAddress)
    {
        firebaseRepository.updateIsPersonalAddress(isPersonAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdatePersonalAddress.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdatePersonalAddress.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdatePersonalAddress.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }



    LiveData<DataResource>observeGetAddressList()
    {
        return onGetAddressList;
    }

    LiveData<StateResource>observeDeleteAddress()
    {
        return onDeleteAddress;
    }

    LiveData<StateResource>observeUpdatePersonalAddress()
    {
        return onUpdatePersonalAddress;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }

}
