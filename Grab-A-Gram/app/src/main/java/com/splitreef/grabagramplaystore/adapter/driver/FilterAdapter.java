package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.driver.FilterItemListener;

import java.util.List;

/**
 * Created by  on 06-01-2021.
 */
public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterItemHolder> {
    private Context context;
    private List<String> itemsList;
    private FilterItemListener filterItemListener;


    public FilterAdapter(@NonNull Context context, List<String> itemsList, FilterItemListener filterItemListener) {
        this.context = context;
        this.itemsList = itemsList;
        this.filterItemListener = filterItemListener;
    }

    @NonNull
    @Override
    public FilterAdapter.FilterItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_filter_view, parent, false);
        return new FilterAdapter.FilterItemHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull FilterAdapter.FilterItemHolder holder, int position) {

        String s = itemsList.get(position);
        holder.tv_item_name.setText(s);
        holder.tv_item_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.tv_item_name.getText().toString().equalsIgnoreCase("10 Mile")) {
                    filterItemListener.onItemClick(10);
                } else if (holder.tv_item_name.getText().toString().equalsIgnoreCase("25 Mile")) {
                    filterItemListener.onItemClick(25);
                } else if (holder.tv_item_name.getText().toString().equalsIgnoreCase("50 Mile")) {
                    filterItemListener.onItemClick(50);
                } else if (holder.tv_item_name.getText().toString().equalsIgnoreCase("100 Mile")) {
                    filterItemListener.onItemClick(100);
                } else if (holder.tv_item_name.getText().toString().equalsIgnoreCase("250 Mile")) {
                    filterItemListener.onItemClick(250);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class FilterItemHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_item_name;

        public FilterItemHolder(@NonNull View itemView) {
            super(itemView);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);

        }
    }
}
