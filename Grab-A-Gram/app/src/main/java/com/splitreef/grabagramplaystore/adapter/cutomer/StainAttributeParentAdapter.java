package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.menu.StainAttributesModel;

import java.util.ArrayList;

public class StainAttributeParentAdapter extends RecyclerView.Adapter<StainAttributeParentAdapter.StainAttributeParentHolder> {

    private Context context;
    private ArrayList<StainAttributesModel> attributesModelArrayList;

    public StainAttributeParentAdapter(Context context, ArrayList<StainAttributesModel> attributesModelArrayList) {
        this.context=context;
        this.attributesModelArrayList=attributesModelArrayList;
    }

    @NonNull
    @Override
    public StainAttributeParentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_strain_attributes,parent,false);
        return new StainAttributeParentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StainAttributeParentHolder holder, int position) {
        StainAttributesModel model=attributesModelArrayList.get(position);
        holder.txtTitle.setText(model.getTitle());

        if (model.getValues()!=null && model.getValues().size()>0)
        {
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            StainAttributeChildAdapter stainAttributeChildAdapter=new StainAttributeChildAdapter(context,model.getValues());
            holder.recyclerView.setAdapter(stainAttributeChildAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return attributesModelArrayList.size();
    }

    public class StainAttributeParentHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtTitle;
        RecyclerView recyclerView;

        public StainAttributeParentHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle= itemView.findViewById(R.id.txt_title);
            recyclerView= itemView.findViewById(R.id.rv_child_stain_attributes);
        }
    }
}
