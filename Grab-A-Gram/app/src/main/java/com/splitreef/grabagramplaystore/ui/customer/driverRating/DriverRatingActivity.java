package com.splitreef.grabagramplaystore.ui.customer.driverRating;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverAccount;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverAddress;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfile;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfileResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRating;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRatingResponse;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerUser;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityDriverRatingBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.viewModelFactory.DriverRatingProviderFactory;
import java.util.Objects;


public class DriverRatingActivity extends BaseActivity implements View.OnClickListener {

    private ActivityDriverRatingBinding binding;
    private DriverRatingViewModel viewModel;
    private GrabAGramApplication application;
    private Order order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDriverRatingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    public void initView() {
        application = (GrabAGramApplication) getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new DriverRatingProviderFactory(application.firebaseRepository)).get(DriverRatingViewModel.class);

        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);
        subscribeObservers();

        binding.btnSave.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);

        if (getIntent() != null) {
            order = getIntent().getParcelableExtra(AppConstant.ORDER_MODEL);
            if (order != null && order.getAssign_driver_id() != null) {
                viewModel.getDriverProfileData(order.getAssign_driver_id());
            } else {
                binding.scrollView.setVisibility(View.GONE);
                binding.llSaveCancel.setVisibility(View.GONE);
                binding.userMsg.setVisibility(View.VISIBLE);
            }
        }

    }

    @SuppressLint("SetTextI18n")
    void updateUi(DriverProfile driverProfile) {

        if (driverProfile != null) {

            DriverAccount driverAccount = driverProfile.getAccount();
            if (driverAccount!=null)
            {
                binding.txtDriverName.setText("" + driverAccount.getFirst_name() + " " + driverAccount.getLast_name());
            }

            if (driverProfile.getAddress()!=null)
            {
                DriverAddress driverAddress=driverProfile.getAddress();

                String address = driverAddress.getStreet_1()+"\n"+
                        driverAddress.getCity()+","+driverAddress.getState()+","+driverAddress.getZip_code()+"\n"+driverAddress.getPhone_number();

                binding.txtDriverAddress.setText(address);
            }

            if (driverProfile.getProfile_image() != null) {
                PicassoManager.setImage(driverProfile.getProfile_image(), binding.imgDriverProfile);
            }

            binding.scrollView.setVisibility(View.VISIBLE);
            binding.llSaveCancel.setVisibility(View.VISIBLE);
            binding.userMsg.setVisibility(View.GONE);

        } else {
            binding.scrollView.setVisibility(View.GONE);
            binding.llSaveCancel.setVisibility(View.GONE);
            binding.userMsg.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.close);
    }


    void subscribeObservers() {

        viewModel.observeGetDriverProfileData().observe(DriverRatingActivity.this, dataResource -> {
            switch (dataResource.status) {
                case LOADING:
                    binding.progressBar.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    DriverProfileResponse driverProfileResponse = (DriverProfileResponse) dataResource.data;
                    if (driverProfileResponse!=null && driverProfileResponse.getStatus().equals(AppConstant.SUCCESS)) {
                        DriverProfile driverProfile = driverProfileResponse.getDriverProfile();
                        viewModel.getDriverRatingData(order.getAssign_driver_id(),order.getId());
                        updateUi(driverProfile);
                    } else {

                        binding.scrollView.setVisibility(View.GONE);
                        binding.llSaveCancel.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.progressBar.setVisibility(View.GONE);
                    }
                    break;

                case ERROR:
                    binding.scrollView.setVisibility(View.GONE);
                    binding.llSaveCancel.setVisibility(View.GONE);
                    binding.progressBar.setVisibility(View.GONE);
                    binding.userMsg.setVisibility(View.VISIBLE);
            }
        });

        viewModel.observeGetDriverRatingData().observe(DriverRatingActivity.this, dataResource -> {
            switch (dataResource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    binding.progressBar.setVisibility(View.GONE);
                    DriverRatingResponse driverRatingResponse = (DriverRatingResponse) dataResource.data;
                    if (driverRatingResponse!=null && driverRatingResponse.getStatus().equals(AppConstant.SUCCESS))
                    {
                        DriverRating driverRating=driverRatingResponse.getDriverRating();
                        if (driverRating !=null)
                        {
                            binding.driverRating.setRating(driverRating.getRating());
                            binding.llSaveCancel.setVisibility(View.GONE);
                        }
                        else
                        {
                            binding.llSaveCancel.setVisibility(View.VISIBLE);
                        }
                    }
                    break;

                case ERROR:
                    binding.progressBar.setVisibility(View.GONE);
                    binding.llSaveCancel.setVisibility(View.VISIBLE);

            }
        });


        viewModel.observeSaveDriverRating().observe(DriverRatingActivity.this, dataResource -> {
            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(DriverRatingActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                       // String message = (String) dataResource.data;
                        finish();
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        break;
                }
            }

        });

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_save:
                 saveReview();
                 break;
            case R.id.btn_cancel:
                 finish();
                 break;
        }
    }

    private void saveReview() {
        MyAccountResponse myAccountResponse = PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);

        CustomerUser customerUser=myAccountResponse.getUser();
        CustomerAccount customerAccount=myAccountResponse.getAccount();

        DriverRating driverRating=new DriverRating();

        driverRating.setCreated_at(AppUtil.getCurrentTimeInMillisecond());
        driverRating.setUpdated_at(AppUtil.getCurrentTimeInMillisecond());
        driverRating.setOrder_id(order.getId());
        driverRating.setUser_id(""+customerUser.getUser_id());
        driverRating.setUser_name(""+customerAccount.getFirst_name()+" "+customerAccount.getLast_name());
        driverRating.setRating(binding.driverRating.getRating());

        viewModel.saveDriverRating(driverRating,order.getAssign_driver_id());
    }
}
