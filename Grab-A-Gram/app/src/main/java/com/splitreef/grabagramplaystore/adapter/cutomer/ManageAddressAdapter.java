package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.DeleteAddressListener;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.customer.addNewAddress.AddAddressActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;

import java.util.ArrayList;

public class ManageAddressAdapter extends RecyclerView.Adapter<ManageAddressAdapter.ManageAddressHolder> {

    private Context context;
    private  ArrayList<UserAddressModel>addressList;
    private DeleteAddressListener deleteAddressListener;
    private boolean isPersonAddress;

    public ManageAddressAdapter(Context context, ArrayList<UserAddressModel>addressList,boolean isPersonAddress,DeleteAddressListener deleteAddressListener) {
        this.context=context;
        this.addressList=addressList;
        this.isPersonAddress=isPersonAddress;
        this.deleteAddressListener=deleteAddressListener;
    }

    @NonNull
    @Override
    public ManageAddressHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manage_address,parent,false);
        return new ManageAddressHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ManageAddressHolder holder, int position) {

            UserAddressModel userAddressModel=addressList.get(position);
            holder.txtAddressType.setText(userAddressModel.getType());

            MyAccountResponse myAccountResponse= PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);
            String address = userAddressModel.getStreet_1()+"\n"+
                    userAddressModel.getCity()+","+userAddressModel.getState()+","+userAddressModel.getZip_code()+"\n"+userAddressModel.getPhone_number();


            if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS) || userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS))
            {
                holder.txtAddressType.setText("DEFAULT PRIMARY SHIPPING ADDRESS");

                if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS))
                {
                    if (myAccountResponse!=null && myAccountResponse.getAccount()!=null)
                    {
                        CustomerAccount customerAccount=myAccountResponse.getAccount();
                        holder.txtName.setText(customerAccount.getFirst_name()+" "+customerAccount.getLast_name());
                    }

                }
                else if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS))
                {
                    holder.txtName.setText(userAddressModel.getFirst_name()+" "+userAddressModel.getLast_name());
                }

            }
            else if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS))
            {
                holder.txtAddressType.setText("DEFAULT SECONDARY SHIPPING ADDRESS");
                holder.txtName.setText(userAddressModel.getFirst_name()+" "+userAddressModel.getLast_name());
            }
            else if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS))
            {
                holder.txtAddressType.setText("DEFAULT BILLING ADDRESS");
                holder.txtName.setText(userAddressModel.getFirst_name()+" "+userAddressModel.getLast_name());
            }

            holder.txtAddress.setText(address);


            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS))
                    {
                        // show dialog
                        new SimpleDialog() {
                            @Override
                            public void onPositiveButtonClick() {

                            }
                        }.showSimpleDialog(context,"Information","User can not be delete this person address.","OK");

                    }
                    else
                    {
                        new AlertDialog() {
                            @Override
                            public void onPositiveButtonClick() {
                                deleteAddressListener.onClickDelete(userAddressModel,isPersonAddress,position);
                            }
                        }.showAlertDialog(context,"Delete",context.getString(R.string.are_you_sure_address_delete),"Yes","No");


                    }

                }
            });

            holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS))
                    {
                        // show dialog
                        new SimpleDialog() {
                            @Override
                            public void onPositiveButtonClick() {

                            }
                        }.showSimpleDialog(context,"Information","User can not be edit this person address.","OK");

                    }
                    else
                    {
                        context.startActivity(new Intent(context, AddAddressActivity.class)
                                .putExtra(AppConstant.FROM,AppConstant.MANAGE_ADDRESS_SCREEN)
                                .putExtra(AppConstant.FOR,AppConstant.FOR_UPDATE_ADDRESS)
                                .putExtra("userAddressModel",userAddressModel));
                    }

                }
            });
    }

    public void removeAddress(int position)
    {
       addressList.remove(position);
       notifyItemChanged(position);
    }

    public ArrayList<UserAddressModel>getAddressList()
    {
        return addressList;
    }

    public void removeAddressByType()
    {
        for (int i=0;i<addressList.size();i++)
        {
            UserAddressModel userAddressModel=addressList.get(i);
            if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS))
            {
                UserAddressModel userAddressModel1=addressList.get(i);
                deleteAddressListener.onClickDelete(userAddressModel,true,i);
            }
        }
    }

    public boolean checkedPrimaryAddressContained()
    {
        boolean isContained=false;

        for (int i=0;i<addressList.size();i++)
        {
            UserAddressModel userAddressModel=addressList.get(i);
            if (userAddressModel.getType().equals(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS))
            {
                isContained=true;
                break;
            }
        }
        return isContained;
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class ManageAddressHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtAddressType;
        MaterialTextView txtAddress;
        MaterialTextView txtName;
        MaterialButton btnEdit;
        MaterialButton btnDelete;
        LinearLayout llRoot;

        public ManageAddressHolder(@NonNull View itemView) {
            super(itemView);
            txtAddressType= itemView.findViewById(R.id.txt_address_type);
            txtAddress= itemView.findViewById(R.id.txt_address);
            btnEdit= itemView.findViewById(R.id.btn_edit);
            btnDelete= itemView.findViewById(R.id.btn_delete);
            txtName= itemView.findViewById(R.id.txt_name);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }
}
