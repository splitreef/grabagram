package com.splitreef.grabagramplaystore.ui.customer.filter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;
import com.splitreef.grabagramplaystore.data.model.customer.dispensaryIdList.DispensaryIdListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distance.DispensaryDistanceResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategoryResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import java.util.ArrayList;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CustomerFilterViewModel extends ViewModel {

    private static final String TAG = "CustomerFilterViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onCategoryList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onDistanceList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onDispensaryUserIdList = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public CustomerFilterViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getCategoryList()
    {
         firebaseRepository.getCategoryList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<ProductCategoryResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onCategoryList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull ProductCategoryResponse productCategoryResponse) {

                         onCategoryList.setValue(DataResource.DataStatus(productCategoryResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onCategoryList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }


    public  void getDistanceList()
    {
        firebaseRepository.getDistanceList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DispensaryDistanceResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onDistanceList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DispensaryDistanceResponse dispensaryDistanceResponse) {

                        onDistanceList.setValue(DataResource.DataStatus(dispensaryDistanceResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onDistanceList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    public  void getDispensaryIdList(ArrayList<String> categoryIdList)
    {
        firebaseRepository.getDispensaryIdList(categoryIdList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DispensaryIdListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onDispensaryUserIdList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DispensaryIdListResponse dispensaryIdListResponse) {

                        onDispensaryUserIdList.setValue(DataResource.DataStatus(dispensaryIdListResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onDispensaryUserIdList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }






    LiveData<DataResource> observeGetCategoryList()
    {
        return onCategoryList;
    }


    LiveData<DataResource> observeGetDistanceList()
    {
        return onDistanceList;
    }

    LiveData<DataResource> observeGetDispensaryUserIdList()
    {
        return onDispensaryUserIdList;
    }


}
