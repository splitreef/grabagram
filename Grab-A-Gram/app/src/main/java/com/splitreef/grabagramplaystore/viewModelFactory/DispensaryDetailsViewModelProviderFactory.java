
package com.splitreef.grabagramplaystore.viewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.DispensaryDetailsViewModel;


public class DispensaryDetailsViewModelProviderFactory implements ViewModelProvider.Factory {

    private final FirebaseRepository firebaseRepository;


    public DispensaryDetailsViewModelProviderFactory(FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(DispensaryDetailsViewModel.class))
        {
            return  (T) new DispensaryDetailsViewModel(firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}





