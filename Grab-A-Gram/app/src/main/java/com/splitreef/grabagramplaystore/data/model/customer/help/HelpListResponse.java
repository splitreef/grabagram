package com.splitreef.grabagramplaystore.data.model.customer.help;

import java.io.Serializable;
import java.util.ArrayList;

public class HelpListResponse implements Serializable {

    private String status;

    ArrayList<Help>helpList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Help> getHelpList() {
        return helpList;
    }

    public void setHelpList(ArrayList<Help> helpList) {
        this.helpList = helpList;
    }
}
