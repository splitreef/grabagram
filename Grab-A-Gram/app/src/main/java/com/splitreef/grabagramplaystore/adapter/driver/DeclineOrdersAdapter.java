package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

/**
 * Created by  on 14-01-2021.
 */
public class DeclineOrdersAdapter extends RecyclerView.Adapter<DeclineOrdersAdapter.DeclineOrdersHolder> {
    private ArrayList<OrdersModel> ordersList;
    private Context context;


    public DeclineOrdersAdapter(@NonNull Context context, ArrayList<OrdersModel> ordersList) {
        this.context = context;
        this.ordersList = ordersList;
    }


    public ArrayList<OrdersModel> getDispensaryList() {
        return ordersList;
    }

    @NonNull
    @Override
    public DeclineOrdersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_past_deliveries_view, parent, false);
        return new DeclineOrdersHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull DeclineOrdersHolder holder, int position) {
        OrdersModel ordersModel = ordersList.get(position);
        holder.tv_order_number.setText("#" + ordersModel.getOrder_no());
        holder.tv_order_date.setText(AppUtil.getDateFromMillisecond(ordersModel.getCreated_at()));
        holder.tv_order_status.setText(ordersModel.getStatus());
        holder.ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OrdersDetailsActivity.class)
                        .putExtra(AppConstant.ID, ordersModel.getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    class DeclineOrdersHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_order_number, tv_order_date, tv_order_status;
        LinearLayout ll_top;

        public DeclineOrdersHolder(@NonNull View itemView) {
            super(itemView);
            tv_order_number = itemView.findViewById(R.id.tv_order_number);
            tv_order_date = itemView.findViewById(R.id.tv_order_date);
            tv_order_status = itemView.findViewById(R.id.tv_order_status);
            ll_top = itemView.findViewById(R.id.ll_top);
        }
    }
}
