package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.OpeningHours;
import com.splitreef.grabagramplaystore.databinding.FragmentDispensaryOverviewBinding;
import com.splitreef.grabagramplaystore.enums.WeekDayEnum;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

public class DispensaryOverviewFragment extends BaseFragment {

    private FragmentDispensaryOverviewBinding binding;
    private DispensaryModel dispensaryModel;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments()!=null)
        {
            dispensaryModel= (DispensaryModel) getArguments().getParcelable(AppConstant.DISPENSARY_MODEL);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding=FragmentDispensaryOverviewBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    public void initView()
    {
        if (dispensaryModel!=null)
        {

            // Set address of dispensary
            String address=dispensaryModel.getAddress().getStreet1()+"\n"
                    +dispensaryModel.getAddress().getCity()+","
                    +dispensaryModel.getAddress().getZip_code()+"\n"
                    +dispensaryModel.getAddress().getPhone_number();
            binding.txtAddress.setText(address);
            binding.txtAbout.setText(dispensaryModel.getAbout_us());

            if (dispensaryModel.getOpening_hours()!=null && dispensaryModel.getOpening_hours().size()>0)
            {
                setHoursOfOperation(dispensaryModel.getOpening_hours());
            }
            else
            {
                showToast(context,"Data not found!");
            }

        }
        else
        {
            showToast(context,"Data not found!");
        }
    }


    void setHoursOfOperation(ArrayList<OpeningHours>arrayList)
    {
         for (int i=0;i<arrayList.size();i++)
         {
             OpeningHours openingHours=arrayList.get(i);
             if (openingHours!=null)
             {
                 if (WeekDayEnum.Monday.isEqual(openingHours.getDay()))
                 {
                   setOpeningTime(binding.txtMonOpeningTime,binding.txtMonClosingTime,binding.monOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Tuesday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtTueOpeningTime,binding.txtTueClosingTime,binding.tueOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Wednesday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtWedOpeningTime,binding.txtWedClosingTime,binding.wedOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Thursday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtThuOpeningTime,binding.txtThuClosingTime,binding.thuOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Friday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtFriOpeningTime,binding.txtFriClosingTime,binding.friOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Saturday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtSatOpeningTime,binding.txtSatClosingTime,binding.satOpenClosed,openingHours,openingHours.getDay());
                 }
                 else if (WeekDayEnum.Sunday.isEqual(openingHours.getDay()))
                 {
                     setOpeningTime(binding.txtSunOpeningTime,binding.txtSunClosingTime,binding.sunOpenClosed,openingHours,openingHours.getDay());
                 }
             }
         }
    }

    public void setOpeningTime(MaterialTextView txtOpenTime,MaterialTextView txtCloseTime,MaterialTextView txtOpenClose,OpeningHours openingHours,String day)
    {
       txtOpenTime.setText(openingHours.getIn_time());
       txtCloseTime.setText(openingHours.getOut_time());

       if (day.equals(AppUtil.getDayOfWeek()))
       {
           if (AppUtil.isDispensaryOpen(openingHours.getIn_time(),openingHours.getOut_time()))
           {
               txtOpenClose.setVisibility(View.VISIBLE);
               txtOpenClose.setText("Open Now");
               txtOpenClose.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
           }
           else
           {
               txtOpenClose.setVisibility(View.INVISIBLE);
               txtOpenClose.setText("Closed") ;
           }
       }
       else
       {
           txtOpenClose.setVisibility(View.INVISIBLE);
       }

    }

}
