package com.splitreef.grabagramplaystore.data.model.driver.vehicle;

import java.io.Serializable;

/**
 * Created by  on 22-01-2021.
 */
public class VehicleModel implements Serializable {
    private String company_name;
    //private String created_at;
    private String id;
   // private String updated_at;
    private String vahicle_model;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getVahicle_model() {
        return vahicle_model;
    }

    public void setVahicle_model(String vahicle_model) {
        this.vahicle_model = vahicle_model;
    }
}
