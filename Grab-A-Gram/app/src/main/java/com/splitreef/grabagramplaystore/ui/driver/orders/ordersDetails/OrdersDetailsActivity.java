package com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.OrderItemsAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.orders.ItemsModel;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityOrderDetailsBinding;
import com.splitreef.grabagramplaystore.dialog.CustomDilalog;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.fcmNotification.CallSendNotificationApi;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersViewModel;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.TrackPendingOrderActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class OrdersDetailsActivity extends BaseActivity implements View.OnClickListener {
    private final String TAG = "OrderDetailsActivity";
    private ActivityOrderDetailsBinding binding;
    private GrabAGramApplication application;
    private LinearLayoutManager linearLayoutManager;
    private OrderItemsAdapter orderItemsAdapter;
    private PendingOrdersViewModel viewModel;
    private ArrayList<ItemsModel> itemsList = new ArrayList<>();
    private String orderStatus = "";
    private OrdersModel myOrdersModel = new OrdersModel();
    private String message = "";
    public static String phoneNumber = "";
    double latitude;
    double longitude;
    String Id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        GPSTracker gpsTracker = new GPSTracker(this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);

        initView();
    }


    // initialized the view
    void initView() {
        if (getIntent() != null) {
            Id = getIntent().getStringExtra(AppConstant.ID);
        }
        application = (GrabAGramApplication) Objects.requireNonNull(this).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory
                (Objects.requireNonNull(this).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);

        linearLayoutManager = new LinearLayoutManager(OrdersDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);
        binding.recycleView.setAdapter(orderItemsAdapter);

        binding.imvCarDelivery.setOnClickListener(this);
        binding.imvDispensaryCar.setOnClickListener(this);
        binding.imvDispensaryCall.setOnClickListener(this);
        binding.imvCallDelivery.setOnClickListener(this);
        binding.btnSupport.setOnClickListener(this);

        binding.btnAccept.setOnClickListener(this);
        binding.btnDecline.setOnClickListener(this);
        binding.imvBack.setOnClickListener(this);
        binding.btnConfirmPickup.setOnClickListener(this);
        binding.btnSingleDecline.setOnClickListener(this);
        binding.btnReportFailedDelivery.setOnClickListener(this);
        binding.btnConfirmDelivery.setOnClickListener(this);
        binding.btnReturn.setOnClickListener(this);

        subscribeObservers();
        if (Id != null && !Id.equalsIgnoreCase("")) {
            viewModel.getOrderDetails(Id);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_back:
                finish();
                break;
            case R.id.btn_confirm_pickup:
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        orderStatus = getResources().getString(R.string.picked);
                        viewModel.updateOrderToAccept(Id, AppConstant.PICKED);

                    }
                }.showCustomDialog(this, getResources().getString(R.string.confirm_pickup), getResources().getString(R.string.have_you_pickup_this_order), "Yes", "No");
                break;
            case R.id.btn_confirm_delivery:
                confirmationCodeDialog();
                break;
            case R.id.btn_report_failed_delivery:
                deliveryFailedDialog();
                break;
            case R.id.btn_return:
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        orderStatus = getResources().getString(R.string.return_pending);
                        viewModel.updateOrderToAccept(Id, AppConstant.RETURN_PENDING);

                    }
                }.showCustomDialog(this, getResources().getString(R.string.return_), getResources().getString(R.string.are_you_sure_want_to_return_this_order), "Yes", "No");


                break;
            case R.id.imv_dispensary_car:
                openTrackActivity(getResources().getString(R.string.despensary));
                break;
            case R.id.imv_dispensary_call:
                if (!NetworkManager.isNetworkAvailable(this)) {
                    Toast.makeText(this, "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    phoneNumber = myOrdersModel.getDispensary_data().getAddress().getPhone_number();
                    requestStorageAndCameraPermission(this, BaseActivity.PHONE_CALL);

                }
                //makeACall(myOrdersModel.getDispensary_data().getAddress().getPhone_number());
                break;
            case R.id.imv_car_delivery:
                openTrackActivity(getResources().getString(R.string.delivery));
                break;
            case R.id.imv_call_delivery:
                if (!NetworkManager.isNetworkAvailable(this)) {
                    Toast.makeText(this, "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    phoneNumber = myOrdersModel.getAddresses().get(0).getPhone_number();
                    requestStorageAndCameraPermission(this, BaseActivity.PHONE_CALL);

                }
                //makeACall(myOrdersModel.getAddresses().get(0).getPhone_number());
                break;
            case R.id.btn_support:
                sendEmail();
                break;
            case R.id.btn_accept:
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        viewModel.getOrderStatus(Id);
                        //orderStatus = getResources().getString(R.string.accept);
                        //viewModel.updateOrderToAccept(Id, AppConstant.ACCEPT);

                    }
                }.showCustomDialog(this, getResources().getString(R.string.confirm_accept), getResources().getString(R.string.are_you_sure_you_want_to_accept_this_order), "Yes", "No");


                break;
            case R.id.btn_decline:
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        viewModel.updateOrderToDecline(Id);

                    }
                }.showCustomDialog(this, getResources().getString(R.string.order_decline), getResources().getString(R.string.are_you_sure_want_to_decline_of_this_order), "Yes", "No");
                break;
            case R.id.btn_single_decline:
                new CustomDilalog() {
                    @Override
                    public void onPositiveButtonClick() {
                        orderStatus = getResources().getString(R.string.scheduled);
                        viewModel.updateOrderToScheduled(Id, AppConstant.SCHEDULED);

                    }
                }.showCustomDialog(this, getResources().getString(R.string.order_decline), getResources().getString(R.string.are_you_sure_want_to_decline_of_this_order), "Yes", "No");
                break;
        }

    }

    private void openTrackActivity(String from) {
        Intent intent = new Intent(OrdersDetailsActivity.this, TrackPendingOrderActivity.class);
        if (from.equalsIgnoreCase(getResources().getString(R.string.despensary))) {
            intent.putExtra(AppConstant.LATITUDE, myOrdersModel.getDispensary_data().getAddress().getLatitude());
            intent.putExtra(AppConstant.LONGITUDE, myOrdersModel.getDispensary_data().getAddress().getLongitude());
        } else if (from.equalsIgnoreCase(getResources().getString(R.string.delivery))) {
            intent.putExtra(AppConstant.LATITUDE, myOrdersModel.getAddresses().get(0).getLatitude());
            intent.putExtra(AppConstant.LONGITUDE, myOrdersModel.getAddresses().get(0).getLongitude());
        }
        startActivity(intent);
    }

    private void putOrderDetailsData(OrdersModel ordersModel) {
        if (ordersModel != null && ordersModel.getOrder_no() != null) {
            myOrdersModel = ordersModel;
            binding.tvScheduleFor.setText("Schedule for " + AppUtil.getDateFromMillisecondTime(myOrdersModel.getCreated_at()) +
                    " at " + AppUtil.getTimeFromMillisecondTime(myOrdersModel.getCreated_at()));

            //delivery details
            binding.tvCustomerName.setText(myOrdersModel.getCustomer_name());
            binding.tvCustomerAddress.setText(myOrdersModel.getAddresses().get(0).getStreet_1() + "," +
                    myOrdersModel.getAddresses().get(0).getStreet_2() + "," + myOrdersModel.getAddresses().get(0).getState());
            double distance = new GetDistance().distance(latitude, longitude, myOrdersModel.getDispensary_location().getLatitude(), myOrdersModel.getDispensary_location().getLongitude());
            binding.tvMilesAway.setText(String.format("%.2f %s", distance, "MILES AWAY"));
            // binding.tvCustomerTimeInMinute.setText();


            //dispensary details
            binding.tvBusinessName.setText(myOrdersModel.getDispensary_data().getBusniess_name());
            binding.tvDispensaryAddress.setText(myOrdersModel.getDispensary_data().getAddress().getStreet1() + "," + myOrdersModel.getDispensary_data().getAddress().getStreet2() + "," + myOrdersModel.getAddresses().get(0).getState());
            double distance2 = new GetDistance().distance(latitude, longitude, myOrdersModel.getDispensary_data().getAddress().getLatitude(),
                    myOrdersModel.getDispensary_data().getAddress().getLongitude());
            binding.tvDispensaryMilesAway.setText(String.format("%.2f %s", distance2, "MILES AWAY"));


            //order info
            binding.tvOrderDate.setText(AppUtil.getDateFromMillisecond(myOrdersModel.getCreated_at()));
            binding.tvOrderNumber.setText(myOrdersModel.getOrder_no());
            binding.tvOrderTotal.setText("" + myOrdersModel.getGrand_total());


            //order item
            itemsList = myOrdersModel.getItems();
            if (itemsList.size() > 0) {
                orderItemsAdapter = new OrderItemsAdapter(this, itemsList);
                binding.recycleView.setAdapter(orderItemsAdapter);
                binding.cardItem.setVisibility(View.VISIBLE);
            } else {
                binding.cardItem.setVisibility(View.GONE);

            }

            //order summary
            binding.tvOrderSubtotal.setText("" + myOrdersModel.getSub_total());
            binding.tvDeliveryFee.setText("" + myOrdersModel.getDelivery_fee());
            binding.tvSalesTax.setText("" + myOrdersModel.getSales_tax());
            binding.tvPaymentMethod.setText(myOrdersModel.getPayment_method().getType());
            binding.tvOrderTotalSummery.setText("" + myOrdersModel.getGrand_total());


            if (myOrdersModel.getStatus().equalsIgnoreCase(getResources().getString(R.string.completed))) {
                binding.tvOrderStatus.setText(getResources().getString(R.string.completed));
                binding.btnReportFailedDelivery.setVisibility(View.GONE);
                binding.btnConfirmDelivery.setVisibility(View.GONE);
                binding.llAcceptDecline.setVisibility(View.GONE);
                binding.btnSupport.setVisibility(View.GONE);
                binding.tvNeedAssistance.setVisibility(View.GONE);
                binding.llOrderStatus.setVisibility(View.VISIBLE);
                binding.tvOrderStatus.setText(getResources().getString(R.string.completed));
            } else if (myOrdersModel.getStatus().equalsIgnoreCase(getResources().getString(R.string.accept))) {
                binding.btnSingleDecline.setVisibility(View.VISIBLE);
                binding.btnConfirmPickup.setVisibility(View.VISIBLE);
                binding.llAcceptDecline.setVisibility(View.GONE);
            } else if (myOrdersModel.getStatus().equalsIgnoreCase(getResources().getString(R.string.picked))) {
                binding.llAcceptDecline.setVisibility(View.GONE);
                binding.btnReportFailedDelivery.setVisibility(View.VISIBLE);
                binding.btnConfirmDelivery.setVisibility(View.VISIBLE);
                binding.llOrderStatus.setVisibility(View.VISIBLE);
                binding.btnConfirmPickup.setVisibility(View.GONE);
                binding.btnSingleDecline.setVisibility(View.GONE);
                binding.btnConfirmPickup.setVisibility(View.GONE);

            }

        } else {
            Toast.makeText(OrdersDetailsActivity.this, "data is null", Toast.LENGTH_SHORT).show();
        }
    }

    void subscribeObservers() {

        viewModel.observeGetOrderDetails().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            OrdersModel ordersModel = (OrdersModel) dataResource.data;
                            String jsonString = new Gson().toJson(ordersModel);
                            Log.d("jsonString", "" + jsonString);
                            putOrderDetailsData(ordersModel);
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });

        viewModel.observeGetOrderStatsu().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String myOrderStatus = (String) dataResource.data;
                            Log.d("orderStatus", "" + myOrderStatus);
                            if (myOrderStatus != null && !myOrderStatus.equalsIgnoreCase("")) {
                                if (myOrderStatus.equalsIgnoreCase(AppConstant.SCHEDULED)) {
                                    orderStatus = getResources().getString(R.string.accept);
                                    viewModel.updateOrderToAccept(Id, AppConstant.ACCEPT);
                                } else {
                                    new SimpleDialog() {
                                        @Override
                                        public void onPositiveButtonClick() {
                                            finish();
                                        }
                                    }.showSimpleDialog(OrdersDetailsActivity.this, "Message", getResources().getString(R.string.order_already_accepted_by_another_driver), "OK");

                                }
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });

        viewModel.observeUpdateOrderToAccept().observe(Objects.requireNonNull(OrdersDetailsActivity.this), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.accept))) {
                                binding.btnSingleDecline.setVisibility(View.VISIBLE);
                                binding.btnConfirmPickup.setVisibility(View.VISIBLE);
                                binding.llAcceptDecline.setVisibility(View.GONE);
                                viewModel.getUserDetails(myOrdersModel.getUser_id());
                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.picked))) {

                                binding.tvOrderStatus.setText(getResources().getString(R.string.picked));
                                binding.btnReportFailedDelivery.setVisibility(View.VISIBLE);
                                binding.btnConfirmDelivery.setVisibility(View.VISIBLE);
                                binding.llOrderStatus.setVisibility(View.VISIBLE);
                                binding.btnConfirmPickup.setVisibility(View.GONE);
                                binding.btnSingleDecline.setVisibility(View.GONE);
                                binding.btnConfirmPickup.setVisibility(View.GONE);

                                viewModel.updateDriverLocation(latitude, longitude, myOrdersModel.getId());

                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.completed))) {
                                binding.tvOrderStatus.setText(getResources().getString(R.string.completed));
                                binding.btnReportFailedDelivery.setVisibility(View.GONE);
                                binding.btnConfirmDelivery.setVisibility(View.GONE);
                                viewModel.getUserDetails(myOrdersModel.getUser_id());
                                // show dialog
                                new SimpleDialog() {
                                    @Override
                                    public void onPositiveButtonClick() {
                                        finish();

                                    }
                                }.showSimpleDialog(OrdersDetailsActivity.this, "", getResources().getString(R.string.you_have_successfully_completed_thid_job), "OK");


                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.delivery_failed))) {
                                binding.tvOrderStatus.setText(getResources().getString(R.string.delivery_failed));
                                binding.btnReportFailedDelivery.setVisibility(View.GONE);
                                binding.btnConfirmDelivery.setVisibility(View.GONE);
                                binding.btnSupport.setVisibility(View.GONE);
                                binding.tvNeedAssistance.setVisibility(View.GONE);
                                binding.btnReturn.setVisibility(View.VISIBLE);
                                // show dialog
                                new SimpleDialog() {
                                    @Override
                                    public void onPositiveButtonClick() {

                                    }
                                }.showSimpleDialog(OrdersDetailsActivity.this, "Message", message, "OK");
                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.scheduled))) {
                                viewModel.updateOrderToDecline(Id);
                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.return_pending))) {
                                finish();
                            }
                            //getDispensaryList();
                            // application.dialogManager.displayProgressDialog(getActivity(),"Registration completed successfully");
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(OrdersDetailsActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeUpdateOrderToScheduled().observe(Objects.requireNonNull(OrdersDetailsActivity.this), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.scheduled))) {
                                viewModel.updateOrderToDecline(Id);
                            } else if (orderStatus != "" && orderStatus.equalsIgnoreCase(getResources().getString(R.string.return_pending))) {
                                finish();
                            }
                            //getDispensaryList();
                            // application.dialogManager.displayProgressDialog(getActivity(),"Registration completed successfully");
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(OrdersDetailsActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeUpdateOrderToDecline().observe(Objects.requireNonNull(this), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            finish();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(OrdersDetailsActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeGetUserDetails().observe(this, new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            UserModel userModel = (UserModel) dataResource.data;
                            String jsonString = new Gson().toJson(userModel);
                            Log.d("jsonString", "" + jsonString);
                            if (userModel.getDevice_id() != null && !userModel.getDevice_id().equalsIgnoreCase("")) {
                                prepareNotificationBody(userModel);
                            } else {
                                Toast.makeText(OrdersDetailsActivity.this, "Unable to send notification, customer don't have device id", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });


        viewModel.observeUpdateDriverLocation().observe(Objects.requireNonNull(this), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(OrdersDetailsActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            viewModel.getUserDetails(myOrdersModel.getUser_id());
                            //finish();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(OrdersDetailsActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }

    public void confirmationCodeDialog() {
        Dialog dialogs = new Dialog(this, R.style.custom_dialog_style);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogs.setCancelable(false);
        dialogs.setContentView(R.layout.dialog_submit);

        TextView tv_tittle = dialogs.findViewById(R.id.tv_tittle);
        TextView tv_message = dialogs.findViewById(R.id.tv_message);
        EditText edt_code = dialogs.findViewById(R.id.edt_code);
        Button btn_submit = dialogs.findViewById(R.id.btn_submit);
        ImageView imv_close = dialogs.findViewById(R.id.imv_close);
        imv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_code.getText().toString().equals("")) {
                    edt_code.setError("Please enter confirmation code");
                    edt_code.requestFocus();


                } else {
                    if (myOrdersModel.getConfirmation_code().equals(edt_code.getText().toString())) {
                        dialogs.dismiss();
                        orderStatus = getResources().getString(R.string.completed);
                        viewModel.updateOrderToAccept(Id, AppConstant.COMPLETED);

                    } else {
                        // show dialog
                        new SimpleDialog() {
                            @Override
                            public void onPositiveButtonClick() {

                            }
                        }.showSimpleDialog(OrdersDetailsActivity.this, "Message", "Please enter valid confirmation code.", "OK");

                    }
                    dialogs.dismiss();
                }
            }
        });

        dialogs.show();

    }


    public void deliveryFailedDialog() {

        Dialog dialogs = new Dialog(this, R.style.custom_dialog_style);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogs.setCancelable(false);
        dialogs.setContentView(R.layout.dialog_delivery_failed);

        TextView tv_tittle = dialogs.findViewById(R.id.tv_tittle);
        ImageView imv_close = dialogs.findViewById(R.id.imv_close);
        Button btn_submit = dialogs.findViewById(R.id.btn_submit);
        RadioButton rb_customer_decline_product = dialogs.findViewById(R.id.rb_customer_decline_product);
        RadioButton rb_customer_was_not_home = dialogs.findViewById(R.id.rb_customer_was_not_home);
        RadioButton rb_destination_not_found = dialogs.findViewById(R.id.rb_destination_not_found);
        imv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!rb_customer_decline_product.isChecked() && !rb_customer_was_not_home.isChecked() && !rb_destination_not_found.isChecked()) {
                    showToast("Please choose a delivery failed reason");
                } else {
                    dialogs.dismiss();
                    if (rb_customer_decline_product.isChecked()) {
                        message = getResources().getString(R.string.customer_decline_product);
                    } else if (rb_customer_was_not_home.isChecked()) {
                        message = getResources().getString(R.string.customer_was_not_at_home);
                    } else if (rb_destination_not_found.isChecked()) {
                        message = getResources().getString(R.string.destination_not_found);
                    }
                    orderStatus = getResources().getString(R.string.delivery_failed);
                    viewModel.updateOrderToAccept(Id, getResources().getString(R.string.delivery_failed));


                }
            }
        });

        dialogs.show();

    }


    private void prepareNotificationBody(UserModel userDriverModel) {
        String userDeviceID = "", notiOrderStatus = "";
        userDeviceID = userDriverModel.getDevice_id();
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        JSONObject notifcationMessage = new JSONObject();
        JSONObject notifcationData = new JSONObject();
        if (orderStatus.equalsIgnoreCase(getResources().getString(R.string.accept))) {
            notiOrderStatus = AppConstant.ORDER_ACCEPTED;
        } else if (orderStatus.equalsIgnoreCase(getResources().getString(R.string.picked))) {
            notiOrderStatus = AppConstant.ORDER_PICKED;
        } else if (orderStatus.equalsIgnoreCase(getResources().getString(R.string.delivery_failed))) {
            notiOrderStatus = AppConstant.ORDER_DELIVERY_FAILED;
        } else if (orderStatus.equalsIgnoreCase(getResources().getString(R.string.completed))) {
            notiOrderStatus = AppConstant.ORDER_COMPLETED;
        }


        try {
            String strstring = "Your order number " + myOrdersModel.getOrder_no() + " " + notiOrderStatus;
            notifcationMessage.put(AppConstant.TITLE, notiOrderStatus);
            notifcationMessage.put(AppConstant.BODY, strstring);
            notifcationMessage.put(AppConstant.BADGE_COUNT, 0);
            notifcationMessage.put(AppConstant.SOUND, "default");

            notifcationBody.put(AppConstant.TITLE, notiOrderStatus);
            notifcationBody.put(AppConstant.BODY, strstring);
            notifcationBody.put(AppConstant.NOTIFICATION_TYPE, AppConstant.DRIVER_OPERATION);
            notifcationBody.put(AppConstant.ORDER_ID, myOrdersModel.getId());

            UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
            if (userModel != null) {
                if (userModel.getRole().equals(AppConstant.CUSTOMER_ROLE)) {
                    notifcationBody.put(AppConstant.USER_TYPE, AppConstant.CUSTOMER_ROLE);
                } else if (userModel.getRole().equals(AppConstant.DRIVER_ROLE)) {
                    notifcationBody.put(AppConstant.USER_TYPE, AppConstant.DRIVER_ROLE);
                }

            }
            notifcationData.put("data", notifcationBody);
            notification.put("to", userDriverModel.getDevice_id());
            notification.put("notification", notifcationMessage);
            notification.put("data", notifcationData);
            Log.d("Token::", "" + PreferenceManger.getPreferenceManger().getString(PrefKeys.FCM_TOKEN));
        } catch (JSONException e) {
            Log.e(TAG, "onCreate: " + e.getMessage());
        }

        new CallSendNotificationApi(OrdersDetailsActivity.this).sendNotification(notification);

    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppConstant.EMAIL});
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, ""));
    }


}
