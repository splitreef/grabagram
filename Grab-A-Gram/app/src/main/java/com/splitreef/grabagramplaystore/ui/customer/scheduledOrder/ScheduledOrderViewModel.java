package com.splitreef.grabagramplaystore.ui.customer.scheduledOrder;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ScheduledOrderViewModel extends ViewModel {

    private static final String TAG = "ScheduledOrderViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetPlacedOrders = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public ScheduledOrderViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getPlacedOrders(String status)
    {
         firebaseRepository.getPlacedOrders(status)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<GetPlacedOrdersResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {
                         disposable.add(d);
                         onGetPlacedOrders.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull GetPlacedOrdersResponse placedOrdersResponse) {

                         onGetPlacedOrders.setValue(DataResource.DataStatus(placedOrdersResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onGetPlacedOrders.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }



    LiveData<DataResource> observeGetPlacedOrders()
    {
        return onGetPlacedOrders;
    }




}
