package com.splitreef.grabagramplaystore.data.model.customer.myAccount;

import java.io.Serializable;

public class CustomerDocumentInfo implements Serializable {
    private String document_type;
    private long expiration_date;
    private String identity_number;
    private String issuing_state;
    private String patient_id;
    private long recommendation_letter_expiration_date;


    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public long getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(long expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getIssuing_state() {
        return issuing_state;
    }

    public void setIssuing_state(String issuing_state) {
        this.issuing_state = issuing_state;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public long getRecommendation_letter_expiration_date() {
        return recommendation_letter_expiration_date;
    }

    public void setRecommendation_letter_expiration_date(long recommendation_letter_expiration_date) {
        this.recommendation_letter_expiration_date = recommendation_letter_expiration_date;
    }
}
