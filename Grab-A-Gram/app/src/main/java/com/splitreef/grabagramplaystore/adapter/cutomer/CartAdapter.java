package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;

import com.splitreef.grabagramplaystore.callBack.UpdateCartProduct;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;

import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {

    private Context context;
    private ArrayList<ProductCart>productCartArrayList;
    private UpdateCartProduct updateCartProduct;


    public CartAdapter(Context context,ArrayList<ProductCart>productCartArrayList,UpdateCartProduct updateCartProduct) {
        this.context=context;
        this.productCartArrayList=productCartArrayList;
        this.updateCartProduct=updateCartProduct;
    }

    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,parent,false);
        return new CartHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartHolder holder, int position) {

        ProductCart productCart=productCartArrayList.get(position);
        PicassoManager.setImage(productCart.getImages().get(0),holder.imgProduct);
        holder.txtProductName.setText(""+productCart.getName());
        holder.txtDispensaryName.setText("Sold By: "+productCart.getBusniess_name());
        //holder.txtProductPrice.setText("$"+productCart.getSale_price());
        holder.txtQuantity.setText(""+productCart.getQuantity());

        double  totalPrice=productCart.getSale_price()*productCart.getQuantity();
        holder.txtProductPrice.setText("$"+productCart.getSale_price()+"X"+productCart.getQuantity()+"=$"+totalPrice);


        holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int quantity = productCart.getQuantity();

                if (quantity<=4)
                {
                    ++quantity;
                    double totalPrice= quantity*productCart.getSale_price();
                    updateCartProduct.updateCartProduct(productCart.getId(),quantity,totalPrice);
                }
                else
                {
                    Toast.makeText(context,"You can add max 5 quantity of product.",Toast.LENGTH_SHORT).show();
                }


            }
        });


        holder.imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!holder.txtQuantity.getText().toString().equals("1"))
                {
                    int quantity = productCart.getQuantity();
                    --quantity;
                    double totalPrice= quantity*productCart.getSale_price();

                    updateCartProduct.updateCartProduct(productCart.getId(),quantity,totalPrice);
                }

            }
        });

        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog() {
                    @Override
                    public void onPositiveButtonClick() {
                        if (productCartArrayList.size()==1)
                        {
                            updateCartProduct.removeCartProduct(productCart.getId(),true);
                        }
                        else
                        {
                            updateCartProduct.removeCartProduct(productCart.getId(),false);
                        }

                    }
                }.showAlertDialog(context,"Delete",context.getString(R.string.cart_delete),"OK","CANCEL");

            }
        });

    }

    @Override
    public int getItemCount() {
        return productCartArrayList.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder
    {
        AppCompatImageView imgProduct;
        AppCompatImageView imgRemove;
        AppCompatImageButton imgMinus;
        AppCompatImageButton imgPlus;
        MaterialTextView txtProductName;
        MaterialTextView txtProductPrice;
        MaterialTextView txtDispensaryName;
        MaterialTextView txtQuantity;

        public CartHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct= itemView.findViewById(R.id.img_product);
            txtProductName= itemView.findViewById(R.id.txt_product_name);
            txtProductPrice= itemView.findViewById(R.id.txt_product_price);
            txtDispensaryName= itemView.findViewById(R.id.txt_dispensary_name);
            txtQuantity= itemView.findViewById(R.id.txt_quantity);
            imgRemove= itemView.findViewById(R.id.img_remove);
            imgMinus= itemView.findViewById(R.id.img_minus);
            imgPlus= itemView.findViewById(R.id.img_plus);

        }
    }
}
