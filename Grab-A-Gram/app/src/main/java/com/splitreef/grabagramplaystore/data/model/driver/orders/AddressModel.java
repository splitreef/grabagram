package com.splitreef.grabagramplaystore.data.model.driver.orders;

import java.io.Serializable;

/**
 * Created by  on 01-12-2020.
 */
public class AddressModel implements Serializable {
    private String city;
    private long created_at;
    private String first_name;
    private String last_name;
    private double latitude;
    private double longitude;
    private String method_of_contact;
    private String name_of_contact;
    private String phone_number;
    private String state;
    private String street1;
    private String street2;
    private String type;
    private long updated_at;
    private String zip_code;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getMethod_of_contact() {
        return method_of_contact;
    }

    public void setMethod_of_contact(String method_of_contact) {
        this.method_of_contact = method_of_contact;
    }

    public String getName_of_contact() {
        return name_of_contact;
    }

    public void setName_of_contact(String name_of_contact) {
        this.name_of_contact = name_of_contact;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }
}
