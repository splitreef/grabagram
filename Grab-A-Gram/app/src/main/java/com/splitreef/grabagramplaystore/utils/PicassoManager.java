package com.splitreef.grabagramplaystore.utils;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.splitreef.grabagramplaystore.R;
import com.squareup.picasso.Picasso;

public class PicassoManager {

    public static void setImage(String url, ImageView imageView) {

        try {

            if (url != null) {
                Picasso.get()
                        .load(url)
                        .placeholder(R.drawable.no_image_found_place_holder) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.no_image_found_place_holder)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(imageView);
            }

        }catch (Exception ex)
        {
            Log.e("Error","="+ex.getMessage());
        }


    }

    public static void setImage(Context context, String url, AppCompatImageView imageView) {
        Glide.with(context).load(url)
                .into(imageView);
    }

}
