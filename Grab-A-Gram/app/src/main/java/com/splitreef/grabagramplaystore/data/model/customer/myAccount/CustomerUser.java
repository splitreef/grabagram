package com.splitreef.grabagramplaystore.data.model.customer.myAccount;

import java.io.Serializable;

public class CustomerUser implements Serializable {
    private String email;
    private boolean email_communication;
    private String user_id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getEmail_communication() {
        return email_communication;
    }

    public void setEmail_communication(boolean email_communication) {
        this.email_communication = email_communication;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
