package com.splitreef.grabagramplaystore.ui.driver.orders.home;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.DriverLocationStatusModel;
import com.splitreef.grabagramplaystore.data.model.driver.driverRating.MyDriverRating;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by  on 30-11-2020.
 */
public class HomeViewModel extends ViewModel {
    private static final String TAG = "HomeViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetUpcoimgDeliveriesList = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetPastDeliveriesList = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateActiveStatus = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateDriverStatus= new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateInActiveStatus = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetDriverStatus = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUserDetails = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetDriverRatingList = new MediatorLiveData<>();


    private Application mApplication;
    private CompositeDisposable disposable = new CompositeDisposable();

    public HomeViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication = mApplication;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    public void userLogout() {
        firebaseRepository.userLogout();
    }


    public void getUpcomingDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getUpcomingDeliveriesList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetUpcoimgDeliveriesList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetUpcoimgDeliveriesList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetUpcoimgDeliveriesList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetUpcomingDeliveriesList() {
        return onGetUpcoimgDeliveriesList;
    }


    public void getPastDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {
        firebaseRepository.getPastDeliveriesList(latitude, longitude, distance, isFirst)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<OrdersModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetPastDeliveriesList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<OrdersModel> ordersModels) {
                        onGetPastDeliveriesList.setValue(DataResource.DataStatus(ordersModels));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetPastDeliveriesList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetPastDeliveriesList() {
        return onGetPastDeliveriesList;
    }

    public void updateActiveStatus(DriverLocationStatusModel driverLocationStatusModel) {
        firebaseRepository.updateActiveStatus(driverLocationStatusModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateActiveStatus.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateActiveStatus.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateActiveStatus.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }

    public LiveData<StateResource> observeUpdateonActiveStatus() {
        return onUpdateActiveStatus;
    }


    public void updateDriverOnlineOfflineStatus(String driverStatus) {
        firebaseRepository.updateDriverOnlineOfflineStatus(driverStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateDriverStatus.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onUpdateDriverStatus.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateDriverStatus.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    public LiveData<StateResource> observeUpdateDriverStatus() {
        return onUpdateDriverStatus;
    }


    public void getDriverStatus() {
        firebaseRepository.getDriverStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DriverLocationStatusModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetDriverStatus.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull DriverLocationStatusModel driverLocationStatusModel) {
                        onGetDriverStatus.setValue(DataResource.DataStatus(driverLocationStatusModel));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDriverStatus.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetDriverStatus() {
        return onGetDriverStatus;
    }


    public void getUserDetails(String userID) {
        firebaseRepository.getUserDetails(userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onUserDetails.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull UserModel userModel) {
                        onUserDetails.setValue(DataResource.DataStatus(userModel));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUserDetails.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetUserDetails() {
        return onUserDetails;
    }


    public void getDriverRatingList() {
        firebaseRepository.getDriverRatingList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<MyDriverRating>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetDriverRatingList.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<MyDriverRating> ratingList) {
                        onGetDriverRatingList.setValue(DataResource.DataStatus(ratingList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetDriverRatingList.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    public LiveData<DataResource> observeGetDriverRatingList() {
        return onGetDriverRatingList;
    }


}
