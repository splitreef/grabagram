package com.splitreef.grabagramplaystore.data.model.customer.productReviewResponse;

import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;

import java.io.Serializable;

public class ProductReviewResponse implements Serializable {
    private String status;
    private ProductReviewModel productReviewModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProductReviewModel getProductReviewModel() {
        return productReviewModel;
    }

    public void setProductReviewModel(ProductReviewModel productReviewModel) {
        this.productReviewModel = productReviewModel;
    }
}
