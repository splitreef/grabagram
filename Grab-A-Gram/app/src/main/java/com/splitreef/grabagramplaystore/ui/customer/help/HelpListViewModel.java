package com.splitreef.grabagramplaystore.ui.customer.help;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.help.HelpListResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HelpListViewModel extends ViewModel {

    private static final String TAG = "HelpListViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onHelpList = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public HelpListViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getHelpList()
    {
         firebaseRepository.getHelpList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<HelpListResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onHelpList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull HelpListResponse helpListResponse) {

                         onHelpList.setValue(DataResource.DataStatus(helpListResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onHelpList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }






    LiveData<DataResource> observeHelpList()
    {
        return onHelpList;
    }





}
