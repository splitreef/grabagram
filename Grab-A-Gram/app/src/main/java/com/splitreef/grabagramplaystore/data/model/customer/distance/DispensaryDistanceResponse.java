package com.splitreef.grabagramplaystore.data.model.customer.distance;

import java.io.Serializable;
import java.util.ArrayList;

public class DispensaryDistanceResponse implements Serializable {

    private ArrayList<Distance>distanceList;
    private String status;

    public ArrayList<Distance> getDistanceList() {
        return distanceList;
    }

    public void setDistanceList(ArrayList<Distance> distanceList) {
        this.distanceList = distanceList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
