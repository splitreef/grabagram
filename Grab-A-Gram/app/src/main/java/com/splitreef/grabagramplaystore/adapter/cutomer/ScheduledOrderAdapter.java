package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.ui.customer.orderDetails.CustomerOrderDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

public class ScheduledOrderAdapter extends RecyclerView.Adapter<ScheduledOrderAdapter.ScheduledOrderHolder> {

    private Context context;
    private ArrayList<Order>orderRequestList;



    public ScheduledOrderAdapter(Context context,ArrayList<Order>orderRequestList) {
        this.context=context;
        this.orderRequestList=orderRequestList;

    }

    @NonNull
    @Override
    public ScheduledOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scheduled_order,parent,false);
        return new ScheduledOrderHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduledOrderHolder holder, int position) {
        Order order=orderRequestList.get(position);
        holder.txtOrderNo.setText("ORDER #"+order.getOrder_no());
        holder.txtDate.setText("SCHEDULED FOR "+ AppUtil.getDateFromMillisecond(order.getCreated_at()));

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CustomerOrderDetailsActivity.class)
                //.putExtra(AppConstant.FROM,AppConstant.FROM_SCHEDULED_ORDER_ADAPTER)
                .putExtra(AppConstant.ORDER_ID,order.getId()));
                ((DispensaryListActivity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderRequestList.size();
    }

    public class ScheduledOrderHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtOrderNo;
        MaterialTextView txtDate;
        LinearLayout llRoot;

        public ScheduledOrderHolder(@NonNull View itemView) {
            super(itemView);
            txtOrderNo = itemView.findViewById(R.id.txt_order_no);
            txtDate = itemView.findViewById(R.id.txt_date);
            llRoot = itemView.findViewById(R.id.ll_root);
        }
    }
}
