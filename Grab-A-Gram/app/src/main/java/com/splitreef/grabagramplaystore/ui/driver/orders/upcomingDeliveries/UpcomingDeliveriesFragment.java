package com.splitreef.grabagramplaystore.ui.driver.orders.upcomingDeliveries;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.driver.UpcomingDeliveriesAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.databinding.FragmentUpcomingDeliveriesBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersViewModel;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class UpcomingDeliveriesFragment extends BaseFragment {
    private FragmentUpcomingDeliveriesBinding binding;
    private PendingOrdersViewModel viewModel;
    private GrabAGramApplication application;
    private int currentItem, totalItemCount, scrollOutItems;
    private boolean isScrolling;
    private LinearLayoutManager linearLayoutManager;
    private UpcomingDeliveriesAdapter upcomingDeliveriesAdapter;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    ArrayList<OrdersModel> OrdersModelArrayList;
    double latitude;
    double longitude;
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUpcomingDeliveriesBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        isShowLoaderDialog = true;
        isFirst = true;
        viewModel.getUpcomingDeliveriesList(latitude, longitude, HomeFragment.miles, true);
    }

    public void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);

        ((HomeActivity)context).setToolbarTitle("UPCOMING DELIVERIES");

        HomeActivity.tvActiveStatus.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);
        /*    binding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState== AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling= true;
                }
            }

          @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem= linearLayoutManager.getChildCount();
                totalItemCount= linearLayoutManager.getItemCount();
                scrollOutItems=linearLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItem+ scrollOutItems== totalItemCount))
                {
                    isScrolling=false;
                    isShowLoaderDialog=false;
                    isFirst=false;
                    viewModel.getDispensaryList(latitude,longitude,40,isFirst);

                }
            }
        });*/
        subscribeObservers();
    }


    public void getDispensaryList() {
        if (viewModel != null)
            viewModel.getUpcomingDeliveriesList(latitude, longitude, HomeFragment.miles, true);
    }

    void subscribeObservers() {
        viewModel.observeGetUpcomingDeliveriesList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            if (isShowLoaderDialog) {
                                // application.dialogManager.displayProgressDialog(getActivity(),"Loading...");
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:

                          /*  if (isShowLoaderDialog)
                            {
                                application.dialogManager.dismissProgressDialog();
                                dispensaryModelArrayList= (ArrayList<DispensaryModel>) dataResource.data;
                                setUpcomingDeliveriesList(dispensaryModelArrayList);
                            }
                            else
                            {
                                if (dispensaryAdapter!=null)
                                {
                                    dispensaryAdapter.notifyDataSetChanged();
                                }
                                binding.progressBar.setVisibility(View.GONE);
                            }

                            if (dispensaryAdapter!=null)
                            {
                                PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,dispensaryAdapter.getDispensaryList());
                            }*/

                            //   application.dialogManager.dismissProgressDialog();
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);
                            setUpcomingDeliveriesList(OrdersModelArrayList);
                            binding.progressBar.setVisibility(View.GONE);

                            break;
                        case ERROR:
                            if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }
                            //    application.dialogManager.dismissProgressDialog();
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });

    }

    void setUpcomingDeliveriesList(ArrayList<OrdersModel> upcomingDeliveriesList) {
//        ArrayList<OrdersModel> ordersModels =
//                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
//                }.getType());
//
//
//        Log.d("size=======", "" + ordersModels.size());

        if (upcomingDeliveriesList.size() > 0) {
            upcomingDeliveriesAdapter = new UpcomingDeliveriesAdapter(getContext(), upcomingDeliveriesList, latitude, longitude);
            binding.recycleView.setAdapter(upcomingDeliveriesAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            //  showToast(getContext(),"Dispensary not found");
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);

        }

    }

}

