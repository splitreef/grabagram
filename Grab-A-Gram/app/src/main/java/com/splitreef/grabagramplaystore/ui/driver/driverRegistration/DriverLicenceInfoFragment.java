package com.splitreef.grabagramplaystore.ui.driver.driverRegistration;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.baoyz.actionsheet.ActionSheet;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentDriver;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.databinding.FragmentDriverLicenseInfoBinding;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.viewModelFactory.DriverRegistrationViewModalProviderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import static com.baoyz.actionsheet.ActionSheet.createBuilder;


/**
 * Created by  on 06-11-2020.
 */

public class DriverLicenceInfoFragment extends BaseFragment implements View.OnClickListener, ActionSheet.ActionSheetListener {
    private FragmentDriverLicenseInfoBinding binding;
    private FragmentDriver fragmentDriver;
    private DriverRegistrationRequest driverRegistrationRequest;
    private int photoTypeCount = 0;
    private DriverRegistrationViewModel driverRegistrationViewModel;
    private GrabAGramApplication grabAGramApplication;
    private Bitmap bitmap;
    private long expDate = 0;
    private Context context;
    private List<String> vehicleMakeCompanyStrList = new ArrayList<>();
    private List<String> vehicleModelStrList = new ArrayList<>();

    private List<VehicleCompany> vehicleCompanyList = new ArrayList<>();
    private List<VehicleModel> vehicleModelList = new ArrayList<>();
    public static Bitmap bitmapDrivingLicence;
    public static Bitmap bitmapCarInsurancePolicy;
    public static Bitmap bitmapCarRegistration;
    public static Bitmap bitmapCareGiverCertificate;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public DriverLicenceInfoFragment(FragmentDriver fragmentDriver) {
        this.fragmentDriver = fragmentDriver;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDriverLicenseInfoBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        grabAGramApplication = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        driverRegistrationViewModel = new ViewModelProvider(getViewModelStore(), new DriverRegistrationViewModalProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), grabAGramApplication.firebaseRepository)).get(DriverRegistrationViewModel.class);

        if (getArguments() != null) {
            driverRegistrationRequest = (DriverRegistrationRequest) getArguments().getSerializable("requestData");
        }
        binding.takePhotoDriverLicence.setOnClickListener(this);
        binding.takePhotoRegistrationProof.setOnClickListener(this);
        binding.takePhotoCarInsurance.setOnClickListener(this);
        binding.takePhotoCargiver.setOnClickListener(this);
        binding.btnContinue.setOnClickListener(this);
        binding.tvExpirationDate.setOnClickListener(this);

        subscribeObservers();

        if (driverRegistrationRequest.getIsCareGiver()) {
            binding.llCarGiverPhoto.setVisibility(View.VISIBLE);
        } else {
            binding.llCarGiverPhoto.setVisibility(View.GONE);
        }
        binding.rbNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });
        // driverRegistrationViewModel.getVehicleCompanyList();
       /* binding.spinnerSelectVehicleCompanyMakeName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setVehicleModelSppiner(vehicleMakeCompanyStrList.get(i).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.take_photo_driver_licence:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 1;
                }
                break;
            case R.id.take_photo_car_insurance:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 2;
                }
                break;
            case R.id.take_photo_registration_proof:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 3;
                }
                break;

            case R.id.take_photo_cargiver:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();

                } else {
                    openActionSheet();
                    photoTypeCount = 4;
                }
                break;
            case R.id.btn_continue:
                moveNextScreen();
                break;
            case R.id.tv_expiration_date:
                setDate(1);
                break;
        }
    }

    void moveNextScreen() {
        if (isValidation()) {
            // Set driver licence information

            driverRegistrationRequest.setDocumentType(getResources().getString(R.string.driving_licence));
            driverRegistrationRequest.setExpirationDate(expDate);
            driverRegistrationRequest.setIdentityNumber(binding.edtDriverLicence.getText().toString());
            driverRegistrationRequest.setIssuingState(binding.spinnerIssuingState.getSelectedItem().toString());
            driverRegistrationRequest.setManufactureYear(binding.spinnerSelectYear.getSelectedItem().toString());
            driverRegistrationRequest.setLicenceIssuingState(binding.spinnerLicencePlateState.getSelectedItem().toString());
            driverRegistrationRequest.setLicencePlateNumber(binding.edtLicencePlateNumber.getText().toString());

            /*driverRegistrationRequest.setManufactureModel(binding.spinnerSelectVehicleModalName.getSelectedItem().toString());
            driverRegistrationRequest.setManufactureCompany(binding.spinnerSelectVehicleCompanyMakeName.getSelectedItem().toString());
*/
            driverRegistrationRequest.setManufactureModel(binding.edtMake.getText().toString());
            driverRegistrationRequest.setManufactureCompany(binding.edtModel.getText().toString());

            if (binding.rbYes.isChecked()) {
                driverRegistrationRequest.setRegularMaintenance("YES");
            } else if (binding.rbNo.isChecked()) {
                driverRegistrationRequest.setRegularMaintenance("NO");
            }

            fragmentDriver.getData(2, driverRegistrationRequest);
        }


    }

    // For Documents Validation
    boolean isValidation() {

        if (Objects.requireNonNull(binding.edtDriverLicence.getText()).toString().equals("")) {
            binding.edtDriverLicence.setError("Please enter Driver License Number");
            binding.edtDriverLicence.requestFocus();
            return false;
        } else if (binding.spinnerIssuingState.getSelectedItemPosition() == 0) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Issuing State");
            return false;
        } else if (Objects.requireNonNull(binding.tvExpirationDate.getText()).toString().equals("")) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Date of Expiration");
            return false;
        } else if (!binding.rbYes.isChecked() && !binding.rbNo.isChecked()) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Is your vehicle reliable and does it receive regular maintenance");
            return false;
        } else if (binding.spinnerSelectYear.getSelectedItemPosition() == 0) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Year");
            return false;
        } else if (Objects.requireNonNull(binding.edtMake.getText().toString().equals(""))) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please enter Make");
            binding.edtMake.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtModel.getText().toString().equals(""))) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Modal Name");
            binding.edtModel.requestFocus();
            return false;
        }
       /* else if (Objects.requireNonNull(binding.spinnerSelectVehicleCompanyMakeName.getSelectedItemPosition() == 0)) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please enter Make");
            return false;
        } else if (binding.spinnerSelectVehicleModalName.getSelectedItemPosition() == 0 && binding.spinnerSelectVehicleModalName.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select_model))) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Modal Name");
            return false;
        }*/


        else if (binding.spinnerLicencePlateState.getSelectedItemPosition() == 0) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Licence Plate State");
            return false;
        } else if (Objects.requireNonNull(binding.edtLicencePlateNumber.getText()).toString().equals("")) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Licence Plate Number");
            binding.edtLicencePlateNumber.requestFocus();
            return false;
        }


       /* else if (driverRegistrationRequest.getDrivingLicenseUrl() == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Licence Image");
            return false;
        } else if (driverRegistrationRequest.getCarInsurancePolicyUrl() == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Car Insurance Image");
            return false;
        } else if (driverRegistrationRequest.getCarRegistrationUrl() == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Photo Registration Proof Image");
            return false;
        } else if (driverRegistrationRequest.getIsCareGiver()) {
            if (driverRegistrationRequest.getCareGiverCertificateURl() == null) {
                grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's CareGiver Image");
                return false;
            }
        }*/

        else if (bitmapDrivingLicence == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Licence Image");
            return false;
        } else if (bitmapCarInsurancePolicy == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Car Insurance Image");
            return false;
        } else if (bitmapCarRegistration == null) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's Photo Registration Proof Image");
            return false;
        } else if (driverRegistrationRequest.getIsCareGiver()) {
            if (bitmapCareGiverCertificate == null) {
                grabAGramApplication.messageManager.DisplayToastMessage("Please select Driver's CareGiver Image");
                return false;
            }
        }
        return true;

    }


    // open action sheet
    void openActionSheet() {
        createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Camera", "Gallery")
                .setCancelableOnTouchOutside(true)
                .setListener(this).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedFileUri);
                    byte[] bytes = getFileDataFromDrawable(bitmap);

                    if (photoTypeCount == 1) {
                        binding.photoDriverLicence.setImageBitmap(bitmap);
                        bitmapDrivingLicence = bitmap;
                    } else if (photoTypeCount == 2) {
                        binding.photoCarInsurance.setImageBitmap(bitmap);
                        bitmapCarInsurancePolicy = bitmap;
                    } else if (photoTypeCount == 3) {
                        binding.photoRegistrationProof.setImageBitmap(bitmap);
                        bitmapCarRegistration = bitmap;
                    } else if (photoTypeCount == 4) {
                        binding.photoCargiver.setImageBitmap(bitmap);
                        bitmapCareGiverCertificate = bitmap;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {

                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount == 1) {
                        binding.photoDriverLicence.setImageBitmap(bitmap);
                        bitmapDrivingLicence = bitmap;
                    } else if (photoTypeCount == 2) {
                        binding.photoCarInsurance.setImageBitmap(bitmap);
                        bitmapCarInsurancePolicy = bitmap;
                    } else if (photoTypeCount == 3) {
                        binding.photoRegistrationProof.setImageBitmap(bitmap);
                        bitmapCarRegistration = bitmap;
                    } else if (photoTypeCount == 4) {
                        binding.photoCargiver.setImageBitmap(bitmap);
                        bitmapCareGiverCertificate = bitmap;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            grabAGramApplication.messageManager.DisplayToastMessage("Something went wrong");
        }
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        if (index == 0) {
            // application.messageManager.DisplayToastMessage("Camera");
            requestStorageAndCameraPermission(getActivity(), CAMERA_PIC);
        } else if (index == 1) {
            requestStorageAndCameraPermission(getActivity(), GALLERY_PIC);
            // application.messageManager.DisplayToastMessage("Gallery");
        }
    }


    void subscribeObservers() {
        driverRegistrationViewModel.observeDriverDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    PicassoManager.setImage(url, binding.photoDriverLicence);
                                    driverRegistrationRequest.setDrivingLicenseUrl(url);
                                } else if (photoTypeCount == 2) {
                                    PicassoManager.setImage(url, binding.photoCarInsurance);
                                    driverRegistrationRequest.setCarInsurancePolicyUrl(url);
                                } else if (photoTypeCount == 3) {
                                    PicassoManager.setImage(url, binding.photoRegistrationProof);
                                    driverRegistrationRequest.setCarRegistrationUrl(url);
                                } else if (photoTypeCount == 4) {
                                    PicassoManager.setImage(url, binding.photoCargiver);
                                    driverRegistrationRequest.setCareGiverCertificateURl(url);
                                }
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

       /* driverRegistrationViewModel.observeVehicleCompany().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            vehicleCompanyList = (List<VehicleCompany>) dataResource.data;
                            setVehicleCompanyMakeSppiner();
                            driverRegistrationViewModel.getVehicleModelList();
                            break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        driverRegistrationViewModel.observeVehicleModel().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            vehicleModelList = (List<VehicleModel>) dataResource.data;
                            break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/
    }


   /* void setVehicleCompanyMakeSppiner() {
        vehicleMakeCompanyStrList.clear();
        vehicleMakeCompanyStrList.add(getResources().getString(R.string.select_make));
        for (int i = 0; i < vehicleCompanyList.size(); i++) {

            vehicleMakeCompanyStrList.add(vehicleCompanyList.get(i).getCompany_name());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, vehicleMakeCompanyStrList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinnerSelectVehicleCompanyMakeName.setAdapter(adapter);
    }


    void setVehicleModelSppiner(String companyName) {
        vehicleModelStrList.clear();
        vehicleModelStrList.add(getResources().getString(R.string.select_model));

        for (int i = 0; i < vehicleModelList.size(); i++) {
            if (companyName.equalsIgnoreCase(vehicleModelList.get(i).getCompany_name())) {
                vehicleModelStrList.add(vehicleModelList.get(i).getVahicle_model());
            }
        }
        if (vehicleModelStrList.size() == 1) {
            vehicleModelStrList.clear();
            vehicleModelStrList.add("Model name not available for this company.");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, vehicleModelStrList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinnerSelectVehicleModalName.setAdapter(adapter);
    }*/

    public void setDate(int flag) {

        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(getActivity().getSupportFragmentManager(), picker.toString());


        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {

                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar.setTimeInMillis(selection);

                int month = calendar.get(Calendar.MONTH);
                String date = (month + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.YEAR);

                if (flag == 1) {
                    expDate = selection;
                    binding.tvExpirationDate.setText("" + date);
                    /* int value = expiryDateOfDoc(date);
                   if (value == 0) {
                        Toast.makeText(getContext(), "your document will expire today.", Toast.LENGTH_SHORT).show();
                    }
                    if (value == 1) {
                        Toast.makeText(getContext(), "Your document will expire in" + value + "day.", Toast.LENGTH_SHORT).show();
                    } else if (value != -1) {
                        Toast.makeText(getContext(), "Your document will expire in" + value + "days., Toast.LENGTH_SHORT).show();
                    }*/
                }

            }
        });
    }


    private int expiryDateOfDoc(String givenDate) {
        int diffrence = -1;
        try {
            int year = AppUtil.getYearByDate(givenDate.replace("-", "/"));
            int month = AppUtil.getMonthByDate(givenDate.replace("-", "/"));
            int day = AppUtil.getDayByDate(givenDate.replace("-", "/"));

            if (AppUtil.getCurrentYear() == year) {
                if (AppUtil.getCurrentMonth() == month) {
                    String strMonth = AppUtil.getLastDateOfGivenMonth(year, month);
                    int lastDay = AppUtil.getDayByDate(strMonth.replace("-", "/"));
                    diffrence = lastDay - day;
                    Log.d("Last Day: ", "" + diffrence);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffrence;

    }
}
