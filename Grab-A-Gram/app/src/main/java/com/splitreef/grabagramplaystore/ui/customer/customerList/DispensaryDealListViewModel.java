package com.splitreef.grabagramplaystore.ui.customer.customerList;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import java.util.ArrayList;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DispensaryDealListViewModel extends ViewModel {
    private static final String TAG = "DealListViewModel";
    private final FirebaseRepository firebaseRepository;
    private final MediatorLiveData<DataResource> onGetDealList = new MediatorLiveData<>();
    private  Application mApplication;
    private final CompositeDisposable disposable = new CompositeDisposable();


    public DispensaryDealListViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication= mApplication;
    }

    // For get list
    void getDealList()
    {
         firebaseRepository.getDealList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<ArrayList<DealModel>>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onGetDealList.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull ArrayList<DealModel> dealModels) {
                         onGetDealList.setValue(DataResource.DataStatus(dealModels));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {
                         onGetDealList.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });

    }

    LiveData<DataResource>observeGetDealList()
    {
        return onGetDealList;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }

}
