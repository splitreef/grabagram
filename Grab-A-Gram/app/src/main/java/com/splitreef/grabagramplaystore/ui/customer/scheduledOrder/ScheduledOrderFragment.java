package com.splitreef.grabagramplaystore.ui.customer.scheduledOrder;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.ScheduledOrderAdapter;
import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.databinding.FragmentScheduledOrdersBinding;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.viewModelFactory.ScheduledOrderProviderFactory;

import java.util.Objects;

public class ScheduledOrderFragment extends Fragment {

    private FragmentScheduledOrdersBinding binding;
    private Context context;
    private ScheduledOrderViewModel viewModel;
    private GrabAGramApplication application;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding=FragmentScheduledOrdersBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPlacedOrders(AppConstant.ORDER_TYPE_SCHEDULED);
    }

    public void initView()
    {
        ((DispensaryListActivity)context).setToolbarTitle("Scheduled Orders");

        application=(GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

        viewModel=new ViewModelProvider(getViewModelStore(), new ScheduledOrderProviderFactory(application.firebaseRepository)).get(ScheduledOrderViewModel.class);
        subscribeObservers();
        binding.rvScheduledOrder.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

    }


    void subscribeObservers()
    {
       viewModel.observeGetPlacedOrders().observe((LifecycleOwner) context, new Observer<DataResource>() {
           @Override
           public void onChanged(DataResource dataResource) {
               switch (dataResource.status)
               {
                   case LOADING:
                       binding.progressBar.setVisibility(View.VISIBLE);
                       break;
                   case SUCCESS:
                       binding.progressBar.setVisibility(View.GONE);
                       GetPlacedOrdersResponse getPlacedOrdersResponse= (GetPlacedOrdersResponse) dataResource.data;
                       if (getPlacedOrdersResponse.getMessage().equals(AppConstant.SUCCESS))
                       {
                           setScheduledOrderAdapter(getPlacedOrdersResponse);
                       }
                       else
                       {
                         binding.userMsg.setVisibility(View.VISIBLE);
                         binding.rvScheduledOrder.setVisibility(View.GONE);
                       }
                       break;

                   case ERROR:
                       binding.progressBar.setVisibility(View.GONE);
                       binding.userMsg.setVisibility(View.VISIBLE);
                       binding.rvScheduledOrder.setVisibility(View.GONE);

               }
           }
       });

    }

    void setScheduledOrderAdapter(GetPlacedOrdersResponse getPlacedOrdersResponse)
    {
       if (getPlacedOrdersResponse.getOrders()!=null && getPlacedOrdersResponse.getOrders().size()>0)
       {
           ScheduledOrderAdapter adapter=new ScheduledOrderAdapter(context,getPlacedOrdersResponse.getOrders());
           binding.rvScheduledOrder.setAdapter(adapter);
           binding.rvScheduledOrder.setVisibility(View.VISIBLE);
           binding.userMsg.setVisibility(View.GONE);
       }
       else
       {
           binding.userMsg.setVisibility(View.VISIBLE);
           binding.rvScheduledOrder.setVisibility(View.GONE);
       }
    }
}
