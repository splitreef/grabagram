package com.splitreef.grabagramplaystore.ui.customer.productReview;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.productReviewResponse.ProductReviewResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProductReviewViewModel extends ViewModel {

    private static final String TAG = "ProductReviewViewModel";
    private FirebaseRepository firebaseRepository;
    private final MediatorLiveData<DataResource> onGetProductReview = new MediatorLiveData<>();
    private final MediatorLiveData<DataResource> onSaveProductReview = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public ProductReviewViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }

    void getProductReview(String orderId, String productId)
    {
        firebaseRepository.getProductReview(orderId, productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ProductReviewResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetProductReview.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull ProductReviewResponse productReviewResponse) {
                        onGetProductReview.setValue(DataResource.DataStatus(productReviewResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onGetProductReview.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    void saveProductReview(ProductReviewModel productReviewModel)
    {
        firebaseRepository.saveProductReview(productReviewModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onSaveProductReview.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String message) {
                        onSaveProductReview.setValue(DataResource.DataStatus(message));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onSaveProductReview.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }



    LiveData<DataResource>observeGetProductReview()
    {
        return onGetProductReview;
    }

    LiveData<DataResource>observeSaveProductReview()
    {
        return onSaveProductReview;
    }



}
