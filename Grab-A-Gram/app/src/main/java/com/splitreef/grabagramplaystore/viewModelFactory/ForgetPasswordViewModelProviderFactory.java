package com.splitreef.grabagramplaystore.viewModelFactory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.forgetPassword.ForgetPasswordViewModel;

/**
 * Created by  on 21-01-2021.
 */
public class ForgetPasswordViewModelProviderFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final FirebaseRepository firebaseRepository;


    public ForgetPasswordViewModelProviderFactory(Application application, FirebaseRepository firebaseRepository) {
        mApplication = application;
        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(ForgetPasswordViewModel.class)) {
            return (T) new ForgetPasswordViewModel(mApplication, firebaseRepository);
        }

        throw new IllegalArgumentException("View model not found");
    }
}





