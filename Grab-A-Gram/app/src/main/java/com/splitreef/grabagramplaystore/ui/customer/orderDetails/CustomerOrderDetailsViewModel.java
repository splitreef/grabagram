package com.splitreef.grabagramplaystore.ui.customer.orderDetails;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CustomerOrderDetailsViewModel extends ViewModel {

    private static final String TAG = "CustomerOrderDetailsViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onUpdateOrderStatus = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetOrderDetails = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public CustomerOrderDetailsViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }

    public  void updateOrderStatus(String id,String status)
    {
         firebaseRepository.updateOrderStatus(id,status)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<String>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onUpdateOrderStatus.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull String msg) {

                         onUpdateOrderStatus.setValue(DataResource.DataStatus(msg));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onUpdateOrderStatus.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }



    public  void getOrderDetailsByOrderId(String orderId)
    {
        firebaseRepository.getOrderDetailsByOrderId(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Order>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onGetOrderDetails.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull Order order) {

                        onGetOrderDetails.setValue(DataResource.DataStatus(order));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetOrderDetails.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeUpdateOrderStatus()
    {
        return onUpdateOrderStatus;
    }

    LiveData<DataResource> observeGetOrderDetails()
    {
        return onGetOrderDetails;
    }

}
