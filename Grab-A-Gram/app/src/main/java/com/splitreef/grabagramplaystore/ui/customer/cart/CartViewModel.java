package com.splitreef.grabagramplaystore.ui.customer.cart;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponModel;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CartViewModel extends ViewModel {

    private static final String TAG = "CartViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onProductCart = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onCouponCode = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateProductCart = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onDeleteProductCart = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateCoupon = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onCancelCoupon = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public CartViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getProductCartList()
    {
         firebaseRepository.getProductCartList()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<GetProductCartResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onProductCart.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull GetProductCartResponse productCartResponse) {

                         onProductCart.setValue(DataResource.DataStatus(productCartResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onProductCart.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }


    public  void updateCartProduct(String id, int quantity, double totalPrice)
    {
        firebaseRepository.updateCartProduct(id,quantity,totalPrice)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateProductCart.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdateProductCart.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUpdateProductCart.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }


    public  void deleteCartProduct(String id,boolean isLastItem)
    {
        firebaseRepository.deleteCartProduct(id,isLastItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onDeleteProductCart.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onDeleteProductCart.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onDeleteProductCart.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }

    public  void updateCouponCode(CouponModel couponModel)
    {
        firebaseRepository.updateCouponCode(couponModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateCoupon.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdateCoupon.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUpdateCoupon.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }

    public  void cancelCouponCode(String couponId)
    {
        firebaseRepository.cancelCouponCode(couponId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onCancelCoupon.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onCancelCoupon.setValue(StateResource.success());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onCancelCoupon.setValue(StateResource.error(e.getMessage()));
                    }
                });

    }


    public  void getCouponProducts(String couponCode)
    {
        firebaseRepository.getCouponProducts(couponCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CouponResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onCouponCode.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull CouponResponse couponResponse) {

                        onCouponCode.setValue(DataResource.DataStatus(couponResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onCouponCode.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeGetProductCartList()
    {
        return onProductCart;
    }
    LiveData<StateResource> observeUpdateProductCartList()
    {
        return onUpdateProductCart;
    }

    LiveData<StateResource> observeDeleteProductCartList()
    {
        return onDeleteProductCart;
    }

    LiveData<DataResource> observeOngGetCouponProduct()
    {
        return onCouponCode;
    }

    LiveData<StateResource> observeUpdateCouponCode()
    {
        return onUpdateCoupon;
    }

    LiveData<StateResource> observeCancelCouponCode()
    {
        return onCancelCoupon;
    }


}
