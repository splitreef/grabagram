package com.splitreef.grabagramplaystore.ui.driver.orders.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.firestore.GeoPoint;
import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.DriverLocationStatusModel;
import com.splitreef.grabagramplaystore.data.model.driver.driverRating.MyDriverRating;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentHomeBinding;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.HomeViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {
    private FragmentHomeBinding binding;
    private HomeViewModel viewModel;
    private GrabAGramApplication application;
    double latitude;
    double longitude;
    ArrayList<OrdersModel> OrdersModelArrayList;
    ArrayList<MyDriverRating> myDriverRatingArrayList;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    public static int miles = 40;
    private DriverLocationStatusModel driverLocationStatusModel;
    private UserModel userModel;
    private String activeStatus = "";
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();


        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);
    }

    private void initView() {
        application = (GrabAGramApplication) getContext().getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new HomeViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(),
                application.firebaseRepository)).get(HomeViewModel.class);

        ((HomeActivity)context).setToolbarTitle("Profile");

        userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
        PicassoManager.setImage(userModel.getDriver_photo(), binding.profileImage);
        binding.tvName.setText(userModel.getName());
        binding.tvAddress.setText(userModel.getStreet_1());

        binding.tvAccountInfo.setOnClickListener(this);
        binding.cardPastDeliveries.setOnClickListener(this);
        binding.cardUpcomingDeliveries.setOnClickListener(this);
        HomeActivity.tvActiveStatus.setVisibility(View.VISIBLE);
        HomeActivity.tvActiveStatus.setOnClickListener(this);
        String fcmToken = PreferenceManger.getFcmToken(getContext());
        Log.d("Token:::", "" + fcmToken);
        subscribeObservers();

    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.tvActiveStatus.setVisibility(View.VISIBLE);
        isShowLoaderDialog = true;
        isFirst = true;
        // viewModel.getUserDetails(userModel.getUser_id());
        viewModel.getUpcomingDeliveriesList(latitude, longitude, HomeFragment.miles, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_account_info:
                ((HomeActivity) getActivity()).replaceDeliveriesFragment(0);
                break;
            case R.id.card_past_deliveries:
                ((HomeActivity) getActivity()).replaceDeliveriesFragment(1);

                break;
            case R.id.card_upcoming_deliveries:
                ((HomeActivity) getActivity()).replaceDeliveriesFragment(2);
                break;
            case R.id.tv_active_status:
                activeStatus = PreferenceManger.getPreferenceManger().getString(PrefKeys.ACTIVE_STATUS);
                if (activeStatus.equalsIgnoreCase(getResources().getString(R.string.online))) {
                    setActiveStatus(AppConstant.ONLINE);
                } else if (activeStatus.equalsIgnoreCase(getResources().getString(R.string.offline))) {
                    setActiveStatus(AppConstant.OFFLINE);
                }
                break;
        }
    }

    private void setActiveStatus(String status) {
        activeStatus = status;
        String deviceID = PreferenceManger.getFcmToken(getContext());
        driverLocationStatusModel = new DriverLocationStatusModel();
        driverLocationStatusModel.setDevice_id(deviceID);
        driverLocationStatusModel.setUser_id(userModel.getUser_id());
        driverLocationStatusModel.setDriver_status(status);
        driverLocationStatusModel.setLocation(new GeoPoint(latitude, longitude));

        viewModel.updateActiveStatus(driverLocationStatusModel);
    }

    void subscribeObservers() {
        viewModel.observeGetUpcomingDeliveriesList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            binding.progressBar.setVisibility(View.VISIBLE);
                            /*if (isShowLoaderDialog) {
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);*/
                            break;
                        case SUCCESS:
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            binding.upcomingDeliveries.setText("" + OrdersModelArrayList.size());
                            viewModel.getPastDeliveriesList(latitude, longitude, HomeFragment.miles, true);
                            break;
                        case ERROR:
                           /* if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }*/
                            binding.progressBar.setVisibility(View.GONE);
                            break;

                    }
                }

            }
        });

        viewModel.observeGetPastDeliveriesList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                           /* if (isShowLoaderDialog) {
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);*/
                            break;
                        case SUCCESS:
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            binding.pastDeliveries.setText("" + OrdersModelArrayList.size());
                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);
                           // binding.progressBar.setVisibility(View.GONE);
                            viewModel.getDriverStatus();

                            break;
                        case ERROR:
                           /* if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }*/
                            binding.tvPastDeliveries.setText("0");
                            //    application.dialogManager.dismissProgressDialog();
                            // binding.userMsg.setVisibility(View.VISIBLE);
                            //binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });

        viewModel.observeUpdateonActiveStatus().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getContext(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            if (activeStatus.equalsIgnoreCase(AppConstant.ONLINE)) {
                                PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.OFFLINE);
                                HomeActivity.tvActiveStatus.setText(AppConstant.TAP_TO_GO_OFFLINE);
                            } else {
                                PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.ONLINE);
                                HomeActivity.tvActiveStatus.setText(AppConstant.TAP_TO_GO_ONLINE);
                            }
                            // finish();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        viewModel.observeGetDriverStatus().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                           // application.dialogManager.displayProgressDialog(getContext(), "Loading...");
                            break;
                        case SUCCESS:
                           // application.dialogManager.dismissProgressDialog();
                            DriverLocationStatusModel driverLocationStatusModel = (DriverLocationStatusModel) dataResource.data;
                            String jsonString = new Gson().toJson(driverLocationStatusModel);
                            Log.d("jsonString", "" + jsonString);
                            setStatus(driverLocationStatusModel);
                            viewModel.getDriverRatingList();
                            break;
                        case ERROR:
                            PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.ONLINE);
                            HomeActivity.tvActiveStatus.setText(AppConstant.TAP_TO_GO_ONLINE);
                           // application.dialogManager.dismissProgressDialog();
                            viewModel.getDriverRatingList();
                            binding.progressBar.setVisibility(View.GONE);
                            //application.messageManager.DisplayToastMessage(dataResource.message);
                            //PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.OFFLINE);
                            //HomeActivity.tvActiveStatus.setText(AppConstant.OFFLINE);
                            break;

                    }
                }

            }
        });


        viewModel.observeGetDriverRatingList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                         /*   if (isShowLoaderDialog) {
                            } else {
                                binding.progressBar.setVisibility(View.VISIBLE);
                            }
                            binding.progressBar.setVisibility(View.VISIBLE);*/
                            break;
                        case SUCCESS:
                            myDriverRatingArrayList = (ArrayList<MyDriverRating>) dataResource.data;
                            updateRating();
                            binding.progressBar.setVisibility(View.GONE);
                            break;
                        case ERROR:
                          /*  if (isShowLoaderDialog) {
                                application.dialogManager.dismissProgressDialog();
                            } else {
                                binding.progressBar.setVisibility(View.GONE);
                            }*/
                            binding.tvPastDeliveries.setText("0");
                            //    application.dialogManager.dismissProgressDialog();
                            // binding.userMsg.setVisibility(View.VISIBLE);
                            //binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);

                            //PreferenceManger.getPreferenceManger().setObject(PrefKeys.DISPENSARY_LIST,new ArrayList<DispensaryModel>());
                            break;

                    }
                }

            }
        });

    }


    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void updateRating() {
        float totalRating = 0;
        for (int i = 0; i < myDriverRatingArrayList.size(); i++) {
            totalRating = totalRating + myDriverRatingArrayList.get(i).getRating();

        }
        Log.d("rating==", "totalRating=" + totalRating);
        Log.d("rating==", "totalSize=" + myDriverRatingArrayList.size());

        if (totalRating!=0)
        {
            totalRating = totalRating / myDriverRatingArrayList.size();
        }

        Log.d("rating==", "" + totalRating);

        binding.tvRating.setText("" + String.format("%.1f", totalRating));

        binding.ratingBar.setRating(totalRating);

    /*    if (totalRating >= 2.0) {
            binding.tvRating.setText("" + String.format("%.1f", totalRating));
            binding.ratingBar.setRating(totalRating);
        }*/

    }

    private void setStatus(DriverLocationStatusModel driverLocationStatusModel) {
        if (driverLocationStatusModel != null) {
            if (driverLocationStatusModel.getDriver_status().equalsIgnoreCase(AppConstant.ONLINE)) {

                PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.OFFLINE);
                HomeActivity.tvActiveStatus.setText(AppConstant.TAP_TO_GO_OFFLINE);

            } else if (driverLocationStatusModel.getDriver_status().equalsIgnoreCase(AppConstant.OFFLINE)) {

                PreferenceManger.getPreferenceManger().setString(PrefKeys.ACTIVE_STATUS, AppConstant.ONLINE);
                HomeActivity.tvActiveStatus.setText(AppConstant.TAP_TO_GO_ONLINE);

            }
        }
    }


}
