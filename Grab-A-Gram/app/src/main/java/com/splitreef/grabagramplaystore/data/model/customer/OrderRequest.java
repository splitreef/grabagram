package com.splitreef.grabagramplaystore.data.model.customer;

import com.splitreef.grabagramplaystore.data.model.PaymentMethod;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderRequest implements Serializable {


    private ArrayList<UserAddressModel>addresses;
    private String assign_driver_id;
    private String busniess_name;
    private String confirmation_code;
    private long created_at;
    private String customer_email;
    private String customer_name;
    private double delivery_fee;
    private double discount_amount;
    private DispensaryModel dispensary_data;
    private double grand_total;
    private boolean isSendNotification;
    private ArrayList<ProductCart>items;
    private String order_no;
    private String patient_id;
    private PaymentMethod payment_method;
    private double sales_tax;
    private String status;
    private double sub_total;
    private long updated_at;
    private String user_id;
    private double delivery_fee_miles;
    private double delivery_fee_minute;
    private double delivery_miles;
    private int delivery_minute;
    private ArrayList<String>driverIdList;
    private boolean payment_success;


    public ArrayList<UserAddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<UserAddressModel> addresses) {
        this.addresses = addresses;
    }

    public String getAssign_driver_id() {
        return assign_driver_id;
    }

    public void setAssign_driver_id(String assign_driver_id) {
        this.assign_driver_id = assign_driver_id;
    }

    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public String getConfirmation_code() {
        return confirmation_code;
    }

    public void setConfirmation_code(String confirmation_code) {
        this.confirmation_code = confirmation_code;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public double getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(double delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public double getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(double discount_amount) {
        this.discount_amount = discount_amount;
    }

    public DispensaryModel getDispensary_data() {
        return dispensary_data;
    }

    public void setDispensary_data(DispensaryModel dispensary_data) {
        this.dispensary_data = dispensary_data;
    }

    public double getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(double grand_total) {
        this.grand_total = grand_total;
    }

    public boolean isSendNotification() {
        return isSendNotification;
    }

    public void setSendNotification(boolean sendNotification) {
        isSendNotification = sendNotification;
    }

    public ArrayList<ProductCart> getItems() {
        return items;
    }

    public void setItems(ArrayList<ProductCart> items) {
        this.items = items;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public PaymentMethod getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(PaymentMethod payment_method) {
        this.payment_method = payment_method;
    }

    public double getSales_tax() {
        return sales_tax;
    }

    public void setSales_tax(double sales_tax) {
        this.sales_tax = sales_tax;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getSub_total() {
        return sub_total;
    }

    public void setSub_total(double sub_total) {
        this.sub_total = sub_total;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public double getDelivery_fee_miles() {
        return delivery_fee_miles;
    }

    public void setDelivery_fee_miles(double delivery_fee_miles) {
        this.delivery_fee_miles = delivery_fee_miles;
    }

    public double getDelivery_fee_minute() {
        return delivery_fee_minute;
    }

    public void setDelivery_fee_minute(double delivery_fee_minute) {
        this.delivery_fee_minute = delivery_fee_minute;
    }

    public double getDelivery_miles() {
        return delivery_miles;
    }

    public void setDelivery_miles(double delivery_miles) {
        this.delivery_miles = delivery_miles;
    }

    public int getDelivery_minute() {
        return delivery_minute;
    }

    public void setDelivery_minute(int delivery_minute) {
        this.delivery_minute = delivery_minute;
    }

    public ArrayList<String> getDriverIdList() {
        return driverIdList;
    }

    public void setDriverIdList(ArrayList<String> driverIdList) {
        this.driverIdList = driverIdList;
    }

    public boolean isPayment_success() {
        return payment_success;
    }

    public void setPayment_success(boolean payment_success) {
        this.payment_success = payment_success;
    }
}
