package com.splitreef.grabagramplaystore.data.model.customer.coupon;

import java.io.Serializable;
import java.util.ArrayList;

public class CouponModel implements Serializable {

    private String category_id;
    private String category_name;
    private String coupon_description;
    private String coupon_code_name;
    private long created_at;
    private String discount_type;
    private String dispensary_user_id;
    private long expiration_date;
    private String id;
    private double percentage;
    private int usage_limit_per_user;
    private long userUsedLimit;
    private ArrayList<CouponProduct> product;
    private double totalDiscount;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCoupon_description() {
        return coupon_description;
    }

    public void setCoupon_description(String coupon_description) {
        this.coupon_description = coupon_description;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getDispensary_user_id() {
        return dispensary_user_id;
    }

    public void setDispensary_user_id(String dispensary_user_id) {
        this.dispensary_user_id = dispensary_user_id;
    }

    public long getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(long expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public int getUsage_limit_per_user() {
        return usage_limit_per_user;
    }

    public void setUsage_limit_per_user(int usage_limit_per_user) {
        this.usage_limit_per_user = usage_limit_per_user;
    }

    public ArrayList<CouponProduct> getProduct() {
        return product;
    }

    public void setProduct(ArrayList<CouponProduct> product) {
        this.product = product;
    }

    public long getUserUsedLimit() {
        return userUsedLimit;
    }

    public void setUserUsedLimit(long userUsedLimit) {
        this.userUsedLimit = userUsedLimit;
    }

    public String getCoupon_code_name() {
        return coupon_code_name;
    }

    public void setCoupon_code_name(String coupon_code_name) {
        this.coupon_code_name = coupon_code_name;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }
}
