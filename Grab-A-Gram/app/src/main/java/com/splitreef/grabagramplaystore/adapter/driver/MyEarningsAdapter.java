package com.splitreef.grabagramplaystore.adapter.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;

import java.util.ArrayList;

/**
 * Created by  on 25-01-2021.
 */
public class MyEarningsAdapter extends RecyclerView.Adapter<MyEarningsAdapter.MyEarningsHolder> {
    private ArrayList<OrdersModel> ordersList;
    private Context context;
    private double latitude;
    private double longitude;


    public MyEarningsAdapter(@NonNull Context context, ArrayList<OrdersModel> ordersList, double latitude, double longitude) {
        this.context = context;
        this.ordersList = ordersList;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public ArrayList<OrdersModel> getDispensaryList() {
        return ordersList;
    }

    @NonNull
    @Override
    public MyEarningsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_earnings_view, parent, false);
        return new MyEarningsHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull MyEarningsHolder holder, int position) {
        OrdersModel ordersModel = ordersList.get(position);
        holder.tv_order_number.setText("#" + ordersModel.getOrder_no());
        holder.tv_order_date.setText(AppUtil.getDateFromMillisecond(ordersModel.getCreated_at()));
        double fee = ordersModel.getDelivery_fee_miles() + ordersModel.getDelivery_fee_minute();
        fee = ((fee * 70) / 100);
        holder.tv_earning_amount.setText(" " + "$" + fee);

        holder.ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OrdersDetailsActivity.class)
                        .putExtra(AppConstant.ID, ordersModel.getId()));
            }
        });


    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    class MyEarningsHolder extends RecyclerView.ViewHolder {
        MaterialTextView tv_order_number, tv_order_date, tv_earning_amount;
        LinearLayout ll_top;

        public MyEarningsHolder(@NonNull View itemView) {
            super(itemView);
            tv_order_number = itemView.findViewById(R.id.tv_order_number);
            tv_order_date = itemView.findViewById(R.id.tv_order_date);
            ll_top = itemView.findViewById(R.id.ll_top);
            tv_earning_amount = itemView.findViewById(R.id.tv_earning_amount);
        }
    }
}

