package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.PendingOrdersAdapter;
import com.splitreef.grabagramplaystore.callBack.driver.OrderAcceptDeclineListener;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentPendingOrdersListBinding;
import com.splitreef.grabagramplaystore.dialog.CustomDilalog;
import com.splitreef.grabagramplaystore.dialog.CustomeBottomSheetDialog;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.fcmNotification.CallSendNotificationApi;
import com.splitreef.grabagramplaystore.ui.driver.notifications.NotificationActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeFragment;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.GPSTracker;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.PendingOrdersViewModelProviderFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Created by  on 26-11-2020.
 */
public class PendingOrdersListFragment extends BaseFragment implements OrderAcceptDeclineListener, View.OnClickListener {
    private final String TAG = "PendingOrdersListFragment";
    private FragmentPendingOrdersListBinding binding;
    private PendingOrdersViewModel viewModel;
    private GrabAGramApplication application;
    private int currentItem, totalItemCount, scrollOutItems;
    private boolean isScrolling;
    private LinearLayoutManager linearLayoutManager;
    private PendingOrdersAdapter pendingOrdersAdapter;
    private boolean isShowLoaderDialog = true;
    private boolean isFirst = true;
    ArrayList<OrdersModel> OrdersModelArrayList;
    ArrayList<DeclineOrders> DeclineOrdersList;
    double latitude;
    double longitude;
    ArrayList<OrdersModel> OrdersModelArrayListOld = new ArrayList<>();
    OrdersModel tempOrdersModel = new OrdersModel();
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPendingOrdersListBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        Log.d("latitude=", "" + latitude);
        Log.d("longitude=", "" + longitude);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        isShowLoaderDialog = true;
        isFirst = true;
        viewModel.getDeclineOrders();
        // viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);

    }

    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new PendingOrdersViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(PendingOrdersViewModel.class);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.recycleView.setLayoutManager(linearLayoutManager);
        PendingOrdersFrgament.btnSort.setOnClickListener(this);
        HomeActivity.imvNotificationBell.setOnClickListener(this);


    /*    binding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState== AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem= linearLayoutManager.getChildCount();
                totalItemCount= linearLayoutManager.getItemCount();
                scrollOutItems=linearLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItem+ scrollOutItems== totalItemCount))
                {
                    isScrolling=false;
                    isShowLoaderDialog=false;
                    isFirst=false;
                    viewModel.getPendingOrdersList(latitude,longitude,40,isFirst);

                }
            }
        });*/

        subscribeObservers();
    }


    public void getPendingOrdersList() {
        if (viewModel != null)
            viewModel.getPendingOrdersList(latitude, longitude, HomeFragment.miles, true);
        Log.d("miles", ":" + HomeFragment.miles);
    }


    private void prepareNotificationBody(UserModel userDriverModel) {
        String userDeviceID = "", notiOrderStatus = "";
        userDeviceID = userDriverModel.getDevice_id();
        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        JSONObject notifcationMessage = new JSONObject();
        JSONObject notifcationData = new JSONObject();
        notiOrderStatus = AppConstant.ORDER_ACCEPTED;


        try {
            String strstring = "Your order number " + tempOrdersModel.getOrder_no() + " " + notiOrderStatus;
            notifcationMessage.put(AppConstant.TITLE, notiOrderStatus);
            notifcationMessage.put(AppConstant.BODY, strstring);
            notifcationMessage.put(AppConstant.BADGE_COUNT, 0);
            notifcationMessage.put(AppConstant.SOUND, "default");

            notifcationBody.put(AppConstant.TITLE, notiOrderStatus);
            notifcationBody.put(AppConstant.BODY, strstring);
            notifcationBody.put(AppConstant.NOTIFICATION_TYPE, AppConstant.DRIVER_OPERATION);
            notifcationBody.put(AppConstant.ORDER_ID, tempOrdersModel.getId());

            UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
            if (userModel != null) {
                if (userModel.getRole().equals(AppConstant.CUSTOMER_ROLE)) {
                    notifcationBody.put(AppConstant.USER_TYPE, AppConstant.CUSTOMER_ROLE);
                } else if (userModel.getRole().equals(AppConstant.DRIVER_ROLE)) {
                    notifcationBody.put(AppConstant.USER_TYPE, AppConstant.DRIVER_ROLE);
                }

            }
            notifcationData.put("data", notifcationBody);
            notification.put("to", userDriverModel.getDevice_id());
            notification.put("notification", notifcationMessage);
            notification.put("data", notifcationData);
            Log.d("Token::", "" + PreferenceManger.getPreferenceManger().getString(PrefKeys.FCM_TOKEN));
        } catch (JSONException e) {
            //Log.e(TAG, "onCreate: " + e.getMessage());
        }

        new CallSendNotificationApi(getContext()).sendNotification(notification);

    }

    void setPendingOrdersList(ArrayList<OrdersModel> pendingOrdersList) {
        List<OrdersModel> containsList = new ArrayList<>();
        if (pendingOrdersList.size() > 0) {
            if (DeclineOrdersList.size() > 0) {
                List<OrdersModel> list = new ArrayList<>();
                list.addAll(OrdersModelArrayList);
                for (int i = 0; i < pendingOrdersList.size(); i++) {
                    for (int j = 0; j < DeclineOrdersList.size(); j++) {
                        if (DeclineOrdersList.get(j).getOrder_no().equalsIgnoreCase(pendingOrdersList.get(i).getId())) {
                            containsList.add(pendingOrdersList.get(i));
                            for (int k = 0; k < list.size(); k++) {
                                if (pendingOrdersList.get(i).getId() == list.get(k).getId()) {
                                    list.remove(k);
                                    break;
                                }
                            }
                        }

                    }
                }

                OrdersModelArrayList.clear();
                OrdersModelArrayList.addAll(list);
            }
        }


        for (int i = 0; i < OrdersModelArrayList.size(); i++) {
            double distance = new GetDistance().distance(latitude, longitude, OrdersModelArrayList.get(i).getDispensary_location().getLatitude(),
                    OrdersModelArrayList.get(i).getDispensary_location().getLongitude());
            OrdersModelArrayList.get(i).setDistance(distance);
            OrdersModelArrayList.get(i).setMyOrderNo(Integer.parseInt(OrdersModelArrayList.get(i).getOrder_no()));
        }

        PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, OrdersModelArrayList);

        ArrayList<OrdersModel> ordersModels =
                PreferenceManger.getPreferenceManger().getArray(PrefKeys.PENDING_ORDERS_LIST, new TypeToken<List<OrdersModel>>() {
                }.getType());


        Log.d("size=======", "" + ordersModels.size());

        if (OrdersModelArrayList.size() > 0) {
            pendingOrdersAdapter = new PendingOrdersAdapter(getContext(), OrdersModelArrayList, latitude, longitude, this);
            binding.recycleView.setAdapter(pendingOrdersAdapter);
            binding.userMsg.setVisibility(View.GONE);
            binding.recycleView.setVisibility(View.VISIBLE);
        } else {
            //  showToast(getContext(),"Dispensary not found");
            binding.userMsg.setVisibility(View.VISIBLE);
            binding.recycleView.setVisibility(View.GONE);

        }

        binding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onAcceptClick(String id, OrdersModel ordersModel) {
        new CustomDilalog() {
            @Override
            public void onPositiveButtonClick() {
                tempOrdersModel = ordersModel;
                viewModel.getOrderStatus(id);
                //viewModel.updateOrderToAccept(id, "ACCEPT");

            }
        }.showCustomDialog(getContext(), getResources().getString(R.string.confirm_accept), getResources().getString(R.string.are_you_sure_you_want_to_accept_this_order), "Yes", "No");


    }

    @Override
    public void onDeclineClick(String id) {
        new CustomDilalog() {
            @Override
            public void onPositiveButtonClick() {

                viewModel.updateOrderToDecline(id);
            }
        }.showCustomDialog(getContext(), getResources().getString(R.string.order_decline), getResources().getString(R.string.are_you_sure_want_to_decline_of_this_order), "Yes", "No");


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sort:
                new CustomeBottomSheetDialog() {
                    @Override
                    public void bottomSheetItemClick(String sortBy) {
                        onSortBy(sortBy);
                    }
                }.initBottomSheet(getContext());
                break;

            case R.id.imv_notification_bell:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                break;
        }
    }


    void subscribeObservers() {
        viewModel.observeGetPendingOrdersList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            binding.progressBar.setVisibility(View.VISIBLE);
                            break;
                        case SUCCESS:
                            OrdersModelArrayList = (ArrayList<OrdersModel>) dataResource.data;
                            setPendingOrdersList(OrdersModelArrayList);
                            break;
                        case ERROR:
                            binding.userMsg.setVisibility(View.VISIBLE);
                            binding.recycleView.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.GONE);
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.PENDING_ORDERS_LIST, new ArrayList<OrdersModel>());
                            break;

                    }
                }

            }
        });
        viewModel.observeGetDeclineList().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.userMsg.setVisibility(View.GONE);
                            break;
                        case SUCCESS:
                            DeclineOrdersList = (ArrayList<DeclineOrders>) dataResource.data;
                            getPendingOrdersList();
                            binding.progressBar.setVisibility(View.GONE);

                            break;
                        case ERROR:
                            binding.progressBar.setVisibility(View.GONE);
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.DECLINE_ORDERS_LIST, new ArrayList<String>());

                            break;

                    }
                }

            }
        });

        viewModel.observeUpdateOrderToAccept().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            viewModel.getDeclineOrders();
                            viewModel.getUserDetails(tempOrdersModel.getUser_id());
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        viewModel.observeUpdateOrderToDecline().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            viewModel.getDeclineOrders();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        viewModel.observeGetUserDetails().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getContext(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            UserModel userDriverModel = (UserModel) dataResource.data;
                            String jsonString = new Gson().toJson(userDriverModel);
                            Log.d("jsonString", "" + jsonString);
                            if (userDriverModel.getDevice_id() != null && !userDriverModel.getDevice_id().equalsIgnoreCase("")) {
                                prepareNotificationBody(userDriverModel);
                            } else {
                                Toast.makeText(getContext(), "Unable to send notification, customer don't have device id", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });


        viewModel.observeGetOrderStatsu().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {
                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getContext(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String orderStatus = (String) dataResource.data;
                            Log.d("orderStatus", "" + orderStatus);
                            if (orderStatus != null && !orderStatus.equalsIgnoreCase("")) {
                                if (orderStatus.equalsIgnoreCase(AppConstant.SCHEDULED)) {
                                    viewModel.updateOrderToAccept(tempOrdersModel.getId(), "ACCEPT");
                                } else {
                                    new SimpleDialog() {
                                        @Override
                                        public void onPositiveButtonClick() {
                                            ///onResume();
                                        }
                                    }.showSimpleDialog(getContext(), "Message", getResources().getString(R.string.order_already_accepted_by_another_driver), "OK");

                                }
                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            application.messageManager.DisplayToastMessage(dataResource.message);
                            break;

                    }
                }

            }
        });


    }


    private void onSortBy(String s) {
        /*https://stackoverflow.com/questions/36595805/how-to-sort-array-list-integer-type*/

        if (s.equalsIgnoreCase(getResources().getString(R.string.alphabetically_a_to_z))) {
            Collections.sort(OrdersModelArrayList, new Comparator<OrdersModel>() {
                @Override
                public int compare(OrdersModel o1, OrdersModel o2) {
                    int value = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        value = Long.compare((o1.getMyOrderNo()), (o2.getMyOrderNo()));
                    }
                    return value;
                }
            });
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.alphabetically_z_to_a))) {
            Collections.sort(OrdersModelArrayList, new Comparator<OrdersModel>() {
                @Override
                public int compare(OrdersModel o1, OrdersModel o2) {
                    int value = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        value = Long.compare((o2.getMyOrderNo()), (o1.getMyOrderNo()));
                    }
                    return value;
                }
            });


        } else if (s.equalsIgnoreCase(getResources().getString(R.string.distance_a_to_z))) {
            Collections.sort(OrdersModelArrayList, new Comparator<OrdersModel>() {
                @Override
                public int compare(OrdersModel o1, OrdersModel o2) {
                    int value = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        value = Double.compare((o1.getDistance()), (o2.getDistance()));
                    }
                    return value;
                }
            });
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.distance_z_to_a))) {
            Collections.sort(OrdersModelArrayList, new Comparator<OrdersModel>() {
                @Override
                public int compare(OrdersModel o1, OrdersModel o2) {
                    int value = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        value = Double.compare(o2.getDistance(), o1.getDistance());
                    }
                    return value;
                }
            });
        }

        if (pendingOrdersAdapter!=null)
        {
            pendingOrdersAdapter.notifyDataSetChanged();
        }

    }

}

