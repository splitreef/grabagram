package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;

import java.io.Serializable;

public class DriverDocumentImages implements Serializable {
    private String photo;


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
