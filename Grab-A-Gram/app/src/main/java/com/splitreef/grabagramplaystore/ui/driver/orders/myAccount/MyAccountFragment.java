package com.splitreef.grabagramplaystore.ui.driver.orders.myAccount;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.baoyz.actionsheet.ActionSheet;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount.MyDriverAccountResponse;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.FragmentMyAccountBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.PicassoManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.DriverMyAccountProviderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by  on 26-11-2020.
 */
public class MyAccountFragment extends BaseFragment implements View.OnClickListener, ActionSheet.ActionSheetListener {

    FragmentMyAccountBinding binding;
    private MyAccountViewModel viewModel;
    private Bitmap bitmap;
    private int photoTypeCount = 0;
    private GrabAGramApplication application;
    private MyDriverAccountResponse myDriverAccountResponse;
    private long date = 0, exLongDate = 0;
    private Context context;
    private boolean flag = false;
    int textLength = 0;

    private List<String> vehicleMakeCompanyStrList = new ArrayList<>();
    private List<String> vehicleModelStrList = new ArrayList<>();

    private List<VehicleCompany> vehicleCompanyList = new ArrayList<>();
    private List<VehicleModel> vehicleModelList = new ArrayList<>();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMyAccountBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initView() {
        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new DriverMyAccountProviderFactory(application.firebaseRepository)).get(MyAccountViewModel.class);
        HomeActivity.tvActiveStatus.setVisibility(View.GONE);

        ((HomeActivity) context).setToolbarTitle("MY ACCOUNT");

        binding.takePhoto1.setOnClickListener(this);
        binding.takePhoto2.setOnClickListener(this);
        binding.takePhoto3.setOnClickListener(this);
        binding.takePhoto4.setOnClickListener(this);

        binding.dateOfBirth.setOnClickListener(this);
        binding.expirationDate.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);

        binding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_yes:
                        myDriverAccountResponse.getAccount().setIs_caregiver(true);
                        binding.relCaregiver.setVisibility(View.VISIBLE);
                        binding.txtCaregiver.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rb_no:
                        myDriverAccountResponse.getAccount().setIs_caregiver(false);
                        binding.relCaregiver.setVisibility(View.GONE);
                        binding.txtCaregiver.setVisibility(View.GONE);
                        myDriverAccountResponse.getDocument_images().setCaregiver_certificate(null);
                        binding.photo4.setImageResource(R.drawable.no_image_found_place_holder);
                        break;
                }
            }
        });

     /*   binding.spinnerMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!flag) {
                    setVehicleModelSppiner(vehicleMakeCompanyStrList.get(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                flag = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        subscribeObservers();
        //  viewModel.getVehicleCompanyList();
        viewModel.getDriverMyAccountInfo();

        binding.mobilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.mobilePhone.getText().toString();
                textLength = binding.mobilePhone.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength == 1) {
                    if (!text.contains("(")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 5) {
                    if (!text.contains(")")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 6) {
                    binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                } else if (textLength == 10) {
                    if (!text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 15) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 18) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private void updateUi(MyDriverAccountResponse myDriverAccountResponse) {
        this.myDriverAccountResponse = myDriverAccountResponse;
        List<String> states = Arrays.asList(getResources().getStringArray(R.array.states));
        List<String> issuingState = Arrays.asList(getResources().getStringArray(R.array.issuing_state));
        List<String> year = Arrays.asList(getResources().getStringArray(R.array.year));
        List<String> plateState = Arrays.asList(getResources().getStringArray(R.array.licence_plate_state));
        List<String> model = Arrays.asList(getResources().getStringArray(R.array.model));

        if (myDriverAccountResponse.getAccount() != null) {
            binding.emailAddress.setText(application.firebaseRepository.getCurrentUser().getEmail());
            binding.cbEmailReceiving.setChecked(myDriverAccountResponse.getAccount().getEmail_communication());

            binding.firstName.setText(myDriverAccountResponse.getAccount().getFirst_name());
            binding.lastName.setText(myDriverAccountResponse.getAccount().getLast_name());

            binding.streetAddress.setText(myDriverAccountResponse.getAddress().getStreet_1());
            binding.streetAddress2.setText(myDriverAccountResponse.getAddress().getStreet_2());
            binding.city.setText(myDriverAccountResponse.getAddress().getCity());
            binding.spinnerState.setSelection(states.indexOf(myDriverAccountResponse.getAddress().getState()));
            binding.zipCode.setText(myDriverAccountResponse.getAddress().getZip_code());
            //binding.mobilePhone.setText(myDriverAccountResponse.getAddress().getPhone_number());
            createPhoneNumberInUsFormat(myDriverAccountResponse.getAddress().getPhone_number());

            binding.dateOfBirth.setText(AppUtil.getDateMDYFromMillisecond(myDriverAccountResponse.getAccount().getDate_of_birth()));
            binding.cbSendNotification.setChecked(myDriverAccountResponse.getAccount().getMessage_notification());


            if (myDriverAccountResponse.getAccount().getIs_caregiver()) {
                binding.rbYes.setChecked(true);
                binding.relCaregiver.setVisibility(View.VISIBLE);
                binding.txtCaregiver.setVisibility(View.VISIBLE);
            } else {
                binding.rbNo.setChecked(true);
                binding.relCaregiver.setVisibility(View.GONE);
                binding.txtCaregiver.setVisibility(View.GONE);
            }

        }

        if (myDriverAccountResponse.getDocuments() != null) {
            binding.spinnerIssuingState.setSelection(issuingState.indexOf(myDriverAccountResponse.getDocuments().getIssuing_state()));
            binding.driverLicense.setText(myDriverAccountResponse.getDocuments().getIdentity_number());
            binding.expirationDate.setText(AppUtil.getDateMDYFromMillisecond(myDriverAccountResponse.getDocuments().getExpiration_date()));
            binding.spinnerYear.setSelection(year.indexOf(myDriverAccountResponse.getDocuments().getManufacture_year()));

//            binding.spinnerMake.setSelection(vehicleMakeCompanyStrList.indexOf(myDriverAccountResponse.getDocuments().getManufacture_company()));
//            setModelName();
//            flag = true;
//            binding.spinnerModel.setSelection(vehicleModelStrList.indexOf(myDriverAccountResponse.getDocuments().getManufacture_model()));

            binding.make.setText(myDriverAccountResponse.getDocuments().getManufacture_company());
            binding.model.setText(myDriverAccountResponse.getDocuments().getManufacture_model());


            binding.spinnerLicencePlateState.setSelection(plateState.indexOf(myDriverAccountResponse.getDocuments().getLicence_issuing_state()));
            binding.licencePlateNumber.setText(myDriverAccountResponse.getDocuments().getLicence_plate_number());

        }

        if (myDriverAccountResponse.getDocument_images() != null) {
            PicassoManager.setImage(myDriverAccountResponse.getDocument_images().getCar_insurance_policy(), binding.photo2);
            PicassoManager.setImage(myDriverAccountResponse.getDocument_images().getCar_registration(), binding.photo3);
            PicassoManager.setImage(myDriverAccountResponse.getDocument_images().getCaregiver_certificate(), binding.photo4);
            PicassoManager.setImage(myDriverAccountResponse.getDocument_images().getDriving_license(), binding.photo1);
            PicassoManager.setImage(myDriverAccountResponse.getProfile_image(), binding.photo0);


        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.date_of_birth:
                setDateOfBirth();
                break;
            case R.id.take_photo1:
                openActionSheet();
                photoTypeCount = 1;
                break;
            case R.id.take_photo2:
                openActionSheet();
                photoTypeCount = 2;
                break;
            case R.id.take_photo3:
                openActionSheet();
                photoTypeCount = 3;
                break;
            case R.id.take_photo4:
                openActionSheet();
                photoTypeCount = 4;
                break;
            case R.id.expiration_date:
                setDate(1);
                break;
            case R.id.btn_save:
                updateMyAccountData();
                break;
        }
    }


    // open action sheet
    void openActionSheet() {
        ActionSheet.createBuilder(getActivity(), getActivity().getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Camera", "Gallery")
                .setCancelableOnTouchOutside(true)
                .setListener(this).show();
    }


    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        if (index == 0) {
            requestStorageAndCameraPermission(getActivity(), CAMERA_PIC);
        } else if (index == 1) {
            requestStorageAndCameraPermission(getActivity(), GALLERY_PIC);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == GALLERY_PIC) {
                try {
                    Uri selectedFileUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedFileUri);
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount == 1) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.DRIVING_LICENCE + ".jpg");
                    } else if (photoTypeCount == 2) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CAR_INSURANCE_POLICY + ".jpg");
                    } else if (photoTypeCount == 3) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CAR_REGISTRATION + ".jpg");
                    } else if (photoTypeCount == 4) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CARE_GIVER_CERTIFICATE + ".jpg");
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_PIC) {
                try {
                    Bundle extras = data.getExtras();
                    bitmap = (Bitmap) extras.get("data");
                    byte[] bytes = getFileDataFromDrawable(bitmap);
                    if (photoTypeCount == 1) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.DRIVING_LICENCE + ".jpg");
                    } else if (photoTypeCount == 2) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CAR_INSURANCE_POLICY + ".jpg");
                    } else if (photoTypeCount == 3) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CAR_REGISTRATION + ".jpg");
                    } else if (photoTypeCount == 4) {
                        viewModel.uploadDriverDocPhoto(bytes, UUID.randomUUID() + AppConstant.CARE_GIVER_CERTIFICATE + ".jpg");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            application.messageManager.DisplayToastMessage("Something went wrong");
        }
    }

    public void setDate(int flag) {
        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointForward.now());

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {

                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar.setTimeInMillis(selection);

                int month = calendar.get(Calendar.MONTH);
                String date = (month + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.YEAR);

                if (flag == 1) {
                    exLongDate = selection;
                    binding.expirationDate.setText("" + date);

                }

            }
        });
    }

    public void setDateOfBirth() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        // calendar.set(Calendar.YEAR,-18);
        //    calendar.add(Calendar.YEAR,32);
        //   long time= calendar.getTimeInMillis();

        // long today=MaterialDatePicker.todayInUtcMilliseconds();


        CalendarConstraints.Builder constraintBuilder = new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointBackward.now());
        // constraintBuilder.setEnd(time);

        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        // builder.setSelection(today);
        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(getActivity().getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {


                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar.setTimeInMillis(selection);
                int month = calendar.get(Calendar.MONTH);
                String dateOfBirth = (month + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.YEAR);


                if (AppUtil.subYears(AppUtil.getCurrentDate(), dateOfBirth) < 18) {
                    date = 0;
                    binding.dateOfBirth.setText("");
                    Toast.makeText(getContext(), "Please enter the valid date of birth", Toast.LENGTH_SHORT).show();
                } else {
                    date = selection;
                    binding.dateOfBirth.setText("" + dateOfBirth);
                }

            }
        });

    }


    boolean isValidation() {
        if (Objects.requireNonNull(binding.firstName.getText()).toString().equals("")) {
            binding.firstName.setError("Please enter First Name");
            binding.firstName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.lastName.getText()).toString().equals("")) {
            binding.lastName.setError("Please enter Last Name");
            binding.lastName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.streetAddress.getText()).toString().equals("")) {
            binding.streetAddress.setError("Please enter Address");
            binding.streetAddress.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.city.getText()).toString().equals("")) {
            binding.city.setError("Please enter City");
            binding.city.requestFocus();
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please select state");
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().equals("")) {
            binding.zipCode.setError("Please enter Zip Code");
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().trim().length() < 5) {
            binding.zipCode.setError("Please Enter Valid Zip Code");
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().equals("")) {
            binding.mobilePhone.setError("Please enter mobile number");
            binding.mobilePhone.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().length() < 14) {
            binding.mobilePhone.setError("Please Enter Valid Phone Number");
            binding.mobilePhone.requestFocus();
            return false;
        } else if (binding.dateOfBirth.getText().toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please select Date of Birth");
            return false;
        }

        /*===========================*/


        else if (binding.spinnerIssuingState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please select Issuing State");
            return false;
        } else if (Objects.requireNonNull(binding.driverLicense.getText()).toString().equals("")) {
            binding.driverLicense.setError("Please enter Driver License Number");
            binding.driverLicense.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.expirationDate.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please select Date of Expiration");
            return false;
        } else if (binding.spinnerYear.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please select Year");
            return false;
        } else if (Objects.requireNonNull(binding.make.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please enter Make");
            binding.make.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.model.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please select Modal Name");
            binding.model.requestFocus();
            return false;
        }

       /* else if (binding.spinnerMake.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please enter Make");
            binding.mobilePhone.requestFocus();
            return false;
        } else if (binding.spinnerModel.getSelectedItemPosition() == 0 && binding.spinnerModel.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.select_model))) {
            application.messageManager.DisplayToastMessage("Please select Modal Name");
            return false;
        } */

        else if (binding.spinnerLicencePlateState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage("Please select Licence Plate State");
            return false;
        } else if (Objects.requireNonNull(binding.licencePlateNumber.getText()).toString().equals("")) {
            application.messageManager.DisplayToastMessage("Please select Licence Plate Number");
            binding.mobilePhone.requestFocus();
            return false;
        }

        /*============================*/
        else if (myDriverAccountResponse.getDocument_images().getDriving_license() == null) {
            application.messageManager.DisplayToastMessage("Please select Driver's Licence Image");
            return false;
        } else if (myDriverAccountResponse.getDocument_images().getCar_insurance_policy() == null) {
            application.messageManager.DisplayToastMessage("Please select Driver's Car Insurance Image");
            return false;
        } else if (myDriverAccountResponse.getDocument_images().getCar_registration() == null) {
            application.messageManager.DisplayToastMessage("Please select Driver's Photo Registration Proof Image");
            return false;
        } else if (myDriverAccountResponse.getAccount().getIs_caregiver()) {
            if (myDriverAccountResponse.getDocument_images().getCaregiver_certificate() == null) {
                application.messageManager.DisplayToastMessage("Please select Driver's CareGiver Image");
                return false;
            }
        }


        return true;

    }

    void updateMyAccountData() {

        /*if ((!userAddressModel.getStreet_1().equals(binding.streetAddress.getText().toString())) || (!userAddressModel.getStreet_2().equals(binding.streetAddress2.getText().toString()))
                || (!userAddressModel.getCity().equals(binding.city.getText().toString())) || (!userAddressModel.getState().equals(binding.spinnerState.getSelectedItem().toString()))
                || (!userAddressModel.getZip_code().equals(binding.zipCode.getText().toString())))
        {
            registerRequest.setAddressProofPhotoUrl(null);
            PicassoManager.setImage("No image",binding.photo3);
        }*/

        if (isValidation()) {
            myDriverAccountResponse.getAccount().setEmail_communication(binding.cbEmailReceiving.isChecked());
            myDriverAccountResponse.getAccount().setFirst_name(binding.firstName.getText().toString());
            myDriverAccountResponse.getAccount().setLast_name(binding.lastName.getText().toString());
            myDriverAccountResponse.getAddress().setStreet_1(binding.streetAddress.getText().toString());
            myDriverAccountResponse.getAddress().setStreet_2(binding.streetAddress2.getText().toString());
            myDriverAccountResponse.getAddress().setCity(binding.city.getText().toString());
            myDriverAccountResponse.getAddress().setState(binding.spinnerState.getSelectedItem().toString());
            myDriverAccountResponse.getAddress().setZip_code(binding.zipCode.getText().toString());

            String tmp = binding.mobilePhone.getText().toString();
            String phone = tmp.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            myDriverAccountResponse.getAddress().setPhone_number(phone);


            myDriverAccountResponse.getAccount().setDate_of_birth(date);

           /* if (binding.cbSendNotification.isChecked()) {
                myDriverAccountResponse.getAccount().setMessage_notification("YES");
            } else {
                myDriverAccountResponse.getAccount().setMessage_notification("NO");
            }*/
            myDriverAccountResponse.getAccount().setMessage_notification(binding.cbSendNotification.isChecked());


            myDriverAccountResponse.getDocuments().setIssuing_state(binding.spinnerState.getSelectedItem().toString());
            myDriverAccountResponse.getDocuments().setDocument_type(getResources().getString(R.string.driving_licence));
            myDriverAccountResponse.getDocuments().setExpiration_date(exLongDate);
            myDriverAccountResponse.getDocuments().setManufacture_year(binding.spinnerYear.getSelectedItem().toString());
            myDriverAccountResponse.getDocuments().setIdentity_number(binding.driverLicense.getText().toString());

            // myDriverAccountResponse.getDocuments().setManufacture_company(binding.spinnerMake.getSelectedItem().toString());
            //myDriverAccountResponse.getDocuments().setManufacture_model(binding.spinnerModel.getSelectedItem().toString());
            myDriverAccountResponse.getDocuments().setManufacture_company(binding.make.getText().toString());
            myDriverAccountResponse.getDocuments().setManufacture_model(binding.model.getText().toString());


            myDriverAccountResponse.getDocuments().setLicence_issuing_state(binding.spinnerLicencePlateState.getSelectedItem().toString());
            myDriverAccountResponse.getDocuments().setLicence_plate_number(binding.licencePlateNumber.getText().toString());


            if (!(myDriverAccountResponse.getAccount().getIs_caregiver())) {
                myDriverAccountResponse.getDocument_images().setCaregiver_certificate(null);
            }
            viewModel.updateDrverMyAccountInfo(myDriverAccountResponse);
        }
    }

    void subscribeObservers() {
        viewModel.observeGetMyAccountInfo().observe(Objects.requireNonNull(getActivity()), dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        MyDriverAccountResponse myDriverAccountResponse = (MyDriverAccountResponse) dataResource.data;
                        if (myDriverAccountResponse.getStatus().equals(AppConstant.SUCCESS)) {
                            updateUi(myDriverAccountResponse);
                        }

                        break;
                    case ERROR:
                        binding.scrollBarAccountInfo.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.VISIBLE);
                        binding.progressBar.setVisibility(View.GONE);
                }
            }
        });

        viewModel.observeUploadDriverDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    PicassoManager.setImage(url, binding.photo1);
                                    myDriverAccountResponse.getDocument_images().setDriving_license(url);
                                } else if (photoTypeCount == 2) {
                                    PicassoManager.setImage(url, binding.photo2);
                                    myDriverAccountResponse.getDocument_images().setCar_insurance_policy(url);
                                } else if (photoTypeCount == 3) {
                                    PicassoManager.setImage(url, binding.photo3);
                                    myDriverAccountResponse.getDocument_images().setCar_registration(url);
                                } else if (photoTypeCount == 4) {
                                    PicassoManager.setImage(url, binding.photo4);
                                    myDriverAccountResponse.getDocument_images().setCaregiver_certificate(url);
                                } else {
                                    Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                                }

                            }
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        viewModel.observeUpdateMyAccountDriverInfo().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
                            userModel.setName(binding.firstName.getText().toString());
                            userModel.setStreet_1(binding.streetAddress.getText().toString());
                            PreferenceManger.getPreferenceManger().setObject(PrefKeys.LOGIN_USER, userModel);
                            application.dialogManager.dismissProgressDialog();

                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {

                                }
                            }.showSimpleDialog(getContext(), "Success", "Account information has been updated successfully.", "OK");

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


      /*  viewModel.observeVehicleCompany().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            vehicleCompanyList = (List<VehicleCompany>) dataResource.data;
                            setVehicleCompanyMakeSppiner();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.observeVehicleModel().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            vehicleModelList = (List<VehicleModel>) dataResource.data;
                            viewModel.getDriverMyAccountInfo();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/

    }

//    void setVehicleCompanyMakeSppiner() {
//        vehicleMakeCompanyStrList.clear();
//        vehicleMakeCompanyStrList.add(getResources().getString(R.string.select_make));
//        for (int i = 0; i < vehicleCompanyList.size(); i++) {
//
//            vehicleMakeCompanyStrList.add(vehicleCompanyList.get(i).getCompany_name());
//        }
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_spinner_item, vehicleMakeCompanyStrList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        binding.spinnerMake.setAdapter(adapter);
//
//        viewModel.getVehicleModelList();
//
//    }
//
//    void setVehicleModelSppiner(String companyName) {
//        vehicleModelStrList.clear();
//        vehicleModelStrList.add(getResources().getString(R.string.select_model));
//
//        for (int i = 0; i < vehicleModelList.size(); i++) {
//            if (companyName.equalsIgnoreCase(vehicleModelList.get(i).getCompany_name())) {
//                vehicleModelStrList.add(vehicleModelList.get(i).getVahicle_model());
//            }
//        }
//        if (vehicleModelStrList.size() == 1) {
//            vehicleModelStrList.clear();
//            vehicleModelStrList.add("Model name not available for this company.");
//        }
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_spinner_item, vehicleModelStrList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        binding.spinnerModel.setAdapter(adapter);
//    }
//
//
//    void setModelName() {
//        vehicleModelStrList.clear();
//        vehicleModelStrList.add(getResources().getString(R.string.select_model));
//
//        for (int i = 0; i < vehicleModelList.size(); i++) {
//            if (binding.spinnerMake.getSelectedItem().toString().equalsIgnoreCase(vehicleModelList.get(i).getCompany_name())) {
//                vehicleModelStrList.add(vehicleModelList.get(i).getVahicle_model());
//            }
//        }
//        if (vehicleModelStrList.size() == 1) {
//            vehicleModelStrList.clear();
//            vehicleModelStrList.add("Model name not available for this company.");
//        }
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_spinner_item, vehicleModelStrList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        binding.spinnerModel.setAdapter(adapter);
//    }

    private void createPhoneNumberInUsFormat(String number) {
        String text = "";

        try {
            if (number.length() == 10) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4) + number.charAt(5) + "-" +
                        number.charAt(6) + number.charAt(7) + number.charAt(8) + number.charAt(9);
            } else if (number.length() == 9) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4) + number.charAt(5) + "-" +
                        number.charAt(6) + number.charAt(7) + number.charAt(8);
            } else if (number.length() == 8) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4) + number.charAt(5) + "-" +
                        number.charAt(6) + number.charAt(7);
            } else if (number.length() == 7) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4) + number.charAt(5) + "-" +
                        number.charAt(6);
            } else if (number.length() == 6) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4) + number.charAt(5);
            } else if (number.length() == 5) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3) + number.charAt(4);
            } else if (number.length() == 4) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")" +
                        " " + number.charAt(3);
            } else if (number.length() == 3) {
                text = "(" + number.charAt(0) + number.charAt(1) + number.charAt(2) + ")";
            } else if (number.length() == 2) {
                text = "(" + number.charAt(0) + number.charAt(1);
            } else if (number.length() == 1) {
                text = "(" + number.charAt(0);
            } else if (number.length() == 0) {
                text = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.mobilePhone.setText(text);

    }


}