package com.splitreef.grabagramplaystore.data.model.customer.coupon;

import java.io.Serializable;

public class CouponResponse implements Serializable {

    private String status;
    private String message;
    private CouponModel couponModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CouponModel getCouponModel() {
        return couponModel;
    }

    public void setCouponModel(CouponModel couponModel) {
        this.couponModel = couponModel;
    }
}
