package com.splitreef.grabagramplaystore.data.model.customer.productCategory;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductCategoryResponse implements Serializable {

    private String status;
    private ArrayList<ProductCategory>productCategoryList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ProductCategory> getProductCategoryList() {
        return productCategoryList;
    }

    public void setProductCategoryList(ArrayList<ProductCategory> productCategoryList) {
        this.productCategoryList = productCategoryList;
    }
}
