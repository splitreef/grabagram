package com.splitreef.grabagramplaystore.ui.customer.dispensaryReview;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerUser;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityDispensaryReviewBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryReviewProviderFactory;

import java.util.ArrayList;


public class DispensaryReviewActivity extends BaseActivity implements View.OnClickListener {

    private ActivityDispensaryReviewBinding binding;
    private DispensaryReviewViewModel viewModel;
    private GrabAGramApplication application;
    private Order order;
    private DispensaryReviewModel dispensaryOwnReview=null;
    float rating = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityDispensaryReviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    public void initView()
    {

        application = (GrabAGramApplication) getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new DispensaryReviewProviderFactory(application.firebaseRepository)).get(DispensaryReviewViewModel.class);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);
        subscribeObservers();

        binding.btnSave.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);


        if (getIntent()!=null)
        {
            order= (Order) getIntent().getParcelableExtra(AppConstant.ORDER_MODEL);
            if (order!=null)
            {
                viewModel.getDispensaryReviewList(order.getDispensary_data().getId());
                updateUi(order);

            }
        }



    }

    @SuppressLint("SetTextI18n")
    void updateUi(Order order)
    {
        DispensaryAddressModel dispensaryAddressModel=order.getDispensary_data().getAddress();
        binding.txtDispensaryName.setText(""+order.getBusniess_name());
        binding.txtDispensaryAddress.setText(""+dispensaryAddressModel.getStreet1()+" "+dispensaryAddressModel.getStreet2()+" "+
                dispensaryAddressModel.getCity()+" "+dispensaryAddressModel.getState());

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }


   void subscribeObservers() {

       viewModel.observeGetDispensaryReviewList().observe(DispensaryReviewActivity.this, dataResource -> {
           if (dataResource!=null)
           {
               switch (dataResource.status)
               {
                   case LOADING:
                       application.dialogManager.displayProgressDialog(DispensaryReviewActivity.this,"Loading...");
                       break;
                   case SUCCESS:
                       application.dialogManager.dismissProgressDialog();
                       ArrayList<DispensaryReviewModel> dispensaryReviewArrayList= (ArrayList<DispensaryReviewModel>) dataResource.data;
                       setReviewAndRating(dispensaryReviewArrayList);
                       break;
                   case ERROR:
                       application.dialogManager.dismissProgressDialog();
                       break;
               }
           }

       });

       viewModel.observeSaveDispensaryReview().observe(DispensaryReviewActivity.this, dataResource -> {
           if (dataResource!=null)
           {
               switch (dataResource.status)
               {
                   case LOADING:
                       application.dialogManager.displayProgressDialog(DispensaryReviewActivity.this,"Loading...");
                       break;
                   case SUCCESS:
                       application.dialogManager.dismissProgressDialog();
                       String message = (String) dataResource.data;
                       finish();
                       break;
                   case ERROR:
                       application.dialogManager.dismissProgressDialog();
                       break;
               }
           }

       });

    }

    public void setReviewAndRating(ArrayList<DispensaryReviewModel> dispensaryReviewArrayList)
    {
         rating = 0;

        if (dispensaryReviewArrayList!=null && dispensaryReviewArrayList.size()>0)
        {
           for (int i=0;i<dispensaryReviewArrayList.size();i++)
           {
               DispensaryReviewModel dispensaryReviewModel=dispensaryReviewArrayList.get(i);
               if (dispensaryReviewModel.getOrder_id().equals(order.getId()))
               {
                   dispensaryOwnReview=dispensaryReviewModel;
               }

               rating=rating+dispensaryReviewModel.getRating();
           }

           rating=rating/dispensaryReviewArrayList.size();

           if (dispensaryOwnReview!=null)
           {
               binding.editReview.setText(dispensaryOwnReview.getComment());
               binding.rating.setRating(dispensaryOwnReview.getRating());
               binding.llSaveCancel.setVisibility(View.GONE);
           }

        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btn_save:
                if (binding.editReview.getText().toString().isEmpty())
                {
                    showToast("Please enter review message");
                }
                else
                {
                    saveReview();
                }

                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private void saveReview()
    {
        MyAccountResponse myAccountResponse = PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);

        CustomerUser customerUser=myAccountResponse.getUser();
        CustomerAccount customerAccount=myAccountResponse.getAccount();

        DispensaryReviewModel dispensaryReviewModel=new DispensaryReviewModel();
        dispensaryReviewModel.setComment(binding.editReview.getText().toString());
        dispensaryReviewModel.setCreated_at(AppUtil.getCurrentTimeInMillisecond());
        dispensaryReviewModel.setDispensary_id(order.getDispensary_data().getId());
        dispensaryReviewModel.setOrder_id(order.getId());
        dispensaryReviewModel.setUpdated_at(AppUtil.getCurrentTimeInMillisecond());
        dispensaryReviewModel.setUser_id(customerUser.getUser_id());
        dispensaryReviewModel.setUser_name(customerAccount.getFirst_name()+" "+customerAccount.getLast_name());
        dispensaryReviewModel.setRating(binding.rating.getRating());

        float totalRating=(binding.rating.getRating()+rating)/2;

        viewModel.saveDispensaryReview(dispensaryReviewModel,totalRating);
    }
}
