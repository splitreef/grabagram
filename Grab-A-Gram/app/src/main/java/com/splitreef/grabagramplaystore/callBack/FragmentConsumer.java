package com.splitreef.grabagramplaystore.callBack;

import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;

public interface FragmentConsumer {

    void getData(int position, CustomerRegisterRequest requestData);
}
