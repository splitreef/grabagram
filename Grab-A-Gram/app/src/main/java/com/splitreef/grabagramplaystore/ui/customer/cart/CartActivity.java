package com.splitreef.grabagramplaystore.ui.customer.cart;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.CartAdapter;
import com.splitreef.grabagramplaystore.callBack.UpdateCartProduct;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponModel;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponProduct;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.databinding.ActivityCartBinding;
import com.splitreef.grabagramplaystore.ui.customer.checkout.CheckoutActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.CartProviderFactory;

public class CartActivity extends BaseActivity implements UpdateCartProduct {
    private ActivityCartBinding binding;
    private CartViewModel viewModel;
    private GrabAGramApplication application;
    private GetProductCartResponse productCartResponse;
    private  CouponModel couponModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityCartBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }


    public void initView()
    {
        application = (GrabAGramApplication)getApplicationContext();

        viewModel = new ViewModelProvider(getViewModelStore(), new CartProviderFactory(application.firebaseRepository)).get(CartViewModel.class);
        subscribeObservers();

        viewModel.getProductCartList();


        binding.rvCart.setLayoutManager(new LinearLayoutManager(CartActivity.this,LinearLayoutManager.VERTICAL,false));

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);


        binding.btnContinueToCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartActivity.this, CheckoutActivity.class)
                .putExtra(AppConstant.FROM,AppConstant.FROM_CART_SCREEN)
                .putExtra(AppConstant.GET_PRODUCT_CART_RESPONSE,productCartResponse));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        binding.btnCoupon.setOnClickListener(view ->
        {
            if (binding.btnCoupon.getText().toString().equalsIgnoreCase("Apply Coupon"))
            {
                if (binding.editCoupon.getText().toString().isEmpty())
                {
                    showToast(getString(R.string.coupon_empty));
                }
                else
                {
                    viewModel.getCouponProducts(binding.editCoupon.getText().toString());
                }
            }
            else
            {
                viewModel.cancelCouponCode(productCartResponse.getUsed_coupon_id());
            }


        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void subscribeObservers() {
        viewModel.observeGetProductCartList().observe(CartActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CartActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        productCartResponse = (GetProductCartResponse) dataResource.data;
                        updateUi(productCartResponse);
                        break;
                    case ERROR:
                        binding.llChild.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.VISIBLE);
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });


        viewModel.observeUpdateProductCartList().observe(CartActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                             viewModel.getProductCartList();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });

        viewModel.observeDeleteProductCartList().observe(CartActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            viewModel.getProductCartList();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });



        viewModel.observeOngGetCouponProduct().observe(CartActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CartActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        CouponResponse couponResponse = (CouponResponse) dataResource.data;
                        if (couponResponse.getStatus().equals(AppConstant.SUCCESS))
                        {
                            applyCouponCode(couponResponse);
                        }
                        else
                        {
                            showToast(couponResponse.getMessage());
                            application.dialogManager.dismissProgressDialog();
                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });



        viewModel.observeUpdateCouponCode().observe(CartActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            viewModel.getProductCartList();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });

        viewModel.observeCancelCouponCode().observe(CartActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(CartActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            viewModel.getProductCartList();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });
    }


    void updateUi(GetProductCartResponse productCartResponse)
    {
        if (productCartResponse!=null && productCartResponse.getProductCarts()!=null && productCartResponse.getProductCarts().size()>0)
        {
            binding.llChild.setVisibility(View.VISIBLE);
            binding.userMsg.setVisibility(View.GONE);

            CartAdapter cartAdapter=new CartAdapter(CartActivity.this,productCartResponse.getProductCarts(),this);
            binding.rvCart.setAdapter(cartAdapter);

            generateBill(productCartResponse);
        }
        else
        {
            binding.llChild.setVisibility(View.GONE);
            binding.userMsg.setVisibility(View.VISIBLE);
        }
    }


    void applyCouponCode(CouponResponse couponResponse)
    {
        double discount=0;
        Log.d("getUserUsedLimit",""+couponResponse.getCouponModel().getUserUsedLimit());
        couponModel=couponResponse.getCouponModel();
        if (couponModel!=null && productCartResponse!=null && couponModel.getProduct()!=null && couponModel.getProduct().size()>0)
        {
            if (couponModel.getDispensary_user_id().equals(productCartResponse.getDispensary_user_id()))
            {
                 if (couponModel.getUsage_limit_per_user()>couponModel.getUserUsedLimit())
                 {
                     if (couponModel.getDiscount_type().equals("percentage"))
                     {
                         for (int i=0;i<productCartResponse.getProductCarts().size();i++)
                         {
                             for (int j=0;j<couponModel.getProduct().size();j++)
                             {
                                 ProductCart productCart=productCartResponse.getProductCarts().get(i);
                                 CouponProduct couponProduct=couponModel.getProduct().get(j);

                                 if (productCart.getProduct_id().equals(couponProduct.getProduct_id()))
                                 {
                                   double totalCount = productCart.getSale_price() * productCart.getQuantity();

                                   discount = discount + totalCount / 100 * couponModel.getPercentage();

                                 }
                             }
                         }

                         if (discount>0)
                         {
                             couponModel.setTotalDiscount(discount);
                             //couponModel.setUserUsedLimit(couponModel.getUserUsedLimit()+1);
                             viewModel.updateCouponCode(couponModel);
                         }
                         else
                         {
                             showToast(AppConstant.COUPON__PRODUCTS_NOT_MATCH);
                             application.dialogManager.dismissProgressDialog();
                         }

                     }
                     else
                     {
                         application.dialogManager.dismissProgressDialog();
                     }
                 }
                 else
                 {
                     showToast(AppConstant.USER_COUPON_LIMIT);
                     application.dialogManager.dismissProgressDialog();
                 }
            }
            else
            {
                showToast(AppConstant.COUPON_DISPENSARY_NOT_MATCH);
                application.dialogManager.dismissProgressDialog();
            }
        }
        else
        {
            showToast(AppConstant.COUPON_NOT_VALID);
            application.dialogManager.dismissProgressDialog();
        }

    }


    void generateBill(GetProductCartResponse productCartResponse)
    {
        double subTotal=0;
        double salesTax=0;
      //  double deliveryFee=50;
        double discount=0;
        double total=0;

        for (int i=0; i<productCartResponse.getProductCarts().size();i++)
        {
            ProductCart productCart=productCartResponse.getProductCarts().get(i);
            int quantity=productCart.getQuantity();
            subTotal =subTotal+(quantity*productCart.getSale_price());
            //salesTax=salesTax+productCart.getTax_rate();
        }

        discount=productCartResponse.getTotal_discount();
        total=subTotal+salesTax-discount;

        Log.d("discount==",""+productCartResponse.getTotal_discount());

        binding.txtSubtotal.setText("$"+subTotal);
      //  binding.txtDeliveryFee.setText("$"+deliveryFee);
        binding.txtSalesTax.setText("$"+salesTax);
        binding.txtSalesTax.setText("$"+salesTax);
        binding.txtDiscount.setText("$"+discount);
        binding.txtTotal.setText("$"+total);

        if ((productCartResponse.getUsed_coupon_id()!=null) && (!productCartResponse.getUsed_coupon_id().equals("")))
        {
            binding.editCoupon.setText(""+productCartResponse.getCoupon_code_name());
            binding.btnCoupon.setText("Cancel");
        }
        else
        {
            binding.btnCoupon.setText("Apply Coupon");
            binding.editCoupon.setText("");
        }

    }

    @Override
    public void updateCartProduct(String id, int quantity, double totalPrice) {
          viewModel.updateCartProduct(id,quantity,totalPrice);
    }

    @Override
    public void removeCartProduct(String id,boolean isLastItem) {
       viewModel.deleteCartProduct(id,isLastItem);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }
}
