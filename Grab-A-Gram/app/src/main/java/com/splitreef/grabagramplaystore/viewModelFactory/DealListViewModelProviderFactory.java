
package com.splitreef.grabagramplaystore.viewModelFactory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryDealListViewModel;


public class DealListViewModelProviderFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final FirebaseRepository firebaseRepository;


    public DealListViewModelProviderFactory(Application application, FirebaseRepository firebaseRepository) {
        mApplication = application;
        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(DispensaryDealListViewModel.class))
        {
            return  (T) new DispensaryDealListViewModel(mApplication, firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}





