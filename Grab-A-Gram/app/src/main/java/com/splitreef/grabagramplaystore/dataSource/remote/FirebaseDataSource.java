package com.splitreef.grabagramplaystore.dataSource.remote;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.reflect.TypeToken;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.splitreef.grabagramplaystore.data.model.PaymentMethod;
import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.AddressRequestParams;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.data.model.customer.Order;
import com.splitreef.grabagramplaystore.data.model.customer.OrderRequest;
import com.splitreef.grabagramplaystore.data.model.customer.OrderResponse;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponModel;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.data.model.customer.dispensaryIdList.DispensaryIdListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distance.DispensaryDistanceResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distance.Distance;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverLocation;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfile;
import com.splitreef.grabagramplaystore.data.model.customer.driverProfile.DriverProfileResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRating;
import com.splitreef.grabagramplaystore.data.model.customer.driverRating.DriverRatingResponse;
import com.splitreef.grabagramplaystore.data.model.customer.driverTokens.DriverTokens;
import com.splitreef.grabagramplaystore.data.model.customer.help.Help;
import com.splitreef.grabagramplaystore.data.model.customer.help.HelpListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Category;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductListResponse;
import com.splitreef.grabagramplaystore.data.model.customer.menu.ProductReviewModel;
import com.splitreef.grabagramplaystore.data.model.customer.menu.RelatedProduct;
import com.splitreef.grabagramplaystore.data.model.customer.menu.Weights;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategory;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategoryResponse;
import com.splitreef.grabagramplaystore.data.model.customer.productReviewResponse.ProductReviewResponse;
import com.splitreef.grabagramplaystore.data.model.driver.DeclineOrders;
import com.splitreef.grabagramplaystore.data.model.driver.DriverLocationStatusModel;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.data.model.driver.UserDriverAccountModel;
import com.splitreef.grabagramplaystore.data.model.driver.driverRating.MyDriverRating;
import com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount.DriverAddress;
import com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount.MyDriverAccountResponse;
import com.splitreef.grabagramplaystore.data.model.driver.orders.OrdersModel;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.GetDistance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.annotations.NonNull;

import static com.splitreef.grabagramplaystore.utils.AppConstant.CUSTOMER_COLLECTION;
import static com.splitreef.grabagramplaystore.utils.AppConstant.DRIVER_COLLECTION;
import static com.splitreef.grabagramplaystore.utils.AppConstant.DRIVER_DECLINE_ORDER;
import static com.splitreef.grabagramplaystore.utils.AppConstant.DRIVER_LOCATION_STATUS;
import static com.splitreef.grabagramplaystore.utils.AppConstant.ERROR;
import static com.splitreef.grabagramplaystore.utils.AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS;
import static com.splitreef.grabagramplaystore.utils.AppConstant.SUCCESS;

public class FirebaseDataSource {

    private static final String TAG = "FirebaseAuthSource";
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    FirebaseStorage firebaseStorage;
    Context context;
    DocumentSnapshot lastVisible;
    ArrayList<DispensaryModel> dispensaryModelArrayList;
    ArrayList<Weights> weightsList;
    ArrayList<RelatedProduct> relatedProducts;
    ArrayList<ProductReviewModel> productReviewList;
    List<OrdersModel> ordersModelList;
    String badgeCount = "0";


    // FirebaseDataSource parametrized constructor
    public FirebaseDataSource(FirebaseAuth firebaseAuth, FirebaseFirestore firebaseFirestore, FirebaseStorage firebaseStorage, Context context) {
        this.firebaseAuth = firebaseAuth;
        this.firebaseFirestore = firebaseFirestore;
        this.firebaseStorage = firebaseStorage;
        this.context = context;

    }

    // get current user id
    public String getCurrentUid() {
        return firebaseAuth.getCurrentUser().getUid();
    }

    //get current user
    public FirebaseUser getCurrentUser() {

        return firebaseAuth.getCurrentUser();
    }

    // Create new User
    public Completable registerUser(String email, String password) {
        return Completable.create(emitter -> firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {

            String fcmToken = PreferenceManger.getFcmToken(context);

            if (task.isSuccessful()) {
                //create new user
                HashMap<String, Object> map = new HashMap<>();
                map.put("activated_at", true);
                map.put("device_id", fcmToken + "");
                map.put("device_mac_id", AppConstant.getDeviceId(context));
                map.put("device_type", AppConstant.DEVICE_TYPE);
                map.put("email", email);
                map.put("password", password);
                map.put("role", AppConstant.CUSTOMER_ROLE);
                map.put("user_id", getCurrentUid());
                map.put("created_at",AppUtil.getCurrentTimeInMillisecond());

                firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).set(map)
                        .addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                emitter.onComplete();
                            }
                        })
                        .addOnFailureListener(emitter::onError);
            }
        }).addOnFailureListener(emitter::onError));

    }


    public Single<Boolean> checkUserExistsOrNot(String email) {
        return Single.create(emitter -> firebaseAuth.fetchSignInMethodsForEmail(email)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        SignInMethodQueryResult result = task.getResult();
                        if (result != null) {
                            List<String> signInMethods = result.getSignInMethods();
                            if (signInMethods != null) {
                                if (signInMethods.isEmpty()) {
                                    emitter.onSuccess(false);
                                } else {
                                    emitter.onSuccess(true);
                                }
                            } else {
                                emitter.onSuccess(false);
                            }
                        } else {
                            emitter.onSuccess(false);
                        }

                    } else {
                        emitter.onSuccess(false);
                        Log.e(TAG, "Error getting sign in methods for user", task.getException());
                    }
                }).addOnFailureListener(emitter::onError));
    }


    // login user
    public Single<UserModel> loginUser(String userName, String password) {
        return Single.create(new SingleOnSubscribe<UserModel>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<UserModel> emitter) throws Exception {

                firebaseAuth.signInWithEmailAndPassword(userName, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {

                        String fcmToken = PreferenceManger.getFcmToken(context);
                        firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).update("device_id", fcmToken);

                        firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot != null) {
                                    UserModel userModel = documentSnapshot.toObject(UserModel.class);

                                    if (userModel.getRole().equalsIgnoreCase(AppConstant.CUSTOMER_ROLE)) {

                                        if (userModel.isActivated_at()) {
                                            firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response").document("data_account").get()
                                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                                                            if (documentSnapshot.get("addresses") != null) {
                                                                JsonElement jsonElement = new Gson().toJsonTree(documentSnapshot.get("addresses"));
                                                                UserAddressModel userAddressModel = new Gson().fromJson(jsonElement, UserAddressModel.class);

                                                                userModel.setStreet_1(userAddressModel.getStreet_1());
                                                                userModel.setBadgeCount(badgeCount);
                                                                emitter.onSuccess(userModel);
                                                            }

                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                    emitter.onError(e);
                                                }
                                            });
                                        } else {
                                            emitter.onSuccess(userModel);
                                        }

                                    } else if (userModel.getRole().equalsIgnoreCase(AppConstant.DRIVER_ROLE)) {
                                        firebaseFirestore.collection(DRIVER_COLLECTION).document(getCurrentUid()).
                                                collection("response").document("data_account").get()
                                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                                                        if (documentSnapshot.get("account") != null) {
                                                            JsonElement jsonElement = new Gson().toJsonTree(documentSnapshot.get("account"));
                                                            UserDriverAccountModel driverAccountModel = new Gson().fromJson(jsonElement, UserDriverAccountModel.class);
                                                            userModel.setName(driverAccountModel.getFirst_name());

                                                            if (documentSnapshot.get("address") != null) {
                                                                JsonElement jsonElementt = new Gson().toJsonTree(documentSnapshot.get("address"));
                                                                DriverAddress driverAddress = new Gson().fromJson(jsonElementt, DriverAddress.class);
                                                                userModel.setStreet_1(driverAddress.getStreet_1());

                                                            }

                                                         /*   if (documentSnapshot.get("document_images") != null) {
                                                                JsonElement jsonElementt = new Gson().toJsonTree(documentSnapshot.get("document_images"));
                                                                DriverDocumentImagesModel driverDocumentImagesModel = new Gson().fromJson(jsonElementt, DriverDocumentImagesModel.class);

                                                                userModel.setDriver_photo(driverDocumentImagesModel.getPhoto());
                                                                emitter.onSuccess(userModel);
                                                            }*/

                                                            if (documentSnapshot.contains("profile_image")) {
                                                                String profileImage = documentSnapshot.getString("profile_image");
                                                                userModel.setDriver_photo(profileImage);
                                                                emitter.onSuccess(userModel);
                                                            }
                                                        }

                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                emitter.onError(e);
                                            }
                                        });
                                    }

                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                emitter.onError(e);
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        emitter.onError(e);
                    }
                });
            }
        });
    }

    // user logout

    public void userLogout() {
        firebaseAuth.signOut();
    }


    // Upload customer document photo
    public Single<String> uploadCustomerDocPhoto(byte[] bytes, String imageName) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                StorageReference storageReference = firebaseStorage.getReference().child("customerDoc/" + getCurrentUid() + "/" + imageName + ".jpg");
                UploadTask uploadTask = storageReference.putBytes(bytes);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                        emitter.onError(exception);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        taskSnapshot.getStorage().getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        emitter.onSuccess(uri.toString());
                                        Log.d("url===", uri.toString());
                                    }
                                });
                        taskSnapshot.getStorage().getDownloadUrl().addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                emitter.onError(e);
                            }
                        });

                    }
                });

            }
        });
    }

    // Add customer data
    public Completable addCustomerData(CustomerRegisterRequest customerRegisterRequest) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {


                HashMap<String, Object> accountMap = new HashMap<>();
                accountMap.put("date_of_birth", customerRegisterRequest.getDateOfBirth());
                accountMap.put("e_signature", customerRegisterRequest.geteSignature());
                accountMap.put("first_name", customerRegisterRequest.getFirstName());
                accountMap.put("last_name", customerRegisterRequest.getLastName());
                accountMap.put("message_notification", customerRegisterRequest.isSendNotification());


                HashMap<String, Object> addressesMap = new HashMap<>();
                addressesMap.put("city", customerRegisterRequest.getCity());
                addressesMap.put("latitude", customerRegisterRequest.getLatitude());
                addressesMap.put("longitude", customerRegisterRequest.getLongitude());
                addressesMap.put("phone_number", customerRegisterRequest.getMobileNumber());
                addressesMap.put("state", customerRegisterRequest.getState());
                addressesMap.put("street_1", customerRegisterRequest.getStreetAddress());
                addressesMap.put("street_2", customerRegisterRequest.getStreetAddress2());
                addressesMap.put("type", KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS);
                addressesMap.put("zip_code", customerRegisterRequest.getZipCode());


                HashMap<String, Object> documentImageMap = new HashMap<>();
                documentImageMap.put("address_proof", customerRegisterRequest.getAddressProofPhotoUrl());
                documentImageMap.put("doctor_recommendation_letter", customerRegisterRequest.getDoctorRecPhotoUrl());
                documentImageMap.put("government_photo_id", customerRegisterRequest.getGovPhotoIDUrl());


                HashMap<String, Object> documentInfoMap = new HashMap<>();
                documentInfoMap.put("document_type", customerRegisterRequest.getDocType());
                documentInfoMap.put("expiration_date", customerRegisterRequest.getExpirationDate());
                documentInfoMap.put("identity_number", customerRegisterRequest.getIdentityNumber());
                documentInfoMap.put("issuing_state", customerRegisterRequest.getIssuingState());
                documentInfoMap.put("patient_id", customerRegisterRequest.getPatientId());
                documentInfoMap.put("recommendation_letter_expiration_date", customerRegisterRequest.getDoctorsRecExpirationDate());


                HashMap<String, Object> userMap = new HashMap<>();
                userMap.put("email", customerRegisterRequest.getEmail());
                userMap.put("email_communication", customerRegisterRequest.isEmailCommunication());
                userMap.put("user_id", getCurrentUid());


                HashMap<String, Object> rootMap = new HashMap<>();

                rootMap.put("account", accountMap);
                rootMap.put("addresses", addressesMap);
                rootMap.put("document_images", documentImageMap);
                rootMap.put("document_info", documentInfoMap);
                rootMap.put("user", userMap);


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").set(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                        List<HashMap<String, Object>> dataAddress = new ArrayList<>();
                        dataAddress.add(addressesMap);

                        HashMap<String, Object> addresses = new HashMap<>();
                        addresses.put("addresses", dataAddress);

                        addresses.put("isPersonAddress", true);

                        firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                .document("data_address").set(addresses).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                emitter.onComplete();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                emitter.onError(e);
                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }


    public Single<MyAccountResponse> getCustomerMyAccountData() {
        return Single.create(new SingleOnSubscribe<MyAccountResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<MyAccountResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {

                        MyAccountResponse myAccountResponse = new MyAccountResponse();

                        if (task.isSuccessful()) {
                            myAccountResponse = task.getResult().toObject(MyAccountResponse.class);
                            myAccountResponse.setStatus(AppConstant.SUCCESS);
                        } else {
                            myAccountResponse.setStatus(AppConstant.ERROR);
                        }
                        emitter.onSuccess(myAccountResponse);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    // Add customer data
    public Completable updateCustomerMyAccountData(CustomerRegisterRequest customerRegisterRequest, MyAccountResponse myAccountResponse) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {


                HashMap<String, Object> accountMap = new HashMap<>();
                accountMap.put("date_of_birth", customerRegisterRequest.getDateOfBirth());
                accountMap.put("e_signature", customerRegisterRequest.geteSignature());
                accountMap.put("first_name", customerRegisterRequest.getFirstName());
                accountMap.put("last_name", customerRegisterRequest.getLastName());
                accountMap.put("message_notification", customerRegisterRequest.isSendNotification());


                HashMap<String, Object> addressesMap = new HashMap<>();
                addressesMap.put("city", customerRegisterRequest.getCity());
                addressesMap.put("latitude", customerRegisterRequest.getLatitude());
                addressesMap.put("longitude", customerRegisterRequest.getLongitude());
                addressesMap.put("phone_number", customerRegisterRequest.getMobileNumber());
                addressesMap.put("state", customerRegisterRequest.getState());
                addressesMap.put("street_1", customerRegisterRequest.getStreetAddress());
                addressesMap.put("street_2", customerRegisterRequest.getStreetAddress2());
                addressesMap.put("type", KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS);
                addressesMap.put("zip_code", customerRegisterRequest.getZipCode());


                HashMap<String, Object> addressesMapDelete = new HashMap<>();
                addressesMapDelete.put("city", myAccountResponse.getAddresses().getCity());
                addressesMapDelete.put("latitude", myAccountResponse.getAddresses().getLatitude());
                addressesMapDelete.put("longitude", myAccountResponse.getAddresses().getLongitude());
                addressesMapDelete.put("phone_number", myAccountResponse.getAddresses().getPhone_number());
                addressesMapDelete.put("state", myAccountResponse.getAddresses().getState());
                addressesMapDelete.put("street_1", myAccountResponse.getAddresses().getStreet_1());
                addressesMapDelete.put("street_2", myAccountResponse.getAddresses().getStreet_2());
                addressesMapDelete.put("type", KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS);
                addressesMapDelete.put("zip_code", myAccountResponse.getAddresses().getZip_code());


                HashMap<String, Object> documentImageMap = new HashMap<>();
                documentImageMap.put("address_proof", customerRegisterRequest.getAddressProofPhotoUrl());
                documentImageMap.put("doctor_recommendation_letter", customerRegisterRequest.getDoctorRecPhotoUrl());
                documentImageMap.put("government_photo_id", customerRegisterRequest.getGovPhotoIDUrl());


                HashMap<String, Object> documentInfoMap = new HashMap<>();
                documentInfoMap.put("document_type", customerRegisterRequest.getDocType());
                documentInfoMap.put("expiration_date", customerRegisterRequest.getExpirationDate());
                documentInfoMap.put("identity_number", customerRegisterRequest.getIdentityNumber());
                documentInfoMap.put("issuing_state", customerRegisterRequest.getIssuingState());
                documentInfoMap.put("patient_id", customerRegisterRequest.getPatientId());
                documentInfoMap.put("recommendation_letter_expiration_date", customerRegisterRequest.getDoctorsRecExpirationDate());


                HashMap<String, Object> userMap = new HashMap<>();
                userMap.put("email", customerRegisterRequest.getEmail());
                userMap.put("email_communication", customerRegisterRequest.isEmailCommunication());
                userMap.put("role", AppConstant.CUSTOMER_ROLE);
                userMap.put("user_id", getCurrentUid());


                HashMap<String, Object> rootMap = new HashMap<>();

                rootMap.put("account", accountMap);
                rootMap.put("addresses", addressesMap);
                rootMap.put("document_images", documentImageMap);
                rootMap.put("document_info", documentInfoMap);
                rootMap.put("user", userMap);


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").update(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                                    .document(getCurrentUid())
                                    .collection("response")
                                    .document("data_address")
                                    .update("addresses", FieldValue.arrayRemove(addressesMapDelete))
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {
                                                firebaseFirestore.collection(CUSTOMER_COLLECTION)
                                                        .document(getCurrentUid())
                                                        .collection("response")
                                                        .document("data_address")
                                                        .update("addresses", FieldValue.arrayUnion(addressesMap)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                        emitter.onComplete();
                                                      /*  firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).update("email_communication", customerRegisterRequest.isEmailCommunication(), "patient_id", customerRegisterRequest.getPatientId()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                emitter.onComplete();
                                                            }
                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                                emitter.onError(e);
                                                            }
                                                        });*/

                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                        emitter.onError(e);
                                                    }
                                                });
                                            }

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                    Log.d("Error==", e.getMessage());
                                    emitter.onError(e);
                                }
                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }


    // For adding address
    public Completable addCustomerNewAddress(AddressRequestParams addressRequestParams) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {


                HashMap<String, Object> addressesMap = new HashMap<>();

                addressesMap.put("address_proof", addressRequestParams.getAddress_proof());
                addressesMap.put("city", addressRequestParams.getCity());
                addressesMap.put("first_name", addressRequestParams.getFirst_name());
                addressesMap.put("last_name", addressRequestParams.getLast_name());
                addressesMap.put("latitude", addressRequestParams.getLatitude());
                addressesMap.put("longitude", addressRequestParams.getLongitude());
                addressesMap.put("phone_number", addressRequestParams.getPhone_number());
                addressesMap.put("state", addressRequestParams.getState());
                addressesMap.put("street_1", addressRequestParams.getStreet_1());
                addressesMap.put("street_2", addressRequestParams.getStreet_2());
                addressesMap.put("type", addressRequestParams.getType());
                addressesMap.put("zip_code", addressRequestParams.getZip_code());


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_address").update("addresses", FieldValue.arrayUnion(addressesMap)).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        emitter.onComplete();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Error==", e.getMessage());
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    // For adding address
    public Completable deleteCustomerAddress(UserAddressModel userAddressModel, boolean isPersonAddress) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                HashMap<String, Object> addressesMap = new HashMap<>();

                addressesMap.put("address_proof", "" + userAddressModel.getAddress_proof());
                addressesMap.put("city", userAddressModel.getCity());
                addressesMap.put("first_name", userAddressModel.getFirst_name());
                addressesMap.put("last_name", userAddressModel.getLast_name());
                addressesMap.put("latitude", userAddressModel.getLatitude());
                addressesMap.put("longitude", userAddressModel.getLongitude());
                addressesMap.put("phone_number", userAddressModel.getPhone_number());
                addressesMap.put("state", userAddressModel.getState());
                addressesMap.put("street_1", userAddressModel.getStreet_1());
                addressesMap.put("street_2", userAddressModel.getStreet_2());
                addressesMap.put("type", userAddressModel.getType());
                addressesMap.put("zip_code", userAddressModel.getZip_code());


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_address")
                        .update("addresses", FieldValue.arrayRemove(addressesMap), "isPersonAddress", isPersonAddress).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        emitter.onComplete();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Error==", e.getMessage());
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    public Completable updateIsPersonalAddress(boolean isPersonAddress) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_address")
                        .update("isPersonAddress", isPersonAddress).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        emitter.onComplete();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Error==", e.getMessage());
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    // For update customer address
    public Completable updateCustomerAddress(AddressRequestParams addressRequestParams, UserAddressModel userAddressModel) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                HashMap<String, Object> addressesMap = new HashMap<>();

                addressesMap.put("address_proof", "" + addressRequestParams.getAddress_proof());
                addressesMap.put("city", addressRequestParams.getCity());
                addressesMap.put("first_name", addressRequestParams.getFirst_name());
                addressesMap.put("last_name", addressRequestParams.getLast_name());
                addressesMap.put("latitude", addressRequestParams.getLatitude());
                addressesMap.put("longitude", addressRequestParams.getLongitude());
                addressesMap.put("phone_number", addressRequestParams.getPhone_number());
                addressesMap.put("state", addressRequestParams.getState());
                addressesMap.put("street_1", addressRequestParams.getStreet_1());
                addressesMap.put("street_2", addressRequestParams.getStreet_2());
                addressesMap.put("type", addressRequestParams.getType());
                addressesMap.put("zip_code", addressRequestParams.getZip_code());


                HashMap<String, Object> addressesMap2 = new HashMap<>();

                addressesMap2.put("address_proof", "" + userAddressModel.getAddress_proof());
                addressesMap2.put("city", userAddressModel.getCity());
                addressesMap2.put("first_name", userAddressModel.getFirst_name());
                addressesMap2.put("last_name", userAddressModel.getLast_name());
                addressesMap2.put("latitude", userAddressModel.getLatitude());
                addressesMap2.put("longitude", userAddressModel.getLongitude());
                addressesMap2.put("phone_number", userAddressModel.getPhone_number());
                addressesMap2.put("state", userAddressModel.getState());
                addressesMap2.put("street_1", userAddressModel.getStreet_1());
                addressesMap2.put("street_2", userAddressModel.getStreet_2());
                addressesMap2.put("type", userAddressModel.getType());
                addressesMap2.put("zip_code", userAddressModel.getZip_code());


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                        .document(getCurrentUid())
                        .collection("response")
                        .document("data_address")
                        .update("addresses", FieldValue.arrayRemove(addressesMap2))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                firebaseFirestore.collection(CUSTOMER_COLLECTION)
                                        .document(getCurrentUid())
                                        .collection("response")
                                        .document("data_address")
                                        .update("addresses", FieldValue.arrayUnion(addressesMap)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                        emitter.onComplete();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                        emitter.onError(e);
                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Error==", e.getMessage());
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    // For dispensary list

    public Single<ArrayList<DispensaryModel>> getDispensaryList(double latitude, double longitude, double distance) {
        return Single.create(emitter -> {

            // ~1 mile of lat and lon in degrees
            double lat = 0.0144927536231884;
            double lon = 0.0181818181818182;
            double lowerLat = latitude - (lat * distance);
            double lowerLon = longitude - (lon * distance);
            double greaterLat = latitude + (lat * distance);
            double greaterLon = longitude + (lon * distance);
            GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
            GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);

            Query query = null;

            query = firebaseFirestore.collection(AppConstant.DISPENSARY_WEB_COLLECTION)
                    .whereGreaterThan("location", lesserGeoPoint)
                    .whereLessThan("location", greaterGeoPoint);

            query.get().addOnSuccessListener(queryDocumentSnapshots -> {

                dispensaryModelArrayList = new ArrayList<>();
                boolean isCategoryFilter = PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_CATEGORY_FILTER);

                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    DispensaryModel dispensaryModel = documentSnapshot.toObject(DispensaryModel.class);
                    double distance1 = new GetDistance().distance(latitude, longitude, dispensaryModel.getLocation().getLatitude(), dispensaryModel.getLocation().getLongitude());
                    dispensaryModel.setDistance((int) distance1);

                    if (isCategoryFilter) {
                        boolean isContained = dispensaryFilterCategoryWise(dispensaryModel);
                        if (isContained) {
                            dispensaryModelArrayList.add(dispensaryModel);
                        }
                    } else {
                        dispensaryModelArrayList.add(dispensaryModel);
                    }
                }

                emitter.onSuccess(dispensaryModelArrayList);

            }).addOnFailureListener(emitter::onError);

        });

    }

    boolean dispensaryFilterCategoryWise(DispensaryModel dispensaryModel) {
        boolean isContained = false;

        ArrayList<String> dispensaryUserIdList = PreferenceManger.getPreferenceManger().getArray(PrefKeys.DISPENSARY_USER_ID_LIST, new TypeToken<List<String>>() {
        }.getType());

        if (dispensaryModel != null && dispensaryUserIdList != null && dispensaryUserIdList.size() > 0) {
            for (int i = 0; i < dispensaryUserIdList.size(); i++) {
                if (dispensaryModel.getUser_id().equals(dispensaryUserIdList.get(i))) {
                    isContained = true;
                    break;
                }
            }
        }
        return isContained;
    }

    // For deal list
    public Single<ArrayList<DealModel>> getDealList() {
        return Single.create(new SingleOnSubscribe<ArrayList<DealModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<ArrayList<DealModel>> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.COUPON_COLLECTION).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<DealModel> dealModelArrayList = new ArrayList<>();

                                if (documentSnapshots != null && documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        DealModel dealModel = documentSnapshots.get(i).toObject(DealModel.class);
                                        dealModelArrayList.add(dealModel);
                                    }

                                }

                                emitter.onSuccess(dealModelArrayList);

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }

    // For deal list by userId
    public Single<ArrayList<DealModel>> getDealListById(String discriptionUserId) {
        return Single.create(new SingleOnSubscribe<ArrayList<DealModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<ArrayList<DealModel>> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.COUPON_COLLECTION).whereEqualTo("dispensary_user_id", discriptionUserId).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<DealModel> dealModelArrayList = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        DealModel dealModel = documentSnapshots.get(i).toObject(DealModel.class);
                                        dealModelArrayList.add(dealModel);
                                    }

                                }

                                emitter.onSuccess(dealModelArrayList);

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }

    // For deal list by dispensaryId
    public Single<ArrayList<DispensaryReviewModel>> getDispensaryReviewList(String dispensaryId) {
        return Single.create(new SingleOnSubscribe<ArrayList<DispensaryReviewModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<ArrayList<DispensaryReviewModel>> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.DISPENSARY_WEB_COLLECTION).document(dispensaryId).collection(AppConstant.REVIEWS_SUB_COLLECTION).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<DispensaryReviewModel> dispensaryReviewList = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        DispensaryReviewModel dispensaryReviewModel = documentSnapshots.get(i).toObject(DispensaryReviewModel.class);
                                        dispensaryReviewList.add(dispensaryReviewModel);
                                    }

                                }
                                emitter.onSuccess(dispensaryReviewList);

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    public Single<ProductReviewResponse> getProductReview(String orderId, String productId) {
        return Single.create(new SingleOnSubscribe<ProductReviewResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<ProductReviewResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION)
                        .document(productId)
                        .collection(AppConstant.REVIEWS_SUB_COLLECTION)
                        .whereEqualTo("order_id", orderId)
                        .whereEqualTo("product_id", productId)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductReviewResponse productReviewResponse = new ProductReviewResponse();
                        if (!queryDocumentSnapshots.isEmpty()) {
                            if (queryDocumentSnapshots.size() > 0) {
                                ProductReviewModel productReviewModel = queryDocumentSnapshots.getDocuments().get(0).toObject(ProductReviewModel.class);
                                productReviewResponse.setProductReviewModel(productReviewModel);
                                productReviewResponse.setStatus(SUCCESS);
                            } else {
                                productReviewResponse.setStatus(ERROR);
                            }
                        } else {
                            productReviewResponse.setStatus(ERROR);
                        }

                        emitter.onSuccess(productReviewResponse);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });

    }

    public Single<String> saveProductReview(ProductReviewModel productReviewModel) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                HashMap<String, Object> reviewMap = new HashMap<>();

                reviewMap.put("comment", productReviewModel.getComment());
                reviewMap.put("created_at", productReviewModel.getCreated_at());
                reviewMap.put("product_id", productReviewModel.getProduct_id());
                reviewMap.put("order_id", productReviewModel.getOrder_id());
                reviewMap.put("rating", productReviewModel.getRating());
                reviewMap.put("updated_at", productReviewModel.getUpdated_at());
                reviewMap.put("user_id", productReviewModel.getUser_id());
                reviewMap.put("user_name", productReviewModel.getUser_name());

                firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION).document(productReviewModel.getProduct_id()).collection(AppConstant.REVIEWS_SUB_COLLECTION)
                        .document().set(reviewMap)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                emitter.onSuccess(SUCCESS);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    public Single<String> saveDispensaryReview(DispensaryReviewModel dispensaryReviewModel, float totalRating) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                HashMap<String, Object> reviewMap = new HashMap<>();


                reviewMap.put("comment", dispensaryReviewModel.getComment());
                reviewMap.put("created_at", dispensaryReviewModel.getCreated_at());
                reviewMap.put("dispensary_id", dispensaryReviewModel.getDispensary_id());
                reviewMap.put("order_id", dispensaryReviewModel.getOrder_id());
                reviewMap.put("rating", dispensaryReviewModel.getRating());
                reviewMap.put("updated_at", dispensaryReviewModel.getUpdated_at());
                reviewMap.put("user_id", dispensaryReviewModel.getUser_id());
                reviewMap.put("user_name", dispensaryReviewModel.getUser_name());


                firebaseFirestore.collection(AppConstant.DISPENSARY_WEB_COLLECTION).document(dispensaryReviewModel.getDispensary_id()).collection(AppConstant.REVIEWS_SUB_COLLECTION)
                        .document().set(reviewMap)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                firebaseFirestore.collection(AppConstant.DISPENSARY_WEB_COLLECTION).document(dispensaryReviewModel.getDispensary_id())
                                        .update("rating", totalRating).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        emitter.onSuccess(SUCCESS);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                        emitter.onError(e);
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    public Single<AddressList> getAddressList() {
        return Single.create(new SingleOnSubscribe<AddressList>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<AddressList> emitter) throws Exception {

                firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response").document("data_address").get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot = task.getResult();
                                    AddressList addressList = documentSnapshot.toObject(AddressList.class);
                                    emitter.onSuccess(addressList);
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }


    public Single<ArrayList<ProductListResponse>> getMenuList(String dispensaryId) {
        Log.d("dispensaryId==", "=========" + dispensaryId);

        return Single.create(new SingleOnSubscribe<ArrayList<ProductListResponse>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<ArrayList<ProductListResponse>> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION)
                        .whereEqualTo("dispensary_id", dispensaryId)
                        .whereEqualTo("isDeleted", false)
                        .whereEqualTo("isPublic", true)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<ProductListResponse> productListResponses = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        ProductListResponse productListResponse = documentSnapshots.get(i).toObject(ProductListResponse.class);

                                        firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION).document(productListResponse.getId()).collection(AppConstant.WEIGHTS_SUB_COLLECTION).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                            @Override
                                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                                if (!queryDocumentSnapshots.isEmpty()) {
                                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                                    weightsList = new ArrayList<>();
                                                    if (queryDocumentSnapshots.size() > 0) {
                                                        for (int j = 0; j < queryDocumentSnapshots.size(); j++) {
                                                            Weights weights = documentSnapshots.get(j).toObject(Weights.class);
                                                            // Log.d("name","==="+weights.getName());
                                                            // Log.d("value","==="+weights.getValue());
                                                            weightsList.add(weights);
                                                        }

                                                        if (weightsList != null && weightsList.size() > 0) {
                                                            productListResponse.setWeights(weightsList);
                                                        }

                                                    }
                                                }


                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                emitter.onError(new Exception());
                                            }
                                        });

                                        // For related products
                                        firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION).document(productListResponse.getId()).collection(AppConstant.RELATED_PRODUCTS_SUB_COLLECTION).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                            @Override
                                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                                if (!queryDocumentSnapshots.isEmpty()) {
                                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                                    relatedProducts = new ArrayList<>();
                                                    if (queryDocumentSnapshots.size() > 0) {
                                                        for (int j = 0; j < queryDocumentSnapshots.size(); j++) {
                                                            RelatedProduct relatedProduct = documentSnapshots.get(j).toObject(RelatedProduct.class);
                                                            relatedProducts.add(relatedProduct);
                                                        }

                                                        if (relatedProducts != null && relatedProducts.size() > 0) {
                                                            productListResponse.setRelatedProducts(relatedProducts);
                                                        }

                                                    }
                                                }

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                emitter.onError(new Exception());
                                            }
                                        });

                                        // For review list

                                        firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION).document(productListResponse.getId()).collection(AppConstant.REVIEWS_SUB_COLLECTION).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                            @Override
                                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                                if (!queryDocumentSnapshots.isEmpty()) {
                                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                                    productReviewList = new ArrayList<>();
                                                    if (queryDocumentSnapshots.size() > 0) {

                                                        for (int j = 0; j < queryDocumentSnapshots.size(); j++) {
                                                            ProductReviewModel productReviewModel = documentSnapshots.get(j).toObject(ProductReviewModel.class);
                                                            productReviewList.add(productReviewModel);
                                                        }

                                                        if (productReviewList != null && productReviewList.size() > 0) {
                                                            productListResponse.setProductReviewList(productReviewList);
                                                        }

                                                    }
                                                }

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                emitter.onError(new Exception());
                                            }
                                        });

                                        productListResponses.add(productListResponse);
                                        Log.d("name==", productListResponse.getName());
                                    }

                                    // emitter.onSuccess(productListResponses);
                                }

                                emitter.onSuccess(productListResponses);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }

    // Add Product to cart

    public Single<String> addProductToCart(ProductListResponse productListResponse) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                HashMap<String, Object> product = new HashMap<>();

                DocumentReference documentReference = firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_orderList").collection("orderList").document();

                product.put("busniess_name", productListResponse.getBusniess_name());
                product.put("description", productListResponse.getDescription());
                product.put("dispensary_id", productListResponse.getDispensaryId());
                product.put("id", documentReference.getId());
                product.put("images", productListResponse.getProduct_gallery());
                product.put("name", productListResponse.getName());
                product.put("product_id", productListResponse.getId());
                product.put("quantity", productListResponse.getQuantity());
                //product.put("rating", productListResponse.getRating());
                product.put("regular_price", productListResponse.getRegular_price());
                product.put("sale_price", productListResponse.getSale_price());
             //   product.put("tax_rate", productListResponse.getTax_rate());
                product.put("total_count", productListResponse.getTotalPrice());
                product.put("weight", productListResponse.getWeightNameValue());


                HashMap<String, Object> map = new HashMap<>();
                map.put("dispensary_id", productListResponse.getDispensaryId());
                map.put("dispensary_user_id", productListResponse.getDispensaryUserId());
                map.put("total_discount", 0);
                map.put("coupon_code_name", "");
                map.put("used_coupon_id", "");


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_orderList").collection("orderList").whereEqualTo("product_id", productListResponse.getId()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.isEmpty()) {
                            // Log.d("Success===","Found");

                            firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                    .document("data_orderList").collection("orderList").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    if (queryDocumentSnapshots.isEmpty()) {
                                        documentReference.set(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                        .document("data_orderList").set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                        if (task.isSuccessful()) {

                                                            firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                                                                    .document(getCurrentUid())
                                                                    .collection("response")
                                                                    .document("data_orderList")
                                                                    .collection("orderList")
                                                                    .get()
                                                                    .addOnCompleteListener(task1 -> {
                                                                        int count = 0;
                                                                        if (task1.isSuccessful()) {
                                                                            if (task1.getResult() != null) {
                                                                                count = task1.getResult().size();
                                                                            }
                                                                        }
                                                                        PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT, "" + count);
                                                                        emitter.onSuccess("Product added successfully.");
                                                                    }).addOnFailureListener(emitter::onError);


                                                        }

                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                        emitter.onError(e);
                                                    }
                                                });


                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                Log.d("Error==", e.getMessage());
                                                emitter.onError(e);
                                            }
                                        });
                                    } else {

                                        firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                .document("data_orderList").collection("orderList").whereEqualTo("dispensary_id", productListResponse.getDispensaryId()).get()
                                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                                        if (!queryDocumentSnapshots.isEmpty()) {
                                                            documentReference.set(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                                    firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                                            .document("data_orderList").set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                                            if (task.isSuccessful()) {

                                                                                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                                                                                        .document(getCurrentUid())
                                                                                        .collection("response")
                                                                                        .document("data_orderList")
                                                                                        .collection("orderList")
                                                                                        .get()
                                                                                        .addOnCompleteListener(task1 -> {
                                                                                            int count = 0;
                                                                                            if (task1.isSuccessful()) {
                                                                                                if (task1.getResult() != null) {
                                                                                                    count = task1.getResult().size();
                                                                                                }
                                                                                            }
                                                                                            PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT, "" + count);
                                                                                            emitter.onSuccess("Product added successfully.");
                                                                                        }).addOnFailureListener(emitter::onError);


                                                                            }

                                                                        }
                                                                    }).addOnFailureListener(new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                                            emitter.onError(e);
                                                                        }
                                                                    });


                                                                }
                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                                    Log.d("Error==", e.getMessage());
                                                                    emitter.onError(e);
                                                                }
                                                            });
                                                        } else {
                                                            emitter.onSuccess("You can not add different dispensary product in cart.");
                                                        }
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                emitter.onError(e);
                                            }
                                        });
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@androidx.annotation.NonNull Exception e) {

                                }
                            });


                        } else {
                            emitter.onSuccess("Product already exist");
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("onFailure===", "" + e.getMessage());
                        emitter.onError(e);
                    }
                });


            }
        });

    }


    public Single<String> getBadgeCount() {
        Log.d("userId", "" + getCurrentUid());
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                        .document(getCurrentUid())
                        .collection("response")
                        .document("data_orderList")
                        .collection("orderList")
                        .get()
                        .addOnCompleteListener(task1 -> {
                            int count = 0;
                            if (task1.isSuccessful()) {
                                if (task1.getResult() != null) {
                                    count = task1.getResult().size();
                                }
                            }
                            emitter.onSuccess("" + count);
                        }).addOnFailureListener(emitter::onError);

            }
        });


    }


    // Add Product to wish list
    public Single<String> addProductToWishList(ProductListResponse productListResponse) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                HashMap<String, Object> product = new HashMap<>();

                DocumentReference documentReference = firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_wishList").collection("wishList").document();

                product.put("busniess_name", productListResponse.getBusniess_name());
                product.put("description", productListResponse.getDescription());
                product.put("dispensary_id", productListResponse.getDispensaryId());
                product.put("id", documentReference.getId());
                product.put("images", productListResponse.getProduct_gallery());
                product.put("name", productListResponse.getName());
                product.put("product_id", productListResponse.getId());
                product.put("quantity", productListResponse.getQuantity());
                product.put("rating", productListResponse.getRating());
                product.put("regular_price", productListResponse.getRegular_price());
                product.put("sale_price", productListResponse.getSale_price());
              //  product.put("tax_rate", productListResponse.getTax_rate());
                product.put("total_count", productListResponse.getTotalPrice());
                product.put("weight", productListResponse.getWeightNameValue());


                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_wishList").collection("wishList").whereEqualTo("product_id", productListResponse.getId()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.isEmpty()) {
                            // Log.d("Success===","Found");

                            firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                    .document("data_wishList").collection("wishList").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    if (queryDocumentSnapshots.isEmpty()) {
                                        documentReference.set(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    emitter.onSuccess("Product added successfully.");
                                                }

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                Log.d("Error==", e.getMessage());
                                                emitter.onError(e);
                                            }
                                        });
                                    } else {

                                        firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                .document("data_wishList").collection("wishList").whereEqualTo("dispensary_id", productListResponse.getDispensaryId()).get()
                                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                                        if (!queryDocumentSnapshots.isEmpty()) {
                                                            documentReference.set(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                                                    if (task.isSuccessful()) {
                                                                        emitter.onSuccess("Product added successfully.");
                                                                    }


                                                                }
                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                                    Log.d("Error==", e.getMessage());
                                                                    emitter.onError(e);
                                                                }
                                                            });
                                                        } else {
                                                            emitter.onSuccess("You can not add different dispensary product in cart.");
                                                        }
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                emitter.onError(e);
                                            }
                                        });
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@androidx.annotation.NonNull Exception e) {

                                }
                            });


                        } else {
                            emitter.onSuccess("Product already exist");
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("onFailure===", "" + e.getMessage());
                        emitter.onError(e);
                    }
                });


            }
        });

    }


    public Single<GetProductCartResponse> getProductCartList() {
        return Single.create(new SingleOnSubscribe<GetProductCartResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<GetProductCartResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_orderList").collection("orderList").get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<ProductCart> productCartArrayList = new ArrayList<>();
                                GetProductCartResponse getProductCartResponse = new GetProductCartResponse();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        ProductCart productCart = documentSnapshots.get(i).toObject(ProductCart.class);
                                        productCartArrayList.add(productCart);
                                    }

                                    getProductCartResponse.setProductCarts(productCartArrayList);

                                }

                                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                        .document("data_orderList").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {

                                        double total_discount = 0;

                                        if (task.isSuccessful()) {
                                            DocumentSnapshot documentSnapshot = task.getResult();
                                            if (documentSnapshot.exists()) {
                                                //  double delivery_fee = documentSnapshot.getDouble("delivery_fee");
                                                String dispensary_id = documentSnapshot.getString("dispensary_id");
                                                String dispensary_user_id = documentSnapshot.getString("dispensary_user_id");
                                                String coupon_code_name = documentSnapshot.getString("coupon_code_name");
                                                String used_coupon_id = documentSnapshot.getString("used_coupon_id");

                                                if (documentSnapshot.contains("total_discount")) {
                                                    total_discount = documentSnapshot.getDouble("total_discount");
                                                }


                                                // getProductCartResponse.setDelivery_fee(delivery_fee);
                                                getProductCartResponse.setDispensary_id(dispensary_id);
                                                getProductCartResponse.setDispensary_user_id(dispensary_user_id);
                                                getProductCartResponse.setTotal_discount(total_discount);
                                                getProductCartResponse.setCoupon_code_name(coupon_code_name);
                                                getProductCartResponse.setUsed_coupon_id(used_coupon_id);
                                            }

                                            emitter.onSuccess(getProductCartResponse);

                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                        emitter.onError(e);
                                    }
                                });


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    public Single<CouponResponse> getCouponProducts(String couponCode) {
        return Single.create(new SingleOnSubscribe<CouponResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<CouponResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.COUPON_COLLECTION)
                        .whereEqualTo("coupon_code_name", couponCode)
                        .whereGreaterThanOrEqualTo("expiration_date", AppUtil.getCurrentTimeInMillisecond())
                        .whereEqualTo("isPublic", true)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        CouponResponse couponResponse = new CouponResponse();
                        if (!queryDocumentSnapshots.isEmpty()) {
                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                            if (documentSnapshots != null && documentSnapshots.size() == 1) {
                                CouponModel couponModel = documentSnapshots.get(0).toObject(CouponModel.class);
                                couponResponse.setCouponModel(couponModel);
                                couponResponse.setStatus(SUCCESS);
                                firebaseFirestore.collection(CUSTOMER_COLLECTION)
                                        .document(getCurrentUid())
                                        .collection("response").document("data_coupon")
                                        .collection("usedCoupon").document(couponModel.getId()).get()
                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    long usedLimit = documentSnapshot.getLong("used_limit");
                                                    couponModel.setUserUsedLimit(usedLimit);
                                                }

                                                couponResponse.setStatus(SUCCESS);
                                                emitter.onSuccess(couponResponse);

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                        emitter.onError(e);
                                    }
                                });
                            } else {
                                couponResponse.setStatus(AppConstant.ERROR);
                                couponResponse.setMessage(AppConstant.COUPON_NOT_VALID);
                                emitter.onSuccess(couponResponse);
                            }

                        } else {
                            couponResponse.setStatus(AppConstant.ERROR);
                            couponResponse.setMessage(AppConstant.COUPON_NOT_VALID);
                            emitter.onSuccess(couponResponse);
                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        emitter.onError(e);
                    }
                });


            }
        });

    }


    public Completable updateCouponCode(CouponModel couponModel) {

        return Completable.create(emitter ->
                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                        .document(getCurrentUid())
                        .collection("response")
                        .document("data_orderList")
                        .update("coupon_code_name", couponModel.getCoupon_code_name()
                                , "total_discount", couponModel.getTotalDiscount(),
                                "used_coupon_id", couponModel.getId())
                        .addOnCompleteListener(task -> emitter.onComplete())
                        .addOnFailureListener(emitter::onError));
    }

    public Completable updateCouponCodeLimit(long limitUsed, String couponId) {

        HashMap<String, Object> usedLimitMap = new HashMap<>();
        usedLimitMap.put("used_limit", limitUsed);

        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_coupon").collection("usedCoupon").document(couponId)
                        .set(usedLimitMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });


    }


    public Completable cancelCouponCode(String couponId) {

        return Completable.create(emitter ->
                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                        .document(getCurrentUid())
                        .collection("response")
                        .document("data_orderList")
                        .update("coupon_code_name", ""
                                , "total_discount", 0,
                                "used_coupon_id", "")
                        .addOnCompleteListener(task -> emitter.onComplete())
                        .addOnFailureListener(emitter::onError));

    }

    public Single<DriverTokens> getDriversTokens(double latitude, double longitude, double distance) {
        return Single.create(new SingleOnSubscribe<DriverTokens>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<DriverTokens> emitter) throws Exception {

                // ~1 mile of lat and lon in degrees
                double lat = 0.0144927536231884;
                double lon = 0.0181818181818182;
                double lowerLat = latitude - (lat * distance);
                double lowerLon = longitude - (lon * distance);
                double greaterLat = latitude + (lat * distance);
                double greaterLon = longitude + (lon * distance);
                GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
                GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);

                firebaseFirestore.collection(AppConstant.DRIVER_LOCATION_STATUS)
                        .whereGreaterThan("location", lesserGeoPoint)
                        .whereLessThan("location", greaterGeoPoint)
                        // .whereEqualTo("driver_status", AppConstant.ONLINE)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                        ArrayList<String> driverTokenList = new ArrayList<>();
                        ArrayList<String> driverIdList = new ArrayList<>();
                        DriverTokens driverTokens = new DriverTokens();

                        if (documentSnapshots.size() > 0) {
                            for (int i = 0; i < documentSnapshots.size(); i++) {
                                String tokens = documentSnapshots.get(i).getString("device_id");
                                String driverId = documentSnapshots.get(i).getString("user_id");
                                String driverStatus = documentSnapshots.get(i).getString("driver_status");

                                if (driverStatus != null && driverStatus.equals("Online")) {
                                    driverTokenList.add(tokens);
                                    driverIdList.add(driverId);
                                }

                            }

                            if (driverTokenList.size() > 0) {
                                driverTokens.setFcmToken(driverTokenList);
                                driverTokens.setDrivers_id(driverIdList);
                                driverTokens.setStatus(SUCCESS);
                            } else {
                                driverTokens.setStatus(AppConstant.ERROR);
                            }

                            emitter.onSuccess(driverTokens);

                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }


    public Single<GetProductCartResponse> getProductWishList() {
        return Single.create(new SingleOnSubscribe<GetProductCartResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<GetProductCartResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_wishList").collection("wishList").get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                ArrayList<ProductCart> productCartArrayList = new ArrayList<>();
                                GetProductCartResponse getProductCartResponse = new GetProductCartResponse();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        ProductCart productCart = documentSnapshots.get(i).toObject(ProductCart.class);
                                        productCartArrayList.add(productCart);
                                    }

                                    getProductCartResponse.setProductCarts(productCartArrayList);

                                }

                                emitter.onSuccess(getProductCartResponse);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    public Single<HelpListResponse> getHelpList() {
        return Single.create(new SingleOnSubscribe<HelpListResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<HelpListResponse> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.HELP_COLLECTION).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                ArrayList<Help> helpList = new ArrayList<>();
                                HelpListResponse helpListResponse = new HelpListResponse();

                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        Help help = documentSnapshots.get(i).toObject(Help.class);
                                        helpList.add(help);
                                    }

                                    helpListResponse.setStatus(AppConstant.SUCCESS);
                                    helpListResponse.setHelpList(helpList);
                                } else {
                                    helpListResponse.setStatus(AppConstant.ERROR);
                                }

                                emitter.onSuccess(helpListResponse);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    public Completable updateCartProduct(String id, int quantity, double totalPrice) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_orderList").collection("orderList").document(id).update("quantity", quantity, "total_count", totalPrice).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });


    }


    public Completable deleteCartProduct2(String id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_orderList").collection("orderList").document(id).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            String badgeCount2 = PreferenceManger.getPreferenceManger().getString(PrefKeys.BADGE_COUNT);
                            int bCount = 0;
                            if (badgeCount2 != null) {
                                bCount = Integer.parseInt(badgeCount2);
                                --bCount;
                            }

                            HashMap<String, Object> badgeCountMap = new HashMap<>();
                            badgeCountMap.put("badge_count", bCount + "");

                            int finalBCount = bCount;
                            firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                    .document("data_badgeCount").set(badgeCountMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT, "" + finalBCount);
                                    emitter.onComplete();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                    emitter.onError(e);
                                }
                            });


                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });


            }
        });


    }


    public Completable deleteCartProduct(String id, boolean isLastItem) {
        return Completable.create(emitter -> firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                .document(getCurrentUid())
                .collection("response")
                .document("data_orderList")
                .collection("orderList")
                .document(id)
                .delete()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                                .document(getCurrentUid())
                                .collection("response")
                                .document("data_orderList")
                                .collection("orderList")
                                .get()
                                .addOnCompleteListener(task1 -> {
                                    int count = 0;
                                    if (task1.isSuccessful()) {
                                        if (task1.getResult() != null) {
                                            count = task1.getResult().size();
                                        }
                                    }
                                    PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT, "" + count);
                                }).addOnFailureListener(emitter::onError);

                        if (isLastItem) {
                            firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION)
                                    .document(getCurrentUid())
                                    .collection("response")
                                    .document("data_orderList")
                                    .delete()
                                    .addOnCompleteListener(task12 -> {
                                        if (task12.isSuccessful()) {
                                            emitter.onComplete();
                                        }
                                    }).addOnFailureListener(emitter::onError);
                        } else {
                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(emitter::onError));

    }


    public Completable deleteWishListProduct(String id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_wishList").collection("wishList").document(id).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });


            }
        });


    }

    // Get dispensary data by id

    public Single<DispensaryModel> getDispensary(String dispensaryId) {
        return Single.create(new SingleOnSubscribe<DispensaryModel>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<DispensaryModel> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.DISPENSARY_WEB_COLLECTION).document(dispensaryId).get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {

                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot = task.getResult();
                                    DispensaryModel dispensaryModel = documentSnapshot.toObject(DispensaryModel.class);
                                    emitter.onSuccess(dispensaryModel);
                                }

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        emitter.onError(e);
                    }
                });
            }
        });
    }


    // For getting category list
    public Single<ProductCategoryResponse> getCategoryList() {
        return Single.create(emitter ->
                firebaseFirestore.collection(AppConstant.PRODUCT_CATEGORIES).get()
                        .addOnSuccessListener(queryDocumentSnapshots -> {

                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                            ArrayList<ProductCategory> productCategoryList = new ArrayList<>();
                            ProductCategoryResponse productCategoryResponse = new ProductCategoryResponse();
                            if (!documentSnapshots.isEmpty()) {
                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {

                                        ProductCategory productCategory = documentSnapshots.get(i).toObject(ProductCategory.class);
                                        productCategoryList.add(productCategory);
                                    }

                                    productCategoryResponse.setProductCategoryList(productCategoryList);
                                    productCategoryResponse.setStatus(AppConstant.SUCCESS);
                                } else {
                                    productCategoryResponse.setStatus(AppConstant.ERROR);
                                }
                            } else {
                                productCategoryResponse.setStatus(AppConstant.ERROR);
                            }

                            emitter.onSuccess(productCategoryResponse);

                        }).addOnFailureListener(emitter::onError));
    }


    // For getting dispensary id list
    public Single<DispensaryIdListResponse> getDispensaryIdList(ArrayList<String> categoryIdList) {
        return Single.create(emitter ->
                firebaseFirestore.collection(AppConstant.PRODUCT_COLLECTION).get()
                        .addOnSuccessListener(queryDocumentSnapshots -> {

                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                            ArrayList<String> dispensaryUserIdList = new ArrayList<>();
                            DispensaryIdListResponse dispensaryIdListResponse = new DispensaryIdListResponse();
                            if (!documentSnapshots.isEmpty()) {

                                for (int i = 0; i < documentSnapshots.size(); i++) {
                                    ProductListResponse productListResponse = documentSnapshots.get(i).toObject(ProductListResponse.class);
                                    String dispensaryUserId = getDispensaryId(productListResponse, categoryIdList);
                                    if (dispensaryUserId != null) {
                                        dispensaryUserIdList.add(dispensaryUserId);
                                    }
                                }

                                if (dispensaryUserIdList.size() > 0) {
                                    LinkedHashSet<String> hashSet = new LinkedHashSet<>(dispensaryUserIdList);

                                    dispensaryUserIdList = new ArrayList<String>(hashSet);
                                    dispensaryIdListResponse.setDispensaryId(dispensaryUserIdList);
                                    dispensaryIdListResponse.setStatus(SUCCESS);
                                } else {
                                    dispensaryIdListResponse.setStatus(AppConstant.ERROR);
                                }

                            } else {

                                dispensaryIdListResponse.setStatus(AppConstant.ERROR);
                            }

                            emitter.onSuccess(dispensaryIdListResponse);

                        }).addOnFailureListener(emitter::onError));
    }

    String getDispensaryId(ProductListResponse productListResponse, ArrayList<String> categoryIdList) {
        String dispensaryUserId = null;
        if (productListResponse != null) {
            ArrayList<Category> categoryList = productListResponse.getCategories();
            if (categoryList != null && categoryList.size() > 0) {
                for (int i = 0; i < categoryIdList.size(); i++) {
                    for (int j = 0; j < categoryList.size(); j++) {
                        if (categoryList.get(j).getCategory_id().equals(categoryIdList.get(i))) {
                            dispensaryUserId = productListResponse.getDispensary_id();
                            break;
                        }
                    }
                }
            }
        }
        return dispensaryUserId;
    }

    // For getting category list
    public Single<DispensaryDistanceResponse> getDistanceList() {
        return Single.create(emitter ->
                firebaseFirestore.collection(AppConstant.DISTANCE_COLLECTION).get()
                        .addOnSuccessListener(queryDocumentSnapshots -> {

                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                            ArrayList<Distance> distanceList = new ArrayList<>();
                            DispensaryDistanceResponse dispensaryDistanceResponse = new DispensaryDistanceResponse();
                            if (!documentSnapshots.isEmpty()) {
                                if (documentSnapshots.size() > 0) {
                                    for (int i = 0; i < documentSnapshots.size(); i++) {

                                        Distance distance = documentSnapshots.get(i).toObject(Distance.class);
                                        distanceList.add(distance);
                                    }

                                    dispensaryDistanceResponse.setDistanceList(distanceList);
                                    dispensaryDistanceResponse.setStatus(AppConstant.SUCCESS);
                                } else {
                                    dispensaryDistanceResponse.setStatus(AppConstant.ERROR);
                                }
                            } else {
                                dispensaryDistanceResponse.setStatus(AppConstant.ERROR);
                            }

                            emitter.onSuccess(dispensaryDistanceResponse);

                        }).addOnFailureListener(emitter::onError));
    }


    public Completable updateOrderIsSendNotification(boolean isSend, String orderId) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection("Order").document(orderId)
                        .update("isSendNotification", isSend).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });

    }

    public Completable updatePaymentStatus(boolean paymentSuccess, PaymentMethod paymentMethod, String orderId) {


        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseFirestore.collection("Order").document(orderId)
                        .update("payment_success", paymentSuccess, "payment_method", paymentMethod).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            emitter.onComplete();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });


    }


    public Single<OrderResponse> placeOrder(OrderRequest orderRequest) {
        return Single.create(new SingleOnSubscribe<OrderResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<OrderResponse> emitter) throws Exception {

                HashMap<String, Object> orderMap = new HashMap<>();
                String orderId = "";
                int orderNumber = 0;

                orderMap.put("addresses", orderRequest.getAddresses());
                orderMap.put("assign_driver_id", orderRequest.getAssign_driver_id());
                orderMap.put("busniess_name", orderRequest.getBusniess_name());
                orderMap.put("confirmation_code", orderRequest.getConfirmation_code());
                orderMap.put("created_at", orderRequest.getCreated_at());
                orderMap.put("customer_email", orderRequest.getCustomer_email());
                orderMap.put("customer_name", orderRequest.getCustomer_name());
                orderMap.put("delivery_fee", orderRequest.getDelivery_fee());
                orderMap.put("discount_amount", orderRequest.getDiscount_amount());
                orderMap.put("dispensary_data", orderRequest.getDispensary_data());
                orderMap.put("dispensary_location", orderRequest.getDispensary_data().getLocation());
                orderMap.put("grand_total", orderRequest.getGrand_total());
                orderMap.put("isSendNotification", orderRequest.isSendNotification());
                orderMap.put("items", orderRequest.getItems());
                orderMap.put("patient_id", orderRequest.getPatient_id());
                orderMap.put("payment_method", orderRequest.getPayment_method());
                orderMap.put("sales_tax", orderRequest.getSales_tax());
                orderMap.put("status", orderRequest.getStatus());
                orderMap.put("sub_total", orderRequest.getSub_total());
                orderMap.put("updated_at", orderRequest.getUpdated_at());
                orderMap.put("user_id", orderRequest.getUser_id());
                orderMap.put("delivery_fee_miles", orderRequest.getDelivery_fee_miles());
                orderMap.put("delivery_fee_minute", orderRequest.getDelivery_fee_minute());
                orderMap.put("delivery_miles", orderRequest.getDelivery_miles());
                orderMap.put("delivery_minute", orderRequest.getDelivery_minute());
                orderMap.put("notification_drivers_id", orderRequest.getDriverIdList());
                orderMap.put("payment_success", orderRequest.isPayment_success());


                //TODO: For getting order number

                firebaseFirestore.collection("Order_No").document("response")
                        .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        if (documentSnapshot.exists()) {
                            long orderNo = documentSnapshot.getLong("order_no");
                            ++orderNo;

                            //TODO: For update order number
                            //  Order_No
                            long finalOrderNo = orderNo;
                            firebaseFirestore.collection("Order_No").document("response")
                                    .update("order_no", orderNo).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        //Order
                                        //TODO: For place order
                                        DocumentReference documentReference = firebaseFirestore.collection("Order").document();

                                        orderMap.put("id", documentReference.getId());
                                        orderMap.put("order_no", "" + finalOrderNo);


                                        documentReference.set(orderMap)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                                    .document("data_orderList").collection("orderList").get()
                                                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                        @Override
                                                                        public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                                                                            if (task.isSuccessful()) {
                                                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                    firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).
                                                                                            collection("response").document("data_orderList")
                                                                                            .collection("orderList").document(document.getId()).delete();
                                                                                }

                                                                                firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                                                        .document("data_orderList").delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                    @Override
                                                                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            firebaseFirestore.collection(CUSTOMER_COLLECTION).document(getCurrentUid()).collection("response")
                                                                                                    .document("data_badgeCount").delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                @Override
                                                                                                public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                                                                    OrderResponse orderResponse = new OrderResponse();

                                                                                                    if (task.isSuccessful()) {
                                                                                                        orderResponse.setStatus(SUCCESS);
                                                                                                        orderResponse.setMessage("Order is placed successfully");
                                                                                                        orderResponse.setOrderId(orderMap.get("id").toString());
                                                                                                        orderResponse.setOrderNo(orderMap.get("order_no").toString());
                                                                                                        orderResponse.setConfirmationCode(orderMap.get("confirmation_code").toString());
                                                                                                    } else {

                                                                                                        orderResponse.setStatus(ERROR);
                                                                                                    }

                                                                                                    emitter.onSuccess(orderResponse);
                                                                                                }
                                                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                                                @Override
                                                                                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                                                                    emitter.onError(e);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                }).addOnFailureListener(new OnFailureListener() {
                                                                                    @Override
                                                                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                                                        emitter.onError(e);
                                                                                    }
                                                                                });


                                                                            }

                                                                        }
                                                                    }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@androidx.annotation.NonNull Exception e) {

                                                                }
                                                            });
                                                        }

                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                emitter.onError(e);
                                            }
                                        });
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                    emitter.onError(e);
                                }
                            });


                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }


    public Single<GetPlacedOrdersResponse> getPlacedOrders(String status) {
        return Single.create(new SingleOnSubscribe<GetPlacedOrdersResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<GetPlacedOrdersResponse> emitter) throws Exception {
                firebaseFirestore.collection("Order")
                        .whereEqualTo("user_id", getCurrentUid())
                        .whereEqualTo("payment_success", true)
                        .whereIn("status", Arrays.asList(AppConstant.ORDER_TYPE_ACCEPT, AppConstant.ORDER_TYPE_SCHEDULED))
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                GetPlacedOrdersResponse getPlacedOrdersResponse = new GetPlacedOrdersResponse();
                                ArrayList<Order> orderList = new ArrayList<>();
                                if (!queryDocumentSnapshots.isEmpty()) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    if (documentSnapshots.size() > 0) {
                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            Order order = documentSnapshots.get(i).toObject(Order.class);
                                            orderList.add(order);
                                        }

                                        getPlacedOrdersResponse.setOrders(orderList);
                                        getPlacedOrdersResponse.setMessage(AppConstant.SUCCESS);
                                    } else {
                                        getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                    }
                                } else {
                                    getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                }

                                emitter.onSuccess(getPlacedOrdersResponse);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }

    public Single<Order> getOrderDetailsByOrderId(String orderId) {

        return Single.create(emitter -> firebaseFirestore.collection("Order")
                .document(orderId).get()
                .addOnSuccessListener(documentSnapshot -> {

                    Order order = null;

                    if (documentSnapshot.exists()) {
                        order = documentSnapshot.toObject(Order.class);
                    }
                    emitter.onSuccess(order);
                }).addOnFailureListener(e -> emitter.onError(e)));

    }


    public Single<GetPlacedOrdersResponse> getTrackOrders() {
        return Single.create(new SingleOnSubscribe<GetPlacedOrdersResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<GetPlacedOrdersResponse> emitter) throws Exception {
                firebaseFirestore.collection("Order")

                        .whereEqualTo("user_id", getCurrentUid())
                        .whereEqualTo("status", AppConstant.ORDER_TYPE_PICKED)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                GetPlacedOrdersResponse getPlacedOrdersResponse = new GetPlacedOrdersResponse();
                                ArrayList<Order> orderList = new ArrayList<>();
                                if (!queryDocumentSnapshots.isEmpty()) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    if (documentSnapshots.size() > 0) {
                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            Order order = documentSnapshots.get(i).toObject(Order.class);
                                            orderList.add(order);
                                        }

                                        getPlacedOrdersResponse.setOrders(orderList);
                                        getPlacedOrdersResponse.setMessage(AppConstant.SUCCESS);
                                    } else {
                                        getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                    }
                                } else {
                                    getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                }

                                emitter.onSuccess(getPlacedOrdersResponse);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }

    public Single<DriverProfileResponse> getDriverProfileData(String driverId) {
        return Single.create(new SingleOnSubscribe<DriverProfileResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<DriverProfileResponse> emitter) throws Exception {

                firebaseFirestore.collection("Driver").document(driverId)
                        .collection("response")
                        .document("data_account").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        DriverProfileResponse driverProfileResponse = new DriverProfileResponse();

                        if (documentSnapshot.exists()) {
                            DriverProfile driverProfile = documentSnapshot.toObject((DriverProfile.class));
                            driverProfileResponse.setDriverProfile(driverProfile);
                            driverProfileResponse.setStatus(SUCCESS);
                        } else {
                            driverProfileResponse.setStatus(ERROR);
                        }

                        emitter.onSuccess(driverProfileResponse);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }


    public Single<DriverRatingResponse> getDriverRatingData(String driverId, String orderId) {
        return Single.create(emitter -> firebaseFirestore.collection(DRIVER_COLLECTION).document(driverId)
                .collection(AppConstant.REVIEWS_SUB_COLLECTION)
                .document(orderId).get().addOnSuccessListener(documentSnapshot -> {

                    DriverRatingResponse driverRatingResponse = new DriverRatingResponse();

                    if (documentSnapshot.exists()) {
                        DriverRating driverRating = documentSnapshot.toObject((DriverRating.class));
                        driverRatingResponse.setDriverRating(driverRating);
                        driverRatingResponse.setStatus(SUCCESS);
                    } else {
                        driverRatingResponse.setStatus(ERROR);
                    }

                    emitter.onSuccess(driverRatingResponse);

                }).addOnFailureListener(emitter::onError));
    }


    public Single<String> saveDriverRating(DriverRating driverRating, String driverId) {
        return Single.create(emitter -> {

            HashMap<String, Object> reviewMap = new HashMap<>();

            reviewMap.put("created_at", driverRating.getCreated_at());
            reviewMap.put("updated_at", driverRating.getUpdated_at());
            reviewMap.put("order_id", driverRating.getOrder_id());
            reviewMap.put("rating", driverRating.getRating());
            reviewMap.put("user_id", driverRating.getUser_id());
            reviewMap.put("user_name", driverRating.getUser_name());

            firebaseFirestore.collection(AppConstant.DRIVER_COLLECTION).document(driverId).collection(AppConstant.REVIEWS_SUB_COLLECTION)
                    .document(driverRating.getOrder_id()).set(reviewMap)
                    .addOnSuccessListener(aVoid -> emitter.onSuccess(SUCCESS)).addOnFailureListener(emitter::onError);
        });
    }


    public Single<DriverLocation> getDriverLocation2(String driverId, String orderId) {
        return Single.create(new SingleOnSubscribe<DriverLocation>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<DriverLocation> emitter) throws Exception {
                firebaseFirestore.collection("user_location").document(driverId)
                        .collection(orderId).document(orderId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                        if (error != null) {
                            Log.w(TAG, "Listen failed.", error);
                            emitter.onError(error);
                            return;
                        }

                        DriverLocation driverLocation = new DriverLocation();
                        if (value != null && value.exists()) {

                            GeoPoint location = value.getGeoPoint("location");
                            driverLocation.setLocation(location);
                            driverLocation.setStatus(AppConstant.SUCCESS);
                            Log.d(TAG, "Current data: " + value.getData());
                        } else {
                            driverLocation.setStatus(AppConstant.ERROR);
                            Log.d(TAG, "Current data: null");
                        }

                        emitter.onSuccess(driverLocation);
                    }
                });
            }
        });
    }

    public Flowable<DriverLocation> getDriverLocation(String driverId, String orderId) {
        return Flowable.create(new FlowableOnSubscribe<DriverLocation>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<DriverLocation> emitter) throws Exception {

                firebaseFirestore.collection("user_location").document(driverId)
                        .collection(orderId).document(orderId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {

                        if (error != null) {
                            Log.w(TAG, "Listen failed.", error);
                            emitter.onError(error);
                            return;
                        }

                        DriverLocation driverLocation = new DriverLocation();
                        if (value != null && value.exists()) {

                            GeoPoint location = value.getGeoPoint("location");
                            driverLocation.setLocation(location);
                            driverLocation.setStatus(AppConstant.SUCCESS);
                            Log.d(TAG, "Current data: " + value.getData());
                        } else {
                            driverLocation.setStatus(AppConstant.ERROR);
                            Log.d(TAG, "Current data: null");
                        }

                        emitter.onNext(driverLocation);
                    }
                });
            }
        }, BackpressureStrategy.BUFFER);
    }


    public Single<GetPlacedOrdersResponse> getPastOrders() {
        return Single.create(new SingleOnSubscribe<GetPlacedOrdersResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<GetPlacedOrdersResponse> emitter) throws Exception {
                firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                        .whereEqualTo("user_id", getCurrentUid())
                        .whereIn("status", Arrays.asList(AppConstant.ORDER_TYPE_COMPLETED, AppConstant.ORDER_TYPE_CANCELED, AppConstant.DELIVERY_FAILED))
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                GetPlacedOrdersResponse getPlacedOrdersResponse = new GetPlacedOrdersResponse();
                                ArrayList<Order> orderList = new ArrayList<>();
                                if (!queryDocumentSnapshots.isEmpty()) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    if (documentSnapshots.size() > 0) {
                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            Order order = documentSnapshots.get(i).toObject(Order.class);
                                            orderList.add(order);
                                        }

                                        getPlacedOrdersResponse.setOrders(orderList);
                                        getPlacedOrdersResponse.setMessage(AppConstant.SUCCESS);
                                    } else {
                                        getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                    }
                                } else {
                                    getPlacedOrdersResponse.setMessage(AppConstant.ERROR);
                                }

                                emitter.onSuccess(getPlacedOrdersResponse);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }

    public Single<String> updateOrderStatus(String id, String status) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {
                firebaseFirestore.collection(AppConstant.ORDER_COLLECTION).document(id)
                        .update("status", status).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            emitter.onSuccess(AppConstant.SUCCESS);
                        } else {
                            emitter.onSuccess(AppConstant.ERROR);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });
    }


    // Create new User
    public Completable registerDriverUser(String email, String password) {

        String fcmToken = PreferenceManger.getFcmToken(context);

        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                firebaseAuth.createUserWithEmailAndPassword(email, password).
                        addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    //create new user
                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("activated_at", true);
                                    map.put("device_id", "" + fcmToken);
                                    map.put("device_type", AppConstant.DEVICE_TYPE);
                                    map.put("device_mac_id", AppConstant.getDeviceId(context));
                                    map.put("email", email);
                                    map.put("password", password);
                                    map.put("role", AppConstant.DRIVER_ROLE);
                                    map.put("user_id", getCurrentUid());
                                    map.put("created_at",AppUtil.getCurrentTimeInMillisecond());

                                    firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).set(map)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        emitter.onComplete();
                                                    }
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                    emitter.onError(e);
                                                }
                                            });
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    // Upload customer document photo
    public Single<String> uploadDriverDocPhoto(byte[] bytes, String imageName) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                StorageReference storageReference = firebaseStorage.getReference().
                        child("driver_images/" + getCurrentUid() + "/" + imageName);

                UploadTask uploadTask = storageReference.putBytes(bytes);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        emitter.onError(exception);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getStorage().getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        emitter.onSuccess(uri.toString());
                                        Log.d("url===", uri.toString());
                                    }
                                });
                        taskSnapshot.getStorage().getDownloadUrl().addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@androidx.annotation.NonNull Exception e) {
                                emitter.onError(e);
                            }
                        });
                    }
                });
            }
        });
    }

    // Add customer data
    public Completable addDriverData(DriverRegistrationRequest driverRegistrationRequest) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {


                HashMap<String, Object> accountMap = new HashMap<>();
                accountMap.put("date_of_birth", driverRegistrationRequest.getDateOfBirth());
                // accountMap.put("email", driverRegistrationRequest.getEmail());
                accountMap.put("email_communication", driverRegistrationRequest.getEmailCommunication());
                accountMap.put("first_name", driverRegistrationRequest.getFirstName());
                accountMap.put("is_caregiver", driverRegistrationRequest.getIsCareGiver());
                accountMap.put("last_name", driverRegistrationRequest.getLastName());
                accountMap.put("message_notification", driverRegistrationRequest.getMessageNotification());


                HashMap<String, Object> addressMap = new HashMap<>();
                addressMap.put("city", driverRegistrationRequest.getCity());
                addressMap.put("phone_number", driverRegistrationRequest.getPhoneNumber());
                addressMap.put("state", driverRegistrationRequest.getState());
                addressMap.put("street_1", driverRegistrationRequest.getStreetAddress1());
                addressMap.put("street_2", driverRegistrationRequest.getStreetAddress2());
                addressMap.put("zip_code", driverRegistrationRequest.getZipCode());

                HashMap<String, Object> documentImageMap = new HashMap<>();
                documentImageMap.put("car_insurance_policy", driverRegistrationRequest.getCarInsurancePolicyUrl());
                documentImageMap.put("car_registration", driverRegistrationRequest.getCarRegistrationUrl());
                documentImageMap.put("caregiver_certificate", driverRegistrationRequest.getCareGiverCertificateURl());
                documentImageMap.put("driving_license", driverRegistrationRequest.getDrivingLicenseUrl());
                documentImageMap.put("photo", driverRegistrationRequest.getPhotoURl());


                HashMap<String, Object> documentInfoMap = new HashMap<>();
                documentInfoMap.put("document_type", driverRegistrationRequest.getDocumentType());
                documentInfoMap.put("expiration_date", driverRegistrationRequest.getExpirationDate());
                documentInfoMap.put("identity_number", driverRegistrationRequest.getIdentityNumber());
                documentInfoMap.put("issuing_state", driverRegistrationRequest.getIssuingState());
                documentInfoMap.put("licence_issuing_state", driverRegistrationRequest.getLicenceIssuingState());
                documentInfoMap.put("licence_plate_number", driverRegistrationRequest.getLicencePlateNumber());
                documentInfoMap.put("manufacture_company", driverRegistrationRequest.getManufactureCompany());
                documentInfoMap.put("manufacture_model", driverRegistrationRequest.getManufactureModel());
                documentInfoMap.put("manufacture_year", driverRegistrationRequest.getManufactureYear());
                documentInfoMap.put("regular_maintenance", driverRegistrationRequest.getRegularMaintenance());


                HashMap<String, Object> phonesMap = new HashMap<>();
                phonesMap.put("phone_manufacture_company", driverRegistrationRequest.getPhoneManufactureCompany());
                phonesMap.put("phone_manufacture_model", driverRegistrationRequest.getPhoneManufactureModel());
                phonesMap.put("smart_phone", driverRegistrationRequest.getHavingSmartPhone());


                HashMap<String, Object> refrences1Map = new HashMap<>();
                refrences1Map.put("email", driverRegistrationRequest.getDriverRefrencesList().get(0).getEmail());
                refrences1Map.put("first_name", driverRegistrationRequest.getDriverRefrencesList().get(0).getFirstName());
                refrences1Map.put("last_name", driverRegistrationRequest.getDriverRefrencesList().get(0).getLastName());
                refrences1Map.put("phone_number", driverRegistrationRequest.getDriverRefrencesList().get(0).getPhoneNumber());

                HashMap<String, Object> refrences2Map = new HashMap<>();
                refrences2Map.put("email", driverRegistrationRequest.getDriverRefrencesList().get(1).getEmail());
                refrences2Map.put("first_name", driverRegistrationRequest.getDriverRefrencesList().get(1).getFirstName());
                refrences2Map.put("last_name", driverRegistrationRequest.getDriverRefrencesList().get(1).getLastName());
                refrences2Map.put("phone_number", driverRegistrationRequest.getDriverRefrencesList().get(1).getPhoneNumber());

                HashMap<String, Object> refrences3Map = new HashMap<>();
                refrences3Map.put("email", driverRegistrationRequest.getDriverRefrencesList().get(2).getEmail());
                refrences3Map.put("first_name", driverRegistrationRequest.getDriverRefrencesList().get(2).getFirstName());
                refrences3Map.put("last_name", driverRegistrationRequest.getDriverRefrencesList().get(2).getLastName());
                refrences3Map.put("phone_number", driverRegistrationRequest.getDriverRefrencesList().get(2).getPhoneNumber());


                HashMap<String, Object> rootMap = new HashMap<>();
                rootMap.put("about_us", "");
                rootMap.put("account", accountMap);
                rootMap.put("address", addressMap);
                rootMap.put("created_at", AppUtil.getCurrentTimeInMillisecond());
                rootMap.put("document_images", documentImageMap);
                rootMap.put("documents", documentInfoMap);
                rootMap.put("phones", phonesMap);
                rootMap.put("profile_image", driverRegistrationRequest.getPhotoURl());
                rootMap.put("updated_at", AppUtil.getCurrentTimeInMillisecond());
                rootMap.put("user_id", getCurrentUid());


                firebaseFirestore.collection(AppConstant.DRIVER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").set(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                        List<HashMap<String, Object>> dataRefrences = new ArrayList<>();
                        dataRefrences.add(refrences1Map);
                        dataRefrences.add(refrences2Map);
                        dataRefrences.add(refrences3Map);

                        HashMap<String, Object> refrences = new HashMap<>();
                        refrences.put("refrences", dataRefrences);

                        firebaseFirestore.collection(AppConstant.DRIVER_COLLECTION).document(getCurrentUid()).collection("response")
                                .document("references").set(refrences).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                                firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).update("activated_at", true).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        String fcmToken = PreferenceManger.getFcmToken(context);
                                        //create new user
                                        HashMap<String, Object> map = new HashMap<>();
                                        map.put("activated_at", true);
                                        map.put("device_id", fcmToken + "");
                                        map.put("device_mac_id", AppConstant.getDeviceId(context));
                                        map.put("device_type", AppConstant.DEVICE_TYPE);
                                        map.put("email", driverRegistrationRequest.getEmail());
                                        map.put("email_communication", driverRegistrationRequest.getEmailCommunication());
                                        map.put("name", driverRegistrationRequest.getFirstName());
                                        map.put("password", driverRegistrationRequest.getPassword());
                                        map.put("role", AppConstant.DRIVER_ROLE);
                                        map.put("user_id", getCurrentUid());
                                        map.put("street_1", driverRegistrationRequest.getStreetAddress1());

                                        firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).set(map)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            emitter.onComplete();
                                                        }
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                        emitter.onError(e);
                                                    }
                                                });


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                                        emitter.onError(e);
                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@androidx.annotation.NonNull Exception e) {

                                emitter.onError(e);
                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }


    // For pending orders list
    public Single<List<OrdersModel>> getPendingOrdersList(double latitude, double longitude, double distance, boolean isFirst) {


        return Single.create(new SingleOnSubscribe<List<OrdersModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<OrdersModel>> emitter) throws Exception {

                // ~1 mile of lat and lon in degrees
                double lat = 0.0144927536231884;

                double lon = 0.0181818181818182;

                double lowerLat = latitude - (lat * distance);

                double lowerLon = longitude - (lon * distance);


                double greaterLat = latitude + (lat * distance);

                double greaterLon = longitude + (lon * distance);

                GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
                GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);
                //dispensaryWeb.

                if (isFirst) {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("status", AppConstant.ORDER_TYPE_SCHEDULED)
                            .whereArrayContains("notification_drivers_id", getCurrentUid())
                            // .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    ordersModelList = new ArrayList<>();

                                    if (documentSnapshots.size() > 0) {
                                        // Get the last visible document
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);

//                                            if (ordersModel.getOpening_hours() != null)
//                                                Log.d("day", ordersModel.getOpening_hours().get(0).getDay());
//                                            //   Log.d("image" ,dispensaryModel.getProfile_image());

                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    } else {
                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                } else {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("status", AppConstant.ORDER_TYPE_SCHEDULED)
                            .whereArrayContains("notification_drivers_id", getCurrentUid())
                            .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();


                                    if (documentSnapshots.size() > 0) {
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                }


            }
        });

    }


    // For pending orders list
    public Single<List<OrdersModel>> getUpcomingDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {


        return Single.create(new SingleOnSubscribe<List<OrdersModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<OrdersModel>> emitter) throws Exception {

                // ~1 mile of lat and lon in degrees
                double lat = 0.0144927536231884;

                double lon = 0.0181818181818182;

                double lowerLat = latitude - (lat * distance);

                double lowerLon = longitude - (lon * distance);


                double greaterLat = latitude + (lat * distance);

                double greaterLon = longitude + (lon * distance);

                GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
                GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);
                //dispensaryWeb.

                if (isFirst) {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("assign_driver_id", getCurrentUid())
                            .whereIn("status", Arrays.asList(AppConstant.ORDER_TYPE_ACCEPT, AppConstant.ORDER_TYPE_PICKED))
                            // .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    ordersModelList = new ArrayList<>();

                                    if (documentSnapshots.size() > 0) {
                                        // Get the last visible document
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);

//                                            if (ordersModel.getOpening_hours() != null)
//                                                Log.d("day", ordersModel.getOpening_hours().get(0).getDay());
//                                            //   Log.d("image" ,dispensaryModel.getProfile_image());

                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    } else {
                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                } else {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("assign_driver_id", getCurrentUid())
                            .whereIn("status", Arrays.asList(AppConstant.ORDER_TYPE_ACCEPT, AppConstant.ORDER_TYPE_PICKED))
                            .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();


                                    if (documentSnapshots.size() > 0) {
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {
                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                }


            }
        });

    }

    // update order to accept
    public Completable updateOrderToAccept(String id, String status) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                DocumentReference documentReference = firebaseFirestore.collection(AppConstant.ORDER_COLLECTION).document(id);
                documentReference.update("status", status);
                documentReference.update("updated_at", AppUtil.getCurrentTimeInMillisecond());
                documentReference.update("assign_driver_id", getCurrentUid())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                emitter.onComplete();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });


    }

    // update order to accept
    public Completable updateOrderToScheduled(String id, String status) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                DocumentReference documentReference = firebaseFirestore.collection(AppConstant.ORDER_COLLECTION).document(id);
                documentReference.update("status", status);
                documentReference.update("updated_at", AppUtil.getCurrentTimeInMillisecond());
                documentReference.update("assign_driver_id", "")
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                emitter.onComplete();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });


    }

    // update order to decline
    public Completable updateOrderToDecline(String id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                HashMap<String, Object> rootMap = new HashMap<>();
                rootMap.put("order_no", id);

                firebaseFirestore.collection(AppConstant.DRIVER_DECLINE_ORDER).document(getCurrentUid()).
                        collection("response").document().
                        set(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        emitter.onComplete();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    // get decline order list
    public Single<List<DeclineOrders>> getDeclineOrders() {
        return Single.create(new SingleOnSubscribe<List<DeclineOrders>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<DeclineOrders>> emitter) throws Exception {
                firebaseFirestore.collection(DRIVER_DECLINE_ORDER).
                        document(getCurrentUid()).
                        collection("response").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DeclineOrders> declineOrdersList = new ArrayList<>();
                        if (!queryDocumentSnapshots.isEmpty()) {
                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                            if (documentSnapshots.size() > 0) {
                                for (int j = 0; j < documentSnapshots.size(); j++) {
                                    DeclineOrders declineOrders = documentSnapshots.get(j).toObject(DeclineOrders.class);
                                    Log.d("value", "===" + declineOrders.getOrder_no());
                                    declineOrdersList.add(declineOrders);
                                }
                                emitter.onSuccess(declineOrdersList);
                            }
                        } else {
                            emitter.onSuccess(declineOrdersList);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });

    }


    // For pending orders list
    public Single<List<OrdersModel>> getPastDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {


        return Single.create(new SingleOnSubscribe<List<OrdersModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<OrdersModel>> emitter) throws Exception {

                // ~1 mile of lat and lon in degrees
                double lat = 0.0144927536231884;

                double lon = 0.0181818181818182;

                double lowerLat = latitude - (lat * distance);

                double lowerLon = longitude - (lon * distance);


                double greaterLat = latitude + (lat * distance);

                double greaterLon = longitude + (lon * distance);

                GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
                GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);
                //dispensaryWeb.

                if (isFirst) {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("assign_driver_id", getCurrentUid())
                            .whereIn("status", Arrays.asList(AppConstant.ORDER_TYPE_COMPLETED, AppConstant.ORDER_TYPE_CANCELED, AppConstant.DELIVERY_FAILED, AppConstant.RETURN_PENDING, AppConstant.ORDER_TYPE_RETURN_COMPLETE))
                            // .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    ordersModelList = new ArrayList<>();

                                    if (documentSnapshots.size() > 0) {
                                        // Get the last visible document
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);

//                                            if (ordersModel.getOpening_hours() != null)
//                                                Log.d("day", ordersModel.getOpening_hours().get(0).getDay());
//                                            //   Log.d("image" ,dispensaryModel.getProfile_image());

                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    } else {
                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                } else {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("assign_driver_id", getCurrentUid())
                            .whereEqualTo("status", AppConstant.ORDER_TYPE_COMPLETED)
                            .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                                    if (documentSnapshots.size() > 0) {
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                }


            }
        });

    }

    // For pending orders list
    public Single<OrdersModel> getOrderDetails(String id) {
        return Single.create(new SingleOnSubscribe<OrdersModel>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<OrdersModel> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                        .whereEqualTo("id", id)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                ordersModelList = new ArrayList<>();
                                OrdersModel ordersModel = new OrdersModel();

                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document
                                    lastVisible = queryDocumentSnapshots.getDocuments()
                                            .get(documentSnapshots.size() - 1);

                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        // OrdersModel ordersModell =
                                        ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                        // ordersModelList.add(ordersModel);
                                    }

                                    emitter.onSuccess(ordersModel);
                                } else {
                                    emitter.onSuccess(ordersModel);
                                }


                                Log.d("success==", "success");
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }


        });
    }

    public Single<MyDriverAccountResponse> getDriverMyAccountInfo() {
        return Single.create(new SingleOnSubscribe<MyDriverAccountResponse>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<MyDriverAccountResponse> emitter) throws Exception {

                firebaseFirestore.collection(DRIVER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {

                        MyDriverAccountResponse myDriverAccountResponse = new MyDriverAccountResponse();

                        if (task.isSuccessful()) {
                            myDriverAccountResponse = task.getResult().toObject(MyDriverAccountResponse.class);
                            myDriverAccountResponse.setStatus(AppConstant.SUCCESS);
                        } else {
                            myDriverAccountResponse.setStatus(AppConstant.ERROR);
                        }
                        emitter.onSuccess(myDriverAccountResponse);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    // Add customer data
    public Completable updateDrverMyAccountInfo(MyDriverAccountResponse myDriverAccountResponse) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {


                HashMap<String, Object> accountMap = new HashMap<>();
                accountMap.put("date_of_birth", myDriverAccountResponse.getAccount().getDate_of_birth());
                //accountMap.put("email", myDriverAccountResponse.getAccount().getEmail());
                accountMap.put("email_communication", myDriverAccountResponse.getAccount().getEmail_communication());
                accountMap.put("first_name", myDriverAccountResponse.getAccount().getFirst_name());
                accountMap.put("is_caregiver", myDriverAccountResponse.getAccount().getIs_caregiver());
                accountMap.put("last_name", myDriverAccountResponse.getAccount().getLast_name());
                accountMap.put("message_notification", myDriverAccountResponse.getAccount().getMessage_notification());


                HashMap<String, Object> addressMap = new HashMap<>();
                addressMap.put("city", myDriverAccountResponse.getAddress().getCity());
                addressMap.put("phone_number", myDriverAccountResponse.getAddress().getPhone_number());
                addressMap.put("state", myDriverAccountResponse.getAddress().getState());
                addressMap.put("street_1", myDriverAccountResponse.getAddress().getStreet_1());
                addressMap.put("street_2", myDriverAccountResponse.getAddress().getStreet_2());
                addressMap.put("zip_code", myDriverAccountResponse.getAddress().getZip_code());


                HashMap<String, Object> documentImageMap = new HashMap<>();
                documentImageMap.put("car_insurance_policy", myDriverAccountResponse.getDocument_images().getCar_insurance_policy());
                documentImageMap.put("car_registration", myDriverAccountResponse.getDocument_images().getCar_registration());
                documentImageMap.put("caregiver_certificate", myDriverAccountResponse.getDocument_images().getCaregiver_certificate());
                documentImageMap.put("driving_license", myDriverAccountResponse.getDocument_images().getDriving_license());
                documentImageMap.put("photo", myDriverAccountResponse.getDocument_images().getPhoto());

                HashMap<String, Object> documentInfoMap = new HashMap<>();
                documentInfoMap.put("document_type", myDriverAccountResponse.getDocuments().getDocument_type());
                documentInfoMap.put("expiration_date", myDriverAccountResponse.getDocuments().getExpiration_date());
                documentInfoMap.put("identity_number", myDriverAccountResponse.getDocuments().getIdentity_number());
                documentInfoMap.put("issuing_state", myDriverAccountResponse.getDocuments().getIssuing_state());
                documentInfoMap.put("licence_issuing_state", myDriverAccountResponse.getDocuments().getLicence_issuing_state());
                documentInfoMap.put("licence_plate_number", myDriverAccountResponse.getDocuments().getLicence_plate_number());
                documentInfoMap.put("manufacture_company", myDriverAccountResponse.getDocuments().getManufacture_company());
                documentInfoMap.put("manufacture_model", myDriverAccountResponse.getDocuments().getManufacture_model());
                documentInfoMap.put("manufacture_year", myDriverAccountResponse.getDocuments().getManufacture_year());
                documentInfoMap.put("regular_maintenance", myDriverAccountResponse.getDocuments().getRegular_maintenance());


                HashMap<String, Object> phonesMap = new HashMap<>();
                phonesMap.put("phone_manufacture_company", myDriverAccountResponse.getPhones().getPhone_manufacture_company());
                phonesMap.put("phone_manufacture_model", myDriverAccountResponse.getPhones().getPhone_manufacture_model());
                phonesMap.put("smart_phone", myDriverAccountResponse.getPhones().getSmart_phone());

                HashMap<String, Object> rootMap = new HashMap<>();
                rootMap.put("about_us", "");
                rootMap.put("account", accountMap);
                rootMap.put("address", addressMap);
                rootMap.put("created_at", AppUtil.getCurrentTimeInMillisecond());
                rootMap.put("document_images", documentImageMap);
                rootMap.put("documents", documentInfoMap);
                rootMap.put("phones", phonesMap);
                rootMap.put("profile_image", myDriverAccountResponse.getDocument_images().getPhoto());
                rootMap.put("updated_at", AppUtil.getCurrentTimeInMillisecond());
                rootMap.put("user_id", getCurrentUid());


                firebaseFirestore.collection(DRIVER_COLLECTION).document(getCurrentUid()).collection("response")
                        .document("data_account").update(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            String fcmToken = PreferenceManger.getFcmToken(context);
                            //create new user
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("device_id", fcmToken + "");
                            map.put("device_mac_id", AppConstant.getDeviceId(context));
                            map.put("device_type", AppConstant.DEVICE_TYPE);
                            map.put("email", myDriverAccountResponse.getAccount().getEmail());
                           /* if (myDriverAccountResponse.getAccount().getEmail_communication().equalsIgnoreCase("yes")) {
                                map.put("email_communication", true);
                            } else {
                                map.put("email_communication", false);
                            }*/
                            map.put("email_communication", myDriverAccountResponse.getAccount().getEmail_communication());


                            map.put("name", myDriverAccountResponse.getAccount().getFirst_name());
                            //map.put("password", myDriverAccountResponse.getAccount().getp());
                            map.put("role", AppConstant.DRIVER_ROLE);
                            map.put("user_id", getCurrentUid());
                            map.put("street_1", myDriverAccountResponse.getAddress().getStreet_1());

                            firebaseFirestore.collection(AppConstant.USER_COLLECTION).document(getCurrentUid()).set(map)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                emitter.onComplete();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@androidx.annotation.NonNull Exception e) {
                                            emitter.onError(e);
                                        }
                                    });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    // For pending orders list
    public Single<UserModel> getUserDetails(String userID) {
        return Single.create(new SingleOnSubscribe<UserModel>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<UserModel> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.USER_COLLECTION)
                        .whereEqualTo("user_id", userID)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                ordersModelList = new ArrayList<>();
                                UserModel userModel = new UserModel();
                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document
                                    lastVisible = queryDocumentSnapshots.getDocuments()
                                            .get(documentSnapshots.size() - 1);
                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        userModel = documentSnapshots.get(i).toObject(UserModel.class);
                                    }
                                    emitter.onSuccess(userModel);
                                } else {
                                    emitter.onSuccess(userModel);
                                }
                                // Log.d("success==", userModel.getName());
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }


        });
    }

    // update driver's active satus
    public Completable updateActiveStatus(DriverLocationStatusModel driverLocationStatusModel) {
        return Completable.create(new CompletableOnSubscribe() {
                                      @Override
                                      public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                                          HashMap<String, Object> rootMap = new HashMap<>();
                                          rootMap.put("device_id", driverLocationStatusModel.getDevice_id());
                                          rootMap.put("driver_status", driverLocationStatusModel.getDriver_status());
                                          rootMap.put("location", driverLocationStatusModel.getLocation());
                                          rootMap.put("user_id", driverLocationStatusModel.getUser_id());
                                          firebaseFirestore.collection(AppConstant.DRIVER_LOCATION_STATUS).
                                                  document(getCurrentUid())
                                                  .set(rootMap)
                                                  .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                      @Override
                                                      public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                          Log.d("Sucess==", "Sucess");
                                                          if (task.isSuccessful()) {
                                                              emitter.onComplete();
                                                          } else {
                                                              emitter.onComplete();
                                                          }
                                                      }
                                                  }).addOnFailureListener(new OnFailureListener() {
                                              @Override
                                              public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                  Log.d("Failed==", "Failed");
                                                  emitter.onError(e);
                                              }
                                          });


                                      }
                                  }
        );


    }


    public Completable updateDriverOnlineOfflineStatus(String driverStatus) {
        return Completable.create(emitter ->
                firebaseFirestore.collection(AppConstant.DRIVER_LOCATION_STATUS).
                        document(getCurrentUid())
                        .update("driver_status", driverStatus)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                emitter.onComplete();
                            }
                        }).addOnFailureListener(emitter::onError)
        );

    }


    // update driver's active satus
    public Completable updateInActiveStatus(String status) {
        return Completable.create(new CompletableOnSubscribe() {
                                      @Override
                                      public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                                          HashMap<String, Object> rootMap = new HashMap<>();
                                          rootMap.put("driver_status", status);
                                          firebaseFirestore.collection(AppConstant.DRIVER_LOCATION_STATUS).
                                                  document(getCurrentUid())
                                                  .update(rootMap)
                                                  .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                      @Override
                                                      public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                                                          Log.d("Sucess==", "Sucess");
                                                          if (task.isSuccessful()) {
                                                              emitter.onComplete();
                                                          } else {
                                                              emitter.onComplete();
                                                          }
                                                      }
                                                  }).addOnFailureListener(new OnFailureListener() {
                                              @Override
                                              public void onFailure(@androidx.annotation.NonNull Exception e) {
                                                  Log.d("Failed==", "Failed");
                                                  emitter.onError(e);
                                              }
                                          });


                                      }
                                  }
        );


    }


    // For pending orders list
    public Single<List<OrdersModel>> getNewDeliveriesList(double latitude, double longitude, double distance, boolean isFirst) {


        return Single.create(new SingleOnSubscribe<List<OrdersModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<OrdersModel>> emitter) throws Exception {

                // ~1 mile of lat and lon in degrees
                double lat = 0.0144927536231884;

                double lon = 0.0181818181818182;

                double lowerLat = latitude - (lat * distance);

                double lowerLon = longitude - (lon * distance);


                double greaterLat = latitude + (lat * distance);

                double greaterLon = longitude + (lon * distance);

                GeoPoint lesserGeoPoint = new GeoPoint(lowerLat, lowerLon);
                GeoPoint greaterGeoPoint = new GeoPoint(greaterLat, greaterLon);
                //dispensaryWeb.

                if (isFirst) {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("assign_driver_id", getCurrentUid())
                            .whereEqualTo("status", AppConstant.ORDER_TYPE_SCHEDULED)
                            // .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                    ordersModelList = new ArrayList<>();

                                    if (documentSnapshots.size() > 0) {
                                        // Get the last visible document
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);

//                                            if (ordersModel.getOpening_hours() != null)
//                                                Log.d("day", ordersModel.getOpening_hours().get(0).getDay());
//                                            //   Log.d("image" ,dispensaryModel.getProfile_image());

                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    } else {
                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                } else {
                    firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereGreaterThan("dispensary_location", lesserGeoPoint)
                            .whereLessThan("dispensary_location", greaterGeoPoint)
                            .whereEqualTo("status", AppConstant.ORDER_TYPE_SCHEDULED)
                            .limit(10)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                    List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();


                                    if (documentSnapshots.size() > 0) {
                                        lastVisible = queryDocumentSnapshots.getDocuments()
                                                .get(documentSnapshots.size() - 1);

                                        for (int i = 0; i < documentSnapshots.size(); i++) {
                                            OrdersModel ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                            ordersModelList.add(ordersModel);
                                        }

                                        emitter.onSuccess(ordersModelList);
                                    }


                                    Log.d("success==", "success");
                                    Log.d("size==", "" + documentSnapshots.size());

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@androidx.annotation.NonNull Exception e) {

                            Log.d("Failed==", "Failed");
                            emitter.onError(e);
                        }
                    });
                }


            }
        });

    }


    // update order to decline
    public Completable updateDriverLocation(double latitude, double longitude, String orderID) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                HashMap<String, Object> rootMap = new HashMap<>();
                rootMap.put("location", new GeoPoint(latitude, longitude));

                firebaseFirestore.collection(AppConstant.USER_LOCATION).document(getCurrentUid()).
                        collection(orderID).document(orderID).
                        set(rootMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<Void> task) {
                        emitter.onComplete();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });

            }
        });

    }

    public Single<DriverLocationStatusModel> getDriverStatus() {
        return Single.create(new SingleOnSubscribe<DriverLocationStatusModel>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<DriverLocationStatusModel> emitter) throws Exception {

                firebaseFirestore.collection(DRIVER_LOCATION_STATUS).document(getCurrentUid())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@androidx.annotation.NonNull Task<DocumentSnapshot> task) {

                                DriverLocationStatusModel driverLocationStatusModel = new DriverLocationStatusModel();

                                if (task.isSuccessful()) {
                                    driverLocationStatusModel = task.getResult().toObject(DriverLocationStatusModel.class);
                                }
                                emitter.onSuccess(driverLocationStatusModel);

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);
                    }
                });
            }
        });
    }


    // For pending orders list
    public Single<String> getOrderStatus(String id) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<String> emitter) throws Exception {

                firebaseFirestore.collection(AppConstant.ORDER_COLLECTION)
                        .whereEqualTo("id", id)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                OrdersModel ordersModel = new OrdersModel();

                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document
                                    lastVisible = queryDocumentSnapshots.getDocuments()
                                            .get(documentSnapshots.size() - 1);

                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        // OrdersModel ordersModell =
                                        ordersModel = documentSnapshots.get(i).toObject(OrdersModel.class);
                                        // ordersModelList.add(ordersModel);
                                    }

                                    Log.d("success==", ordersModel.getStatus());
                                    emitter.onSuccess(ordersModel.getStatus());
                                } else {
                                    emitter.onSuccess(ordersModel.getStatus());
                                }


                                Log.d("success==", "success");
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }


        });
    }


    // Create new User
    public Completable forgetPassword(String email) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {

                firebaseAuth.sendPasswordResetEmail(email)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                emitter.onComplete();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        emitter.onError(e);

                    }
                });

            }
        });
    }


    public Single<List<VehicleModel>> getVehicleModelList() {
        return Single.create(new SingleOnSubscribe<List<VehicleModel>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<VehicleModel>> emitter) throws Exception {
                firebaseFirestore.collection(AppConstant.VEHICLE_MODEL).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                List<VehicleModel> vehicleModelList = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document
                                    lastVisible = queryDocumentSnapshots.getDocuments()
                                            .get(documentSnapshots.size() - 1);

                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        VehicleModel vehicleModel = documentSnapshots.get(i).toObject(VehicleModel.class);
                                        vehicleModelList.add(vehicleModel);
                                    }

                                    emitter.onSuccess(vehicleModelList);
                                } else {
                                    emitter.onSuccess(vehicleModelList);
                                }


                                Log.d("success==", "success");
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });

    }


    public Single<List<VehicleCompany>> getVehicleCompanyList() {
        return Single.create(new SingleOnSubscribe<List<VehicleCompany>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<VehicleCompany>> emitter) throws Exception {
                firebaseFirestore.collection(AppConstant.VEHICLE_COMPANY).get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                List<VehicleCompany> vehicleCompanyList = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document
                                    lastVisible = queryDocumentSnapshots.getDocuments()
                                            .get(documentSnapshots.size() - 1);

                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        VehicleCompany vehicleCompany = documentSnapshots.get(i).toObject(VehicleCompany.class);
                                        vehicleCompanyList.add(vehicleCompany);
                                    }

                                    emitter.onSuccess(vehicleCompanyList);
                                } else {
                                    emitter.onSuccess(vehicleCompanyList);
                                }


                                Log.d("success==", "success");
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {
                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });

    }

    public Single<List<MyDriverRating>> getDriverRatingList() {
        return Single.create(new SingleOnSubscribe<List<MyDriverRating>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<MyDriverRating>> emitter) throws Exception {
                firebaseFirestore.collection(DRIVER_COLLECTION).document(getCurrentUid()).
                        collection("reviews").get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                List<MyDriverRating> ratingList = new ArrayList<>();

                                if (documentSnapshots.size() > 0) {
                                    // Get the last visible document

                                    for (int i = 0; i < documentSnapshots.size(); i++) {
                                        MyDriverRating vehicleCompany = documentSnapshots.get(i).toObject(MyDriverRating.class);
                                        ratingList.add(vehicleCompany);
                                    }


                                }

                                emitter.onSuccess(ratingList);

                                Log.d("success==", "success");
                                Log.d("size==", "" + documentSnapshots.size());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@androidx.annotation.NonNull Exception e) {

                        Log.d("Failed==", "Failed");
                        emitter.onError(e);
                    }
                });
            }
        });

    }

}

