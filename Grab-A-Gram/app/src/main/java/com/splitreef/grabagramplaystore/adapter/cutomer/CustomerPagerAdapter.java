package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryDealsFragment;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListFragment;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryMapsFragment;

public class CustomerPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    int totalTabs;

    public CustomerPagerAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        switch (position) {

            case 0:
                fragment=new DispensaryListFragment();
                break;
            case 1:
                fragment=new DispensaryMapsFragment();
                break;
            case 2:
                fragment=new DispensaryDealsFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
