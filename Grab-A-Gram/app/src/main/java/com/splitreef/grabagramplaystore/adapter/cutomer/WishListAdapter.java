package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.RemoveWishListProduct;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.WishListHolder> {

    private Context context;
    private ArrayList<ProductCart>productCartArrayList;
    private RemoveWishListProduct removeWishListProduct;



    public WishListAdapter(Context context, ArrayList<ProductCart>productCartArrayList, RemoveWishListProduct removeWishListProduct) {
        this.context=context;
        this.productCartArrayList=productCartArrayList;
        this.removeWishListProduct=removeWishListProduct;
    }

    @NonNull
    @Override
    public WishListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wish_list,parent,false);
        return new WishListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WishListHolder holder, int position) {

        ProductCart productCart=productCartArrayList.get(position);
        PicassoManager.setImage(productCart.getImages().get(0),holder.imgProduct);
        holder.txtProductName.setText(""+productCart.getName());
        holder.itemRating.setRating(productCart.getRating());

        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog() {
                    @Override
                    public void onPositiveButtonClick() {
                        removeWishListProduct.removeWishListProduct(productCart.getId());
                    }
                }.showAlertDialog(context,"Delete",context.getString(R.string.wish_delete),"OK","CANCEL");



            }
        });

    }

    @Override
    public int getItemCount() {
        return productCartArrayList.size();
    }

    public class WishListHolder extends RecyclerView.ViewHolder
    {
        AppCompatImageView imgProduct;
        AppCompatImageView imgRemove;
        MaterialTextView txtProductName;
        AppCompatRatingBar itemRating;


        public WishListHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct= itemView.findViewById(R.id.img_product);
            txtProductName= itemView.findViewById(R.id.txt_product_name);
            imgRemove= itemView.findViewById(R.id.img_remove);
            itemRating= itemView.findViewById(R.id.item_rating);

        }
    }
}
