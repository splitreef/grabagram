package com.splitreef.grabagramplaystore.ui.driver.driverRegistration;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DriverRegistrationViewModel extends ViewModel {
    private static final String TAG = "RegistrationViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<StateResource> onRegister = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUploadDriverDocPhoto = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onAddDriverData = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> ongetUserExistOrNot = new MediatorLiveData<>();

    private MediatorLiveData<DataResource> ongetVehicleModel = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> ongetVehicleCompany = new MediatorLiveData<>();

    private CompositeDisposable disposable = new CompositeDisposable();
    private Application mApplication;


    public DriverRegistrationViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication = mApplication;
    }

    void registerUser(String email, String password) {
        firebaseRepository.registerDriverUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onRegister.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onRegister.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onRegister.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }

    void uploadDriverDocPhoto(byte[] bytes, String imageName) {
        firebaseRepository.uploadDriverDocPhoto(bytes, imageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUploadDriverDocPhoto.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        onUploadDriverDocPhoto.setValue(DataResource.DataStatus(s));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUploadDriverDocPhoto.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    void addDriverData(DriverRegistrationRequest driverRegistrationRequest) {
        firebaseRepository.addDriverData(driverRegistrationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onAddDriverData.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onAddDriverData.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onAddDriverData.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }

    void checkUserExistsOrNot(String email) {
        firebaseRepository.checkUserExistsOrNot(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        ongetUserExistOrNot.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull Boolean aBoolean) {
                        ongetUserExistOrNot.setValue(DataResource.DataStatus(aBoolean));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ongetUserExistOrNot.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    LiveData<StateResource> observeRegister() {
        return onRegister;
    }

    LiveData<DataResource> observeDriverDocPhoto() {
        return onUploadDriverDocPhoto;
    }

    LiveData<StateResource> observeAddDriverData() {
        return onAddDriverData;
    }

    LiveData<DataResource> observeUserExistOrNot() {
        return ongetUserExistOrNot;
    }


    void getVehicleModelList() {
        firebaseRepository.getVehicleModelList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<VehicleModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        ongetVehicleModel.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<VehicleModel> vehicleModelList) {
                        ongetVehicleModel.setValue(DataResource.DataStatus(vehicleModelList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ongetVehicleModel.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    LiveData<DataResource> observeVehicleModel() {
        return ongetVehicleModel;
    }


    void getVehicleCompanyList() {
        firebaseRepository.getVehicleCompanyList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<VehicleCompany>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        ongetVehicleCompany.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<VehicleCompany> vehicleCompanyList) {
                        ongetVehicleCompany.setValue(DataResource.DataStatus(vehicleCompanyList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ongetVehicleCompany.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    LiveData<DataResource> observeVehicleCompany() {
        return ongetVehicleCompany;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }


}
