package com.splitreef.grabagramplaystore.fcmNotification;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.splitreef.grabagramplaystore.utils.AppConstant;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CallSendNotificationApi {
    private Context context;
    private final String TAG = "CallSendNotificationApi";

    public CallSendNotificationApi(Context context) {
        this.context = context;
    }

    public void sendNotification(JSONObject notification) {
        Log.i(TAG, "JSONObject: " + notification.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(AppConstant.FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Request error", Toast.LENGTH_LONG).show();
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", AppConstant.SERVER_KEY_);
                params.put("Content-Type", AppConstant.CONTENT_TYPE);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}
