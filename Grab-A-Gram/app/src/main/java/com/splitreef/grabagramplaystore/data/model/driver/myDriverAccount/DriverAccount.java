package com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount;

import java.io.Serializable;


/**
 * Created by  on 05-01-2021.
 */
public class DriverAccount implements Serializable {
    private long date_of_birth;
    private String email;
    private boolean email_communication;
    private String first_name;
    private boolean is_caregiver;
    private String last_name;
    private boolean message_notification;


    /*private String device_id;
    private String device_mac_id;
    private String device_type;
    private String email;
    private String password;
    private String role;
    private String term_and_condition;


    private String city;
    private String phone_number;
    private String state;
    private String street_1;
    private String street_2;
    private String zip_code;*/

    public long getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(long date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public boolean getEmail_communication() {
        return email_communication;
    }

    public void setEmail_communication(boolean email_communication) {
        this.email_communication = email_communication;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public boolean getIs_caregiver() {
        return is_caregiver;
    }

    public void setIs_caregiver(boolean is_caregiver) {
        this.is_caregiver = is_caregiver;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public boolean getMessage_notification() {
        return message_notification;
    }

    public void setMessage_notification(boolean message_notification) {
        this.message_notification = message_notification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
