package com.splitreef.grabagramplaystore.ui.driver.orders.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dialog.AlertDialog;
import com.splitreef.grabagramplaystore.ui.customer.help.HelpListFragment;
import com.splitreef.grabagramplaystore.ui.driver.help.HelpDriverListFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.myAccount.MyAccountFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.my_earnings.MyEarningsFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pastDeliveries.PastDeliveriesFragment;
import com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders.PendingOrdersFrgament;
import com.splitreef.grabagramplaystore.ui.driver.orders.upcomingDeliveries.UpcomingDeliveriesFragment;
import com.splitreef.grabagramplaystore.ui.login.LoginActivity;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.HomeViewModelProviderFactory;

import java.util.Objects;

/**
 * Created by  on 25-11-2020.
 */
public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    public MaterialTextView toolbarTitle;
    public static TextView tvActiveStatus;
    public static AppCompatImageView imvNotificationBell;
    private GrabAGramApplication application;
    private HomeViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initView();
        initListeners();
    }


    // Initialized view of this screen.
    public void initView() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        MaterialTextView search = headerView.findViewById(R.id.search);
        tvActiveStatus = findViewById(R.id.tv_active_status);
        imvNotificationBell = findViewById(R.id.imv_notification_bell);
        application = (GrabAGramApplication) Objects.requireNonNull(HomeActivity.this).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new HomeViewModelProviderFactory(getApplication(), application.firebaseRepository)).get(HomeViewModel.class);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolbarTitle("Profile");


        navigationView.setNavigationItemSelectedListener(HomeActivity.this);
        actionBarDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.mipmap.menu);
        imvNotificationBell.setVisibility(View.GONE);
        tvActiveStatus.setVisibility(View.VISIBLE);
        replaceFragment(new HomeFragment(), false);
        subscribeObservers();

    }


    void initListeners() {
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }


    public void replaceFragment(Fragment fragment, boolean isAllowBack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (isAllowBack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }

    public void replaceDeliveriesFragment(int i) {
        if (i == 0) {
            setToolbarTitle("MY ACCOUNT");
            imvNotificationBell.setVisibility(View.GONE);
            tvActiveStatus.setVisibility(View.GONE);
            replaceFragment(new MyAccountFragment(), true);
        } else if (i == 1) {
            setToolbarTitle("PAST DELIVERIES");
            tvActiveStatus.setVisibility(View.GONE);
            imvNotificationBell.setVisibility(View.GONE);
            replaceFragment(new PastDeliveriesFragment(), true);
        } else if (i == 2) {
            setToolbarTitle("UPCOMING DELIVERIES");
            tvActiveStatus.setVisibility(View.GONE);
            imvNotificationBell.setVisibility(View.GONE);
            replaceFragment(new UpcomingDeliveriesFragment(), true);
        }


    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_home:
                setToolbarTitle("PROFILE");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new HomeFragment(), false);
                break;
            case R.id.nav_pending_orders:
                setToolbarTitle("PENDING ORDERS");
                replaceFragment(new PendingOrdersFrgament(), true);
                break;
            case R.id.nav_upcoming_deliveries:
                setToolbarTitle("UPCOMING DELIVERIES");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new UpcomingDeliveriesFragment(), true);
                break;
            case R.id.nav_past_deliveries:
                setToolbarTitle("PAST DELIVERIES");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new PastDeliveriesFragment(), true);
                break;
            case R.id.nav_account_info:
                setToolbarTitle("MY ACCOUNT");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new MyAccountFragment(), true);
                break;
            case R.id.nav_my_earnings:
                setToolbarTitle("MY EARNINGS");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new MyEarningsFragment(), true);
                break;
            case R.id.nav_help:
                setToolbarTitle("HELP");
                imvNotificationBell.setVisibility(View.GONE);
                replaceFragment(new HelpDriverListFragment(), true);

                break;
            case R.id.nav_logout:
                openLogoutDialog();
                break;
        }

        drawerLayout.closeDrawers();
        return false;
    }


    public void openLogoutDialog() {
        new AlertDialog() {
            @Override
            public void onPositiveButtonClick() {

                String activeStatus = PreferenceManger.getPreferenceManger().getString(PrefKeys.ACTIVE_STATUS);

                if (activeStatus == null) {
                    logout();
                } else if (activeStatus.equalsIgnoreCase(getResources().getString(R.string.offline))) {
                    viewModel.updateDriverOnlineOfflineStatus("Offline");
                } else if (activeStatus.equalsIgnoreCase(getResources().getString(R.string.online))) {
                    logout();
                }

            }
        }.showAlertDialog(HomeActivity.this, "Logout", "Do you want to logout?", "Yes", "No");
    }

   /* @Override
    public void onBackPressed() {

        new AlertDialog() {
            @Override
            public void onPositiveButtonClick() {
                finish();
            }
        }.showAlertDialog(HomeActivity.this, "Alert!", "Are you sure to want exit.?", "Yes", "No");

    }
*/

    void subscribeObservers() {
        viewModel.observeUpdateDriverStatus().observe(HomeActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(HomeActivity.this, "Loading...");
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            logout();

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(HomeActivity.this, stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }


    public void logout() {
        viewModel.userLogout();
        PreferenceManger.getPreferenceManger().clearSession();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
}

