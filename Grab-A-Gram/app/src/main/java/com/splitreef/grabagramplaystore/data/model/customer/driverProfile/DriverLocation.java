package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;

import com.google.firebase.firestore.GeoPoint;

public class DriverLocation {

    private String status;
    private GeoPoint location;

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
