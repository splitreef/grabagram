package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.productCategory.ProductCategory;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;

import java.util.ArrayList;

public class CustomerCategoryFilterAdapter extends RecyclerView.Adapter<CustomerCategoryFilterAdapter.CustomerCategoryFilterHolder> {

    private Context context;
    private ArrayList<ProductCategory>productCategoryList;
    int index=-1;


    public CustomerCategoryFilterAdapter(Context context, ArrayList<ProductCategory>productCategoryList) {
        this.context=context;
        this.productCategoryList=productCategoryList;
    }

    @NonNull
    @Override
    public CustomerCategoryFilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_category,parent,false);
        return new CustomerCategoryFilterHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CustomerCategoryFilterHolder holder, int position) {

        ProductCategory productCategory=productCategoryList.get(position);
        holder.txtCategoryName.setText(""+productCategory.getCategory_name());

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productCategory.isSelected())
                {
                    productCategory.setSelected(false);
                }
                else
                {
                    productCategory.setSelected(true);
                }
                notifyDataSetChanged();
            }
        });


        if (productCategory.isSelected())
        {
            holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_cheked);
        }
        else
        {
            holder.imgRadioBtn.setImageResource(R.mipmap.radio_button_unchked);
        }

    }

    @Override
    public int getItemCount() {
        return productCategoryList.size();
    }

    public class CustomerCategoryFilterHolder extends RecyclerView.ViewHolder
    {
        MaterialTextView txtCategoryName;
        AppCompatImageView imgRadioBtn;
        LinearLayout llRoot;

        public CustomerCategoryFilterHolder(@NonNull View itemView) {
            super(itemView);
            txtCategoryName= itemView.findViewById(R.id.txt_category_name);
            imgRadioBtn= itemView.findViewById(R.id.img_radio_btn);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }


    public void resetCategoryFilter()
    {
        if (productCategoryList!=null && productCategoryList.size()>0)
            for (int i=0;i<productCategoryList.size();i++)
            {
                productCategoryList.get(i).setSelected(false);
            }
        PreferenceManger.getPreferenceManger().setString(PrefKeys.CATEGORY_ID_LIST,null);
        PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_CATEGORY_FILTER,false);
        notifyDataSetChanged();
    }

    public void setSelectedCategory()
    {
        ArrayList<String> categoryIdList=new ArrayList<>();

        for (int i=0;i<productCategoryList.size();i++)
        {
            boolean isSelected=productCategoryList.get(i).isSelected();
            if (isSelected)
            {
                categoryIdList.add(productCategoryList.get(i).getId());

            }
        }

        if (categoryIdList.size()>0)
        {
            PreferenceManger.getPreferenceManger().setObject(PrefKeys.CATEGORY_ID_LIST,categoryIdList);
            PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_CATEGORY_FILTER,true);
        }
        else
        {
            PreferenceManger.getPreferenceManger().setObject(PrefKeys.CATEGORY_ID_LIST,categoryIdList);
            PreferenceManger.getPreferenceManger().setBoolean(PrefKeys.IS_CATEGORY_FILTER,false);
        }

    }
}
