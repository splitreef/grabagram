package com.splitreef.grabagramplaystore.ui.driver.driverRegistration;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentDriver;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.databinding.FragmentDriverPhoneBinding;

import java.util.Objects;

/**
 * Created by  on 06-11-2020.
 */
public class DriverPhoneFragment extends BaseFragment implements View.OnClickListener {
    private FragmentDriverPhoneBinding binding;
    private FragmentDriver fragmentDriver;
    private DriverRegistrationViewModel driverRegistrationViewModel;
    private GrabAGramApplication grabAGramApplication;
    private DriverRegistrationRequest driverRegistrationRequest;
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public DriverPhoneFragment(FragmentDriver fragmentDriver) {
        this.fragmentDriver = fragmentDriver;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDriverPhoneBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        grabAGramApplication = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        if (getArguments() != null) {
            driverRegistrationRequest = (DriverRegistrationRequest) getArguments().getSerializable("requestData");
        }
        binding.btnContinue.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                moveNextScreen();
                break;
        }
    }

    void moveNextScreen() {
        if (isPhoneInfoValidation()) {
            // Set driver phone information
            driverRegistrationRequest.setPhoneManufactureCompany(binding.edtMake.getText().toString());
            driverRegistrationRequest.setPhoneManufactureModel(binding.edtModel.getText().toString());

            if (binding.rbYes.isChecked()) {
                driverRegistrationRequest.setHavingSmartPhone("YES");
            } else if (binding.rbNo.isChecked()) {
                driverRegistrationRequest.setHavingSmartPhone("NO");
            }
            fragmentDriver.getData(3, driverRegistrationRequest);
        }


    }

    boolean isPhoneInfoValidation() {
        if (!binding.rbYes.isChecked() && !binding.rbNo.isChecked()) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select do you have a Smart Phone?");
            return false;
        } else if (Objects.requireNonNull(binding.edtMake.getText()).toString().equals("")) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Phone Make");
            return false;
        } else if (Objects.requireNonNull(binding.edtModel.getText()).toString().equals("")) {
            grabAGramApplication.messageManager.DisplayToastMessage("Please select Phone Model");
            return false;
        }
        return true;
    }
}
