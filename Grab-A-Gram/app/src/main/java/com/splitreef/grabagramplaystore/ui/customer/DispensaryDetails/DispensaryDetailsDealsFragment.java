package com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.adapter.cutomer.DealListAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.DealModel;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.databinding.FragmentDealsListBinding;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.DispensaryDetailsViewModelProviderFactory;

import java.util.ArrayList;
import java.util.Objects;

public class DispensaryDetailsDealsFragment extends BaseFragment {
    FragmentDealsListBinding binding;
    private DispensaryDetailsViewModel viewModel;
    private GrabAGramApplication application;
    private DispensaryModel dispensaryModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null)
        {
            dispensaryModel= (DispensaryModel) getArguments().getParcelable(AppConstant.DISPENSARY_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding= FragmentDealsListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void  initView()
    {

       application=(GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();

       viewModel=new ViewModelProvider(getViewModelStore(), new DispensaryDetailsViewModelProviderFactory(application.firebaseRepository)).get(DispensaryDetailsViewModel.class);

       binding.recycleView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        subscribeObservers();

        if (dispensaryModel!=null)
        {
            viewModel.getDealListById(dispensaryModel.getUser_id());
        }


    }


    void subscribeObservers()
    {
        viewModel.observeGetDealList().observe(Objects.requireNonNull(getActivity()), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.progressBar.setVisibility(View.GONE);
                        ArrayList<DealModel> dealModelArrayList= (ArrayList<DealModel>) dataResource.data;
                        updateUI(dealModelArrayList);
                        break;
                    case ERROR:
                        binding.progressBar.setVisibility(View.GONE);
                        binding.userMsg.setVisibility(View.GONE);
                        break;

                }
            }

        });

    }

    public void updateUI(ArrayList<DealModel> dispensaryModelArrayList)
    {
        if (dispensaryModelArrayList!=null && dispensaryModelArrayList.size()>0)
        {
            binding.userMsg.setVisibility(View.GONE);
            DealListAdapter adapter=new DealListAdapter(getContext(),dispensaryModelArrayList);
            binding.recycleView.setAdapter(adapter);
        }
        else
        {
           binding.userMsg.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
     }
}
