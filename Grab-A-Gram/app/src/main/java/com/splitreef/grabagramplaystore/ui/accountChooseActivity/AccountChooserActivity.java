package com.splitreef.grabagramplaystore.ui.accountChooseActivity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.databinding.ActivityAccountChooserBinding;
import com.splitreef.grabagramplaystore.ui.customer.registration.RegistrationActivity;
import com.splitreef.grabagramplaystore.ui.driver.signUpPhoto.SignUpPhotoActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;

import java.util.Objects;

public class AccountChooserActivity extends AppCompatActivity {

    private ActivityAccountChooserBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityAccountChooserBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);


        binding.btnContinue.setOnClickListener(view -> {

            if (binding.rbCustomer.isChecked())
            {
                startActivity(new Intent(AccountChooserActivity.this, RegistrationActivity.class)
                .putExtra(AppConstant.FROM,AppConstant.FROM_ACCOUNT_CHOOSER));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
            else if (binding.rbDriver.isChecked())
            {
                startActivity(new Intent(AccountChooserActivity.this, SignUpPhotoActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }

        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
