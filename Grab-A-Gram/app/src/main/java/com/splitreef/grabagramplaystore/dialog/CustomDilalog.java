package com.splitreef.grabagramplaystore.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.splitreef.grabagramplaystore.R;

/**
 * Created by  on 29-12-2020.
 */
public abstract class CustomDilalog {


    public abstract void onPositiveButtonClick();

    public void showCustomDialog(Context context, String title, String message, String btnPosName, String btnNegName) {

        Dialog dialogs = new Dialog(context, R.style.custom_dialog_style);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogs.setCancelable(false);
        dialogs.setContentView(R.layout.dialog_custom);

        TextView tv_tittle = dialogs.findViewById(R.id.tv_tittle);
        TextView tv_message = dialogs.findViewById(R.id.tv_message);
        Button btn_yes = dialogs.findViewById(R.id.btn_yes);
        Button btn_no = dialogs.findViewById(R.id.btn_no);
        tv_tittle.setText(title);
        tv_message.setText(message);


        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClick();
                dialogs.dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogs.dismiss();
            }
        });
        dialogs.show();
    }


}



