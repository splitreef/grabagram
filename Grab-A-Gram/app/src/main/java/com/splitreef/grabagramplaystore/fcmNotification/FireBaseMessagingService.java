package com.splitreef.grabagramplaystore.fcmNotification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.ui.customer.orderDetails.CustomerOrderDetailsActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import org.json.JSONObject;

public class FireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = FireBaseMessagingService.class.getName();
    public static final String CHANNEL_ID = "CHANNEL_ID";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("data"));
                String title = jsonObject.getString("title");
                String body = jsonObject.getString("body");
                String orderId = jsonObject.getString(AppConstant.ORDER_ID);
                String userType = jsonObject.getString(AppConstant.USER_TYPE);
                createNotificationChannel();
                getNotification(title, body, orderId, userType);
            } catch (Exception ex) {
                Log.d(TAG, "" +ex.getMessage());
            }

        }

    }


    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        PreferenceManger.setFcmToken(this,token);
        Log.e("onNewToken",token);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.canShowBadge();
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
    }

    private void getNotification(String title, String body, String orderId, String userType) {

        PendingIntent pendingIntent = null;
        UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);
        if (userModel != null) {
            if (userType.equals("Driver")) {
                Intent intent = new Intent(this, CustomerOrderDetailsActivity.class);
                intent.putExtra(AppConstant.ORDER_ID, orderId);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            } else if (userType.equals("Customer")) {
                Intent intent = new Intent(this, OrdersDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Log.d("ORDER_ID", "" + orderId);
                intent.putExtra(AppConstant.ID, orderId);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            }

        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setTicker(title)
                .setSmallIcon(R.mipmap.logo)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(999, builder.build());
    }


}
