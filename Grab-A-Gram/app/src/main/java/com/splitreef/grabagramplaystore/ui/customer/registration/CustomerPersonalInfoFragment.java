package com.splitreef.grabagramplaystore.ui.customer.registration;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentConsumer;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.viewModelFactory.RegistrationViewModelProviderFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;
import com.splitreef.grabagramplaystore.databinding.FragmentCustomerPersonalInfoBinding;
import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_KEY;
import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_URL;

public class CustomerPersonalInfoFragment extends Fragment implements View.OnClickListener {

    private FragmentCustomerPersonalInfoBinding binding;
    FragmentConsumer fragmentConsumer;
    private CustomerRegisterRequest registerRequest;
    private RegistrationViewModel registrationViewModel;
    private GrabAGramApplication application;
    private long date = 0;
    private int textLength=0;

    public CustomerPersonalInfoFragment(FragmentConsumer fragmentConsumer) {
        this.fragmentConsumer = fragmentConsumer;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCustomerPersonalInfoBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        if (getArguments() != null) {
            registerRequest = (CustomerRegisterRequest) getArguments().getSerializable("requestData");
        }

        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        registrationViewModel = new ViewModelProvider(getViewModelStore(), new RegistrationViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(RegistrationViewModel.class);

        binding.btnContinue.setOnClickListener(this);
        binding.dateOfBirth.setOnClickListener(this);


        binding.mobilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.mobilePhone.getText().toString();
                textLength = binding.mobilePhone.getText().length();
                if (text.endsWith("-") || text.endsWith(" "))
                    return;
                if (textLength == 1) {
                    if (!text.contains("(")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 5) {
                    if (!text.contains(")")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 6) {
                    binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                } else if (textLength == 10) {
                    if (!text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 15) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                } else if (textLength == 18) {
                    if (text.contains("-")) {
                        binding.mobilePhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.mobilePhone.setSelection(binding.mobilePhone.getText().length());
                    }
                }
            }



            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                if (!NetworkManager.isNetworkAvailable(getContext())) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
                } else {
                    moveNextScreen();
                }
                break;
            case R.id.date_of_birth:
                setDateOfBirth();
        }
    }


    void moveNextScreen() {
        if (isValidation()) {

            String tmp = Objects.requireNonNull(binding.mobilePhone.getText()).toString();
            String phoneNumber = tmp.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");


            registerRequest.setFirstName(Objects.requireNonNull(binding.firstName.getText()).toString());
            registerRequest.setLastName(Objects.requireNonNull(binding.lastName.getText()).toString());
            registerRequest.setStreetAddress(Objects.requireNonNull(binding.streetAddress.getText()).toString());
            registerRequest.setStreetAddress2(Objects.requireNonNull(binding.streetAddress2.getText()).toString());
            registerRequest.setCity(Objects.requireNonNull(binding.city.getText()).toString());
            registerRequest.setState(binding.spinnerState.getSelectedItem().toString());
            registerRequest.setZipCode(Objects.requireNonNull(binding.zipCode.getText()).toString());
            registerRequest.setMobileNumber(phoneNumber);
            registerRequest.setDateOfBirth(date);
            registerRequest.setSendNotification(binding.cbSendNotification.isChecked());


            String address = registerRequest.getStreetAddress() + "," + registerRequest.getCity() + "," + registerRequest.getState() + "," + registerRequest.getZipCode();

            getLatLng(address);
            application.dialogManager.displayProgressDialog(getActivity(), "Loading");


        }
    }

    // For account info Validation
    boolean isValidation() {
        if (Objects.requireNonNull(binding.firstName.getText()).toString().equals("")) {
            binding.firstName.setError(getString(R.string.enter_your_first_name));
            binding.firstName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.lastName.getText()).toString().equals("")) {
            binding.lastName.setError(getString(R.string.enter_your_last_name));
            binding.lastName.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.streetAddress.getText()).toString().equals("")) {
            binding.streetAddress.setError(getString(R.string.enter_your_address));
            binding.streetAddress.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.city.getText()).toString().equals("")) {
            binding.city.setError(getString(R.string.enter_your_city));
            binding.city.requestFocus();
            return false;
        } else if (binding.spinnerState.getSelectedItemPosition() == 0) {
            application.messageManager.DisplayToastMessage(getString(R.string.enter_your_state));
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().equals("")) {
            binding.zipCode.setError(getString(R.string.enter_your_zip_code));
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.zipCode.getText()).toString().trim().length() < 5) {
            binding.zipCode.setError(getString(R.string.enter_your_valid_zip_code));
            binding.zipCode.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().equals("")) {
            binding.mobilePhone.setError(getString(R.string.enter_your_mobile_number));
            binding.mobilePhone.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.mobilePhone.getText()).toString().length() < 14) {
            binding.mobilePhone.setError(getString(R.string.enter_your_valid_mobile_number));
            binding.mobilePhone.requestFocus();
            return false;
        } else if (binding.dateOfBirth.getText().toString().equals("")) {
            application.messageManager.DisplayToastMessage(getString(R.string.enter_date_of_birth));
            return false;
        }

        return true;

    }


    public void setDateOfBirth()
    {
        Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();

        CalendarConstraints.Builder constraintBuilder=new CalendarConstraints.Builder();
        constraintBuilder.setValidator(DateValidatorPointBackward.now());
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(constraintBuilder.build());
        MaterialDatePicker<Long> picker = builder.build();

        picker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(selection -> {


            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar1.setTimeInMillis(selection);
            int month= calendar1.get(Calendar.MONTH);
            String dateOfBirth=(month+1)+"-"+ calendar1.get(Calendar.DAY_OF_MONTH)+"-"+ calendar1.get(Calendar.YEAR);


            if (AppUtil.subYears(AppUtil.getCurrentDate(),dateOfBirth)<18)
            {
                date=0;
                binding.dateOfBirth.setText("");
                Toast.makeText(getContext(), getString(R.string.enter_valid_date_of_birth), Toast.LENGTH_SHORT).show();
            }
            else
            {
                date=selection;
                binding.dateOfBirth.setText(""+dateOfBirth);
            }

        });

    }


    void getLatLng(String address) {

        String googleApiUrl = GOOGLE_API_URL + "?address=" + address + "&key=" + GOOGLE_API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                googleApiUrl, new Response.Listener<String>() {

            public void onResponse(String response) {

                application.dialogManager.dismissProgressDialog();

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");

                        if (status.equals("OK")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("geometry");

                            JSONObject jsonObject3 = jsonObject2.getJSONObject("location");

                            double latitude = jsonObject3.getDouble("lat");
                            double longitude = jsonObject3.getDouble("lng");


                            registerRequest.setLatitude(latitude);
                            registerRequest.setLongitude(longitude);

                            fragmentConsumer.getData(2, registerRequest);

                            Log.d("status", status + "lat/longitude" + latitude + "---" + longitude);

                        }
                        else
                        {
                            Toast.makeText(getContext(), "Invalid address", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                Log.d("Response", response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}
