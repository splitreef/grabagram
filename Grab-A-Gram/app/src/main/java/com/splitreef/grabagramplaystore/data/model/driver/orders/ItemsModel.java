package com.splitreef.grabagramplaystore.data.model.driver.orders;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by  on 01-12-2020.
 */
public class ItemsModel implements Serializable {
    private String busniess_name;
    private String description;
    private String dispensary_id;
    private String id;
    private ArrayList<String> images;
    private String name;
    private String product_id;
    private Object quantity;
  //  private int rating;
    private long regular_price;
    private long sale_price;
    private long tax_rate;
    private long total_count;
    private String weight;

    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDispensary_id() {
        return dispensary_id;
    }

    public void setDispensary_id(String dispensary_id) {
        this.dispensary_id = dispensary_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

//    public int getRating() {
//        return rating;
//    }
//
//    public void setRating(int rating) {
//        this.rating = rating;
//    }

    public long getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(long regular_price) {
        this.regular_price = regular_price;
    }

    public long getSale_price() {
        return sale_price;
    }

    public void setSale_price(long sale_price) {
        this.sale_price = sale_price;
    }

    public long getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(long tax_rate) {
        this.tax_rate = tax_rate;
    }

    public long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(long total_count) {
        this.total_count = total_count;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
