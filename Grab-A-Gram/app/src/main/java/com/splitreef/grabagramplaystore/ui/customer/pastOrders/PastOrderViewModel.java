package com.splitreef.grabagramplaystore.ui.customer.pastOrders;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.GetPlacedOrdersResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PastOrderViewModel extends ViewModel {

    private static final String TAG = "ScheduledOrderViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetPastOrders = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public PastOrderViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }


    public  void getPastOrders()
    {
         firebaseRepository.getPastOrders()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<GetPlacedOrdersResponse>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {
                         disposable.add(d);
                         onGetPastOrders.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull GetPlacedOrdersResponse placedOrdersResponse) {

                         onGetPastOrders.setValue(DataResource.DataStatus(placedOrdersResponse));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onGetPastOrders.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }



    LiveData<DataResource> observeGetPastOrders()
    {
        return onGetPastOrders;
    }




}
