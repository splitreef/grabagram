package com.splitreef.grabagramplaystore.ui.driver.driverRegistration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentDriver;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRefrences;
import com.splitreef.grabagramplaystore.data.model.driver.DriverRegistrationRequest;
import com.splitreef.grabagramplaystore.databinding.FragmentDriverRefrencesBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.driver.signUpPhoto.SignUpPhotoActivity;
import com.splitreef.grabagramplaystore.ui.login.LoginActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.DriverRegistrationViewModalProviderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by  on 06-11-2020.
 */
public class DriverRefrencesFramgent extends BaseFragment implements View.OnClickListener {
    private FragmentDriverRefrencesBinding binding;
    private FragmentDriver fragmentDriver;
    private GrabAGramApplication grabAGramApplication;
    private DriverRegistrationRequest driverRegistrationRequest;
    private DriverRegistrationViewModel driverRegistrationViewModel;
    private Context context;
    int textLength1 = 0, textLength2 = 0, textLength3 = 0;
    private int photoTypeCount = 0;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public DriverRefrencesFramgent(FragmentDriver fragmentDriver) {
        this.fragmentDriver = fragmentDriver;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDriverRefrencesBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        grabAGramApplication = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        driverRegistrationViewModel = new ViewModelProvider(getViewModelStore(), new DriverRegistrationViewModalProviderFactory(
                Objects.requireNonNull(getActivity()).getApplication(), grabAGramApplication.firebaseRepository)).
                get(DriverRegistrationViewModel.class);

        if (getArguments() != null) {
            driverRegistrationRequest = (DriverRegistrationRequest) getArguments().getSerializable("requestData");
        }
        binding.btnContinue.setOnClickListener(this);
        subscribeObservers();

        binding.edtPhoneNumber1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.edtPhoneNumber1.getText().toString();
                textLength1 = binding.edtPhoneNumber1.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength1 == 1) {
                    if (!text.contains("(")) {
                        binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                    }
                } else if (textLength1 == 5) {
                    if (!text.contains(")")) {
                        binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                    }
                } else if (textLength1 == 6) {
                    binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                } else if (textLength1 == 10) {
                    if (!text.contains("-")) {
                        binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                    }
                } else if (textLength1 == 15) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                    }
                } else if (textLength1 == 18) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber1.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber1.setSelection(binding.edtPhoneNumber1.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        binding.edtPhoneNumber2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.edtPhoneNumber2.getText().toString();
                textLength2 = binding.edtPhoneNumber2.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength2 == 1) {
                    if (!text.contains("(")) {
                        binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                    }
                } else if (textLength2 == 5) {
                    if (!text.contains(")")) {
                        binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                    }
                } else if (textLength2 == 6) {
                    binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                } else if (textLength2 == 10) {
                    if (!text.contains("-")) {
                        binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                    }
                } else if (textLength2 == 15) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                    }
                } else if (textLength2 == 18) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber2.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber2.setSelection(binding.edtPhoneNumber2.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        binding.edtPhoneNumber3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = binding.edtPhoneNumber3.getText().toString();
                textLength3 = binding.edtPhoneNumber3.getText().length();
                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return;
                if (textLength3 == 1) {
                    if (!text.contains("(")) {
                        binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                        binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                    }
                } else if (textLength3 == 5) {
                    if (!text.contains(")")) {
                        binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                        binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                    }
                } else if (textLength3 == 6) {
                    binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                } else if (textLength3 == 10) {
                    if (!text.contains("-")) {
                        binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                    }
                } else if (textLength3 == 15) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                    }
                } else if (textLength3 == 18) {
                    if (text.contains("-")) {
                        binding.edtPhoneNumber3.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                        binding.edtPhoneNumber3.setSelection(binding.edtPhoneNumber3.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                moveNextScreen();
                break;
        }
    }


    // For account info Validation
    boolean isRefrenceInfoValidation() {

        if (Objects.requireNonNull(binding.edtFirstName1.getText()).toString().equals("")) {
            binding.edtFirstName1.setError("Please enter First Name");
            binding.edtFirstName1.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtLastName1.getText()).toString().equals("")) {
            binding.edtLastName1.setError("Please enter Last Name");
            binding.edtLastName1.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtEmail1.getText()).toString().equals("")) {
            binding.edtEmail1.setError("Please enter email");
            binding.edtEmail1.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(binding.edtEmail1.getText()).toString()).matches()) {
            binding.edtEmail1.setError("Please enter valid Email");
            binding.edtEmail1.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber1.getText()).toString().equals("")) {
            binding.edtPhoneNumber1.setError("Please enter mobile number");
            binding.edtPhoneNumber1.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber1.getText()).toString().length() < 14) {
            binding.edtPhoneNumber1.setError("Please Enter Valid Phone Number");
            binding.edtPhoneNumber1.requestFocus();
            return false;
        }

        /*-----------------------------------------*/

        if (Objects.requireNonNull(binding.edtFirstName2.getText()).toString().equals("")) {
            binding.edtFirstName2.setError("Please enter First Name");
            binding.edtFirstName2.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtLastName2.getText()).toString().equals("")) {
            binding.edtLastName2.setError("Please enter Last Name");
            binding.edtLastName2.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtEmail2.getText()).toString().equals("")) {
            binding.edtEmail2.setError("Please enter email");
            binding.edtEmail2.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(binding.edtEmail2.getText()).toString()).matches()) {
            binding.edtEmail2.setError("Please enter valid Email");
            binding.edtEmail2.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber2.getText()).toString().equals("")) {
            binding.edtPhoneNumber2.setError("Please enter mobile number");
            binding.edtPhoneNumber2.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber2.getText()).toString().length() < 14) {
            binding.edtPhoneNumber2.setError("Please Enter Valid Phone Number");
            binding.edtPhoneNumber2.requestFocus();
            return false;
        }

        /*--------------------*/
        if (Objects.requireNonNull(binding.edtFirstName3.getText()).toString().equals("")) {
            binding.edtFirstName3.setError("Please enter First Name");
            binding.edtFirstName3.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtLastName3.getText()).toString().equals("")) {
            binding.edtLastName3.setError("Please enter Last Name");
            binding.edtLastName3.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtEmail3.getText()).toString().equals("")) {
            binding.edtEmail3.setError("Please enter email");
            binding.edtEmail3.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(binding.edtEmail3.getText()).toString()).matches()) {
            binding.edtEmail3.setError("Please enter valid Email");
            binding.edtEmail3.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber3.getText()).toString().equals("")) {
            binding.edtPhoneNumber3.setError("Please enter mobile number");
            binding.edtPhoneNumber3.requestFocus();
            return false;
        } else if (Objects.requireNonNull(binding.edtPhoneNumber3.getText()).toString().length() < 14) {
            binding.edtPhoneNumber3.setError("Please Enter Valid Phone Number");
            binding.edtPhoneNumber3.requestFocus();
            return false;
        }
        return true;

    }

    void moveNextScreen() {
        if (isRefrenceInfoValidation()) {
            List<DriverRefrences> refrencesList = new ArrayList<>();
            DriverRefrences driverRefrences = new DriverRefrences();

            driverRefrences.setFirstName(binding.edtFirstName1.getText().toString());
            driverRefrences.setLastName(binding.edtLastName1.getText().toString());
            driverRefrences.setEmail(Objects.requireNonNull(binding.edtEmail1.getText()).toString());
            String tmp1 = binding.edtPhoneNumber1.getText().toString();
            String phone1 = tmp1.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            driverRefrences.setPhoneNumber(phone1);

            refrencesList.add(driverRefrences);
            /*--------------*/
            DriverRefrences driverRefrences2 = new DriverRefrences();

            driverRefrences2.setFirstName(binding.edtFirstName2.getText().toString());
            driverRefrences2.setLastName(binding.edtLastName2.getText().toString());
            driverRefrences2.setEmail(Objects.requireNonNull(binding.edtEmail2.getText()).toString());
            String tmp2 = binding.edtPhoneNumber2.getText().toString();
            String phone2 = tmp2.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            driverRefrences2.setPhoneNumber(phone2);
            refrencesList.add(driverRefrences2);

            /*----------------*/
            DriverRefrences driverRefrences3 = new DriverRefrences();

            driverRefrences3.setFirstName(binding.edtFirstName3.getText().toString());
            driverRefrences3.setLastName(binding.edtLastName3.getText().toString());
            driverRefrences3.setEmail(Objects.requireNonNull(binding.edtEmail3.getText()).toString());
            String tmp3 = binding.edtPhoneNumber3.getText().toString();
            String phone3 = tmp3.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
            driverRefrences3.setPhoneNumber(phone3);
            refrencesList.add(driverRefrences3);

            driverRegistrationRequest.setDriverRefrencesList(refrencesList);
            if (!NetworkManager.isNetworkAvailable(getContext())) {
                Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
            } else {
                addDriverData();
            }


        }
    }

    public void addDriverData() {
        driverRegistrationViewModel.registerUser(Objects.requireNonNull(driverRegistrationRequest.getEmail()).toString(), Objects.requireNonNull(driverRegistrationRequest.getPassword()).toString());

       /* if (driverRegistrationRequest != null) {
            driverRegistrationViewModel.addDriverData(driverRegistrationRequest);
        }*/

    }

    void subscribeObservers() {

        driverRegistrationViewModel.observeRegister().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS: {
                           // grabAGramApplication.dialogManager.dismissProgressDialog();
                            photoTypeCount = 1;
                            driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable(SignUpPhotoActivity.bitmapPhoto),
                                    UUID.randomUUID() + AppConstant.PHOTO + ".jpg");

                        }
                        break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "" + stateResource.message, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        driverRegistrationViewModel.observeDriverDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                           // grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                           // grabAGramApplication.dialogManager.dismissProgressDialog();
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    driverRegistrationRequest.setPhotoURl(url);
                                    photoTypeCount = 2;
                                    driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable
                                                    (DriverLicenceInfoFragment.bitmapDrivingLicence),
                                            UUID.randomUUID() + AppConstant.DRIVING_LICENCE + ".jpg");

                                } else if (photoTypeCount == 2) {
                                    driverRegistrationRequest.setDrivingLicenseUrl(url);
                                    photoTypeCount = 3;
                                    driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable
                                            (DriverLicenceInfoFragment.bitmapCarInsurancePolicy),
                                            UUID.randomUUID() + AppConstant.CAR_INSURANCE_POLICY + ".jpg");

                                } else if (photoTypeCount == 3) {
                                    driverRegistrationRequest.setCarInsurancePolicyUrl(url);
                                    photoTypeCount = 4;
                                    driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable
                                            (DriverLicenceInfoFragment.bitmapCarRegistration),
                                            UUID.randomUUID() + AppConstant.CAR_REGISTRATION + ".jpg");

                                } else if (photoTypeCount == 4) {
                                    driverRegistrationRequest.setCarRegistrationUrl(url);
                                    photoTypeCount = 5;
                                    driverRegistrationViewModel.uploadDriverDocPhoto(getFileDataFromDrawable
                                            (DriverLicenceInfoFragment.bitmapCareGiverCertificate),
                                            UUID.randomUUID() + AppConstant.CARE_GIVER_CERTIFICATE + ".jpg");

                                }else if (photoTypeCount == 5) {
                                    driverRegistrationRequest.setCareGiverCertificateURl(url);
                                    if (driverRegistrationRequest != null) {
                                        driverRegistrationViewModel.addDriverData(driverRegistrationRequest);
                                    }
                                }
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        driverRegistrationViewModel.observeAddDriverData().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                           // grabAGramApplication.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {
                                    startActivity(new Intent(getContext(), LoginActivity.class));
                                    Objects.requireNonNull(getActivity()).finish();
                                }
                            }.showSimpleDialog(getContext(), "Message", "Your have successfully registered", "Ok");
                            // application.dialogManager.displayProgressDialog(getActivity(),"Registration completed successfully");
                            break;
                        case ERROR:
                            grabAGramApplication.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


}
