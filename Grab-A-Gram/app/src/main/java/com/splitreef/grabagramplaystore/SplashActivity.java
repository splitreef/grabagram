package com.splitreef.grabagramplaystore;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowInsetsController;
import android.view.WindowManager;
import androidx.annotation.Nullable;

import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivitySplashBinding;
import com.splitreef.grabagramplaystore.ui.customer.dashboard.DashboardActivity;
import com.splitreef.grabagramplaystore.ui.customer.orderDetails.CustomerOrderDetailsActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.ordersDetails.OrdersDetailsActivity;
import com.splitreef.grabagramplaystore.ui.login.LoginActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by  on 04-11-2020.
 */
public class SplashActivity extends BaseActivity {
    private final String TAG=SplashActivity.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            final WindowInsetsController insetsController = getWindow().getInsetsController();
            if (insetsController != null) {
                insetsController.hide(WindowInsets.Type.statusBars());
            }
        } else {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            );
        }
        ActivitySplashBinding binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        CountDownTimer _countDownTimer = new CountDownTimer(2500, 10) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                navigateNewScreen();
            }
        };
        _countDownTimer.start();
    }

    void navigateNewScreen() {
        UserModel userModel = PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);

        if (userModel != null) {
            if (userModel.getRole().equals(AppConstant.CUSTOMER_ROLE)) {

                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("data") && userModel.isActivated_at()) {
                    Bundle bundle = getIntent().getExtras();
                    try {

                        JSONObject jsonObject = new JSONObject(String.valueOf(bundle.get("data")));
                        String orderId = jsonObject.getString(AppConstant.ORDER_ID);
                        Intent intent = new Intent(this, CustomerOrderDetailsActivity.class);
                        intent.putExtra(AppConstant.ORDER_ID, orderId);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        finish();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e.getMessage());
                    }
                } else {
                    if (userModel.isActivated_at()) {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    } else {
                        showToast("User is not activated.");
                        finish();
                    }
                }

            } else if (userModel.getRole().equals(AppConstant.DRIVER_ROLE)) {
                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("data")) {
                    Bundle bundle = getIntent().getExtras();

                    try {

                        JSONObject jsonObject = new JSONObject(String.valueOf(bundle.get("data")));
                        String orderId = jsonObject.getString(AppConstant.ORDER_ID);
                        Intent intent = new Intent(this, OrdersDetailsActivity.class);
                        intent.putExtra(AppConstant.ID, orderId);
                        startActivity(intent);
                        finish();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e.getMessage());
                    }
                } else {

                    if (userModel.isActivated_at())
                    {
                        CustomerRegisterRequest customerRegisterRequest = new CustomerRegisterRequest();
                        customerRegisterRequest.setEmailCommunication(userModel.getEmail_communication());
                        customerRegisterRequest.setEmail(userModel.getEmail());
                        customerRegisterRequest.setUsername(userModel.getName());
                        customerRegisterRequest.setRole(AppConstant.DRIVER_ROLE);

                        startActivity(new Intent(SplashActivity.this, HomeActivity.class)
                                .putExtra(AppConstant.FROM, AppConstant.FROM_SPLASH)
                                .putExtra("requestData", customerRegisterRequest));
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);

                    }
                    else
                    {
                        showToast("User is not activated.");
                    }


                }
            }
        } else {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }



        finish();
    }

}
