package com.splitreef.grabagramplaystore.data.model.customer.driverProfile;
import java.io.Serializable;


public class DriverProfile implements Serializable {
    private DriverAccount account;
    private DriverDocuments documents;
    private DriverAddress address;
    private String profile_image;
    private String user_id;
    private double lat;
    private double lng;

    public DriverAccount getAccount() {
        return account;
    }

    public void setAccount(DriverAccount account) {
        this.account = account;
    }

    public DriverDocuments getDocuments() {
        return documents;
    }

    public void setDocuments(DriverDocuments documents) {
        this.documents = documents;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public DriverAddress getAddress() {
        return address;
    }

    public void setAddress(DriverAddress address) {
        this.address = address;
    }
}
