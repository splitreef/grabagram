package com.splitreef.grabagramplaystore.data.model.driver.orders;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by  on 01-12-2020.
 */
public class OrdersModel implements Parcelable {
    private ArrayList<AddressesModel> addresses;
    private String assign_driver_id;
    private String busniess_name;
    private String confirmation_code;
    private long created_at;
    private String customer_email;
    private String customer_name;
    private long delivery_fee;
    private long delivery_fee_miles;
    private long delivery_fee_minute;
    private long delivery_miles;
    private long delivery_minute;
    private long discount_amount;
    private DispensaryDataModel dispensary_data;
    private GeoPoint dispensary_location;
    private Object grand_total;
    private String id;
    private boolean isSendNotification;
    private ArrayList<ItemsModel> items;
    private ArrayList<String> notification_drivers_id;
    private String order_no;
    private long myOrderNo;
    private String patient_id;
    private PaymentMethodModel payment_method;
    private long sales_tax;
    private String status;
    private Object sub_total;
    private long updated_at;
    private String user_id;
    private Double distance;

    public OrdersModel() {
    }

    public ArrayList<AddressesModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<AddressesModel> addresses) {
        this.addresses = addresses;
    }

    public String getAssign_driver_id() {
        return assign_driver_id;
    }

    public void setAssign_driver_id(String assign_driver_id) {
        this.assign_driver_id = assign_driver_id;
    }

    public String getBusniess_name() {
        return busniess_name;
    }

    public void setBusniess_name(String busniess_name) {
        this.busniess_name = busniess_name;
    }

    public String getConfirmation_code() {
        return confirmation_code;
    }

    public void setConfirmation_code(String confirmation_code) {
        this.confirmation_code = confirmation_code;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public long getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(long delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public long getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(long discount_amount) {
        this.discount_amount = discount_amount;
    }

    public DispensaryDataModel getDispensary_data() {
        return dispensary_data;
    }

    public void setDispensary_data(DispensaryDataModel dispensary_data) {
        this.dispensary_data = dispensary_data;
    }

    public GeoPoint getDispensary_location() {
        return dispensary_location;
    }

    public void setDispensary_location(GeoPoint dispensary_location) {
        this.dispensary_location = dispensary_location;
    }

    public Object getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(Object grand_total) {
        this.grand_total = grand_total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSendNotification() {
        return isSendNotification;
    }

    public void setSendNotification(boolean sendNotification) {
        isSendNotification = sendNotification;
    }

    public ArrayList<ItemsModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemsModel> items) {
        this.items = items;
    }

    public ArrayList<String> getNotification_drivers_id() {
        return notification_drivers_id;
    }

    public void setNotification_drivers_id(ArrayList<String> notification_drivers_id) {
        this.notification_drivers_id = notification_drivers_id;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public PaymentMethodModel getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(PaymentMethodModel payment_method) {
        this.payment_method = payment_method;
    }

    public long getSales_tax() {
        return sales_tax;
    }

    public void setSales_tax(long sales_tax) {
        this.sales_tax = sales_tax;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getSub_total() {
        return sub_total;
    }

    public void setSub_total(Object sub_total) {
        this.sub_total = sub_total;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static Creator<OrdersModel> getCREATOR() {
        return CREATOR;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public long getMyOrderNo() {
        return myOrderNo;
    }

    public void setMyOrderNo(long myOrderNo) {
        this.myOrderNo = myOrderNo;
    }

    public long getDelivery_fee_miles() {
        return delivery_fee_miles;
    }

    public void setDelivery_fee_miles(long delivery_fee_miles) {
        this.delivery_fee_miles = delivery_fee_miles;
    }

    public long getDelivery_fee_minute() {
        return delivery_fee_minute;
    }

    public void setDelivery_fee_minute(long delivery_fee_minute) {
        this.delivery_fee_minute = delivery_fee_minute;
    }

    public long getDelivery_miles() {
        return delivery_miles;
    }

    public void setDelivery_miles(long delivery_miles) {
        this.delivery_miles = delivery_miles;
    }

    public long getDelivery_minute() {
        return delivery_minute;
    }

    public void setDelivery_minute(long delivery_minute) {
        this.delivery_minute = delivery_minute;
    }

    protected OrdersModel(Parcel in) {
        /*if (in.readByte() == 0x01) {
            addresses = new ArrayList<AddressesModel>();
            in.readList(addresses, AddressesModel.class.getClassLoader());
        } else {
            addresses = null;
        }*/
        addresses = (ArrayList<AddressesModel>) in.readSerializable();
        assign_driver_id = in.readString();
        busniess_name = in.readString();
        confirmation_code = in.readString();
        created_at = in.readLong();
        customer_email = in.readString();
        customer_name = in.readString();
        delivery_fee = in.readLong();
        discount_amount = in.readLong();
        delivery_fee_miles = in.readLong();
        delivery_fee_minute = in.readLong();
        delivery_miles = in.readLong();
        delivery_minute = in.readLong();
        dispensary_data = (DispensaryDataModel) in.readValue(DispensaryDataModel.class.getClassLoader());
        dispensary_location = (GeoPoint) in.readValue(GeoPoint.class.getClassLoader());
        //grand_total = in.readLong();
        grand_total = new Gson().fromJson(in.readString(), Object.class);
        id = in.readString();
        isSendNotification = in.readByte() != 0x00;
        items = (ArrayList<ItemsModel>) in.readSerializable();
        notification_drivers_id = (ArrayList<String>) in.readSerializable();
        order_no = in.readString();
        patient_id = in.readString();
        payment_method = (PaymentMethodModel) in.readValue(PaymentMethodModel.class.getClassLoader());
        sales_tax = in.readLong();
        status = in.readString();
        // sub_total = in.readLong();
        sub_total = new Gson().fromJson(in.readString(), Object.class);
        updated_at = in.readLong();
        user_id = in.readString();
        distance = in.readDouble();
        myOrderNo = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       /* if (addresses == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(addresses);
        }*/
        dest.writeSerializable(addresses);
        dest.writeString(assign_driver_id);
        dest.writeString(busniess_name);
        dest.writeString(confirmation_code);
        dest.writeLong(created_at);
        dest.writeString(customer_email);
        dest.writeString(customer_name);
        dest.writeLong(delivery_fee);
        dest.writeLong(discount_amount);
        dest.writeLong(delivery_fee_miles);
        dest.writeLong(delivery_fee_minute);
        dest.writeLong(delivery_miles);
        dest.writeLong(delivery_minute);
        dest.writeValue(dispensary_data);
        dest.writeValue(dispensary_location);
        //dest.writeLong(grand_total);
        dest.writeString(new Gson().toJson(grand_total));
        dest.writeString(id);
        dest.writeByte((byte) (isSendNotification ? 0x01 : 0x00));
        dest.writeSerializable(items);
        dest.writeSerializable(notification_drivers_id);
        dest.writeString(order_no);
        dest.writeString(patient_id);
        dest.writeValue(payment_method);
        dest.writeLong(sales_tax);
        dest.writeString(status);
        //dest.writeLong(sub_total);
        dest.writeString(new Gson().toJson(sub_total));
        dest.writeLong(updated_at);
        dest.writeString(user_id);
        dest.writeDouble(distance);
        dest.writeLong(myOrderNo);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OrdersModel> CREATOR = new Parcelable.Creator<OrdersModel>() {
        @Override
        public OrdersModel createFromParcel(Parcel in) {
            return new OrdersModel(in);
        }

        @Override
        public OrdersModel[] newArray(int size) {
            return new OrdersModel[size];
        }
    };
}
