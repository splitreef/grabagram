package com.splitreef.grabagramplaystore.ui.driver.orders.pendingOrders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.driver.DriverPagerAdapter;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.databinding.FragmentPendingOrdersBinding;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;

/**
 * Created by  on 26-11-2020.
 */
public class PendingOrdersFrgament extends BaseFragment implements View.OnClickListener {
    private FragmentPendingOrdersBinding binding;
    private GrabAGramApplication application;
    private PendingOrdersViewModel viewModel;
    public DriverPagerAdapter adapter;
    public static MaterialButton btnSort;
    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPendingOrdersBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    public void initView() {
        HomeActivity.tvActiveStatus.setVisibility(View.GONE);
        btnSort = binding.btnSort;

        ((HomeActivity)context).setToolbarTitle("PENDING ORDERS");

        adapter = new DriverPagerAdapter(getContext(), getChildFragmentManager(), binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        binding.viewPager.setOffscreenPageLimit(0);

        // For change fragment on click of tab
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.btnFilter.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_filter:
                startActivity(new Intent(getContext(), FilterActivity.class));
                break;
        }
    }


}


