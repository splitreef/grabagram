package com.splitreef.grabagramplaystore.ui.customer.checkout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.adapter.cutomer.CheckoutAdapter;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.PaymentMethod;
import com.splitreef.grabagramplaystore.data.model.customer.AddressList;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;
import com.splitreef.grabagramplaystore.data.model.customer.OpeningHours;
import com.splitreef.grabagramplaystore.data.model.customer.OrderRequest;
import com.splitreef.grabagramplaystore.data.model.customer.OrderResponse;
import com.splitreef.grabagramplaystore.data.model.customer.UserAddressModel;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.GetProductCartResponse;
import com.splitreef.grabagramplaystore.data.model.customer.cartList.ProductCart;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponModel;
import com.splitreef.grabagramplaystore.data.model.customer.coupon.CouponResponse;
import com.splitreef.grabagramplaystore.data.model.customer.distanceMatrix.Distance;
import com.splitreef.grabagramplaystore.data.model.customer.distanceMatrix.DistanceMatrixReponse;
import com.splitreef.grabagramplaystore.data.model.customer.distanceMatrix.Duration;
import com.splitreef.grabagramplaystore.data.model.customer.distanceMatrix.Element;
import com.splitreef.grabagramplaystore.data.model.customer.driverTokens.DriverTokens;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerAccount;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerDocumentInfo;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.CustomerUser;
import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityCheckoutBinding;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.enums.WeekDayEnum;
import com.splitreef.grabagramplaystore.fcmNotification.VolleySingleton;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.ui.customer.thankYouScreen.ThankYouActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.AppUtil;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.CheckoutProviderFactory;

import net.authorize.acceptsdk.AcceptSDKApiClient;
import net.authorize.acceptsdk.datamodel.common.Message;
import net.authorize.acceptsdk.datamodel.merchant.ClientKeyBasedMerchantAuthentication;
import net.authorize.acceptsdk.datamodel.transaction.CardData;
import net.authorize.acceptsdk.datamodel.transaction.EncryptTransactionObject;
import net.authorize.acceptsdk.datamodel.transaction.TransactionObject;
import net.authorize.acceptsdk.datamodel.transaction.TransactionType;
import net.authorize.acceptsdk.datamodel.transaction.callbacks.EncryptTransactionCallback;
import net.authorize.acceptsdk.datamodel.transaction.response.EncryptTransactionResponse;
import net.authorize.acceptsdk.datamodel.transaction.response.ErrorTransactionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_API_KEY;
import static com.splitreef.grabagramplaystore.utils.AppConstant.GOOGLE_DISTANCE_MATRIX_API_URL;
import static com.splitreef.grabagramplaystore.utils.AppConstant.NET_AUTHORISE_CLIENT_KEY;

public class CheckoutActivity extends BaseActivity implements View.OnClickListener, EncryptTransactionCallback {
    private static String TAG="CheckoutActivity";
    private ActivityCheckoutBinding binding;
    private CheckoutViewModel viewModel;
    private GrabAGramApplication application;
    private GetProductCartResponse getProductCartResponse;
    private ArrayList<UserAddressModel>userAddressList;
    private  DispensaryModel dispensaryModel;
    private double shippingLat=0;
    private double shippingLng=0;
    double subTotal=0;
    double salesTax=0;
    double deliveryFee=0;
    double discount=0;
    double total=0;
    double deliveryMiles = 0;
    int deliveryMinute  = 0;
    double deliveryFeeMinute=0;
    double deliveryFeeMiles=0;
    private ArrayList<String>driverTokenList=null;
    private boolean isDispensaryOpen= false;
    private ArrayList<String>driverIdList;
    private boolean isUpdateNotificationStatus=true;
    private String shippingUsername;
    private String shippingFirstName;
    private String shippingLastName;
    private UserAddressModel shippingUserAddressModel;
    private UserAddressModel billingUserAddressModel;
    private int textLength=0;
    private final String YEAR_PREFIX = "20";
    private AcceptSDKApiClient apiClient;
    private String cardNumber;
    private String month;
    private String year;
    private String cvv;
    private OrderResponse orderResponse=null;
    private String paymentToken;
    private String descriptorData;
    private CouponModel couponModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityCheckoutBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
        initListeners();
    }

    public void initView()
    {
        application = (GrabAGramApplication) Objects.requireNonNull(getApplicationContext());

        viewModel = new ViewModelProvider(getViewModelStore(), new CheckoutProviderFactory(application.firebaseRepository)).get(CheckoutViewModel.class);

         /*
       build an Accept SDK Api client to make API calls.
       parameters:
         1) Context - current context
         2) AcceptSDKApiClient.Environment - Authorize.net ENVIRONMENT
    */

        try {
            apiClient = new AcceptSDKApiClient.Builder(CheckoutActivity.this,
                    AcceptSDKApiClient.Environment.PRODUCTION).connectionTimeout(
                    4000) // optional connection time out in milliseconds
                    .build();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        subscribeObservers();

        if (getIntent()!=null) {
            if (getIntent().getStringExtra(AppConstant.FROM).equals(AppConstant.FROM_CART_SCREEN))
            {
                 getProductCartResponse= (GetProductCartResponse) getIntent().getSerializableExtra(AppConstant.GET_PRODUCT_CART_RESPONSE);
            }
        }

        binding.rvCart.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this,LinearLayoutManager.VERTICAL,false));

        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.arrowback);

        setUpCreditCardEditText();
        setUpExpiryDate();
    }

    public void initListeners()
    {
        binding.txtChangeBillingAddress.setOnClickListener(this);
        binding.txtChangeShippingAddress.setOnClickListener(this);
        binding.btnPlaceYourOrder.setOnClickListener(this);
        binding.btnPlaceYourOrder2.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SpannableString content = new SpannableString("Add");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        binding.txtChangeBillingAddress.setText(content);
        binding.txtChangeShippingAddress.setText(content);
        binding.txtBillingAddress.setText("");
        binding.txtShippingAddress.setText("");
        viewModel.getAddressList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void subscribeObservers() {
        viewModel.observeGetAddressList().observe(Objects.requireNonNull(CheckoutActivity.this), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(CheckoutActivity.this,"Loading");
                        break;
                    case SUCCESS:
                       // application.dialogManager.dismissProgressDialog();
                        viewModel.getDispensary(getProductCartResponse.getDispensary_id());
                        AddressList addressList=(AddressList)dataResource.data;
                        setAddress(addressList);
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        break;
                }
            }

        });


        viewModel.observeGetDispensary().observe(Objects.requireNonNull(CheckoutActivity.this), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                     //   application.dialogManager.displayProgressDialog(CheckoutActivity.this,"Loading");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        dispensaryModel=(DispensaryModel)dataResource.data;
                        callDistanceMatrixApi();
                        getDriverTokens();
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        break;
                }
            }

        });

        viewModel.observePlaceOrder().observe(Objects.requireNonNull(CheckoutActivity.this), dataResource -> {
            if (dataResource!=null)
            {
                switch (dataResource.status)
                {
                    case LOADING:
                           //application.dialogManager.displayProgressDialog(CheckoutActivity.this,"Loading");
                        break;
                    case SUCCESS:
                        //application.dialogManager.dismissProgressDialog();
                        orderResponse = (OrderResponse) dataResource.data;
                        if (orderResponse.getStatus().equals(AppConstant.SUCCESS))
                        {
                            setPaymentData();
                        }
                        else
                        {
                            showToast("Something went wrong!");
                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        break;
                }
            }

        });

        viewModel.observeOngGetCouponProduct().observe(CheckoutActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                       // application.dialogManager.displayProgressDialog(CheckoutActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                       // application.dialogManager.dismissProgressDialog();
                        CouponResponse couponResponse = (CouponResponse) dataResource.data;
                        if (couponResponse.getStatus().equals(AppConstant.SUCCESS))
                        {
                            checkCouponCodeUserLimit(couponResponse);
                        }
                        else
                        {
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {
                                    finish();
                                }
                            }.showSimpleDialog(CheckoutActivity.this,"Information","Coupon code is expired.","OK");

                        }

                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });


        viewModel.observeOnGetDriversTokens().observe(CheckoutActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                        DriverTokens driverTokens = (DriverTokens) dataResource.data;
                        if (driverTokens!=null)
                        {
                            if (driverTokens.getStatus().equals(AppConstant.SUCCESS))
                            {
                               driverTokenList=driverTokens.getFcmToken();
                               driverIdList=driverTokens.getDrivers_id();
                            }
                        }
                        if (dispensaryModel!=null && dispensaryModel.getOpening_hours()!=null && dispensaryModel.getOpening_hours().size()>0)
                        {
                            setHoursOfOperation(dispensaryModel.getOpening_hours());
                        }

                        break;
                    case ERROR:

                }
            }
        });


        viewModel.observeUpdateOrderNotificationStatus().observe(CheckoutActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            application.dialogManager.dismissProgressDialog();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });

        viewModel.observeUpdatePaymentStatus().observe(CheckoutActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:

                            if (couponModel!=null && getProductCartResponse !=null)
                            {
                                if ((!getProductCartResponse.getCoupon_code_name().equals("")) && (getProductCartResponse.getCoupon_code_name()!=null))
                                {
                                    viewModel.updateCouponCodeLimit(couponModel.getUserUsedLimit()+1,couponModel.getId());
                                }
                                else
                                {
                                    redirectToThankYouScreen(orderResponse);
                                    application.dialogManager.dismissProgressDialog();
                                }
                            }
                            else
                            {
                                redirectToThankYouScreen(orderResponse);
                                application.dialogManager.dismissProgressDialog();
                            }


                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });

        viewModel.observeUpdateCouponLimit().observe(CheckoutActivity.this, new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {
                if (stateResource!=null)
                {
                    switch (stateResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            redirectToThankYouScreen(orderResponse);
                            application.dialogManager.dismissProgressDialog();
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();

                    }
                }
            }
        });


    }

    private void getDriverTokens()
    {
        boolean isDistanceFilter=PreferenceManger.getPreferenceManger().getBoolean(PrefKeys.IS_DISTANCE_FILTER);
        if (isDistanceFilter)
        {
            String distanceIDStr=PreferenceManger.getPreferenceManger().getString(PrefKeys.DISTANCE_ID);
            int distance= Integer.parseInt(distanceIDStr);

            viewModel.getDriversTokens(dispensaryModel.getLocation().getLatitude(),dispensaryModel.getLocation().getLongitude(),distance);
        }
        else
        {
            viewModel.getDriversTokens(dispensaryModel.getLocation().getLatitude(),dispensaryModel.getLocation().getLongitude(),AppConstant.DEFAULT_DISTANCE);
        }
    }

    void redirectToThankYouScreen(OrderResponse orderResponse)
    {
        if (isDispensaryOpen)
        {
            if (driverTokenList!=null && driverTokenList.size()>0)
            {
                for (int i=0;i<driverTokenList.size();i++)
                {
                    if (driverTokenList.get(i)!=null)
                    {
                        sendNotificationToDriver(orderResponse,driverTokenList.get(i));
                        Log.d("driverTokenId",""+driverTokenList.get(i));
                    }
                }

            }
        }

        PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,"0");
        Intent intent = new Intent(CheckoutActivity.this, ThankYouActivity.class);
        intent.putExtra(AppConstant.ORDER_RESPONSE,orderResponse);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();

    }

    void sendNotificationToDriver(OrderResponse orderResponse,String driverTokenId)
    {
        UserModel userModel=PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER,UserModel.class);

        JSONObject notification = new JSONObject();
        JSONObject notificationBody = new JSONObject();
        JSONObject notificationMessage = new JSONObject();
        JSONObject notificationData= new JSONObject();

        try {
            notificationMessage.put(AppConstant.TITLE, "Grab a Gram");
            notificationMessage.put(AppConstant.BODY, "New order");
            notificationMessage.put(AppConstant.BADGE_COUNT, 0);
            notificationMessage.put(AppConstant.SOUND, "default");

            notificationBody.put(AppConstant.TITLE, "Grab a Gram");
            notificationBody.put("message", "New order");
            notificationBody.put("body", "New order");
            notificationBody.put("name", "Grab a Gram");
            notificationBody.put(AppConstant.ORDER_ID, orderResponse.getOrderId());
            notificationBody.put("order_display_no", orderResponse.getOrderNo());
            notificationBody.put("order_user_id", ""+userModel.getUser_id());
            notificationBody.put("dispensary_name", ""+dispensaryModel.getBusniess_name());
            notificationBody.put("notification_type", "new_order");
            notificationBody.put(AppConstant.USER_TYPE, userModel.getRole());

            notificationData.put("data",notificationBody);

            notification.put("to", driverTokenId);
            notification.put("notification", notificationMessage);
            notification.put("data", notificationData);


            sendNotification(notification,orderResponse.getOrderId());

        } catch (JSONException e) {
            Log.e("TAG", "onCreate: " + e.getMessage());
        }
    }

    private void setAddress(AddressList addressList) {

        userAddressList=new ArrayList<>();

        if (addressList!=null && addressList.getAddresses()!=null && addressList.getAddresses().size()>0)
        {
            UserAddressModel personalAddressType=filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PERSONAL_ADDRESS, addressList.getAddresses());
            if (personalAddressType!=null && addressList.getIsPersonAddress()) {

                shippingLat = personalAddressType.getLatitude();
                shippingLng = personalAddressType.getLongitude();
                shippingUserAddressModel=personalAddressType;


                MyAccountResponse myAccountResponse=PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);
                if (myAccountResponse!=null && myAccountResponse.getAccount()!=null)
                {
                    CustomerAccount customerAccount=myAccountResponse.getAccount();
                    shippingUsername=customerAccount.getFirst_name()+" "+customerAccount.getLast_name();
                    shippingUserAddressModel.setFirst_name(customerAccount.getFirst_name());
                    shippingUserAddressModel.setLast_name(customerAccount.getLast_name());
                    shippingFirstName=customerAccount.getFirst_name();
                    shippingLastName=customerAccount.getLast_name();
                }


                personalAddressType.setFirst_name("");
                personalAddressType.setLast_name("");
                personalAddressType.setAddress_proof("");
                userAddressList.add(personalAddressType);

                String address = personalAddressType.getStreet_1() + "\n"+
                        personalAddressType.getCity() + "," + personalAddressType.getState() + "," + personalAddressType.getZip_code() + "\n" + personalAddressType.getPhone_number();

                SpannableString content = new SpannableString("Change");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                binding.txtShippingAddress.setText(address);
                binding.txtChangeShippingAddress.setText(content);

            }


            UserAddressModel primaryAddressType=filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_PRIMARY_ADDRESS, addressList.getAddresses());
            if (primaryAddressType!=null) {

                shippingLat=primaryAddressType.getLatitude();
                shippingLng=primaryAddressType.getLongitude();
                shippingUserAddressModel=primaryAddressType;
                shippingFirstName=primaryAddressType.getFirst_name();
                shippingLastName=primaryAddressType.getLast_name();

                shippingUsername=primaryAddressType.getFirst_name()+" "+primaryAddressType.getLast_name();

                userAddressList.add(primaryAddressType);

                String address = primaryAddressType.getStreet_1()+"\n"+
                        primaryAddressType.getCity()+","+primaryAddressType.getState()+","+primaryAddressType.getZip_code()+"\n"+primaryAddressType.getPhone_number();

                binding.txtShippingAddress.setText(address);

                SpannableString content = new SpannableString("Change");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                binding.txtChangeShippingAddress.setText(content);

            }

            if (userAddressList.size()==1)
            {
                UserAddressModel billingAddressType=filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS, addressList.getAddresses());
                if (billingAddressType!=null)
                {
                    billingAddressType.setAddress_proof("");
                    userAddressList.add(billingAddressType);

                    String address = billingAddressType.getStreet_1()+"\n"+
                            billingAddressType.getCity()+","+billingAddressType.getState()+","+billingAddressType.getZip_code()+"\n"+billingAddressType.getPhone_number();

                    billingUserAddressModel=billingAddressType;

                    binding.txtBillingAddress.setText(address);
                    SpannableString content = new SpannableString("Change");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    binding.txtChangeBillingAddress.setText(content);

                }

            }
            else
            {
                UserAddressModel secondaryAddressType=filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_SECONDARY_SHIPPING_ADDRESS, addressList.getAddresses());
                if (secondaryAddressType!=null)
                {
                    shippingLat=secondaryAddressType.getLatitude();
                    shippingLng=secondaryAddressType.getLongitude();
                    userAddressList.add(secondaryAddressType);
                    shippingUserAddressModel=secondaryAddressType;

                    shippingFirstName=secondaryAddressType.getFirst_name();
                    shippingLastName=secondaryAddressType.getLast_name();


                    shippingUsername=secondaryAddressType.getFirst_name()+" "+secondaryAddressType.getLast_name();

                    String address = secondaryAddressType.getStreet_1()+"\n"+
                            secondaryAddressType.getCity()+","+secondaryAddressType.getState()+","+secondaryAddressType.getZip_code()+"\n"+secondaryAddressType.getPhone_number();


                    binding.txtShippingAddress.setText(address);
                    SpannableString content = new SpannableString("Change");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    binding.txtChangeShippingAddress.setText(content);
                }


                UserAddressModel billingAddressType=filterAddressType(AppConstant.KEY_ADDRESS_TYPE_DEFAULT_BILLING_ADDRESS, addressList.getAddresses());
                if (billingAddressType!=null)
                {
                    userAddressList.add(billingAddressType);

                    String address = billingAddressType.getStreet_1()+"\n"+
                            billingAddressType.getCity()+","+billingAddressType.getState()+","+billingAddressType.getZip_code()+"\n"+billingAddressType.getPhone_number();

                    billingUserAddressModel=billingAddressType;
                    binding.txtBillingAddress.setText(address);
                    SpannableString content = new SpannableString("Change");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    binding.txtChangeBillingAddress.setText(content);
                }

            }

        }
    }

    private UserAddressModel filterAddressType(String addressType, ArrayList<UserAddressModel>list)
    {
        UserAddressModel userAddressModel=null;

         for (int i=0;i<list.size();i++)
         {
             if (addressType.equals(list.get(i).getType()))
             {
                 userAddressModel = list.get(i);
                 break;
             }
         }

         return userAddressModel;
    }


    void updateUi(GetProductCartResponse productCartResponse)
    {
        if (productCartResponse!=null && productCartResponse.getProductCarts()!=null && productCartResponse.getProductCarts().size()>0)
        {
            CheckoutAdapter cartAdapter=new CheckoutAdapter(CheckoutActivity.this,productCartResponse.getProductCarts());
            binding.rvCart.setAdapter(cartAdapter);
           // generateBill(productCartResponse);

            try {
                JSONObject jsonParentObject = new JSONObject();


                jsonParentObject.put("from_country","US");
                jsonParentObject.put("from_zip",dispensaryModel.getAddress().getZip_code());
                jsonParentObject.put("from_state",AppUtil.getStateCode(dispensaryModel.getAddress().getState()));
                jsonParentObject.put("to_country","US");
                jsonParentObject.put("to_zip",shippingUserAddressModel.getZip_code());
                jsonParentObject.put("to_state",AppUtil.getStateCode(shippingUserAddressModel.getState()));
                jsonParentObject.put("amount",0);
                jsonParentObject.put("shipping",deliveryFee);

                JSONArray jsonLineItemsArray=new JSONArray();
                for (int i=0; i<productCartResponse.getProductCarts().size();i++) {
                    ProductCart productCart = productCartResponse.getProductCarts().get(i);
                    int quantity = productCart.getQuantity();
                    double salePrice = productCart.getSale_price();

                    JSONObject jsonLineItemObject = new JSONObject();
                    jsonLineItemObject.put("quantity", quantity);
                    jsonLineItemObject.put("unit_price", salePrice);
                    jsonLineItemObject.put("product_tax_code", productCart.getId());

                    jsonLineItemsArray.put(i, jsonLineItemObject);
                }

                jsonParentObject.put("line_items",jsonLineItemsArray);

                callTaxJarApi(jsonParentObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }

           // generateBill(productCartResponse);
        }
    }


    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    void generateBill(GetProductCartResponse productCartResponse)
    {
         subTotal=0;
         //salesTax=0;
         //deliveryFee=50;
         discount=0;
         total=0;

        for (int i=0; i<productCartResponse.getProductCarts().size();i++)
        {
            ProductCart productCart=productCartResponse.getProductCarts().get(i);
            int quantity=productCart.getQuantity();
            subTotal =subTotal+(quantity*productCart.getSale_price());
           // salesTax=salesTax+productCart.getTax_rate();

        }

        discount=productCartResponse.getTotal_discount();

        total=subTotal+salesTax+deliveryFee-discount;

        total=Math.round(total * 100.0) / 100.0;

        binding.txtSubtotal.setText(""+String.format("$%.2f", subTotal));
        binding.txtDeliveryFee.setText(""+String.format("$%.2f", deliveryFee));
        binding.txtSalesTax.setText(""+String.format("$%.2f", salesTax));
        binding.txtDiscount.setText(""+String.format("$%.2f", discount));
        binding.txtTotal.setText(""+String.format("$%.2f", total));

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_change_billing_address:
            case R.id.txt_change_shipping_address:
                  navigateToManageAddressScreen();
                  break;
            case R.id.btn_place_your_order:
            case R.id.btn_place_your_order2:

                if (isPaymentValidation())
                {
                    String cardNoTmp = Objects.requireNonNull(binding.editCardNumber.getText()).toString();
                    cardNumber = cardNoTmp.replaceAll(" ", "");

                    String expiryDateArray[]=binding.editCardExpiryDate.getText().toString().split("/");

                    month= expiryDateArray[0];
                    year=YEAR_PREFIX+expiryDateArray[1];
                    cvv=binding.editCardCvc.getText().toString();

                    callPaymentMethod();

                }
                break;
        }
    }

    void navigateToManageAddressScreen()
    {
        startActivity(new Intent(CheckoutActivity.this, DispensaryListActivity.class)
                .putExtra(AppConstant.FROM,AppConstant.FROM_CHECKOUT_SCREEN)
                .putExtra(AppConstant.FOR,AppConstant.FOR_MANAGE_ADDRESS));
        overridePendingTransition(R.anim.right_in, R.anim.left_out);


    }


    void placeOrder()
    {
        MyAccountResponse myAccountResponse=PreferenceManger.getPreferenceManger().getObject(PrefKeys.MY_ACCOUNT_DATA, MyAccountResponse.class);

        // String uuiId=UUID.randomUUID().toString().replace("-","").substring(0,6);

        Random random = new Random();
        @SuppressLint("DefaultLocale") String uuiId = String.format("%04d", random.nextInt(10000));

        PaymentMethod paymentMethod=new PaymentMethod();

        paymentMethod.setAccountType("");
        paymentMethod.setAuthCode("");
        paymentMethod.setResponseCode("");
        paymentMethod.setTransationId("");

        OrderRequest orderRequest=new OrderRequest();
        orderRequest.setAddresses(userAddressList);
        orderRequest.setAssign_driver_id("");
        orderRequest.setBusniess_name(dispensaryModel.getBusniess_name());
        orderRequest.setConfirmation_code(uuiId);
        orderRequest.setCreated_at(AppUtil.getCurrentTimeInMillisecond());

        orderRequest.setCustomer_name(shippingUsername);
        orderRequest.setDelivery_fee(deliveryFee);
        orderRequest.setDiscount_amount(discount);
        orderRequest.setDispensary_data(dispensaryModel);
        orderRequest.setGrand_total(total);
        orderRequest.setSendNotification(false);
        orderRequest.setItems(getProductCartResponse.getProductCarts());
        orderRequest.setOrder_no("");

        orderRequest.setPayment_method(paymentMethod);
        orderRequest.setSales_tax(salesTax);
        orderRequest.setStatus(AppConstant.ORDER_TYPE_SCHEDULED);
        orderRequest.setSub_total(subTotal);
        orderRequest.setUpdated_at(AppUtil.getCurrentTimeInMillisecond());

        orderRequest.setDelivery_fee_miles(deliveryFeeMiles);
        orderRequest.setDelivery_fee_minute(deliveryFeeMinute);
        orderRequest.setDelivery_miles(deliveryMiles);
        orderRequest.setDelivery_minute(deliveryMinute);
        orderRequest.setDriverIdList(driverIdList);
        orderRequest.setPayment_success(false);


        if (myAccountResponse!=null)
        {
            if (myAccountResponse.getUser()!=null)
            {
                CustomerUser customerUser=myAccountResponse.getUser();
                orderRequest.setCustomer_email(customerUser.getEmail());
                orderRequest.setUser_id(customerUser.getUser_id());
            }

            if (myAccountResponse.getDocument_info()!=null)
            {
                CustomerDocumentInfo customerDocumentInfo=myAccountResponse.getDocument_info();
                orderRequest.setPatient_id(customerDocumentInfo.getPatient_id());
            }
        }

        viewModel.placeOrder(orderRequest);

        Log.d("order_request", ""+new Gson().toJson(orderRequest));
    }


    void checkCouponCodeUserLimit(CouponResponse couponResponse) {

         couponModel = couponResponse.getCouponModel();
        if (couponModel != null) {

            if (couponModel.getUsage_limit_per_user() > couponModel.getUserUsedLimit()) {
                  placeOrder();
            } else {

                new SimpleDialog() {
                    @Override
                    public void onPositiveButtonClick() {
                      finish();
                    }
                }.showSimpleDialog(CheckoutActivity.this,"Information",AppConstant.USER_COUPON_LIMIT,"OK");
            }
        } else {

            new SimpleDialog() {
                @Override
                public void onPositiveButtonClick() {
                    finish();
                }
            }.showSimpleDialog(CheckoutActivity.this,"Information",AppConstant.COUPON_NOT_VALID,"OK");

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,  R.anim.close);
    }


    void callDistanceMatrixApi() {

      //  String googleApiUrl = GOOGLE_DISTANCE_MATRIX_API_URL + "?origins=22.7578965,75.8730916&destinations=22.724355,75.8838944"+"&key=" + GOOGLE_API_KEY;
        String googleApiUrl = GOOGLE_DISTANCE_MATRIX_API_URL + "?origins="+dispensaryModel.getLocation().getLatitude()+","+dispensaryModel.getLocation().getLongitude()+"&destinations="+shippingLat+","+shippingLng+"&key=" + GOOGLE_API_KEY;
        Log.d("googleApiUrl",""+googleApiUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                googleApiUrl, new Response.Listener<String>() {

            public void onResponse(String response) {

                application.dialogManager.dismissProgressDialog();

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");

                        if (status.equals("OK")) {

                            DistanceMatrixReponse distanceMatrixReponse=new Gson().fromJson(jsonObject.toString(), DistanceMatrixReponse.class);

                            if (distanceMatrixReponse!=null && distanceMatrixReponse.getRows() !=null &&
                                    distanceMatrixReponse.getRows().size()>0 &&
                                    distanceMatrixReponse.getRows().get(0).getElements()!=null &&
                                    distanceMatrixReponse.getRows().get(0).getElements().size()>0 &&  distanceMatrixReponse.getRows().get(0).getElements().get(0)!=null &&
                                    distanceMatrixReponse.getRows().get(0).getElements().get(0).getStatus().equals("OK"))
                            {
                                Element element=distanceMatrixReponse.getRows().get(0).getElements().get(0);
                                calculateDeliveryFee(element);

                            }
                            else
                            {
                                //showToast("Invalid address");
                               // finish();
                                calculateDeliveryFeeByAndroidFramework();
                            }

                        }
                        else
                        {
                           // showToast("Invalid address");
                           // finish();
                            calculateDeliveryFeeByAndroidFramework();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                      //  binding.btnPlaceYourOrder.setEnabled(false);
                       // binding.btnPlaceYourOrder2.setEnabled(false);
                        calculateDeliveryFeeByAndroidFramework();
                    }


                }
                else
                {
                    calculateDeliveryFeeByAndroidFramework();
                }
                Log.d("Response", response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
               // showToast("Something went wrong");
                calculateDeliveryFeeByAndroidFramework();
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
        requestQueue.add(stringRequest);
    }

    public void calculateDeliveryFee(Element element)
    {
       Distance distance=element.getDistance();
       Duration duration=element.getDuration();
        deliveryMiles = distance.getValue()/1609.34;
        deliveryMiles = Math.round(deliveryMiles * 100.0) / 100.0;// for round off
        deliveryMinute  = duration.getValue()/60;
        deliveryFeeMinute=deliveryMinute*0.15;
        deliveryFeeMinute=Math.round(deliveryFeeMinute * 100.0) / 100.0;
        deliveryFeeMiles=0;

        if (deliveryMiles<=5)
       {
           deliveryFeeMiles=14;
       }
        else if (deliveryMiles<=10)
        {
            deliveryFeeMiles=24;
        }
        else if (deliveryMiles>10)
        {
            double remainingMiles=deliveryMiles-10;
            deliveryFeeMiles=(remainingMiles*0.99)+24;
        }

        deliveryFeeMiles = Math.round(deliveryFeeMiles * 100.0) / 100.0;
        deliveryFee= deliveryFeeMiles+deliveryFeeMinute;
        Log.d("deliveryFeeMiles=",""+deliveryMiles);

        Log.d("deliveryMiles=",""+deliveryMiles);
        Log.d("deliveryMinute=",""+deliveryMinute);
        Log.d("deliveryFeeMinute=",""+deliveryFeeMinute);
        Log.d("deliveryFeeMiles=",""+deliveryFeeMiles);
        Log.d("deliveryFee=",""+deliveryFee);

        updateUi(getProductCartResponse);

    }


    public void calculateDeliveryFeeByAndroidFramework()
    {
        deliveryMiles = new GetDistance().distance(dispensaryModel.getLocation().getLatitude(), dispensaryModel.getLocation().getLongitude(), shippingLat, shippingLng);


       // Distance distance=element.getDistance();
      //  Duration duration=element.getDuration();
      //  deliveryMiles = distance.getValue()/1609.34;

        deliveryMiles = Math.round(deliveryMiles * 100.0) / 100.0;// for round off
        deliveryMinute  = 5;
        deliveryFeeMinute=deliveryMinute*0.15;
        deliveryFeeMinute=Math.round(deliveryFeeMinute * 100.0) / 100.0;
        deliveryFeeMiles=0;

        if (deliveryMiles<=5)
        {
            deliveryFeeMiles=14;
        }
        else if (deliveryMiles<=10)
        {
            deliveryFeeMiles=24;
        }
        else if (deliveryMiles>10)
        {
            double remainingMiles=deliveryMiles-10;
            deliveryFeeMiles=(remainingMiles*0.99)+24;
        }

        deliveryFeeMiles = Math.round(deliveryFeeMiles * 100.0) / 100.0;
        deliveryFee= deliveryFeeMiles+deliveryFeeMinute;

        Log.d("deliveryMiles==",""+deliveryMiles);
        Log.d("deliveryMinute==",""+deliveryMinute);
        Log.d("deliveryFeeMinute==",""+deliveryFeeMinute);
        Log.d("deliveryFeeMiles==",""+deliveryFeeMiles);
        Log.d("deliveryFee==",""+deliveryFee);
        updateUi(getProductCartResponse);

    }

    void setHoursOfOperation(ArrayList<OpeningHours>arrayList)
    {
        for (int i=0;i<arrayList.size();i++)
        {
            OpeningHours openingHours=arrayList.get(i);
            if (openingHours!=null)
            {
                if (WeekDayEnum.Monday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Tuesday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Wednesday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Thursday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Friday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Saturday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
                else if (WeekDayEnum.Sunday.isEqual(openingHours.getDay()))
                {
                    setOpeningTime(openingHours,openingHours.getDay());
                }
            }
        }
    }

    public void setOpeningTime(OpeningHours openingHours, String day)
    {

        if (day.equals(AppUtil.getDayOfWeek()))
        {
            if (AppUtil.isDispensaryOpen(openingHours.getIn_time(),openingHours.getOut_time()))
            {
                isDispensaryOpen = true;
            }

        }

    }

    public void sendNotification(JSONObject notification,String orderId) {
        Log.i(TAG, "JSONObject: " + notification.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(AppConstant.FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "onResponse: " + response.toString());

                        if (isUpdateNotificationStatus)
                        {
                            isUpdateNotificationStatus=false;
                            viewModel.updateCartProduct(true,orderId);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CheckoutActivity.this, "Request error", Toast.LENGTH_LONG).show();
                        Log.i(TAG, "onErrorResponse: Didn't work");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", AppConstant.SERVER_KEY_);
                params.put("Content-Type", AppConstant.CONTENT_TYPE);
                return params;
            }
        };
        VolleySingleton.getInstance(CheckoutActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void setUpCreditCardEditText() {
        binding.editCardNumber.addTextChangedListener(new TextWatcher() {
            private boolean spaceDeleted;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // check if a space was deleted
                CharSequence charDeleted = s.subSequence(start, start + count);
                spaceDeleted = " ".equals(charDeleted.toString());
            }

            public void afterTextChanged(Editable editable) {
                // disable text watcher
                binding.editCardNumber.removeTextChangedListener(this);

                // record cursor position as setting the text in the textview
                // places the cursor at the end
                int cursorPosition = binding.editCardNumber.getSelectionStart();
                String withSpaces = formatText(editable);
                binding.editCardNumber.setText(withSpaces);
                // set the cursor at the last position + the spaces added since the
                // space are always added before the cursor
                binding.editCardNumber.setSelection(cursorPosition + (withSpaces.length() - editable.length()));

                // if a space was deleted also deleted just move the cursor
                // before the space
                if (spaceDeleted) {
                    binding.editCardNumber.setSelection(binding.editCardNumber.getSelectionStart() - 1);
                    spaceDeleted = false;
                }

                // enable text watcher
                binding.editCardNumber.addTextChangedListener(this);
            }

            private String formatText(CharSequence text) {
                StringBuilder formatted = new StringBuilder();
                int count = 0;
                for (int i = 0; i < text.length(); ++i) {
                    if (Character.isDigit(text.charAt(i))) {
                        if (count % 4 == 0 && count > 0) formatted.append(" ");
                        formatted.append(text.charAt(i));
                        ++count;
                    }
                }
                return formatted.toString();
            }
        });
    }

    public void setUpExpiryDate()
    {


        binding.editCardExpiryDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence charSequence, int start, int removed, int added) {
                String text = binding.editCardExpiryDate.getText().toString();
                textLength = binding.editCardExpiryDate.getText().length();

                if (textLength == 3) {
                    if (!text.contains("/")) {
                        binding.editCardExpiryDate.setText(new StringBuilder(text).insert(text.length() - 1, "/").toString());
                        binding.editCardExpiryDate.setSelection(binding.editCardExpiryDate.getText().length());
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    boolean isPaymentValidation()
    {
        if (binding.txtBillingAddress.getText().toString().isEmpty())
        {
            showToast("Please add billing address");
            return false;
        }
        else if(binding.txtShippingAddress.getText().toString().isEmpty())
        {
            showToast("Please add shipping address");
            return false;
        }
        else if (dispensaryModel==null)
        {
            showToast("Dispensary data not found");
            return false;
        }
        else if (total==0)
        {
            showToast("Order amount not found");
            return false;
        }
        else if (binding.editCardNumber.getText().toString().isEmpty())
        {
            binding.editCardNumber.setError(getString(R.string.enter_card_no));
            binding.editCardNumber.requestFocus();
            return false;
        }
        else if (binding.editCardNumber.getText().toString().length()<19)
        {
            binding.editCardNumber.setError(getString(R.string.enter_valid_card_no));
            binding.editCardNumber.requestFocus();
            return false;
        }
        else if (binding.editCardExpiryDate.getText().toString().isEmpty())
        {
            binding.editCardExpiryDate.setError(getString(R.string.enter_expiry_date));
            binding.editCardExpiryDate.requestFocus();
            return false;
        }
        else if (binding.editCardExpiryDate.getText().toString().length()<5)
        {
            binding.editCardExpiryDate.setError(getString(R.string.enter_valid_expiry_date));
            binding.editCardExpiryDate.requestFocus();
            return false;
        }
        else if (binding.editCardExpiryDate.getText().toString().length()==5)
        {
            String monYearArray[]=binding.editCardExpiryDate.getText().toString().split("/");
            int monthNum = Integer.parseInt(monYearArray[0]);
            if (monthNum <1 || monthNum>12)
            {
                binding.editCardExpiryDate.setError(getString(R.string.invalid_month));
                binding.editCardExpiryDate.requestFocus();
                return false;
            }
        }
        else if (binding.editCardCvc.getText().toString().isEmpty())
        {
            binding.editCardCvc.setError(getString(R.string.enter_cvc));
            binding.editCardCvc.requestFocus();
            return false;
        }
        else if (binding.editCardCvc.getText().toString().length()<3)
        {
            binding.editCardCvc.setError(getString(R.string.enter_valid_cvc));
            binding.editCardCvc.requestFocus();
            return false;
        }

        return true;
    }

    private void callPaymentMethod()
    {
        application.dialogManager.displayProgressDialog(CheckoutActivity.this,"Loading...");

        try {
            EncryptTransactionObject transactionObject = prepareTransactionObject();

      /*
        Make a call to get Token API
        parameters:
          1) EncryptTransactionObject - The transactionObject for the current transaction
          2) callback - callback of transaction
       */
            apiClient.getTokenWithRequest(transactionObject, this);
        } catch (NullPointerException e) {
            // Handle exception transactionObject or callback is null.
            Toast.makeText(CheckoutActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
           application.dialogManager.dismissProgressDialog();
            e.printStackTrace();
        }
    }


    private EncryptTransactionObject prepareTransactionObject() {
        ClientKeyBasedMerchantAuthentication merchantAuthentication =
                ClientKeyBasedMerchantAuthentication.
                        createMerchantAuthentication(AppConstant.NET_AUTHORISE_API_LOGIN_ID, NET_AUTHORISE_CLIENT_KEY);

        // create a transaction object by calling the predefined api for creation
        return TransactionObject.
                createTransactionObject(
                        TransactionType.SDK_TRANSACTION_ENCRYPTION) // type of transaction object
                .cardData(prepareCardDataFromFields()) // card data to get Token
                .merchantAuthentication(merchantAuthentication).build();
    }

    private EncryptTransactionObject prepareTestTransactionObject() {
        ClientKeyBasedMerchantAuthentication merchantAuthentication =
                ClientKeyBasedMerchantAuthentication.
                        createMerchantAuthentication(AppConstant.NET_AUTHORISE_API_LOGIN_ID, NET_AUTHORISE_CLIENT_KEY);

        // create a transaction object by calling the predefined api for creation
        return EncryptTransactionObject.
                createTransactionObject(
                        TransactionType.SDK_TRANSACTION_ENCRYPTION) // type of transaction object
                .cardData(prepareTestCardData())
                // card data to prepare token
                .merchantAuthentication(merchantAuthentication).build();
    }

    private CardData prepareTestCardData() {
        return new CardData.Builder(cardNumber, month, year).cvvCode(cvv)
                .zipCode(""+shippingUserAddressModel.getZip_code())
                .cardHolderName(""+shippingFirstName)
                .build();
    }

    private CardData prepareCardDataFromFields() {
        return new CardData.Builder(cardNumber, month, year).cvvCode(cvv) //CVV Code is optional
                .build();
    }



    /* ---------------------- Callback Methods - Start -----------------------*/

    @Override public void onEncryptionFinished(EncryptTransactionResponse response) {

        if (response!=null)
        {
            paymentToken=response.getDataValue();
            descriptorData=response.getDataDescriptor();

            if (getProductCartResponse!=null && (getProductCartResponse.getCoupon_code_name()==null||getProductCartResponse.getCoupon_code_name().equals(""))) {
                placeOrder();
            } else {
                Log.d("coupon_code", "" + getProductCartResponse.getCoupon_code_name());
                viewModel.getCouponProducts(getProductCartResponse.getCoupon_code_name());
            }
        }
        else
        {
            showToast("Server error!");
            application.dialogManager.dismissProgressDialog();
        }

        Log.d("response==","" + response.getDataDescriptor()+
                "     "+response.getDataValue());
    }

    @Override public void onErrorReceived(ErrorTransactionResponse errorResponse) {
        application.dialogManager.dismissProgressDialog();
        Message error = errorResponse.getFirstErrorMessage();
        String errorString = getString(R.string.code) + error.getMessageCode() + "\n" +
                "Message: " + error.getMessageText();
        new SimpleDialog() {
            @Override
            public void onPositiveButtonClick() {

            }
        }.showSimpleDialog(CheckoutActivity.this,"Error",error.getMessageText(),"OK");
        Log.d("Error==",""+errorString);

    }


    public void setPaymentData()
    {
        UserModel userModel=PreferenceManger.getPreferenceManger().getObject(PrefKeys.LOGIN_USER, UserModel.class);

        JSONObject jsonObject =new JSONObject();

        try {
            jsonObject.put("token",""+paymentToken);
            jsonObject.put("email",""+userModel.getEmail());
            jsonObject.put("dataDescriptor",""+descriptorData);
            jsonObject.put("orderNumber",""+orderResponse.getOrderNo());
            jsonObject.put("amount",""+total);
            jsonObject.put("billingFirstName",""+billingUserAddressModel.getFirst_name());
            jsonObject.put("billingLastName",""+billingUserAddressModel.getLast_name());
            jsonObject.put("billingAddress",""+billingUserAddressModel.getStreet_1());
            jsonObject.put("billingPhoneNumber",""+billingUserAddressModel.getPhone_number());
            jsonObject.put("billingCity",""+billingUserAddressModel.getCity());
            jsonObject.put("billingState",""+billingUserAddressModel.getState());
            jsonObject.put("billingZipCode",""+billingUserAddressModel.getZip_code());
            jsonObject.put("shippingFirstName",""+shippingFirstName);
            jsonObject.put("shippingLastName",""+shippingLastName);
            jsonObject.put("shippingAddress",""+shippingUserAddressModel.getStreet_1());
            jsonObject.put("shippingCity",""+shippingUserAddressModel.getCity());
            jsonObject.put("shippingState",""+shippingUserAddressModel.getState());
            jsonObject.put("shippingZipCode",""+shippingUserAddressModel.getZip_code());

            Log.d("json_payment",""+jsonObject);
            callPaymentApi(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* ---------------------- Callback Methods - End -----------------------*/


    void callPaymentApi(JSONObject jsonObject) {

        Log.d("jsonObjectRequest",""+jsonObject);

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,AppConstant.PAYMENT_API,
                jsonObject, new Response.Listener<JSONObject>() {

            public void onResponse(JSONObject response) {
                PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,"0");
                try {
                    if (response.getInt("status")==1)
                    {

                        PaymentMethod paymentMethod=new PaymentMethod();
                        paymentMethod.setAccountType(response.getString("accountType"));
                        paymentMethod.setTransationId(response.getString("getTransId"));
                        paymentMethod.setResponseCode(response.getString("getResponseCode"));
                        paymentMethod.setAuthCode(response.getString("getAuthCode"));

                        viewModel.updatePaymentStatus(true,paymentMethod,orderResponse.getOrderId());

                     /*   if (!response.getString("getTransId").equals("0"))
                        {

                            PaymentMethod paymentMethod=new PaymentMethod();
                            paymentMethod.setAccountType(response.getString("accountType"));
                            paymentMethod.setTransationId(response.getString("getTransId"));
                            paymentMethod.setResponseCode(response.getString("getResponseCode"));
                            paymentMethod.setAuthCode(response.getString("getAuthCode"));

                            viewModel.updatePaymentStatus(true,paymentMethod,orderResponse.getOrderId());
                        }
                        else
                        {
                            application.dialogManager.dismissProgressDialog();
                            String message="Authorize Server Error!";
                            new SimpleDialog() {
                                @Override
                                public void onPositiveButtonClick() {

                                    application.dialogManager.dismissProgressDialog();
                                }
                            }.showSimpleDialog(CheckoutActivity.this,"Information",message,"Ok");

                        }*/

                    }
                    else
                    {
                        application.dialogManager.dismissProgressDialog();
                        String message=response.getString("getErrorText");
                        new SimpleDialog() {
                            @Override
                            public void onPositiveButtonClick() {
                                application.dialogManager.dismissProgressDialog();
                            }
                        }.showSimpleDialog(CheckoutActivity.this,"Information",message,"Ok");

                    }
                }
                catch (Exception ex)
                {
                    PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,"0");
                    application.dialogManager.dismissProgressDialog();
                    showToast("Server error!");
                }

                Log.d("Response", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                String message="Authorize Server Error!";
                new SimpleDialog() {
                    @Override
                    public void onPositiveButtonClick() {
                        application.dialogManager.dismissProgressDialog();
                    }
                }.showSimpleDialog(CheckoutActivity.this,"Information",message,"Ok");
                PreferenceManger.getPreferenceManger().setString(PrefKeys.BADGE_COUNT,"0");
                application.dialogManager.dismissProgressDialog();
            }
        });


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }



    void callTaxJarApi(JSONObject jsonObject) {

        Log.d("jsonObjectRequest",""+jsonObject);

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,AppConstant.TAX_JAR_API,
                jsonObject, new Response.Listener<JSONObject>() {

            public void onResponse(JSONObject response) {
                application.dialogManager.dismissProgressDialog();

                try {
                    if (response.has("tax"))
                    {
                        JSONObject jsonObjectTax=response.getJSONObject("tax");

                        if (jsonObjectTax.has("amount_to_collect"))
                        {
                            salesTax=jsonObjectTax.getDouble("amount_to_collect");
                            generateBill(getProductCartResponse);
                        }
                        else
                        {
                            showToast("Sales tax data not found!");

                        }
                    }
                    else
                    {
                        showToast("Sales tax data not found!");

                    }
                }
                catch (Exception ex)
                {
                    application.dialogManager.dismissProgressDialog();
                    showToast("Tax jar server error!");
                    Log.d("Exception", ex.getMessage());
                }

                Log.d("Response", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", String.valueOf(error));
                //showToast("Server error!");
                String message = "Tax jar server error!";

                NetworkResponse response = error.networkResponse;
                if (response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                        JSONObject jsonObject = new JSONObject(res);
                        Log.d("obj",""+jsonObject);

                        if (jsonObject.has("error"))
                        {
                            message=jsonObject.getString("detail");
                        }
                    } catch (Exception e2) {

                        e2.printStackTrace();
                    }

                    new SimpleDialog() {
                        @Override
                        public void onPositiveButtonClick() {
                            CheckoutActivity.this.finish();
                        }
                    }.showSimpleDialog(CheckoutActivity.this,"Error",message,"OK");
                }

                application.dialogManager.dismissProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization","Bearer "+AppConstant.TAX_JAR_LIVE_TOKEN);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

}
