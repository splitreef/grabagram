package com.splitreef.grabagramplaystore.data.model.customer.menu;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoryMenu implements Serializable {

    private  String category_id;
    private  String category_name;

    private ArrayList<ProductListResponse>productList;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public ArrayList<ProductListResponse> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductListResponse> productList) {
        this.productList = productList;
    }
}
