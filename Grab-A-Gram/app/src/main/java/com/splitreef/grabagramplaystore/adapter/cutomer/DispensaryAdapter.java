package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.DispensaryModel;

import com.splitreef.grabagramplaystore.enums.DispensaryComparator;
import com.splitreef.grabagramplaystore.ui.customer.DispensaryDetails.DispensaryDetailsActivity;
import com.splitreef.grabagramplaystore.ui.customer.customerList.DispensaryListActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.GetDistance;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;

public class DispensaryAdapter extends RecyclerView.Adapter<DispensaryAdapter.DispensaryHolder> implements Filterable {

    private  ArrayList<DispensaryModel> dispensaryList;
    private  ArrayList<DispensaryModel> dispensaryListFiltered;
    private Context context;
    private double latitude;
    private double longitude;



    public DispensaryAdapter(@NonNull Context context,ArrayList<DispensaryModel> dispensaryList,double latitude,double longitude) {
       this.context=context;
       this.dispensaryList=dispensaryList;
       this.dispensaryListFiltered=dispensaryList;
       this.latitude=latitude;
       this.longitude=longitude;
    }

    public  ArrayList<DispensaryModel> getDispensaryList()
    {
       return dispensaryList;
    }

    public void setDispensaryList(ArrayList<DispensaryModel> dispensaryList,String sortingType)
    {
        this.dispensaryList.addAll(dispensaryList);

        LinkedHashSet<DispensaryModel> hashSet = new LinkedHashSet<>(this.dispensaryList);

        this.dispensaryList = new ArrayList<DispensaryModel>(hashSet);

        if (sortingType.equals(AppConstant.ALPHABETICALLY_A_Z_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.A_TO_Z_SORT));
        }
        else if (sortingType.equals(AppConstant.ALPHABETICALLY_Z_A_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.A_TO_Z_SORT)));
        }

        else if (sortingType.equals(AppConstant.DISTANCE_MIN_MAX_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.DIS_MIN_TO_MAX_SORT));
        }
        else if (sortingType.equals(AppConstant.DISTANCE_MAX_MIN_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.DIS_MIN_TO_MAX_SORT)));
        }
        else if (sortingType.equals(AppConstant.RATING_LOW_HIGH_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.getComparator(DispensaryComparator.RATING_LOW_HIGH_SORT));
        }
        else if (sortingType.equals(AppConstant.RATING_HIGH_LOW_SORTING_TYPE))
        {
            Collections.sort(this.dispensaryList, DispensaryComparator.descending(DispensaryComparator.getComparator(DispensaryComparator.RATING_LOW_HIGH_SORT)));
        }


        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public DispensaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_view,parent,false);
        return new DispensaryHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull DispensaryHolder holder, int position) {

        DispensaryModel dispensaryModel=dispensaryListFiltered.get(position);
        PicassoManager.setImage(dispensaryModel.getProfile_image(),holder.dispensaryImage);
        holder.dispensaryName.setText(dispensaryModel.getBusniess_name());
        //holder.dispensaryName.setText(dispensaryModel.getName());
        holder.dispensaryRating.setRating(dispensaryModel.getRating());

        double distance=new GetDistance().distance(latitude,longitude,dispensaryModel.getLocation().getLatitude(),dispensaryModel.getLocation().getLongitude());
        holder.milesAway.setText(String.format("%.2f %s",distance,"miles away"));

        holder.llTop.setOnClickListener(view -> {

            context.startActivity(new Intent(context, DispensaryDetailsActivity.class)
                    .putExtra(AppConstant.DISPENSARY_MODEL,dispensaryModel));
            ((DispensaryListActivity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
        });

    }

    @Override
    public int getItemCount() {
        return dispensaryListFiltered.size();
    }

    class DispensaryHolder extends RecyclerView.ViewHolder
    {
        AppCompatImageView dispensaryImage;
        MaterialTextView dispensaryName;
        MaterialTextView milesAway;
        AppCompatRatingBar dispensaryRating;
        LinearLayout llTop;

        public DispensaryHolder(@NonNull View itemView) {
            super(itemView);
            dispensaryImage = itemView.findViewById(R.id.img_dispensary);
            dispensaryName = itemView.findViewById(R.id.txt_dispensary_name);
            dispensaryRating = itemView.findViewById(R.id.dispensary_rating);
            milesAway = itemView.findViewById(R.id.txt_map);
            llTop = itemView.findViewById(R.id.ll_top);
        }
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    dispensaryListFiltered = dispensaryList;
                } else {
                    ArrayList<DispensaryModel> filteredItemList = new ArrayList<>();

                    for (DispensaryModel model : dispensaryList) {
                        if (model.getBusniess_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredItemList.add(model);
                        }
                    }
                    dispensaryListFiltered = filteredItemList;

                }
                Filter.FilterResults filterResults = new Filter.FilterResults();
                filterResults.values = dispensaryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                dispensaryListFiltered = (ArrayList<DispensaryModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
