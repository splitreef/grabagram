package com.splitreef.grabagramplaystore.ui.customer.dashboard;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.myAccount.MyAccountResponse;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DashBoardViewModel extends ViewModel {

    private static final String TAG = "DashBoardViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onBadgeCount = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onGetMyAccountData = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public DashBoardViewModel(FirebaseRepository firebaseRepository) {

        this.firebaseRepository=firebaseRepository;
    }

    public  void getBadgeCount()
    {
         firebaseRepository.getBadgeCount()
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new SingleObserver<String>() {
                     @Override
                     public void onSubscribe(@NonNull Disposable d) {

                         disposable.add(d);
                         onBadgeCount.setValue(DataResource.loading());
                     }

                     @Override
                     public void onSuccess(@NonNull String badgeCount) {

                         onBadgeCount.setValue(DataResource.DataStatus(badgeCount));
                     }

                     @Override
                     public void onError(@NonNull Throwable e) {

                         onBadgeCount.setValue(DataResource.DataStatusError(e.getMessage()));
                     }
                 });
    }


    public  void getCustomerMyAccountData()
    {
        firebaseRepository.getCustomerMyAccountData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MyAccountResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetMyAccountData.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull MyAccountResponse myAccountResponse) {

                        onGetMyAccountData.setValue(DataResource.DataStatus(myAccountResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetMyAccountData.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeGetBadgeCount()
    {
        return onBadgeCount;
    }
    LiveData<DataResource> observeGetMyAccountData()
    {
        return onGetMyAccountData;
    }


}
