package com.splitreef.grabagramplaystore.data.model.driver;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

/**
 * Created by  on 15-01-2021.
 */
public class DriverLocationStatusModel implements Parcelable {
    private String device_id;
    private String driver_status;
    private GeoPoint location;
    private String user_id;

    public DriverLocationStatusModel() {
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getDriver_status() {
        return driver_status;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setDriver_status(String driver_status) {
        this.driver_status = driver_status;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }


    protected DriverLocationStatusModel(Parcel in) {
        device_id = in.readString();
        driver_status = in.readString();
        location = (GeoPoint) in.readValue(GeoPoint.class.getClassLoader());
        user_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(device_id);
        dest.writeString(driver_status);
        dest.writeValue(location);
        dest.writeString(user_id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DriverLocationStatusModel> CREATOR = new Parcelable.Creator<DriverLocationStatusModel>() {
        @Override
        public DriverLocationStatusModel createFromParcel(Parcel in) {
            return new DriverLocationStatusModel(in);
        }

        @Override
        public DriverLocationStatusModel[] newArray(int size) {
            return new DriverLocationStatusModel[size];
        }
    };
}
