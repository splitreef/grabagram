package com.splitreef.grabagramplaystore.ui.customer.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.callBack.FragmentConsumer;
import com.splitreef.grabagramplaystore.common.BaseFragment;
import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.dialog.SimpleDialog;
import com.splitreef.grabagramplaystore.ui.login.LoginActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.NetworkManager;
import com.splitreef.grabagramplaystore.utils.StateResource;
import com.splitreef.grabagramplaystore.viewModelFactory.RegistrationViewModelProviderFactory;
import com.splitreef.grabagramplaystore.databinding.FragmentCustomerMemberAgreementBinding;
import java.util.Objects;

public class CustomerMemberAgreementFragment extends BaseFragment implements View.OnClickListener {

    private FragmentCustomerMemberAgreementBinding binding;
    private FragmentConsumer fragmentConsumer;
    private CustomerRegisterRequest customerRegisterRequest;
    private GrabAGramApplication application;
    private RegistrationViewModel viewModel;
    private Context context;
    private int photoTypeCount=0;


    public CustomerMemberAgreementFragment(FragmentConsumer fragmentConsumer) {
        this.fragmentConsumer = fragmentConsumer;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCustomerMemberAgreementBinding.inflate(inflater, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        if (getArguments() != null) {
            customerRegisterRequest = (CustomerRegisterRequest) getArguments().getSerializable("requestData");
        }

        application = (GrabAGramApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        viewModel = new ViewModelProvider(getViewModelStore(), new RegistrationViewModelProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), application.firebaseRepository)).get(RegistrationViewModel.class);
        subscribeObservers();


        binding.btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_submit) {
            if (Objects.requireNonNull(binding.fullName.getText()).toString().equals("")) {
                binding.fullName.setError(getString(R.string.enter_your_full_name));
                binding.fullName.requestFocus();
            } else {
                customerRegisterRequest.seteSignature(binding.fullName.getText().toString());

                if (!NetworkManager.isNetworkAvailable(context)) {
                    Toast.makeText(getContext(), "Internet connection is to slow or may be not working.", Toast.LENGTH_SHORT).show();
                } else {

                    addCustomerData();
                }
            }

        }
    }

    void addCustomerData() {
        if (customerRegisterRequest != null) {
            viewModel.registerUser(customerRegisterRequest.getEmail(),customerRegisterRequest.getPassword());
           // viewModel.addCustomerData(customerRegisterRequest);
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
    }

    void subscribeObservers() {

        viewModel.observeRegister().observe(Objects.requireNonNull(getActivity()), new Observer<StateResource>() {
            @Override
            public void onChanged(StateResource stateResource) {

                if (stateResource != null) {
                    switch (stateResource.status) {
                        case LOADING:
                            application.dialogManager.displayProgressDialog(getActivity(), "Loading...");
                            break;
                        case SUCCESS:
                            viewModel.uploadCustomerDocPhoto(customerRegisterRequest.getDoctorRecPhotoBytes(), AppConstant.IMAGE_DOCTOR_RECOMMENDATION_LETTER);
                            photoTypeCount=1;
                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "" + stateResource.message, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });


        viewModel.observeCustomerDocPhoto().observe(getActivity(), new Observer<DataResource>() {
            @Override
            public void onChanged(DataResource dataResource) {

                if (dataResource != null) {
                    switch (dataResource.status) {
                        case LOADING:
                            break;
                        case SUCCESS:
                            String url = (String) dataResource.data;
                            Log.d("url====", "" + url);

                            if (url != null) {
                                if (photoTypeCount == 1) {
                                    photoTypeCount=2;
                                    customerRegisterRequest.setDoctorRecPhotoUrl(url);
                                    viewModel.uploadCustomerDocPhoto(customerRegisterRequest.getGovPhotoIDUrlBytes(), AppConstant.IMAGE_GOVERNMENT_PHOTO_ID);
                                } else if (photoTypeCount == 2) {
                                    photoTypeCount=3;
                                    customerRegisterRequest.setGovPhotoIDUrl(url);
                                    viewModel.uploadCustomerDocPhoto(customerRegisterRequest.getAddressProofPhotoBytes(), AppConstant.IMAGE_PERSONAL_ADDRESS);
                                } else if (photoTypeCount == 3) {
                                    customerRegisterRequest.setAddressProofPhotoUrl(url);
                                    viewModel.addCustomerData(customerRegisterRequest);
                                }
                            } else {
                                Toast.makeText(getContext(), "Firebase error", Toast.LENGTH_SHORT).show();
                                application.dialogManager.dismissProgressDialog();
                            }

                            break;
                        case ERROR:
                            application.dialogManager.dismissProgressDialog();
                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        viewModel.observeAddCustomerData().observe(Objects.requireNonNull(getActivity()), stateResource -> {

            if (stateResource != null) {
                switch (stateResource.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                         application.dialogManager.dismissProgressDialog();
                         moveToLoginScreen();
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        Toast.makeText(getContext(), stateResource.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    void moveToLoginScreen()
    {
        new SimpleDialog() {
            @Override
            public void onPositiveButtonClick() {
                PreferenceManger.getPreferenceManger().clearSession();
                startActivity(new Intent(getContext(), LoginActivity.class));
                Objects.requireNonNull(getActivity()).finish();
            }
        }.showSimpleDialog(getContext(),"Success",getString(R.string.registration_successfully),"OK");
    }


}
