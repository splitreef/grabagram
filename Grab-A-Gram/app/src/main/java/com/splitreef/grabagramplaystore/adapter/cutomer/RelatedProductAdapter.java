package com.splitreef.grabagramplaystore.adapter.cutomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.data.model.customer.menu.RelatedProduct;
import com.splitreef.grabagramplaystore.utils.PicassoManager;

import java.util.ArrayList;

public class RelatedProductAdapter extends RecyclerView.Adapter<RelatedProductAdapter.ProductHolder> {

    private Context context;
    private ArrayList<RelatedProduct>relatedProductList;

    public RelatedProductAdapter(Context context, ArrayList<RelatedProduct>relatedProductList) {
        this.context=context;
        this.relatedProductList=relatedProductList;

    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ProductHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {

        RelatedProduct relatedProduct= relatedProductList.get(position);
        PicassoManager.setImage(relatedProduct.getProduct_image(),holder.imgProduct);
        holder.txtProductName.setText(relatedProduct.getProduct_name());
        holder.txtProductPrice.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return relatedProductList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder
    {
        ShapeableImageView imgProduct;
        MaterialTextView txtProductName;
        MaterialTextView txtProductPrice;
        LinearLayout llRoot;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct= itemView.findViewById(R.id.ima_product);
            txtProductName= itemView.findViewById(R.id.txt_product_name);
            txtProductPrice= itemView.findViewById(R.id.txt_product_price);
            llRoot= itemView.findViewById(R.id.ll_root);
        }
    }
}
