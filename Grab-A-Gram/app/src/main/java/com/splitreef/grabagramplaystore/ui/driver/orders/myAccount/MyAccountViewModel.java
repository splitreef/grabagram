package com.splitreef.grabagramplaystore.ui.driver.orders.myAccount;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.driver.myDriverAccount.MyDriverAccountResponse;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleCompany;
import com.splitreef.grabagramplaystore.data.model.driver.vehicle.VehicleModel;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by  on 04-01-2021.
 */
public class MyAccountViewModel extends ViewModel {
    private static final String TAG = "MyAccountViewModel";
    private final FirebaseRepository firebaseRepository;
    private MediatorLiveData<DataResource> onGetMyAccountDriverInfo = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUploadDriverDocPhoto = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onUpdateMyAccountDriverInfo = new MediatorLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    private MediatorLiveData<DataResource> ongetVehicleModel = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> ongetVehicleCompany = new MediatorLiveData<>();

    public MyAccountViewModel(FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
    }


    public void getDriverMyAccountInfo() {
        firebaseRepository.getDriverMyAccountInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MyDriverAccountResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onGetMyAccountDriverInfo.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull MyDriverAccountResponse myAccountResponse) {

                        onGetMyAccountDriverInfo.setValue(DataResource.DataStatus(myAccountResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onGetMyAccountDriverInfo.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }

    void uploadDriverDocPhoto(byte[] bytes, String imageName) {
        firebaseRepository.uploadDriverDocPhoto(bytes, imageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUploadDriverDocPhoto.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        onUploadDriverDocPhoto.setValue(DataResource.DataStatus(s));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUploadDriverDocPhoto.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    void updateDrverMyAccountInfo(MyDriverAccountResponse myDriverAccountResponse) {
        firebaseRepository.updateDrverMyAccountInfo(myDriverAccountResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onUpdateMyAccountDriverInfo.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {
                        onUpdateMyAccountDriverInfo.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onUpdateMyAccountDriverInfo.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


    LiveData<DataResource> observeGetMyAccountInfo() {
        return onGetMyAccountDriverInfo;
    }

    LiveData<DataResource> observeUploadDriverDocPhoto() {
        return onUploadDriverDocPhoto;
    }


    LiveData<StateResource> observeUpdateMyAccountDriverInfo() {
        return onUpdateMyAccountDriverInfo;
    }


    void getVehicleModelList() {
        firebaseRepository.getVehicleModelList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<VehicleModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        ongetVehicleModel.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<VehicleModel> vehicleModelList) {
                        ongetVehicleModel.setValue(DataResource.DataStatus(vehicleModelList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ongetVehicleModel.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    LiveData<DataResource> observeVehicleModel() {
        return ongetVehicleModel;
    }


    void getVehicleCompanyList() {
        firebaseRepository.getVehicleCompanyList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<VehicleCompany>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        ongetVehicleCompany.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull List<VehicleCompany> vehicleCompanyList) {
                        ongetVehicleCompany.setValue(DataResource.DataStatus(vehicleCompanyList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ongetVehicleCompany.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }

    LiveData<DataResource> observeVehicleCompany() {
        return ongetVehicleCompany;
    }


}
