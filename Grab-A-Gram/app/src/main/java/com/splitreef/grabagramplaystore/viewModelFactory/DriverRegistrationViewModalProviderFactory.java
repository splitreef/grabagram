package com.splitreef.grabagramplaystore.viewModelFactory;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.ui.driver.driverRegistration.DriverRegistrationViewModel;

/**
 * Created by  on 10-11-2020.
 */
public class DriverRegistrationViewModalProviderFactory implements ViewModelProvider.Factory {
    private final Application mApplication;
    private final FirebaseRepository firebaseRepository;


    public DriverRegistrationViewModalProviderFactory(Application application, FirebaseRepository firebaseRepository) {
        mApplication = application;
        this.firebaseRepository = firebaseRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(DriverRegistrationViewModel.class))
        {
            return  (T) new DriverRegistrationViewModel(mApplication, firebaseRepository);
        }

        throw  new IllegalArgumentException("View model not found");
    }
}