package com.splitreef.grabagramplaystore.dialog;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.splitreef.grabagramplaystore.R;

/**
 * Created by  on 07-01-2021.
 */
public abstract class CustomeBottomSheetDialog {
    public abstract void bottomSheetItemClick(String sortBy);

    public void initBottomSheet(Context context) {
        //BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        //bottomSheetDialog.setContentView(R.layout.layout_sort);

        try {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context, R.style.CustomDialog);
            // View sheetView = context.getLayoutInflater().inflate(R.layout.layout_sort, null);

            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;
            int maxHeight = (int) (height * 0.88);
            bottomSheetDialog.setDismissWithAnimation(true);
            bottomSheetDialog.getBehavior().setPeekHeight(maxHeight);
            bottomSheetDialog.setContentView(R.layout.layout_sort);
            bottomSheetDialog.show();

            TextView tvCancel = bottomSheetDialog.findViewById(R.id.txt_cancel);
            TextView txt_a_to_z = bottomSheetDialog.findViewById(R.id.txt_a_to_z);
            TextView txt_z_to_a = bottomSheetDialog.findViewById(R.id.txt_z_to_a);
            TextView txt_dis_max_min = bottomSheetDialog.findViewById(R.id.txt_dis_max_min);
            TextView txt_dis_min_max = bottomSheetDialog.findViewById(R.id.txt_dis_min_max);
            TextView txt_rating_high_low = bottomSheetDialog.findViewById(R.id.txt_rating_high_low);
            TextView txt_rating_low_high = bottomSheetDialog.findViewById(R.id.txt_rating_low_high);

            txt_a_to_z.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.alphabetically_a_to_z));
                    bottomSheetDialog.dismiss();
                }
            });
            txt_z_to_a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.alphabetically_z_to_a));
                    bottomSheetDialog.dismiss();
                }
            });
            txt_dis_max_min.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.distance_a_to_z));
                    bottomSheetDialog.dismiss();
                }
            });
            txt_dis_min_max.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.distance_z_to_a));
                    bottomSheetDialog.dismiss();
                }
            });
            txt_rating_high_low.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.rating_a_to_z));
                    bottomSheetDialog.dismiss();
                }
            });
            txt_rating_low_high.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetItemClick(context.getResources().getString(R.string.rating_z_to_a));
                    bottomSheetDialog.dismiss();
                }
            });
            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
