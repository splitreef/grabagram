package com.splitreef.grabagramplaystore.ui.customer.registration;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.splitreef.grabagramplaystore.data.model.customer.CustomerRegisterRequest;
import com.splitreef.grabagramplaystore.data.repository.FirebaseRepository;
import com.splitreef.grabagramplaystore.utils.DataResource;
import com.splitreef.grabagramplaystore.utils.StateResource;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegistrationViewModel extends ViewModel {
    private static final String TAG = "RegistrationViewModel";
    private FirebaseRepository firebaseRepository;
    private MediatorLiveData<StateResource> onRegister = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onUploadCustomerDocPhoto = new MediatorLiveData<>();
    private MediatorLiveData<DataResource> onCheckUserExist = new MediatorLiveData<>();
    private MediatorLiveData<StateResource> onAddCustomerData = new MediatorLiveData<>();
    private  CompositeDisposable disposable = new CompositeDisposable();
    private  Application mApplication;


    public RegistrationViewModel(Application mApplication, FirebaseRepository firebaseRepository) {
        this.firebaseRepository = firebaseRepository;
        this.mApplication= mApplication;
    }

    void registerUser(String email, String password)
    {
         firebaseRepository.registerUser(email,password)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new CompletableObserver() {
                     @Override
                     public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                           disposable.add(d);
                           onRegister.setValue(StateResource.loading());
                     }

                     @Override
                     public void onComplete() {

                         onRegister.setValue(StateResource.success());

                     }

                     @Override
                     public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                         onRegister.setValue(StateResource.error(e.getMessage()));
                     }
                 });
    }


    public  void checkUserExistsOrNot(String email)
    {
        firebaseRepository.checkUserExistsOrNot(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                        disposable.add(d);
                        onCheckUserExist.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull Boolean isExist) {

                        onCheckUserExist.setValue(DataResource.DataStatus(isExist));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        onCheckUserExist.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });
    }



    void uploadCustomerDocPhoto(byte[] bytes,String imageName)
    {
        firebaseRepository.uploadCustomerDocPhoto(bytes,imageName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                        onUploadCustomerDocPhoto.setValue(DataResource.loading());
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatus(s));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onUploadCustomerDocPhoto.setValue(DataResource.DataStatusError(e.getMessage()));
                    }
                });

    }


    void addCustomerData(CustomerRegisterRequest customerRegisterRequest)
    {
        firebaseRepository.addCustomerData(customerRegisterRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        disposable.add(d);
                        onAddCustomerData.setValue(StateResource.loading());
                    }

                    @Override
                    public void onComplete() {

                        onAddCustomerData.setValue(StateResource.success());

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        onAddCustomerData.setValue(StateResource.error(e.getMessage()));
                    }
                });
    }


     LiveData<StateResource>observeRegister()
     {
         return onRegister;
     }


    LiveData<DataResource>observeCustomerDocPhoto()
    {
        return onUploadCustomerDocPhoto;
    }

    LiveData<StateResource>observeAddCustomerData()
    {
        return onAddCustomerData;
    }

    LiveData<DataResource>observeCheckUserExistOrNot()
    {
        return onCheckUserExist;
    }



    @Override
    protected void onCleared() {
        super.onCleared();
         disposable.clear();
    }





}
