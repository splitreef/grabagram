package com.splitreef.grabagramplaystore.ui.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.google.gson.Gson;
import com.splitreef.grabagramplaystore.GrabAGramApplication;
import com.splitreef.grabagramplaystore.R;
import com.splitreef.grabagramplaystore.common.BaseActivity;
import com.splitreef.grabagramplaystore.data.model.customer.UserModel;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PrefKeys;
import com.splitreef.grabagramplaystore.dataSource.local.sharePref.PreferenceManger;
import com.splitreef.grabagramplaystore.databinding.ActivityLoginBinding;
import com.splitreef.grabagramplaystore.ui.accountChooseActivity.AccountChooserActivity;
import com.splitreef.grabagramplaystore.ui.customer.dashboard.DashboardActivity;
import com.splitreef.grabagramplaystore.ui.driver.orders.home.HomeActivity;
import com.splitreef.grabagramplaystore.ui.forgetPassword.ForgetPasswordActivity;
import com.splitreef.grabagramplaystore.utils.AppConstant;
import com.splitreef.grabagramplaystore.viewModelFactory.LoginViewModelProviderFactory;
import java.util.Objects;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG=LoginActivity.class.getName();
    private ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;
    private GrabAGramApplication application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
    }

    // For initialization and set click listener the view of this screen.
    public void initView() {

        application = (GrabAGramApplication) Objects.requireNonNull(getApplicationContext());

        loginViewModel = new ViewModelProvider(getViewModelStore(), new LoginViewModelProviderFactory(getApplication(), application.firebaseRepository)).get(LoginViewModel.class);

        binding.login.setOnClickListener(this);
        binding.signUp.setOnClickListener(this);
        binding.forgotPassword.setOnClickListener(this);
        subscribeObservers();
    }

    @SuppressLint({"NonConstantResourceId"})
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (isValidation()) {
                    loginUser();
                }
                break;
            case R.id.sign_up:
                startActivity(new Intent(LoginActivity.this, AccountChooserActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.forgot_password:
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;
        }
    }


    // For login Validation
    boolean isValidation() {
        if (binding.emailAddress.getText().toString().equals("")) {
            binding.emailAddress.setError(getString(R.string.enter_your_email));
            binding.emailAddress.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.emailAddress.getText().toString()).matches()) {
            binding.emailAddress.setError(getString(R.string.enter_your_valid_email));
            binding.emailAddress.requestFocus();
            return false;
        } else if (binding.password.getText().toString().equals("")) {
            binding.password.setError(getString(R.string.enter_your_password));
            binding.password.requestFocus();
            return false;
        }

        return true;

    }


    // login user
    public void loginUser() {
        loginViewModel.login(binding.emailAddress.getText().toString(), binding.password.getText().toString());
    }

    void subscribeObservers() {
        loginViewModel.observeLogin().observe(LoginActivity.this, dataResource -> {

            if (dataResource != null) {
                switch (dataResource.status) {
                    case LOADING:
                        application.dialogManager.displayProgressDialog(LoginActivity.this, "Loading...");
                        break;
                    case SUCCESS:
                        application.dialogManager.dismissProgressDialog();
                        UserModel userModel = (UserModel) dataResource.data;
                        String jsonString = new Gson().toJson(userModel);
                        Log.d(TAG, ""+jsonString);
                        navigateToNewScreen(userModel);
                        break;
                    case ERROR:
                        application.dialogManager.dismissProgressDialog();
                        application.messageManager.DisplayToastMessage(dataResource.message);
                }
            }
        });
    }

    public void navigateToNewScreen(UserModel userModel) {
        if (userModel != null) {
            PreferenceManger.getPreferenceManger().setObject(PrefKeys.LOGIN_USER, userModel);

            if (userModel.getRole().equals(AppConstant.CUSTOMER_ROLE)) {

                if (userModel.isActivated_at()) {
                    startActivity(new Intent(LoginActivity.this, DashboardActivity.class));

                } else {
                    showToast("User is not activated.");
                }

            } else if (userModel.getRole().equals(AppConstant.DRIVER_ROLE)) {

                if (userModel.isActivated_at()) {
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    finish();
                } else {
                    showToast("User is not activated.");
                }


            }
        }
    }

}

